//
//  IGHour.h
//  IGTimeSheet
//
//  Created by Manish on 19/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGHour : NSObject

@property (nonatomic, strong) NSNumber *totalHours;
@property (nonatomic, strong) NSString *numberOfDays;
@property (nonatomic, strong) NSNumber *weeklyAverage;

@end


@interface IGWeeklyHour : IGHour

@property (nonatomic, strong) NSString *week;
@property (nonatomic, assign) NSInteger order;

@end


@interface IGMonthlyHour : IGHour

@property (nonatomic, strong) NSNumber *totalHoursAverage;

@end
