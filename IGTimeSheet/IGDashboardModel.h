//
//  IGDashboardModel.h
//  IGTimeSheet
//
//  Created by Manish on 12/05/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, IGDashboardSection) {
    kIGDashboardSectionUndefined = 0,
    kIGDashboardSectionMissingTimesheet,
    kIGDashboardSectionInProgressTimesheet,
    kIGDashboardSectionEnterTimesheet,
    kIGDashboardSectionSubmit
};


@interface IGDashboardModel : NSObject

@property (strong, nonatomic) NSArray * missingTimesheet;
@property (strong, nonatomic) NSArray * inProgressTimesheet;

@property (nonatomic, readonly) NSInteger numberOfSections;

@property (nonatomic, assign) BOOL expandMissingTimesheet;
@property (nonatomic, assign) BOOL expandInProgressTimesheet;


// call each time the data changes, so as to update internal state
- (void)refresh;

- (IGDashboardSection)sectionTypeForSection:(NSInteger)sectionIndex;

/*! To get section index for a given dashboard Section
 \param dashboardSection  enum for section Type
 \returns section Index for a Section Type, -1 if no match is found
 */
- (NSInteger)sectionIndexForSectionType:(IGDashboardSection)dashboardSection;

- (NSInteger)numberOfRowsInSection:(NSInteger)section;

@end
