//
//  IGClipView.h
//  IGTimeSheet
//
//  Created by Manish on 03/03/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGClipView : UIView

@property (nonatomic, assign) UIRectCorner corners;
@property (nonatomic, assign) CGFloat cornerRadius;

@end
