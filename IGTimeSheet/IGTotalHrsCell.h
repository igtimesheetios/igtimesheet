//
//  IGTotalHrsCell.h
//  IGTimeSheet
//
//  Created by Neha on 20/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGTotalHrsCell : UITableViewCell

@property (strong) IBOutlet UILabel* totalHrsLabel;

@end
