//
//  IGBadgeView.m
//  IGTimeSheet
//
//  Created by Manish on 20/05/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGBadgeView.h"

#define VIEW_HEIGHT         40

@implementation IGBadgeView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        _labelTitle = [[UILabel alloc] initWithFrame:frame];
        _labelTitle.textAlignment = NSTextAlignmentCenter;
        _labelTitle.textColor = [UIColor whiteColor];
        _labelTitle.font = [UIFont boldSystemFontOfSize:14];
        [self addSubview:_labelTitle];
        
        _labelTitle.translatesAutoresizingMaskIntoConstraints = NO;
        NSDictionary *views = NSDictionaryOfVariableBindings(_labelTitle);
        NSDictionary *metrics = [[NSDictionary alloc] initWithObjectsAndKeys:@(IGBADGE_PADDING), @"padding", nil];
        NSArray *verticalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_labelTitle]|" options:0 metrics:nil views:views];
        NSArray *horizontalConstraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-padding-[_labelTitle]-padding-|" options:0 metrics:metrics views:views];
        [self addConstraints:verticalConstraints];
        [self addConstraints:horizontalConstraints];
        
//        self.layer.borderColor = [UIColor whiteColor].CGColor;
//        self.layer.borderWidth = 2;
        self.layer.shadowColor = [UIColor clearColor].CGColor;
//        self.layer.shadowRadius = 5;
//        self.layer.shadowOffset = CGSizeMake(0, 0);
    }
    return self;
}

@end
