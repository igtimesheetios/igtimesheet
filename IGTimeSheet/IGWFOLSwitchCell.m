//
//  IGWFOLSwitchCell.m
//  IGTimeSheet
//
//  Created by Neha on 23/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGWFOLSwitchCell.h"

@implementation IGWFOLSwitchCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
