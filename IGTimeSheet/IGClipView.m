//
//  IGClipView.m
//  IGTimeSheet
//
//  Created by Manish on 03/03/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGClipView.h"
#import "IGUtils.h"

@implementation IGClipView

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [IGUtils roundCorner:self.corners
                  radius:self.cornerRadius
                 forView:self];
}

@end
