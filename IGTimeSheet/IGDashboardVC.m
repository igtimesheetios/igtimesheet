//
//  IGDashboardVC.m
//  IGTimeSheet
//
//  Created by Rajat on 11/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGDashboardVC.h"
#import "IGDashBoardTableViewNormalCell.h"
#import "IGDashboardTableViewDateCell.h"
#import "IGDashboardMacros.h"
#import "IGDashBoardParser.h"
#import "IGDashboardObjectModel.h"
#import "IGTaskViewVC.h"
#import "IGDashboardSubmitCell.h"
#import "IGYearPicker.h"
#import "IGMonthPicker.h"
#import "IGWeekEndPicker.h"
#import "IGTutorialRootViewController.h"

#import "IGCommonHeaders.h"
#import "IGDashboardModel.h"
#import "IGBadgeView.h"

#define kfont   @"HelveticaNeue-light"


#define kIGDashboardMissingTimesheetSectionHeaderTag            2000
#define kIGDashboardInProgressTimesheetSectionHeaderTag         2001


@interface IGDashboardVC () <UITableViewDataSource, UITableViewDelegate, IGMonthPickerDelegate, IGWeekEndPickerDelegate, IGPickerDelegate>{
    
    BOOL flag;
    NSString *yearString;
    NSString *monthString;
    NSString *weekDate;
    
    IGYearPicker *yearPicker;
    IGMonthPicker *monthPicker;
    IGWeekEndPicker *weekPicker;
    
    IGDashboardModel *dashboardModel;
    
}

@property (weak, nonatomic) IBOutlet UITableView *dashboardTableView;

@property(strong, nonatomic) NSArray *monthArray;

@property (strong, nonatomic) NSMutableArray *weekEndArray;


@end

@implementation IGDashboardVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupBackButton];
    [self setupOpaqueNavigationBar];

    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title =DashBoardTitle;
    NSDictionary *titleDetail = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [UIFont fontWithName:kfont size:17.0f],NSFontAttributeName,
                                 [UIColor whiteColor] , NSForegroundColorAttributeName,
                                 nil];
    self.navigationController.navigationBar.titleTextAttributes = titleDetail;
    
    self.dashboardTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    /* Tutorial */
    IGUserDefaults *userDefaults = [IGUserDefaults sharedInstance];
    
    if (!userDefaults.isFirstTimeLogin)
    {
        
        IGTutorialRootViewController *tutorialRootVC = [self.storyboard instantiateViewControllerWithIdentifier:@"IGTutorialRootViewController"];
        tutorialRootVC.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:kImageScreenBG]];
        
        [self presentViewController:tutorialRootVC animated:YES completion:^{
            [IGUserDefaults sharedInstance].isFirstTimeLogin = YES;
        }];
    }
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    [_tableView addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(refreshView) forControlEvents:UIControlEventValueChanged];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self refreshView];
    
    
    [IGAnalytics trackScreen:screenDashBoard];    
}

- (void)populateView:(NSDictionary *)dictionary
{
    IGDashBoardParser* parser = [[IGDashBoardParser alloc] init];

    if (!dashboardModel)
    {
        dashboardModel = [[IGDashboardModel alloc] init];
    }
    
    dashboardModel.missingTimesheet = [[NSArray alloc] initWithArray:[parser parseMissingTimeSheet:dictionary]];
    dashboardModel.inProgressTimesheet = [[NSArray alloc] initWithArray:[parser parseInProgressTimeSheet:dictionary]];
    
    [dashboardModel refresh];
}

- (void)refreshView
{
    //Reload Dashboard
     kAlertAndReturnIfNetworkNotReachable
    
    [[IGService sharedInstance] lloginDetailsWithUserName:[IGUserDefaults sharedInstance].userName
                                                   domain:[IGUserDefaults sharedInstance].domainID
                                               completion:^(NSDictionary *userInfo, NSDictionary *timeSheetInfo) {
                                                   ;
                                                   //TODO: check for object not nil
                                                   if([userInfo objectForKey:@"ISUS"] != nil)
                                                   {
                                                   
                                                       [IGUserDefaults sharedInstance].ifUserUSEmployee = [[userInfo objectForKey:kIGServiceKeyisUserUSEmp]integerValue];
                                                   }
                                                   
                                                   [self populateView:timeSheetInfo];
                                                   [_tableView reloadData];
                                               } error:^(NSError *error) {
                                                   [IGUtils showOKAlertWithTitle:kStringError message:[error localizedDescription]];
                                               }];
    
    
    
    //TODO: Replace with iguserdefault domain instead of appdelegate domain and login name
//    [[IGService sharedInstance] loginDetailsWithUserName:[IGUserDefaults sharedInstance].userName
//                                                  domain:[IGUserDefaults sharedInstance].domainID
//                                              completion:^(id response)
//     {
//         NSDictionary *jsonDictionary = (NSDictionary *)response;
//         [self populateView:[jsonDictionary[@"TimesheetStatus"] objectAtIndex:0]];
//         [_tableView reloadData];
//     }
//                                                   error:^(NSError *error)
//     {
//         [IGUtils showOKAlertWithTitle:kStringError message:@"Could not get User Information"];
//     }];
    
    flag = TRUE;
    self.weekEndArray = [[NSMutableArray alloc] initWithCapacity:1];
    yearString = [IGUtils stringFromDate:[NSDate date] dateFormat:@"yyyy"];
    monthString = [IGUtils stringFromDate:[NSDate date] dateFormat:@"MM"];
    kAlertAndReturnIfNetworkNotReachable
    [self fetchWeekEndDateFromServer];
    //TODO: remove month array
    self.monthArray = @[
                        @"Jan",
                        @"Feb",
                        @"Mar",
                        @"Apr",
                        @"May",
                        @"Jun",
                        @"Jul",
                        @"Aug",
                        @"Sep",
                        @"Oct",
                        @"Nov",
                        @"Dec"
                        ];
    
    [self.refreshControl endRefreshing];
}

- (UIView *)headerViewWithImageName:(NSString *)imageName title:(NSString *)title
{
    UIImage *myImage = [UIImage imageNamed:imageName];
    UIImageView *imageView = [[UIImageView alloc] initWithImage:myImage];
    imageView.frame = CGRectMake(20, 10, 40, 40);
    UILabel *myLabel =[[UILabel alloc]initWithFrame:CGRectMake(80, 10, 250, 40)];
    myLabel.text = title;
    myLabel.font = [UIFont fontWithName:kfont size:17];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 40)];
    [view setBackgroundColor:[UIColor colorWithRed:244/255.6 green:244/255.6 blue:244/255.6 alpha:1]];
    [view addSubview:imageView];
    [view addSubview:myLabel];
    return view;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return dashboardModel.numberOfSections;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [dashboardModel numberOfRowsInSection:section];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IGDashboardSection dashboardSection = [dashboardModel sectionTypeForSection:indexPath.section];
    
    UITableViewCell *cell = nil;
    
    switch (dashboardSection)
    {
        case kIGDashboardSectionMissingTimesheet:
        {
            IGDashBoardTableViewNormalCell *missingTimesheetCell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellType1ReuseIdentifier forIndexPath:indexPath];
            IGDashboardObjectModel* modelObj = dashboardModel.missingTimesheet[indexPath.row];
            missingTimesheetCell.valueLabel.text = modelObj.value;
            cell = missingTimesheetCell;
            break;
        }
            
        case kIGDashboardSectionInProgressTimesheet:
        {
            IGDashBoardTableViewNormalCell *inProgressTimesheetCell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellType1ReuseIdentifier forIndexPath:indexPath];
            IGDashboardObjectModel* modelObj = dashboardModel.inProgressTimesheet[indexPath.row];
            inProgressTimesheetCell.valueLabel.text = modelObj.value;
            cell = inProgressTimesheetCell;
            break;
        }
            
        case kIGDashboardSectionEnterTimesheet:
        {            
            IGDashboardTableViewDateCell *dateCell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellType2ReuseIdentifier forIndexPath:indexPath];
            
            if (indexPath.row == 0)
            {
                dateCell.labelName.text = @"Year";
                dateCell.labelValue.text = yearString;
            }
            else if (indexPath.row == 1)
            {
                dateCell.labelName.text = @"Month";
                
                if (monthString != nil)
                {
                    dateCell.labelValue.text = self.monthArray[[monthString integerValue] - 1];
                }
                else
                {
                    dateCell.labelValue.text = @"Select";
                }
            }
            else if (indexPath.row == 2)
            {
                dateCell.labelName.text = @"Week End";
                
                if (weekDate != nil)
                {
                    dateCell.labelValue.text = weekDate;
                }
                else
                {
                    dateCell.labelValue.text = @"Select";
                }
                
            }
            
            cell = dateCell;
            break;
        }
            
        case kIGDashboardSectionSubmit:
        {
            IGDashboardSubmitCell *submitCell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellType3ReuseIdentifier forIndexPath:indexPath];

            if (monthString == nil || weekDate == nil)
            {
                [IGUtils setupButton:submitCell.submitButton enable:FALSE backgroundColor: nil];
            }
            else
            {
                [IGUtils setupButton:submitCell.submitButton enable:TRUE backgroundColor: kColorSubmitEnable];
            }
            cell = submitCell;
            break;
        }
            
        default:
        {
            break;
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
   
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    IGDashboardSection section = [dashboardModel sectionTypeForSection:indexPath.section];

    if (section == kIGDashboardSectionEnterTimesheet)
    {
        switch (indexPath.row)
        {
            case 0:
            {
                [self openYearSelectionController:self];
                break;
            }
                
            case 1:
            {
                [self openMonthSelectionController:self];
                break;
            }
                
            case 2:
            {
                [self openWeekEndDateSelectionController:self];
                break;
            }
            
            default:
            {
                break;
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IGDashboardSection dashboardSection = [dashboardModel sectionTypeForSection:indexPath.section];
    if (dashboardSection == kIGDashboardSectionSubmit)
    {
        return 50.0f;
    }
    
    return 40.0f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    IGDashboardSection dashboardSection = [dashboardModel sectionTypeForSection:section];
    if (dashboardSection == kIGDashboardSectionSubmit)
    {
        return 0;
    }
    return 60;
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
//{
//    IGDashboardSection dashboardSection = [dashboardModel sectionTypeForSection:section];
//    if(dashboardSection == kIGDashboardSectionEnterTimesheet)
//    {
//        return 0;
//    }
//    
//    return 1;
//}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = nil;
    
    IGDashboardSection dashboardSection = [dashboardModel sectionTypeForSection:section];
    
    IGBadgeView *badgeView = nil;
    
    switch (dashboardSection)
    {
        case kIGDashboardSectionMissingTimesheet:
        {
            headerView = [self headerViewWithImageName:MTimeSheetIcon title:MTimeSheetTitle];
            headerView.tag = kIGDashboardMissingTimesheetSectionHeaderTag;
            
            badgeView = [[IGBadgeView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
            badgeView.backgroundColor = kDefaultBarTintColor;
            badgeView.labelTitle.text = [NSString stringWithFormat:@"%lu", (unsigned long)dashboardModel.missingTimesheet.count];
            break;
        }
            
        case kIGDashboardSectionInProgressTimesheet:
        {
            headerView = [self headerViewWithImageName:PTimeSheetIcon title:PTimeSheetTitle];
            headerView.tag = kIGDashboardInProgressTimesheetSectionHeaderTag;

            badgeView = [[IGBadgeView alloc] initWithFrame:CGRectMake(0, 0, 40, 40)];
            badgeView.backgroundColor = kColorOrange;
            badgeView.labelTitle.text = [NSString stringWithFormat:@"%d", dashboardModel.inProgressTimesheet.count];
            
            break;
        }
            
        case kIGDashboardSectionEnterTimesheet:
        {
            headerView = [self headerViewWithImageName:CTimesheetIcon title:CTimeSheetTitle];
            break;
        }

        default:
        {
            break;
        }
    }
    
    if (dashboardSection == kIGDashboardSectionMissingTimesheet ||
        dashboardSection == kIGDashboardSectionInProgressTimesheet)
    {
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleSectionRowVisibility:)];
        [headerView addGestureRecognizer:tapGestureRecognizer];
    }
    
    UIView *separator = [[UIView alloc] initWithFrame:CGRectZero];
    separator.backgroundColor = [UIColor lightGrayColor];
    [headerView addSubview:separator];
    separator.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:separator
                                                                        attribute:NSLayoutAttributeHeight
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:nil
                                                                        attribute:NSLayoutAttributeNotAnAttribute
                                                                       multiplier:1
                                                                         constant:1];
    [separator addConstraint:heightConstraint];
    
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:separator
                                                                      attribute:NSLayoutAttributeLeading
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:headerView
                                                                      attribute:NSLayoutAttributeLeading
                                                                     multiplier:1
                                                                       constant:0];
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:separator
                                                                      attribute:NSLayoutAttributeTrailing
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:headerView
                                                                      attribute:NSLayoutAttributeTrailing
                                                                     multiplier:1
                                                                       constant:0];

    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:separator
                                                                      attribute:NSLayoutAttributeBottom
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:headerView
                                                                      attribute:NSLayoutAttributeBottom
                                                                     multiplier:1
                                                                       constant:0];
    
    [headerView addConstraints:@[leftConstraint, rightConstraint, bottomConstraint]];
    
    
    if (badgeView != nil)
    {
        [headerView addSubview:badgeView];
        badgeView.translatesAutoresizingMaskIntoConstraints = NO;
        
        CGFloat height = 30;
        CGFloat width = 30;
        
        CGSize size = [badgeView.labelTitle sizeThatFits:CGSizeMake(9999, height)];
        if (size.width >=  (width - IGBADGE_PADDING))
        {
            if (size.width > 70)
            {
                width = 70;
            }
            else
            {
                width = size.width + IGBADGE_PADDING;
            }
        }
        
        NSLayoutConstraint *widthConstraint = [NSLayoutConstraint constraintWithItem:badgeView
                                                                           attribute:NSLayoutAttributeWidth
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:nil
                                                                              attribute:NSLayoutAttributeNotAnAttribute
                                                                             multiplier:1
                                                                               constant:width];

        NSLayoutConstraint *heightConstraint = [NSLayoutConstraint constraintWithItem:badgeView
                                                                            attribute:NSLayoutAttributeHeight
                                                                            relatedBy:NSLayoutRelationEqual
                                                                               toItem:nil
                                                                            attribute:NSLayoutAttributeNotAnAttribute
                                                                           multiplier:1
                                                                             constant:height];
        [badgeView addConstraints:@[widthConstraint, heightConstraint]];
        
        NSLayoutConstraint *trailingConstraint = [NSLayoutConstraint constraintWithItem:badgeView
                                                                              attribute:NSLayoutAttributeTrailing
                                                                              relatedBy:NSLayoutRelationEqual
                                                                                 toItem:headerView
                                                                              attribute:NSLayoutAttributeTrailing
                                                                             multiplier:1
                                                                               constant:-10];
        
        NSLayoutConstraint *centerHorizontallyConstraint = [NSLayoutConstraint constraintWithItem:badgeView
                                                                                        attribute:NSLayoutAttributeCenterY
                                                                                        relatedBy:NSLayoutRelationEqual
                                                                                           toItem:headerView
                                                                                        attribute:NSLayoutAttributeCenterY
                                                                                       multiplier:1
                                                                                         constant:0];
        [headerView addConstraints:@[trailingConstraint, centerHorizontallyConstraint]];
        
        badgeView.layer.cornerRadius = height * 0.5;
        badgeView.layer.shadowOpacity = 0.4f;
    }
    
    return headerView;
}

- (void)toggleSectionRowVisibility:(UITapGestureRecognizer *)tapGestureRecognizer
{
    NSInteger tag = tapGestureRecognizer.view.tag;
    NSInteger sectionIndex = -1;
    
    switch (tag)
    {
        case kIGDashboardMissingTimesheetSectionHeaderTag:
        {
            dashboardModel.expandMissingTimesheet = !dashboardModel.expandMissingTimesheet;            
            sectionIndex = [dashboardModel sectionIndexForSectionType:kIGDashboardSectionMissingTimesheet];
            break;
        }
            
        case kIGDashboardInProgressTimesheetSectionHeaderTag:
        {
            dashboardModel.expandInProgressTimesheet = !dashboardModel.expandInProgressTimesheet;
            sectionIndex = [dashboardModel sectionIndexForSectionType:kIGDashboardSectionInProgressTimesheet];
            break;
        }
            
        default:
        {
            break;
        }
    }

    if (sectionIndex != -1)
    {
        
        @try
        {
            NSIndexSet *indexSet = [[NSIndexSet alloc] initWithIndex:sectionIndex];
            [_tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        @catch (NSException *exception)
        {
            NSLog(@"%@", [exception description]);
        }
        @finally
        {
            ;
        }
        
    }
}

//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    IGDashboardSection dashboardSection = [dashboardModel sectionTypeForSection:section];
//    if(dashboardSection == kIGDashboardSectionEnterTimesheet)
//    {
//        return nil;
//    }
//    else
//    {
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
//        view.backgroundColor = [UIColor lightGrayColor];
//        return view;
//    }
//}


#pragma mark -

- (NSDate *)getDateFromString:(NSString *)dateString
{
    if([dateString rangeOfString:@"Jan"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Jan" withString:@"01"];
    }
    else if([dateString rangeOfString:@"Feb"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Feb" withString:@"02"];
    }
    else if([dateString rangeOfString:@"Mar"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Mar" withString:@"03"];
    }
    else if([dateString rangeOfString:@"Apr"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Apr" withString:@"04"];
    }
    else if([dateString rangeOfString:@"May"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"May" withString:@"05"];
    }
    else if([dateString rangeOfString:@"Jun"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Jun" withString:@"06"];
    }
    else if([dateString rangeOfString:@"Jul"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Jul" withString:@"07"];
    }
    else if([dateString rangeOfString:@"Aug"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Aug" withString:@"08"];
    }
    else if([dateString rangeOfString:@"Sep"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Sep" withString:@"09"];
    }
    else if([dateString rangeOfString:@"Oct"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Oct" withString:@"10"];
    }
    else if([dateString rangeOfString:@"Nov"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Nov" withString:@"11"];
    }
    else if([dateString rangeOfString:@"Dec"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Dec" withString:@"12"];
        
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd MM yyyy";
    NSDate *date = [dateFormatter dateFromString:dateString];
    return date;
}

- (IBAction)datePicked:(id)sender
{
//    [_tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:3] animated:NO scrollPosition:UITableViewScrollPositionNone];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    NSIndexPath *selectedIndexPath = [_tableView indexPathForSelectedRow];

    //Added by Pulkit -
    //selected index path is coming nil for Submit/Enter Timesheet
    //If it is nil then, the section is Submit time sheet
    if (selectedIndexPath == nil)
    {
        CGPoint buttonPosition = [sender convertPoint:CGPointZero toView:_tableView];
        selectedIndexPath = [_tableView indexPathForRowAtPoint:buttonPosition];
    }

    NSDate *date = nil;
    NSString *status = @"";
    
    IGDashboardSection dashboardSection = [dashboardModel sectionTypeForSection:selectedIndexPath.section];
    
    switch (dashboardSection)
    {
        case kIGDashboardSectionMissingTimesheet:
        {
            NSString* dateString = [(IGDashboardObjectModel *)[dashboardModel.missingTimesheet objectAtIndex:selectedIndexPath.row] value];
            date = [self getDateFromString:dateString];
            status = @"InComplete";
            break;
        }

        case kIGDashboardSectionInProgressTimesheet:
        {
            NSString* dateString = [(IGDashboardObjectModel *)[dashboardModel.inProgressTimesheet objectAtIndex:selectedIndexPath.row] value];
            date = [self getDateFromString:dateString];
            status = @"InPogress";
            break;
        }

        case kIGDashboardSectionEnterTimesheet:
        {
            date = [self getDateFromString:weekDate];
            status = @"InComplete";
            break;
        }
         
            //added by Pulkit
            //selected index path is coming nil for Submit/Enter Timesheet
            //If it is nil then, the section is Submit time sheet
        case kIGDashboardSectionSubmit:
        {
            date = [self getDateFromString:weekDate];
            status = @"InComplete";
            break;
        }
        default:
            break;
    }

    [segue.destinationViewController initilizeDataForDate:date employeeID:[[IGUserDefaults sharedInstance].employee.staffID integerValue] andStatus:status];
}

#pragma marks - pickerView

- (IBAction)openYearSelectionController:(id)sender
{
    if (!yearPicker)
    {
        yearPicker = [IGYearPicker picker];
        yearPicker.delegate = (id)self;
        yearPicker.titleLabel.text = @"Select Year";
        [yearPicker presentOnView:self.view animated:YES];
    }
    else
    {
        [yearPicker show:YES animated:YES];
    }
}

- (IBAction)openMonthSelectionController:(id)sender
{
    if (!monthPicker)
    {
        monthPicker = [IGMonthPicker picker];
        monthPicker.delegate = (id)self;
        monthPicker.titleLabel.text = @"Select Month";
        monthPicker.selectedYear = yearString;
        [monthPicker presentOnView:self.view animated:YES];
    }
    else
    {
        monthPicker.selectedYear = yearString;
        [monthPicker presentOnView:self.view animated:YES];
    }
}

- (IBAction)openWeekEndDateSelectionController:(id)sender
{
    if (!weekPicker)
    {
        weekPicker = [IGWeekEndPicker picker];
        weekPicker.delegate = (id)self;
        weekPicker.titleLabel.text = @"Select Week";
        weekPicker.weekArray = self.weekEndArray;
        [weekPicker presentOnView:self.view animated:YES];
    }
    else
    {
        weekPicker.weekArray = self.weekEndArray;
        [weekPicker presentOnView:self.view animated:YES];
    }
}


#pragma mark - IGPickerDelegate

- (void)monthPicker:(IGMonthPicker *)picker
{
    NSInteger index = [picker.pickerView selectedRowInComponent:0] + 1;
    monthString = [@(index) stringValue];
    weekDate = nil;
    kAlertAndReturnIfNetworkNotReachable
    [self fetchWeekEndDateFromServer];
    
}

- (void)yearPicker:(IGYearPicker *)picker didSelectDate:(NSString *)year;
{
    yearString = year;
    if ([yearString isEqualToString:[IGUtils stringFromDate:[NSDate date] dateFormat:@"yyyy"]] && flag == TRUE)
    {
        monthString = [IGUtils stringFromDate:[NSDate date] dateFormat:@"MM"];
        flag = TRUE;
    }
    else
    {
        monthString = nil;
        flag = FALSE;
    }
    weekDate = nil;
    [self.dashboardTableView reloadData];
}

- (void)weekEndPicker:(IGWeekEndPicker *)picker didSelectDate:(NSString *)weekEnd
{
    NSInteger index = [picker.pickerView selectedRowInComponent:0];
    weekDate = self.weekEndArray[index];
    [self.dashboardTableView reloadData];
}

-(void)fetchWeekEndDateFromServer
{
    kAlertAndReturnIfNetworkNotReachable
    [self showSpinner:YES];
    [[IGService sharedInstance] weekEndDates:yearString month:monthString completion:^(id response) {
        NSDictionary *dictionary = (NSDictionary *)response;
        NSArray *dataArray = [dictionary objectForKey:@"Weekends"];
        [self.weekEndArray removeAllObjects];
        for (int index = 0; index < [dataArray count]; index++)
        {
            [self.weekEndArray addObject:[dataArray[index] objectForKey:@"Weekenddate"]];
        }
        if (([yearString isEqualToString:[IGUtils stringFromDate:[NSDate date] dateFormat:@"yyyy"]] &&
            [monthString isEqualToString:[IGUtils stringFromDate:[NSDate date] dateFormat:@"MM"]]) && self.weekEndArray.count > 0)
        {
            weekDate = self.weekEndArray[[self.weekEndArray count] - 1];
        }
        
        [self showSpinner:NO];
        [self.dashboardTableView reloadData];
        
    } error:^(NSError *error) {        
        [IGUtils showOKAlertWithTitle:kStringError message:[error localizedDescription]];
        [self showSpinner:NO];
    }];
    
}

@end
