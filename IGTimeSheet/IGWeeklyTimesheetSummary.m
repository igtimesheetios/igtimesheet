//
//  IGWeeklyTimesheetSummary.m
//  IGTimeSheet
//
//  Created by Manish on 23/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGWeeklyTimesheetSummary.h"
#import "NSDictionary+IGAddition.h"
#import "IGServiceDefine.h"

@implementation IGWeeklyTimesheetStatus

- (instancetype)initWithDictionary:(NSDictionary *)timesheetStatusInfo
{
    if ([timesheetStatusInfo respondsToSelector:@selector(objectForKey:)] == NO)
    {
        return nil;
    }
    
    self = [super init];
    if (self)
    {
        _status = [timesheetStatusInfo IGStringValueForKey:kIGServiceKeyStatus];
        _projectMangerStatus = [timesheetStatusInfo IGStringValueForKey:kIGServiceKeyProjectManagerStatus];
    }
    return self;
}

@end


@implementation IGWeeklyEffort

- (instancetype)initWithEffort:(NSDictionary *)effortInfo
{
    if ([effortInfo respondsToSelector:@selector(objectForKey:)] == NO)
    {
        return nil;
    }
    
    self = [super init];
    if (self)
    {
        _date = [effortInfo IGStringValueForKey:kIGServiceKeyDATE];
        _weekDay = [effortInfo IGStringValueForKey:kIGServiceKeyWeekday];
        _order = [effortInfo IGStringValueForKey:kIGServiceKeyOrder];
        _effort = [effortInfo IGNumberValueForKey:kIGServiceKeyEffortsHrs];
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"Date: %@\r"
            "weekDay: %@\r"
            "order: %@\r"
            "effort: %@\r", _date, _weekDay, _order, _effort];
}

@end


@implementation IGWeeklyTimesheetSummary

- (instancetype)initWithDictionary:(NSDictionary *)weeklyTimesheetInfo
{
    if ([weeklyTimesheetInfo respondsToSelector:@selector(objectForKey:)] == NO)
    {
        return nil;
    }
    
    self = [super init];
    if (self)
    {
        NSArray *timesheetStatus = weeklyTimesheetInfo[kIGServiceKeyTimeSheetStatus];
        if ([timesheetStatus respondsToSelector:@selector(objectAtIndex:)] && timesheetStatus.count > 0)
        {
            _timesheetStatus = [[IGWeeklyTimesheetStatus alloc] initWithDictionary:timesheetStatus[0]];
        }
        
        NSArray *weekIDArray = weeklyTimesheetInfo[kIGServiceKeyWeekID];
        if ([weekIDArray respondsToSelector:@selector(objectAtIndex:)] && weekIDArray.count > 0)
        {
            NSDictionary *weekIDDictionary = weekIDArray[0];
            if ([weekIDDictionary respondsToSelector:@selector(objectForKey:)])
            {
                id weekIDObject = weekIDDictionary[kIGServiceKeyWeekID];
                if ([weekIDObject respondsToSelector:@selector(integerValue)])
                {
                    _weekID = [weekIDObject integerValue];
                }
            }
        }
        
        NSArray *timesheetSummary = weeklyTimesheetInfo[kIGServiceKeyWeekTimeSheetSummary];
        NSInteger count = timesheetSummary.count;
        
        if (count != 0)
        {
            NSMutableArray *efforts = [[NSMutableArray alloc] initWithCapacity:1];
            
            for (int i = 0; i < count; i++)
            {
                NSDictionary *effortInfo = timesheetSummary[i];
                IGWeeklyEffort *weeklyEffort = [[IGWeeklyEffort alloc] initWithEffort:effortInfo];
                if (weeklyEffort != nil)
                {
                    [efforts addObject:weeklyEffort];
                }
            }
            
            if (efforts.count > 1)
            {
                NSSortDescriptor *orderSort = [[NSSortDescriptor alloc] initWithKey:@"order" ascending:YES];
                _timesheetSummary = [efforts sortedArrayUsingDescriptors:@[orderSort]];
            }
        }
    }
    return self;
}

@end
