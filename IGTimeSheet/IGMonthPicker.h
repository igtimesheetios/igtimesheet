//
//  IGMonthPickerViewController.h
//  IGTimeSheet
//
//  Created by Kamal on 27/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IGMonthPicker;
@protocol IGMonthPickerDelegate <NSObject>

- (void)monthPicker:(IGMonthPicker *)picker;

@end



@interface IGMonthPicker : UIView

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *buttonDone;
@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewBottomConstraint;


@property (nonatomic, strong) NSString *selectedYear;

@property (nonatomic, readonly) NSString *monthString;

//@property (nonatomic, readonly) NSInteger month;
//@property (nonatomic, readonly) NSInteger year;

@property (nonatomic, weak) id<IGMonthPickerDelegate> delegate;

+ (IGMonthPicker *)picker;
- (void)presentOnView:(UIView *)view animated:(BOOL)animated;

- (void)show:(BOOL)show animated:(BOOL)animated;

@end
