//
//  IGMacro.h
//  IGTimeSheet
//
//  Created by Manish on 09/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#ifndef IGTimeSheet_IGMacro_h
#define IGTimeSheet_IGMacro_h

#define kAppDelegate                (AppDelegate *)[[UIApplication sharedApplication] delegate]

#define kDefaultBarTintColor        [UIColor colorWithRed:(29.0f/255.0f) green:(151.0f/255.0f) blue:(242.0f/255.0f) alpha:1.0f]

#define kDefaultTintColor           [UIColor whiteColor]

#define kColorGrayType1             [UIColor colorWithRed:(79.0f/255.0f) green:(79.0f/255.0f) blue:(79.0f/255.0f) alpha:1.0f]
#define kColorGrayType2             [UIColor colorWithRed:(246.0f/255.0f) green:(246.0f/255.0f) blue:(246.0f/255.0f) alpha:1.0f]
#define kColorOrange                [UIColor colorWithRed:(247.0/255.0f) green:(152.0/255.0f) blue:(50.0/255.0f) alpha:1.0f]
#define kColorGreen                 [UIColor colorWithRed:(15.0/255.0f) green:(151.0/255.0f) blue:(70.0/255.0f) alpha:1.0f]
#define kColorSigninGreen           [UIColor colorWithRed:(48.0/255.0f) green:(169.0/255.0f) blue:(116.0/255.0f) alpha:1.0f]
#define kColorButtonGray           [UIColor colorWithRed:(221.0/255.0f) green:(218.0/255.0f) blue:(218.0/255.0f) alpha:1.0f]
#define kColorSubmitEnable          [UIColor colorWithRed:(53.0/255.0f) green:(182.0/255.0f) blue:(255.0/255.0f) alpha:1.0f]
#define kColorButtonShadow          [UIColor colorWithRed:(138.0/255.0f) green:(202.0/255.0f) blue:(242.0/255.0f) alpha:1.0f]
#define kColorButtonFontColor      [UIColor colorWithRed:(139.0/255.0f) green:(139.0/255.0f) blue:(139.0/255.0f) alpha:1.0f]
#define kColorLoginVCFontColor      [UIColor colorWithRed:(25.0/255.0f) green:(130.0/255.0f) blue:(211.0/255.0f) alpha:1.0f]

#define kFontName                   @"HelveticaNeue-light"

//#define ServerURL                   @"http://172.18.80.25:9810/api/TimeSheet/"
//#define ServerURL                   @"http://172.18.65.62:81/api/TimeSheet/"
#define ServerURL                   @"https://timesheetapi.infogain.com:447/api/TimeSheet/"


#define LogInAPI                    @"Login"
#define InOutTimeAPI                @"InOutTime"
#define TaskListAPI                 @"TaskList"
#define SubTaskListAPI              @"SubTaskList"
#define kVersionCheckURL            @"https://mobileapps.infogain.com/igTimeSheet_Resources/VersionControl/Version.plist";
#define kTimeSheetDownlaodPage      @"https://mobileapps.infogain.com/igtimesheet.html"


#define SubmittedSuccessfully       @"Your TimeSheet has been submitted successfully"



#define ERROR                   @"Error!!"
#define ParseError              @"Error occured in parsing. Please contact the developer"
#define RemoveError             @"Not able to remove TimeSheet entry from server"
#define TotalWeekHrError        @"Total hours in a week should not be less than 9 times the number of weekdays"

#define TotalWeekHrErrorForUS        @"Total hours in a week should not be less than 8 times the number of weekdays"

#define TotalDayHrError         @"Total hours in a weekday should not be less than 8"


#pragma mark - Keys

#define kKeyLatestVersion_iOS                               @"LatestVersion_iOS"
#define kStroryboardKeyName                                 @"name"
#define kIGServiceKeyIsUpdate                               @"isUpdate"
#define kIGServiceKeyTitle                                  @"title"
#define kIGServiceKeyMessage                                @"message"
#define kIGServiceKeyURL                                    @"url"
#define kIGServiceKeyIsMandatory                            @"isMandatory"
#define kIGServiceKeyUpdateResponse                         @"updateResponse"
#define kIGServiceValueSuccess                              @"Success"
#define kIGServiceValueFailure                              @"failure"
#define kIGServiceValueTrue                                 @"true"
#define kIGServiceValueFalse                                @"false"
#define kIGServiceValueNoUpdateAvailable                    @"No Update Available"
#define kIGServiceValueNewUpdateAvailable                   @"New Update Available"
#define kKeyLoginStatus                                     @"LoginStatus"
#define kKeyTitle                                           @"title"
#define kStringDone                                         @"Done"
#define kKeyIndex                                           @"index"
#define kKeyImage                                           @"image"
#define kKeyDescription                                     @"description"







#pragma mark - UserDefault

#define kKeyUserDefaultsAcceptTerms                         @"acceptTerms"
#define kKeyUserDefaultsFirstTimeLogin                      @"firstTimeLogin"
#define kKeyUserDefaultsIsDeviceTokenRegistered             @"isDeviceTokenRegistered"
#define kKeyUserDefaultsIsUserLoggedIn                      @"isUserLoggedIn"
#define kKeyUserDefaultsEmployeeName                        @"employeeName"
#define kKeyUserDefaultsEmployeeID                          @"employeeID"
#define kKeyUserDefaultsEmployeeEmail                       @"employeeEmail"
#define kKeyUserDefaultsEmployeeStatus                      @"employeeStatus"




#pragma mark - Strings

// for US employee in case of Non Billable

#define kTaskSubtask                                        @"TaskSubtask"
#define kNotRequired                                        @"Not Required"

#define kNotificationAlertMsg                               @"Please fill your timesheet"
#define kSuccesAlert                                        @"Success"
#define kSuccesAlertMessage                                  @"Do you want to submit the timesheet."
#define kStringAlert                                        @"Alert"
#define kStringError                                        @"Error"
#define kStringWarning                                      @"Warning"
#define kStringOK                                           @"OK"
#define kStringCancel                                       @"Cancel"
#define kStringUpdateAvailable                              @"Update Available"
#define kStringANewVersion_IsAvailableForDownload           @"A new version %@ is available for download."
#define kStringNoNewUpdates                                 @"No New Updates"
#define kStringThereAreNoNewUpdatesForigTimeSheet           @"There are no new updates for igTimeSheet."
#define kStringServerError                                  @"Server Error"
#define kStringUnexpectedResponseFromServer                 @"Unexpected response from server."
#define kStringSomethingWentWrongPleaseTryAgain             @"Something went wrong. Please try Again."
#define kStringCannotOpenURL                                @"Cannot open url."
#define kStringMessageFileNotFoundInBundle_                 @"File not found in bundle: %@.html"
#define kStringLogOut                                       @"Log Out"
#define kStringMessageYouNeedToAgreeToTermsAndConditions    @"You need to agree to the terms and conditions to use the application"
#define kStringMessageExpectedParameterOfKindNSData         @"Expected parameter of kind NSData"
#define kStringMessageExpectedParameterOfKindNSArray        @"Expected parameter of kind NSArray"
#define kStringMessageNoDataToWrite                         @"No data to write"
#define kStringMessageFileNameNotValid                      @"File name not valid"
#define kStringMessageDirectoryNotRecognized                @"Directory not recognized"
#define kStringMessageDeleteOperationFailed                 @"Delete operation failed"
#define kStringMessageWriteOperationFailed                  @"Write operation failed"
#define kStringMessageExpectedParameterOfKindNSDictionary   @"Expected parameter of kind NSDictionary"
#define kStringMessageErrorRemovingItemAtPath_Reason_       @"Error removing Item at path: %@. Reason: %@"
#define kStringMessageDefaultProfilePicNotFound             @"Default profile pic not found"
#define kStringCouldNotGetStoryBoard                        @"Could not get storyboard"
#define kStringMessageCouldNotLoadVC_FromStoryboard_        @"Could not get a view controller for identifier: %@ from Storyboard: %@"
#define kStringNetworkIssue                                 @"Network Issue"
#define kStringAuthenticating                               @"Authenticating..."
#define kStringLoginError                                   @"Login Error"
#define kStringYES                                          @"YES"
#define kStringNO                                           @"NO"
#define kStringMessageServerDidNotReturnAValidUserData      @"Server did not return a valid User data"
#define kStringMessageIncorrectCredentialsPleaseCheckUsernamePasswordAndTryAgain @"Incorrect Credentials. Please check Username/Password and try again."
#define kStringMessageInvalidResponseFromServer             @"Invalid response from server"
#define kStringMessageThankYouForYourValuebleFeedback       @"Thank you for valuable feedback"





#pragma mark - StoryboardIdentifiers

#define kStoryBoardIdentifierLoginVC                    @"IGLoginVC"
#define kStoryboardSegueIndentifierExitInfoVCSegue      @"exitInfoVCSegue"
#define kStoryBoardIdentifierTermsConditionVC           @"igTermConditionVC"
#define kStoryBoardIdentifierAuthenticationVC           @"IGAuthenticationViewController"
#define kStoryBoardIdentifierMandatoryUpdateVC          @"igMandatoryUpdateVC"
#define kStoryboardIdentifierGMDataVC                   @"GMDataViewController"



#pragma mark - TimeSheetStatus

#define TimeSheetPendingStatus              @"P"
#define TimeSheetCompleteStatus             @"C"
#define TimeSheetApprovedStatus             @"A"
#define TimeSheetRejectedStatus             @"R"

#pragma mark - Images

#define kUserDefaultImage                   @"profile-avtaar-pic"
#define kImageInfogainLogoCircle            @"infogain-logo-circle"
#define kImageScreenBG                      @"screen-bg"

typedef NS_ENUM(NSInteger, IGTimeSheetStatus) {
    kIGTimeSheetStatusPending,
    kIGTimeSheetStatusCompleted,
    kIGTimeSheetStatusApproved,
    kIGTimeSheetStatusRejected
};

typedef NS_ENUM(NSInteger, igScene)
{
    kSceneUndefined,
    kSceneDashBoard,
    kSceneMyViewLeaves,
    kSceneMyArea,
    kSceneMonthlyAverage,
    kSceneHelp,
    kSceneWalkthrough
};


#endif
