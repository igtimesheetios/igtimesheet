//
//  IGViewLeavesParser.h
//  IGTimeSheet
//
//  Created by Kamal on 19/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGViewLeavesParser : NSObject

-(NSArray *)parseLeaveViewData:(NSArray *)leavesArray;

@end
