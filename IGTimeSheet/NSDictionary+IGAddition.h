//
//  NSDictionary+IGAddition.h
//  IGTimeSheet
//
//  Created by Manish on 23/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (IGAddition)

- (NSString *)IGStringValueForKey:(NSString *)key;
- (NSNumber *)IGNumberValueForKey:(NSString *)key;


@end
