//
//  IGDashboardTableViewDateCell.m
//  IGTimeSheet
//
//  Created by Rajat on 11/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGDashboardTableViewDateCell.h"

@interface IGDashboardTableViewDateCell ()

@property (weak, nonatomic) IBOutlet UIView *groupView;

@end

@implementation IGDashboardTableViewDateCell

- (void)awakeFromNib
{
    self.groupView.layer.cornerRadius = 5.0f;
   // self.submitButton.layer.cornerRadius = 7.0f;
    
//    self.datepicker.datePickerMode = UIDatePickerModeDate;
//    self.datepicker.maximumDate = [NSDate date];
//    self.datepicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:-(6*30*24*60*60)];
}

@end
