//
//  AppDelegate.m
//  IGTimeSheet
//
//  Created by Manish on 06/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "AppDelegate.h"
#import "IGLoginVC.h"
#import "IGSideMenuRootVC.h"
#import "IGCommonHeaders.h"
#import "IGAuthenticationVC.h"
#import "igTermConditionVC.h"
#import "igUpdateResponse.h"
#import "igMandatoryUpdateVC.h"
#import "igTActivityBar.h"
#import <FirebaseCore/FirebaseCore.h>
#import <FirebaseMessaging/FirebaseMessaging.h>
#import <FirebaseAnalytics/FirebaseAnalytics.h>
#import "IGGoogleAnalytics.h"
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

#ifndef NSFoundationVersionNumber_iOS_9_x_Max
#define NSFoundationVersionNumber_iOS_9_x_Max 1299
#endif


#define FRIDAY  5 // 5th day of week
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

NSString *const kGCMMessageIDKey = @"gcm.message_id";

@interface AppDelegate ()<UNUserNotificationCenterDelegate, FIRMessagingDelegate>{

bool allowNotif;
bool allowsSound;
bool allowsBadge;
bool allowsAlert;
}
@property (nonatomic, strong) NSString *deviceTokenString;

@end

@implementation AppDelegate

//@synthesize employeeId , domain, userName , loginName;


#pragma mark - Application Life Cycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    self.requestType = -1;
    // Initializing IGService instance for fetching Network Reachability Status
    [IGService sharedInstance];
    
    //Write code after launch is finished
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageNamed:@"header-bg.png"] forBarMetrics:UIBarMetricsDefault];
    [[UINavigationBar appearance] setShadowImage:[[UIImage alloc] init]];
    [UINavigationBar appearance].tintColor = kDefaultTintColor;
    
    
    NSDictionary *titleTextAttributes = @{
        NSForegroundColorAttributeName: [UIColor whiteColor]
    };
    [[UINavigationBar appearance] setTitleTextAttributes:titleTextAttributes];
    
    [self setupRootViewControllerWithInfo:nil];
    
    //TODO: Uncomment once the server implementation is done
//    [self configureRemoteNotification];
    
   // [self setUpTimeSheetAlertNotification];
    [UIApplication sharedApplication].applicationIconBadgeNumber = 0;
    
    [IGAnalytics initialize];
    
    [self configureFireBase];
    [self registerForFireBaseRemoteNotification];
    
    return YES;
}

-(void)setUpTimeSheetAlertNotification
{
    
    UIUserNotificationType types = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings settingsForTypes:types categories:nil];
    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
    
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    [self setNotificationTypesAllowed];
    if (notification)
    {
        if (allowNotif)
        {
            NSDate *currentDate  = [NSDate date];
            NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
            
            
            NSDateComponents *components = [gregorian components:(NSCalendarUnitWeekOfMonth | NSCalendarUnitMonth | NSCalendarUnitDay | NSCalendarUnitWeekday | NSCalendarUnitWeekOfMonth)  fromDate:currentDate];
            [components setWeekday:FRIDAY];
            [components setHour:19]; // 7 PM
            [components setMinute:0];
            
            NSDate *notificationDate = [gregorian dateFromComponents: components];
            
            
            notification.fireDate = notificationDate ;//[NSDate dateWithTimeIntervalSinceNow:60];;
            notification.timeZone = [NSTimeZone defaultTimeZone];
            notification.repeatInterval = NSCalendarUnitWeekday;
            
            
        }
        if (allowsAlert)
        {
            notification.alertBody =  kNotificationAlertMsg;
            notification.applicationIconBadgeNumber = 0;
        }
//        if (allowsBadge)
//        {
//            notification.applicationIconBadgeNumber = 1;
//        }
        if (allowsSound)
        {
            notification.soundName = UILocalNotificationDefaultSoundName;
        }
        
        // this will schedule the notification to fire at the fire date
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        // this will fire the notification right away, it will still also fire at the date we set
       // [[UIApplication sharedApplication] presentLocalNotificationNow:notification];
    }

}

- (void)setNotificationTypesAllowed
{
    UIUserNotificationSettings *currentSettings = [[UIApplication sharedApplication] currentUserNotificationSettings];
    allowNotif = (currentSettings.types != UIUserNotificationTypeNone);
    allowsSound = (currentSettings.types & UIUserNotificationTypeSound) != 0;
    allowsBadge = (currentSettings.types & UIUserNotificationTypeBadge) == 0;
    allowsAlert = (currentSettings.types & UIUserNotificationTypeAlert) != 0;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
}

- (void)applicationWillTerminate:(UIApplication *)application
{
}

- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification
{
     [igTActivityBar showSuccessWithStatus:notification.alertBody duration:2.0f];
   
}


- (void)setupRootViewControllerWithInfo:(NSDictionary *)dictionary
{
#if TEST
    //    [self setupSideMenuControllerAsRootVCWithStoryBoard:storyboard];
    //    return;
    
    [igUserDefaults sharedInstance].user.isAuthenticated = YES;
    
#endif
    
    if ([self showLoginScreen] == NO)
    {
        if ([self showAuthenticationScreen] == NO)
        {
            if ([self showTermsAndConditioinScreen] == NO)
            {
                [self showMainScreenWithInfo:dictionary];
            }
        }
    }
    
    if ([self.window.rootViewController isKindOfClass:[IGSideMenuRootVC class]])
    {
        self.sideMenuRootVC = (IGSideMenuRootVC *)self.window.rootViewController;
    }
    else
    {
        self.sideMenuRootVC = nil;
    }
}

- (BOOL)showLoginScreen
{
    // if user is not logged in
    // show login screen
    if ([IGUserDefaults sharedInstance].isUserLoggedIn == NO)
    {
        if (![self.window.rootViewController isKindOfClass:[IGLoginVC class]])
        {
            [self setupLoginControllerAsRootViewController];
        }
        else
        {
            // reset
        }
        return YES;
    }
    return NO;
}

- (BOOL)showAuthenticationScreen
{
    // if user had earlier logged in
    // but user is not authenticated
    // show Authentication screen
    if ([IGUserDefaults sharedInstance].employee.isAuthenticated == NO)
    {
        if (![self.window.rootViewController isKindOfClass:[IGAuthenticationVC class]])
        {
            [self setupAuthenticationAsRootViewController];
        }
        else
        {
            // reset
        }
        return YES;
    }
    return NO;
}

- (BOOL)showTermsAndConditioinScreen
{
    // if user had earlier logged in
    // and user is authenticated
    // but user has not accepted terms and conditions
    // show Terms and Condition Screen
    if ([IGUserDefaults sharedInstance].hasAcceptedTermsAndConditions == NO)
    {
        if (![self.window.rootViewController isKindOfClass:[igTermConditionVC class]])
        {
            [self setupTermsConditionControllerAsRootViewController];
        }
        else
        {
            // reset
        }
        return YES;
    }
    return NO;
}

- (void)showMainScreenWithInfo:(NSDictionary *)dictionary
{
    // if user had earlier logged in
    // and user is authenticated
    // and user has accepted terms and conditions
    // setup Root Controller
    
    IGSideMenuRootVC *rootVC = (IGSideMenuRootVC *)self.window.rootViewController;
    if ([rootVC isKindOfClass:[IGSideMenuRootVC class]])
    {
        rootVC.showLeftMenuOnViewDidAppear = !self.hideLeftMenu;
        [self.sideMenuRootVC populateDashBoardWithInfo:dictionary];
        [self.sideMenuRootVC showSignedInScreen];
//        if (_sceneToLoad != rootVC.currentScene)
//        {
//            [rootVC setupContentVCForScene:_sceneToLoad];
//        }
    }
    else
    {
        [self setupSideMenuControllerAsRootVC];
    }
    
    self.sideMenuRootVC = rootVC;
    
}


- (void)setupAuthenticationAsRootViewController
{
    UIStoryboard *storyboard = self.window.rootViewController.storyboard;
    NSAssert(storyboard != nil, kStringCouldNotGetStoryBoard);
    
    IGAuthenticationVC *authenticationVC = (IGAuthenticationVC *)[storyboard instantiateViewControllerWithIdentifier:kStoryBoardIdentifierAuthenticationVC];
    self.window.rootViewController = authenticationVC;
}

- (void)setupTermsConditionControllerAsRootViewController
{
    UIStoryboard *storyboard = self.window.rootViewController.storyboard;
    NSAssert(storyboard != nil, kStringCouldNotGetStoryBoard);
    
    igTermConditionVC *termsConditionViewController = (igTermConditionVC *)[storyboard instantiateViewControllerWithIdentifier:kStoryBoardIdentifierTermsConditionVC];
    NSAssert2(termsConditionViewController != nil, kStringMessageCouldNotLoadVC_FromStoryboard_, kStoryBoardIdentifierTermsConditionVC, [storyboard valueForKey:kStroryboardKeyName]);
    termsConditionViewController.isLaunchScene = YES;
    [self animateAddRootViewController:termsConditionViewController completion:nil];
    
}

- (void)setupLoginControllerAsRootViewController
{
    UIStoryboard *storyboard = self.window.rootViewController.storyboard;
    NSAssert(storyboard != nil, kStringCouldNotGetStoryBoard);
    
    IGLoginVC *loginViewController = [storyboard instantiateViewControllerWithIdentifier:kStoryBoardIdentifierLoginVC];
    NSAssert2(loginViewController != nil, kStringMessageCouldNotLoadVC_FromStoryboard_, kStoryBoardIdentifierLoginVC, [storyboard valueForKey:kStroryboardKeyName]);
    [self animateAddRootViewController:loginViewController completion:nil];
}

- (void)setupSideMenuControllerAsRootVC
{
    UIStoryboard *storyboard = self.window.rootViewController.storyboard;
    NSAssert(storyboard != nil, kStringCouldNotGetStoryBoard);
    
    IGSideMenuRootVC *rootVC = [storyboard instantiateViewControllerWithIdentifier:@"IGSideMenuRootVC"];
    rootVC.showLeftMenuOnViewDidAppear = !self.hideLeftMenu;
    [self animateAddRootViewController:rootVC completion:nil];
}

- (void)animateAddRootViewController:(UIViewController *)viewController completion:(void(^)(BOOL finished))completion
{
    UIView *view = [self.window.rootViewController.view snapshotViewAfterScreenUpdates:NO];
    self.window.rootViewController = viewController;
    [self.window addSubview:view];
    
    [UIView transitionWithView:view
                      duration:0.3
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        view.alpha = 0.0f;
                    }
                    completion:^(BOOL finished) {
                        [view removeFromSuperview];
                        if (completion)
                        {
                            completion(finished);
                        }
                    }];
    
}

- (void)showMandatoryUpdateSceneWithResponse:(igUpdateResponse *)response
{
    
    UIStoryboard *storyboard = self.window.rootViewController.storyboard;
    NSAssert(storyboard != nil, kStringCouldNotGetStoryBoard);
    
    igMandatoryUpdateVC *viewController = (igMandatoryUpdateVC *)[storyboard instantiateViewControllerWithIdentifier:kStoryBoardIdentifierMandatoryUpdateVC];
    viewController.updateDescriptionString = response.message;
    NSAssert2(viewController != nil, kStringMessageCouldNotLoadVC_FromStoryboard_, kStoryBoardIdentifierMandatoryUpdateVC, [storyboard valueForKey:kStroryboardKeyName]);
    NSAssert([viewController isKindOfClass:[igMandatoryUpdateVC class]],@"expected a controller of type igMandatoryUpdateVC");
    viewController.updateResponse = response;
    [self animateAddRootViewController:viewController completion:nil];
}


//Old
#pragma mark - View Transition

- (void)showSignInScreen
{
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    IGLoginVC *loginVC = [storyboard instantiateViewControllerWithIdentifier:@"IGLoginVC"];
    
    UIView *view = [self.window.rootViewController.view snapshotViewAfterScreenUpdates:YES];
    [self.window addSubview:view];

    self.window.rootViewController = loginVC;
    
    __weak UIView *weakView = view;
    
    [UIView transitionWithView:self.window
                      duration:0.5
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:^{
                        weakView.alpha = 0.0f;
                    } completion:^(BOOL finished) {
                        [weakView removeFromSuperview];
                        self.sideMenuRootVC = nil;
                    }];
}

//TODO: To be deleted
- (void)showSignedInScreenWithInfo:(NSDictionary *)dictionary
{
    if (self.sideMenuRootVC == nil)
    {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        self.sideMenuRootVC = [storyboard instantiateViewControllerWithIdentifier:@"IGSideMenuRootVC"];
        [self.sideMenuRootVC populateDashBoardWithInfo:dictionary];
        
        UIView *view = [self.window.rootViewController.view snapshotViewAfterScreenUpdates:YES];
        [self.window addSubview:view];

        self.window.rootViewController = self.sideMenuRootVC;
        
        __weak UIView *weakView = view;

        [UIView transitionWithView:self.window
                          duration:0.5
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            weakView.alpha = 0.0f;
                        }
                        completion:^(BOOL finished){
                            [weakView removeFromSuperview];
                        }];
    }
    else
    {
        [self.sideMenuRootVC populateDashBoardWithInfo:dictionary];
        [self.sideMenuRootVC showSignedInScreen];
    }
    
}

#pragma mark - Remote Notification

- (void)configureRemoteNotification
{
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)])
    {
        UIUserNotificationType userNotificationType = UIUserNotificationTypeAlert | UIUserNotificationTypeBadge | UIUserNotificationTypeSound;
        UIUserNotificationSettings *userNotificationSettings = [UIUserNotificationSettings settingsForTypes:userNotificationType categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:userNotificationSettings];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else
    {
        UIRemoteNotificationType remoteNotificationType = UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:remoteNotificationType];
    }
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    if (deviceToken)
    {
        NSString *deviceTokenString = [NSString stringWithFormat:@"%@", deviceToken];
        deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString:@"<" withString:@""];
        deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString:@">" withString:@""];
        deviceTokenString = [deviceTokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
        self.deviceTokenString = deviceTokenString;
        NSLog(@"Device Token: %@", self.deviceTokenString);
        
        //TODO: Implement in UserDefaults
       // [[IGUserDefaults sharedInstance] updateDeviceTokenOnServer];
    }
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    NSLog(@"Error registering for Remote Notification: %@", [error localizedDescription]);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    //TODO: parse payload
    
    completionHandler(UIBackgroundFetchResultNewData);
}

-(void)configureFireBase{
    [FIRApp configure];
}

-(void)registerForFireBaseRemoteNotification{
    
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings =
        [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        UNAuthorizationOptions authOptions =
        UNAuthorizationOptionAlert
        | UNAuthorizationOptionSound
        | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
        }];
        
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        // For iOS 10 data message (sent via FCM)
        [FIRMessaging messaging].remoteMessageDelegate = self;
#endif
    }
    
    [[UIApplication sharedApplication] registerForRemoteNotifications];
    // [END register_for_notifications]
}

@end
