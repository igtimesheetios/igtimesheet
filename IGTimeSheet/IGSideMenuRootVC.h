//
//  IGSideMenuRootVC.h
//  IGTimeSheet
//
//  Created by Manish on 09/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RESideMenu.h"

@interface IGSideMenuRootVC : RESideMenu <RESideMenuDelegate>

- (void)showSignedInScreen;

- (void)showDashBorad;
- (void)populateDashBoardWithInfo:(NSDictionary *)dictionary;

- (void)showMonthlyTimeSheetAverageScreen;
- (void)showViewLeaves;
- (void)showMyArea;
- (void)showHelpScreen;

@property (nonatomic, assign) BOOL showLeftMenuOnViewDidAppear;

@end