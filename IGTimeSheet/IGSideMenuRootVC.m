//
//  IGSideMenuRootVC.m
//  IGTimeSheet
//
//  Created by Manish on 09/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGSideMenuRootVC.h"
#import "IGDashboardVC.h"
#import "IGLeftMenuVC.h"
#import "IGDocumentViewer.h"
#import "IGTutorialRootViewController.h"
#import "IGCommonHeaders.h"
#import <Instabug/Instabug.h>

@interface IGSideMenuRootVC () <IGLeftMenuVCDelegate>

@property (nonatomic, assign) NSInteger selectionIndex;

@end


@implementation IGSideMenuRootVC

- (void)awakeFromNib
{
    self.menuPreferredStatusBarStyle = UIStatusBarStyleLightContent;
    self.contentViewShadowColor = [UIColor blackColor];
    self.contentViewShadowOffset = CGSizeMake(0, 0);
    self.contentViewShadowOpacity = 0.6;
    self.contentViewShadowRadius = 12;
    self.contentViewShadowEnabled = YES;
    self.contentViewScaleValue = 0.85f;

    if ([UIScreen mainScreen].bounds.size.width >= 414)
    {
        self.contentViewInPortraitOffsetCenterX  = 100.0f;
    }
    else if ([UIScreen mainScreen].bounds.size.width > 320 && [UIScreen mainScreen].bounds.size.width < 414)
    {
        self.contentViewInPortraitOffsetCenterX  = 90.0f;
        
    }
    else
    {
        self.contentViewInPortraitOffsetCenterX  = 80.0f;
    }

    
    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"contentVC"];
    
    // ContentVC
    self.selectionIndex = 0;
    
    IGLeftMenuVC *leftMenuVC = [self.storyboard instantiateViewControllerWithIdentifier:@"leftMenuVC"];
    leftMenuVC.delegate = self;
    
    BOOL isUSEmployee = [IGUserDefaults sharedInstance].employee.isUSEmployee;
    BOOL isPM = [IGUserDefaults sharedInstance].employee.isPMRole;
    
    if (isUSEmployee) {
        
        if (isPM) {
            leftMenuVC.optionsArray = @[
                                        @"Dashboard",
                                        @"Help",
                                        @"Walkthrough",
                                        @"Share Feedback",
                                        @"Timesheet Approval"];
            // @"Timesheet Approval"];
            
            leftMenuVC.imageArray = @[@"dashboard-icon",
                                      @"help-icon",
                                      @"Walkthrough",
                                      @"share-feedback",
                                      @"Approval-icon"];
        }else{
            leftMenuVC.optionsArray = @[
                                        @"Dashboard",
                                        @"Help",
                                        @"Walkthrough",
                                        @"Share Feedback"];
            // @"Timesheet Approval"];
            
            leftMenuVC.imageArray = @[@"dashboard-icon",
                                      @"help-icon",
                                      @"Walkthrough",
                                      @"share-feedback"];

        }
        //
    }
    else{
        if (isPM) {
            leftMenuVC.optionsArray = @[
                                        @"Dashboard",
                                        @"View Leaves",
                                        @"Help",
                                        @"Walkthrough",
                                        @"Share Feedback",
                                        @"Timesheet Approval"];
            // @"Timesheet Approval"];
            
            leftMenuVC.imageArray = @[@"dashboard-icon",
                                      @"view-leaves-icon",
                                      @"help-icon",
                                      @"Walkthrough",
                                      @"share-feedback",
                                      @"Approval-icon"];
        }else{
            leftMenuVC.optionsArray = @[
                                        @"Dashboard",
                                        @"View Leaves",
                                        @"Help",
                                        @"Walkthrough",
                                        @"Share Feedback"];
            // @"Timesheet Approval"];
            
            leftMenuVC.imageArray = @[@"dashboard-icon",
                                      @"view-leaves-icon",
                                      @"help-icon",
                                      @"Walkthrough",
                                      @"share-feedback"];

        }
        
    }
        
    
    

    
    
//    leftMenuVC.optionsArray = @[
//                                @"Dashboard",
//                                @"View Leaves",
//                                @"My Area",
//                                @"Monthly Timesheet Average",
//                                @"Help",
//                                @"Walkthrough",
//                                @"Share Feedback"];
//                               // @"Timesheet Approval"];
//    
//    leftMenuVC.imageArray = @[@"dashboard-icon",
//                              @"view-leaves-icon",
//                              @"my-area-icon",
//                              @"my-monthly-average-icon",
//                              @"help-icon",
//                              @"help-icon",
//                              @"share-feedback"];
//                              //@"share-feedback"];
    
    self.leftMenuViewController = leftMenuVC;

    self.rightMenuViewController = nil;
    
    self.backgroundImage = [UIImage imageNamed:@"screen-bg"];
    self.delegate = self;
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
   return UIStatusBarStyleLightContent;
}

//- (void)setupContentVCForScene:(igScene)sceneToLoad
//{
//    NSString *idenfierOfControllerToLoad = [igStoryboardIdentifier storyboardIdentifierForScene:sceneToLoad];
//    self.contentViewController = [self.storyboard instantiateViewControllerWithIdentifier:idenfierOfControllerToLoad];
//    self.currentScene = sceneToLoad;
//}


#pragma mark - RESideMenuDelegate

- (void)sideMenu:(RESideMenu *)sideMenu willShowMenuViewController:(UIViewController *)menuViewController
{
//    NSLog(@"willShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didShowMenuViewController:(UIViewController *)menuViewController
{
//    NSLog(@"didShowMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu willHideMenuViewController:(UIViewController *)menuViewController
{
//    NSLog(@"willHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}

- (void)sideMenu:(RESideMenu *)sideMenu didHideMenuViewController:(UIViewController *)menuViewController
{
//    NSLog(@"didHideMenuViewController: %@", NSStringFromClass([menuViewController class]));
}


#pragma mark - IGLeftMenuVCDelegate

- (void)leftMenu:(IGLeftMenuVC *)leftMenuVC selectionIndex:(NSInteger)index
{
    if (index == self.selectionIndex && (index != 2 && index != 3 && index != 4))
    {
        [self hideMenuViewController];
    }
    else
    {
        self.selectionIndex = index;
        
        BOOL isUSEmp =  [IGUserDefaults sharedInstance].employee.isUSEmployee;
        
        if (isUSEmp) {
            switch (index)
            {
                    
                case 0:
                {
                    [self showDashBorad];
                    break;
                }
                case 1:
                {
                    [self showHelpScreen];
                    break;
                }
                    
                case 2:
                {

                    [self showWalkthrough];
                    break;
                }
                    
                case 3:
                {

                    [Instabug invoke];
                    break;
                }
                case 4:{
                    [self showTimesheetApproval];
                    break;
                }
                    
                default:
                    break;
            }
            //
        }
        else{
            switch (index)
            {
                    
                case 0:
                {
                    [self showDashBorad];
                    break;
                }
                    
                case 1:
                {
                    [self showViewLeaves];
                    break;
                }
                case 2:
                {

                    [self showHelpScreen];
                    break;
                }
                    
                case 3:
                {

                    [self showWalkthrough];
                    break;
                }
                    
                case 4:
                {

                    [Instabug invoke];
                    break;
                }
                    
                case 5:{
                    [self showTimesheetApproval];
                    break;
                }
                default:
                    break;
            }
            
        }
        
        
    }
}



#pragma mark -

- (void)showSignedInScreen
{
    [self showDashBorad];
}

- (void)showDashBorad
{
    UIViewController *contentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"contentVC"];
    
    [self setContentViewController:contentVC animated:YES];
    [self hideMenuViewController];
}

- (void)populateDashBoardWithInfo:(NSDictionary *)dictionary
{
    UINavigationController* navController =  (UINavigationController *)self.contentViewController;
    id controller = navController.viewControllers[0];
    if ([controller isKindOfClass:[IGDashboardVC class]])
    {
        IGDashboardVC *dashBoardVC = controller;
        [dashBoardVC populateView:dictionary];
    }
}

- (void)showMonthlyTimeSheetAverageScreen
{
    UIViewController *contentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MonthlyTimeSheetAverageNav"];
    
    [self setContentViewController:contentVC animated:YES];
    [self hideMenuViewController];
}

-(void)showViewLeaves
{
    UIViewController *contentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"IGViewLeavesNavigationVC"];
    
    [self setContentViewController:contentVC animated:YES];
    [self hideMenuViewController];
    
}

-(void)showMyArea
{
    UIViewController *contentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"myAreaNav"];
    
    [self setContentViewController:contentVC animated:YES];
    [self hideMenuViewController];
    
}

- (void)showHelpScreen
{
//    UIViewController *contentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpNavigationVC"];
    
    IGDocumentViewer *documentViewer = [self.storyboard instantiateViewControllerWithIdentifier:@"IGDocumentViewer"];
    documentViewer.fileURL = [[NSBundle mainBundle] URLForResource:@"help-book" withExtension:@"pdf"];
    documentViewer.title = @"Help";
    UINavigationController *contentVC = [[UINavigationController alloc] initWithRootViewController:documentViewer];
    
    [self setContentViewController:contentVC animated:YES];
    [self hideMenuViewController];    
}


- (void) showWalkthrough
{
    IGTutorialRootViewController *tutorialRootVC = [self.storyboard instantiateViewControllerWithIdentifier:@"IGTutorialRootViewController"];
    tutorialRootVC.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:kImageScreenBG]];
    [self presentViewController:tutorialRootVC animated:YES completion:nil];
}

- (void)showTimesheetApproval
{
    UIViewController *contentVC = [self.storyboard instantiateViewControllerWithIdentifier:@"timesheetApprovalNav"];
    
    [self setContentViewController:contentVC animated:YES];
    [self hideMenuViewController];

}

@end
