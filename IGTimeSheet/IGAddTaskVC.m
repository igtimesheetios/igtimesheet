//
//  IGAddTaskVC.m
//  IGTimeSheet
//
//  Created by Neha on 18/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#define kCellReuseIdentifierA @"AddTaskCellA"
#define kCellReuseIdentifierB @"AddTaskCellB"
#define kCellReuseIdentifierC @"AddTaskCellC"
#define kCellReuseIdentifierD @"AddTaskCellD"
#define kCellReuseIdentifierE @"AddTaskCellE"

#define kHeaderCellReuseIdentifierA @"AddTaskHeaderCellA"
#define kHeaderCellReuseIdentifierB @"AddTaskHeaderCellB"
#define kFooterCellReuseIdentifier @"AddTaskFooterCell"

#import "IGAddTaskVC.h"
#import "IGPickerCell.h"
#import "IGAddTaskParser.h"
#import "IGTaskDataModel.h"
#import "IGTaskHrsCell.h"
#import "IGTaskDescriptionCell.h"
#import "IGTaskViewVC.h"
#import "IGTotalHrsCell.h"
#import "IGWFOLSwitchCell.h"
#import "IGAddTaskMacros.h"

#import "IGCommonHeaders.h"
#import "IGTextInputView.h"
#import "IGInOutTimeCell.h"
#import "IGInOutModalClass.h"
#import "IGCRIdModel.h"
#import "IGCategoryTaskDataModel.h"

#define kInOutTimeCollectionViewMaxHeight       60

@interface IGAddTaskVC ()<UITableViewDataSource, UITableViewDelegate, IGPickerCellDelegate, UITextFieldDelegate, IGTextInputViewDelegate, UICollectionViewDataSource>
{
  //  UIActivityIndicatorView *indicator;
    BOOL projectSelected;
    UIView *halfTransparentView;
    int weekendDateIndex;
    int invalidDayCount;
    NSMutableDictionary* hrsTfDict;
    NSIndexPath *editingIndexPath;
    UILabel *totalHrsLabel;
    UIToolbar* keyboardToolbar;
    BOOL isEditable;
    
    BOOL isSubtaskChangeRequired;
    
    int selectedCRIID;
    
    NSDate *weekendDate;
    BOOL callingFromInProgress;
}

- (void) setSeperatorInsetZero: (UITableViewCell*)cell;
- (void) fetchTaskFromServerForEmployee:(NSInteger)empId projectId:(NSInteger)prjId forWeek:(NSInteger)weekId;
- (void) fetchSubtaskFromServerForEmployee:(NSInteger)empId projectId:(NSInteger)prjId taskId:(NSInteger)taskId;
- (void) getWeekDates :(NSDate*)weekendDate;
- (void) setHrsTextField: (UITextField*)textF forWeekday:(NSInteger)day;
- (void) initHrsWithZero;
- (void) loadTableViewPickerCell:(IGPickerCell*)cell forRow:(NSInteger)row;
- (void) woflSwitchChanged: (id)sender;
- (void) editTask: (id)sender;
- (IGTaskModalClass*) createCopyOfTask: (IGTaskModalClass*)task;
- (void) disableCellIfTimesheetCompleted : (UITableViewCell*)cell ofClass: (Class)classType;
- (NSDate*) getPreviousMonday:(NSDate*)weekendDate;
- (int) getInvalidDayCount: (NSDate*)weekendDate;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightInOutTimeView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionViewInOutTime;
@property (weak, nonatomic) IBOutlet UIView *viewInOutTime;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;

@end

@implementation IGAddTaskVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
    
    hrsTfDict = [[NSMutableDictionary alloc]init];
    [self setupOpaqueNavigationBar];
    isSubtaskChangeRequired = false;
    
    if(_isAddTask)
    {
        self.btnViewHeightConstraint.constant = 58;
        [self.buttonsView needsUpdateConstraints];
        [self.buttonsView.subviews setValue:@NO forKeyPath:@"hidden"];
        self.navigationItem.title = @"Add Task";
    }
    else
    {
        self.btnViewHeightConstraint.constant = 0;
        [self.buttonsView needsUpdateConstraints];
        [self.buttonsView.subviews setValue:@YES forKeyPath:@"hidden"];
        self.navigationItem.title = @"Task";
        
        if(self.timesheetStatus == kIGTimeSheetStatusPending || self.timesheetStatus == kIGTimeSheetStatusRejected)
        {
            UIBarButtonItem *editButton = [[UIBarButtonItem alloc] initWithTitle:@"Edit" style:UIBarButtonItemStylePlain
                                                                         target:self action:@selector(editTask:)];
            NSArray *rightBarButtons = self.navigationItem.rightBarButtonItems;
            if (rightBarButtons.count != 0)
            {
                NSMutableArray *barButtons = [[NSMutableArray alloc] initWithArray:rightBarButtons];
                [barButtons addObject:editButton];
                self.navigationItem.rightBarButtonItems = barButtons;
            }
            else
            {
                self.navigationItem.rightBarButtonItem = editButton;
            }
        }
    }
    
    keyboardToolbar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 0, 320, 50)];
    keyboardToolbar.barStyle = UIBarStyleBlackOpaque;
    keyboardToolbar.translucent = NO;
    keyboardToolbar.barTintColor = [UIColor colorWithRed:237.0/255 green:237.0/255 blue:237.0/255 alpha:1]; 
    keyboardToolbar.items = [NSArray arrayWithObjects:
                           [[UIBarButtonItem alloc]initWithTitle:@"Cancel" style:UIBarButtonItemStylePlain target:self action:@selector(cancelNumberPad)],
                           [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                           [[UIBarButtonItem alloc]initWithTitle:@"Next" style:UIBarButtonItemStylePlain target:self action:@selector(nextWithNumberPad)],
                           nil];
    [keyboardToolbar sizeToFit];
    
    self.automaticallyAdjustsScrollViewInsets = YES;
    
    // setup inout time view
    if (self.inOutDetails.count == 0)
    {
        self.collectionViewInOutTime.hidden = YES;
        self.labelMessage.hidden = NO;
    }
    else
    {
        self.collectionViewInOutTime.hidden = NO;
        self.labelMessage.hidden = YES;
    }
    
    self.view.backgroundColor = kDefaultBarTintColor;
}

- (IBAction)showInOutTime:(id)sender
{
    NSLog(@"%@", self.inOutDetails);

    if (self.constraintHeightInOutTimeView.constant == 0)
    {
        self.constraintHeightInOutTimeView.constant = kInOutTimeCollectionViewMaxHeight;
    }
    else
    {
        self.constraintHeightInOutTimeView.constant = 0;
    }
    
    [self.view setNeedsUpdateConstraints];
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
    }];
    
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [IGAnalytics trackScreen:screenAddTask];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    self.navigationController.interactivePopGestureRecognizer.enabled = YES;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void) addTaskForProjects: (NSArray*)projects weekEndDate:(NSDate*)date withWeekId:(NSInteger)weekId timesheetStatus:(IGTimeSheetStatus)status parentView: (id)vc
{
    callingFromInProgress = false;
    isEditable = true;
    projectSelected = NO;
    _isAddTask = true;
    _taskVC = vc;
    [self getWeekDates:date];
    self.weekId = weekId;
    weekendDate = date;
    self.locationList = LocationTextList;
    _locationValues = LocationValueList;
    self.taskTypeForUSEmployees = kTaskTypeListForUS;
    self.taskCategoryValueForUSEmployees  = kTaskTypeListValueForUS;
    self.hrsTypeList = HrsTypeList;
    self.taskData = [[IGTaskModalClass alloc]init];
    self.taskData.billable = @"N";
    self.taskData.wfol = 0;
    self.taskData.location = @"0";
    self.taskData.isUSEmployeeBillable = 0;

    [self.taskData setDescription:@""];
    
    self.projectsList = projects;
    self.timesheetStatus = status;
    [self.tableView reloadData];
    
    if((self.taskData.timeSheetStatus == kIGTimeSheetStatusPending || self.taskData.timeSheetStatus == kIGTimeSheetStatusRejected || self.taskData.timeSheetStatus == kIGTimeSheetStatusCompleted) && [IGUserDefaults sharedInstance].ifUserUSEmployee)
    {
        self.taskData.categoryID = -1;

        if([self.taskData.billable isEqualToString:@"Y"])
        {
            self.taskData.isUSEmployeeBillable = 1;
        }
        
        [self categoryListForUSEmployee:[[IGUserDefaults sharedInstance].employee.staffID integerValue] projectId: self.taskData.prjId forWeek:self.weekId];
        
    }

}

- (void) showTask:(IGTaskModalClass*)taskData  weekEndDate:(NSDate*)date withWeekId:(NSInteger)weekId forProjects:(NSArray*)projectList
{
    callingFromInProgress = true;
    isEditable = false;
    projectSelected = YES;
    _isAddTask = false;
    
    [self getWeekDates:date];
    self.weekId = weekId;
    weekendDate = date;
    self.projectsList = projectList;
    self.locationList = LocationTextList;
     _locationValues = LocationValueList;
    self.hrsTypeList = HrsTypeList;
    self.taskTypeForUSEmployees = kTaskTypeListForUS;
    self.taskCategoryValueForUSEmployees  = kTaskTypeListValueForUS;
    self.timesheetStatus = taskData.timeSheetStatus;
    
    if(taskData.timeSheetStatus == kIGTimeSheetStatusCompleted)
    {
        self.taskData = [self createCopyOfTask:taskData];
    }
    else
    {
        self.taskData = taskData;
    }
    if((taskData.timeSheetStatus == kIGTimeSheetStatusPending || self.taskData.timeSheetStatus == kIGTimeSheetStatusRejected || taskData.timeSheetStatus == kIGTimeSheetStatusCompleted) && [IGUserDefaults sharedInstance].ifUserUSEmployee)
    {
        if([self.taskData.billable isEqualToString:@"Y"])
        {
            self.taskData.isUSEmployeeBillable = 1;
        }
      
        [self categoryListForUSEmployee:[[IGUserDefaults sharedInstance].employee.staffID integerValue] projectId: self.taskData.prjId forWeek:self.weekId];
        
    }
    
    kAlertAndReturnIfNetworkNotReachable;
    
    [self fetchTaskFromServerForEmployee:[[IGUserDefaults sharedInstance].employee.staffID integerValue] projectId:self.taskData.prjId forWeek:weekId];
    [self fetchSubtaskFromServerForEmployee:[[IGUserDefaults sharedInstance].employee.staffID integerValue] projectId:self.taskData.prjId taskId:self.taskData.taskId];
    
    
    

}

//get tasks based on the selected CRI
- (void)getTaskListBasedOnCRI:(NSString*)criID
{
    [self showSpinner:YES];
    [[IGService sharedInstance] CRIListForEmployee:criID completion:^(id response)
    {
         [self showSpinner:NO];
        if(response != nil)
        {
            NSDictionary *dictionary = (NSDictionary *)response;
            
            IGAddTaskParser* parser = [[IGAddTaskParser alloc] init];
            NSArray* tasks = [parser parseTasksList:dictionary];
         
             if(tasks != nil && [tasks count] > 0){
                 NSLog(@"2222222");
                 if([[tasks objectAtIndex:0]isKindOfClass:[IGTaskDataModel class]])
                 {
                     NSLog(@"4444444");
                     self.taskList = tasks;
                     if(self.taskData.timeSheetStatus == kIGTimeSheetStatusPending || self.taskData.timeSheetStatus == kIGTimeSheetStatusCompleted || self.taskData.timeSheetStatus == kIGTimeSheetStatusRejected )
                     {
                         if(self.cridList != nil && self.cridList.count > 1 && selectedCRIID)
                         {
                             //To Do
                              self.taskData.taskId = ((IGTaskDataModel*)self.taskList[1]).taskId.integerValue;
                        
                             
                              [self fetchSubtaskFromServerForEmployee:[[IGUserDefaults sharedInstance].employee.staffID integerValue] projectId: self.taskData.prjId taskId:self.taskData.taskId];
                         }
                     }
                     selectedCRIID = 0;
                    [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:TaskCell inSection:0]]withRowAnimation:UITableViewRowAnimationNone];
                    // [self.tableView reloadData];

                 }
            }
        }
        
     }error:^(NSError *error)
     {
         //[indicator stopAnimating];
         //[self.view setUserInteractionEnabled:YES];
         
         [self showSpinner:NO];
         
         [IGUtils showOKAlertWithTitle:kStringError message:[error localizedDescription]];
     }];
    
}



- (void)categoryListForUSEmployee:(NSInteger)empId projectId:(NSInteger)prjId forWeek:(NSInteger)weekId
{
    // [indicator startAnimating];
    // [self.view setUserInteractionEnabled:NO];
    
    [self showSpinner:YES];
    
//    NSString* billable = @"N";
//    
//    self.taskData.taskSubtaskValueForNonBillable = 0;
//
//    if([self.taskData.billable isEqualToString:@"Y"])
//    {
//        billable = @"Y";
//       
//    }
    
    if(self.taskData.cRID == nil)
    {
        self.taskData.cRID = @"0";
    }
    [[IGService sharedInstance] categoryListForUSEmployee:empId projectID:prjId isUSEmployee:NO CRID:self.taskData.cRID isBillableTaskType:self.taskData.billable forWeek:weekId completion:^(id response)
     {
         
         [self showSpinner:NO];
         
         NSDictionary *dictionary = (NSDictionary *)response;
         
         if([self.taskData.billable isEqualToString:@"N"])
         {
             self.taskData.isUSEmployeeBillable = 0;
         }
         
         
         NSLog(@"11111111111");
         
         IGAddTaskParser* parser = [[IGAddTaskParser alloc] init];
         NSArray* tasks = [parser parseTasksList:dictionary];
         
         if(tasks != nil && [tasks count] > 0){
             NSLog(@"2222222");
             if(![[tasks objectAtIndex:0]isKindOfClass:[IGTaskDataModel class]] && [[tasks objectAtIndex:0]isEqualToString:WarningKey])
             {
//                 projectSelected = NO;
                 self.taskData.projectText = @"";
                 
                 NSString *message = [tasks objectAtIndex:1];
                 
                 //TODO: replace UIAlertView with Custom Alert View after fixing custom Alert view for Dynamic height
                 //             [IGUtils showOKAlertWithTitle:kStringWarning message:message];
                 
                 [[[UIAlertView alloc] initWithTitle:kStringWarning message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                 NSLog(@"33333333");
                 
             }
             else
             {
                 NSLog(@"4444444");
                 self.taskList = tasks;
             }
         }
         NSLog(@"55555555");
         self.categoryList = [parser parseCategoryList:dictionary];
         NSLog(@"66666666");
         if(self.taskData.timeSheetStatus == kIGTimeSheetStatusPending || self.taskData.timeSheetStatus == kIGTimeSheetStatusCompleted|| self.taskData.timeSheetStatus == kIGTimeSheetStatusRejected)
         {
             if(self.taskList != nil && self.taskList.count > 1 && self.taskData.taskId  == 0)
             {
                 //To Do
                 self.taskData.taskId = ((IGTaskDataModel*)self.taskList[1]).taskId.integerValue;
                 
                 
                 [self fetchSubtaskFromServerForEmployee:[[IGUserDefaults sharedInstance].employee.staffID integerValue] projectId: self.taskData.prjId taskId:self.taskData.taskId];
             }
         }
        /* [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:TaskCell inSection:0]]withRowAnimation:UITableViewRowAnimationNone];
         // [self.tableView reloadData];
         [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:kTaskCategoryCellForUS inSection:0]]withRowAnimation:UITableViewRowAnimationNone];*/
        
          [self.tableView reloadData];
         NSLog(@"7777777");
         
     }error:^(NSError *error)
     {
         //[indicator stopAnimating];
         //[self.view setUserInteractionEnabled:YES];
         
         [self showSpinner:NO];
         
         [IGUtils showOKAlertWithTitle:kStringError message:[error localizedDescription]];
     }];
    
}


#pragma mark - Service methods
- (void)fetchTaskFromServerForEmployee:(NSInteger)empId projectId:(NSInteger)prjId forWeek:(NSInteger)weekId
{
   // [indicator startAnimating];
   // [self.view setUserInteractionEnabled:NO];
    
    [self showSpinner:YES];
    
    BOOL billable = NO;
    
    if([self.taskData.billable isEqualToString:@"Y"])
    {
        billable = YES;
    }
    if(self.taskData.cRID == nil)
    {
        self.taskData.cRID = @"0";
    }
    
    [[IGService sharedInstance] taskListForEmployee:empId projectID:prjId isUSEmployee:NO CRID:self.taskData.cRID isBillableTaskType:billable forWeek:weekId completion:^(id response)
     {
        // [indicator stopAnimating];
        // [self.view setUserInteractionEnabled:YES];
         
         [self showSpinner:NO];
        
         
         NSDictionary *dictionary = (NSDictionary *)response;
         
         NSLog(@"11111111111");
         
         IGAddTaskParser* parser = [[IGAddTaskParser alloc] init];
         NSArray* tasks = [parser parseTasksList:dictionary];
         
         if(tasks != nil && [tasks count] > 0){
             NSLog(@"2222222");
             if(![[tasks objectAtIndex:0]isKindOfClass:[IGTaskDataModel class]] && [[tasks objectAtIndex:0]isEqualToString:WarningKey])
             {
                 projectSelected = NO;
                 self.taskData.projectText = @"";
                 
                 NSString *message = [tasks objectAtIndex:1];
                 
                 //TODO: replace UIAlertView with Custom Alert View after fixing custom Alert view for Dynamic height
                 //             [IGUtils showOKAlertWithTitle:kStringWarning message:message];
                 
                 [[[UIAlertView alloc] initWithTitle:kStringWarning message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
                 NSLog(@"33333333");
                 
             }
             else
             {
                 NSLog(@"4444444");
                 self.taskList = tasks;
             }
         }
         NSLog(@"55555555");
          self.cridList = [parser parseCRIdList:dictionary];
        NSLog(@"66666666");
          [self.tableView reloadData];
         NSLog(@"7777777");
         
         
         if(self.taskData.cRID  != nil && ![self.taskData.cRID isEqualToString:@"0"])
         {
             [self getTaskListBasedOnCRI:self.taskData.cRID];
         }

         
     }error:^(NSError *error)
     {
         //[indicator stopAnimating];
         //[self.view setUserInteractionEnabled:YES];
         
         [self showSpinner:NO];
         
         [IGUtils showOKAlertWithTitle:kStringError message:[error localizedDescription]];
     }];

}

- (void)fetchSubtaskFromServerForEmployee:(NSInteger)empId projectId:(NSInteger)prjId taskId:(NSInteger)taskId
{
    
    [self showSpinner:YES];
    if(self.taskData.cRID == nil)
    {
        self.taskData.cRID = @"0";
    }
    [[IGService sharedInstance] subTaskListForEmployee:empId projectID:prjId taskID:taskId timeSheetStatus:self.timesheetStatus CRID:self.taskData.cRID isUSEmployee:NO completion:^(id response)
     {

         [self showSpinner:NO];
         NSDictionary *dictionary = (NSDictionary *)response;
         if(dictionary != nil && dictionary.count > 0)
         {
         
             IGAddTaskParser* parser = [[IGAddTaskParser alloc] init];
             self.subtaskList = [parser parseSubTasksList:dictionary];
             if(self.taskData.timeSheetStatus == kIGTimeSheetStatusPending || self.taskData.timeSheetStatus == kIGTimeSheetStatusCompleted || self.taskData.timeSheetStatus == kIGTimeSheetStatusRejected)
             {
                 if(self.subtaskList != nil && self.subtaskList.count > 1 && isSubtaskChangeRequired)
                 {
                     //To Do
                     self.taskData.subTaskId = ((IGTaskDataModel*)self.subtaskList[1]).taskId.integerValue;
                     isSubtaskChangeRequired = false;

                 }
             }
        
            [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:SubtaskCell inSection:0]]withRowAnimation:UITableViewRowAnimationNone];
          //   [self.tableView reloadData];
         }
     }
    error:^(NSError *error)
     {
         [self showSpinner:NO];
         [IGUtils showOKAlertWithTitle:kStringError message:[error localizedDescription]];
     }];
    
}

-(void)fetchValidDaysForSelectedProject:(NSInteger)empId projectId:(NSInteger)prjId weekendDate:(NSDate *)date
{
    [[IGService sharedInstance] validDaysOfPorject:empId projectID:prjId weekendDate:date completion:^(id response)
    {
        NSLog(@"Response : %@",response);
        NSDictionary *dictionary = (NSDictionary *)response;
        if(dictionary != nil  && [dictionary objectForKey:@"ValidateProject"] != nil){
            self.taskData.validDays = [[NSMutableArray alloc]init];
            NSString *responseStr = [dictionary objectForKey:@"ValidateProject"];
            if([responseStr containsString:@"mon"])
                [self.taskData.validDays addObject:[NSNumber numberWithInt:0]];
            if([responseStr containsString:@"tue"])
                [self.taskData.validDays addObject:[NSNumber numberWithInt:1]];
            if([responseStr containsString:@"wed"])
                [self.taskData.validDays addObject:[NSNumber numberWithInt:2]];
            if([responseStr containsString:@"thu"])
                [self.taskData.validDays addObject:[NSNumber numberWithInt:3]];
            if([responseStr containsString:@"fri"])
                [self.taskData.validDays addObject:[NSNumber numberWithInt:4]];
            if([responseStr containsString:@"sat"])
                [self.taskData.validDays addObject:[NSNumber numberWithInt:5]];
            if([responseStr containsString:@"sun"])
                [self.taskData.validDays addObject:[NSNumber numberWithInt:6]];
            
        }
        
    
    } error:^(NSError *error)
    {
        NSLog(@"Error  : %@",error);
    }];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == TaskDetailSection)
    {
        return 10;// add row- task type and task category for US employees
    }
    else if(section == TaskHrsSection)
    {
        return 8;
    }
    
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath

{
    
    // Table View Section 0
    if(indexPath.section == TaskDetailSection)
    {
        if(indexPath.row == LocationSwitchRow)
        {
            IGWFOLSwitchCell* cell = [tableView dequeueReusableCellWithIdentifier:kHeaderCellReuseIdentifierA];
            if (cell == nil)
            {
                
                cell = [[IGWFOLSwitchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kHeaderCellReuseIdentifierA];
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell.woflSwitch addTarget:self action:@selector(woflSwitchChanged:) forControlEvents:UIControlEventValueChanged];
            [cell.woflSwitch setOn:self.taskData.wfol];
            [self disableCellIfTimesheetCompleted:cell ofClass:[IGWFOLSwitchCell class]];
            [self setSeperatorInsetZero:cell];
            if([IGUserDefaults sharedInstance].ifUserUSEmployee)
            {
                cell.hidden = true;
            }
            return cell;
            
        }
        else if(indexPath.row == DescriptionRow)
        {
            IGTaskDescriptionCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifierB];
            if (cell == nil)
            {
                
                cell = [[IGTaskDescriptionCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kCellReuseIdentifierB];
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [cell.descriptionLabel setText:self.taskData.description];
            [self disableCellIfTimesheetCompleted:cell ofClass:[IGTaskDescriptionCell class]];
            return cell;
            
        }
        IGPickerCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifierA];
        if (cell == nil)
        {
            cell = [[IGPickerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellReuseIdentifierA];
            
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [self loadTableViewPickerCell:cell forRow:indexPath.row];
        [self disableCellIfTimesheetCompleted:cell ofClass:[IGPickerCell class]];
        if(indexPath.row == kTaskTypeCellForUS || indexPath.row == kTaskCategoryCellForUS)
        {
            if(![IGUserDefaults sharedInstance].ifUserUSEmployee)
            {
                cell.hidden = true;
            }
        }
        return cell;
    }
    // Table View Section 1
    if(indexPath.row == TotalHrsRow)
    {
        
        IGTotalHrsCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifierD];
        if (cell == nil) {
            cell = [[IGTotalHrsCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kCellReuseIdentifierD];
            
        }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        [self setSeperatorInsetZero:cell];
        cell.totalHrsLabel.text = [NSString stringWithFormat:@"%02.02f",self.taskData.totalTime];
        totalHrsLabel = cell.totalHrsLabel;
        return cell;
    }
    IGTaskHrsCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifierC];
    if (cell == nil) {
    
        cell = [[IGTaskHrsCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kCellReuseIdentifierC];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    cell.dateLabel.text = [self.weekDates objectAtIndex:indexPath.row];
    cell.hrsTextField.delegate = self;
    cell.hrsTextField.tag = indexPath.row;
    [self setHrsTextField:cell.hrsTextField forWeekday:indexPath.row];
    [self disableCellIfTimesheetCompleted:cell ofClass:[IGTaskHrsCell class]];
    if((indexPath.row > weekendDateIndex) || (indexPath.row < invalidDayCount))
    {
        [self disableTaskHrsCell:cell];
        
    }
    if(![self.taskData.validDays containsObject:[NSNumber numberWithInt:indexPath.row]]){
        
        [self disableTaskHrsCell:cell];
        
    }
    [hrsTfDict setObject:cell.hrsTextField forKey:[NSNumber numberWithInteger: indexPath.row]];
    return cell;
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == TaskDetailSection)
    {
        if(indexPath.row == LocationSwitchRow)
        {
            if([IGUserDefaults sharedInstance].ifUserUSEmployee)
            {
                return 0;
            }

        }
        else if(indexPath.row == kTaskTypeCellForUS || indexPath.row == kTaskCategoryCellForUS)
        {
            if(![IGUserDefaults sharedInstance].ifUserUSEmployee)
            {
                return 0;
            }
        }
        else if(indexPath.row == DescriptionRow)
        {
            
            NSString *cellText = self.taskData.description;
            CGSize constraintSize = CGSizeMake(280.0f, MAXFLOAT);
            CGSize labelSize = [cellText boundingRectWithSize:constraintSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:17.0]} context:nil].size;
            //[cellText sizeWithFont:[UIFont systemFontOfSize:17.0] constrainedToSize:constraintSize lineBreakMode:NSLineBreakByWordWrapping];
            return labelSize.height + 50;
        }
        else if (indexPath.row == 0)
        {
            return CustomRowHeight;
        }
        
    }
    
    if(indexPath.row == TotalHrsRow)
    {
        return 55;
    }
    return DefaultRowHeight;
    
}


- (CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == TaskHrsSection)
    {
        return kIGAddTaskSectionHeight;
    }
    return 0;
}

- (UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view;
    
    if(section == TaskDetailSection)
    {
        view = [[UIView alloc]initWithFrame: CGRectZero];
    }
    else
    {
        UITableViewCell* headerCell = [tableView dequeueReusableCellWithIdentifier:kHeaderCellReuseIdentifierB];
        [self setSeperatorInsetZero:headerCell];
        view = [headerCell contentView];
        [view setFrame:CGRectMake(0.0, 0.0, tableView.bounds.size.width, kIGAddTaskSectionHeight)];
    }
    
    return view;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    editingIndexPath = [indexPath copy];
    if(indexPath.section == TaskDetailSection && indexPath.row == DescriptionRow)
    {
        IGTextInputView *view = [IGTextInputView textInputView];
        view.labelTitle.text = @"Description";
        view.textView.text = self.taskData.description;
        view.labelPlaceholder.text = @"Enter Task Description";
        view.delegate = self;
        [view presentOnView:self.view];
    }
}

#pragma mark - Utility Methods

- (void) setSeperatorInsetZero: (UITableViewCell*)cell
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        cell.preservesSuperviewLayoutMargins = false;
    }
}

- (void) disableTaskHrsCell: (IGTaskHrsCell*)cell
{
    cell.dateLabel.enabled = false;
    cell.hrsTextField.enabled = false;
    cell.hrsTextField.textColor = [UIColor lightGrayColor];
}

- (void) disableCellIfTimesheetCompleted : (UITableViewCell*)cell ofClass: (Class)classType
{
    if([IGPickerCell class] == classType)
    {
        IGPickerCell* pCell = (IGPickerCell*)cell;
         if((!_isAddTask && !isEditable) || !projectSelected )
        {
        
            if(!projectSelected && [pCell.leftLabel.text isEqualToString:ProjectLabel])
            {
            }
            else
            {
                pCell.pickerTextField.enabled = false;
                pCell.leftLabel.enabled = false;
                if([self.taskData.billable isEqualToString:@"N"] && (pCell.tag == TaskCell  || pCell.tag == SubtaskCell) && [IGUserDefaults sharedInstance].ifUserUSEmployee)
                {
                    pCell.pickerTextField.text = kNotRequired;
                }
            }
            if(!_isAddTask && !isEditable)
            {
                pCell.disclosureWidthConstraint.constant = 10;
                [pCell.disclosureBtn needsUpdateConstraints];
                [pCell.disclosureBtn setHidden:TRUE];
                //[pCell.disclosureBtn removeFromSuperview];
            }
       
        }
        else if([self.taskData.billable isEqualToString:@"N"] && (pCell.tag == TaskCell  || pCell.tag == SubtaskCell) && [IGUserDefaults sharedInstance].ifUserUSEmployee)
        {
            pCell.pickerTextField.enabled = false;
            pCell.leftLabel.enabled = false;
            
            pCell.pickerTextField.text = kNotRequired;

        }
        else
        {
            pCell.pickerTextField.enabled = true;
            pCell.leftLabel.enabled = true;
            pCell.disclosureBtn.hidden = false;
            pCell.disclosureWidthConstraint.constant = 25;
            [pCell.disclosureBtn needsUpdateConstraints];
        }
        if(callingFromInProgress  && pCell.tag == ProjectsCell){
            pCell.pickerTextField.enabled = false;
            pCell.leftLabel.enabled = false;
            
        }
        
    }
    else if([IGTaskHrsCell class] == classType)
    {
        [self enableHoursCellForEditing:cell];

    }
    else if([IGTaskDescriptionCell class] == classType)
    {
        IGTaskDescriptionCell* dCell = (IGTaskDescriptionCell*)cell;
        if((!_isAddTask && !isEditable) || !projectSelected)
        {
            dCell.titleLabel.enabled = false;
            dCell.descriptionLabel.enabled = false;
            dCell.userInteractionEnabled = false;
        }
        else
        {
            dCell.titleLabel.enabled = true;
            dCell.descriptionLabel.enabled = true;
            dCell.userInteractionEnabled = true;
        }
    }
    else if([IGWFOLSwitchCell class] == classType)
    {
        IGWFOLSwitchCell* wCell = (IGWFOLSwitchCell*)cell;
        if((!_isAddTask && !isEditable))
        {
            wCell.woflSwitch.enabled = false;
        }
        else
        {
            wCell.woflSwitch.enabled = true;
        }
    }
}

-(void)enableHoursCellForEditing: (UITableViewCell*)cell
{
    IGTaskHrsCell* tCell = (IGTaskHrsCell*)cell;
    if([self.taskData.cRID isEqualToString:@"0"])
    {
        NSLog(@"TRUE");
    }
    if([IGUserDefaults sharedInstance].ifUserUSEmployee)
    {
        if([self.taskData.billable isEqualToString:@"N"])
        {
            if(self.taskData.prjId != 0 && ![self.taskData.cRID isEqualToString:@"0"]  && self.taskData.cRID != nil && ![self.taskData.hrsType isEqualToString:@""] && ![self.taskData.location isEqualToString:@"0"] && self.taskData.location != nil && self.taskData.categoryID != 0 && ![self.taskData.billable isEqualToString:@""] && (_isAddTask || isEditable))
            {
                [self enableCell:tCell isEnable:true Color:@"Black"];
            }
            else
            {
                [self enableCell:tCell isEnable:false Color:@"Gray"];
            }

        }
        else
        {
            if(self.taskData.prjId != 0 && ![self.taskData.cRID isEqualToString:@"0"]  && self.taskData.cRID != nil && self.taskData.hrsType != 0 && self.taskData.categoryID != 0 && ![self.taskData.location isEqualToString:@"0"] && self.taskData.location != nil && ![self.taskData.billable isEqualToString:@""] && self.taskData.taskId != 0 && self.taskData.subTaskId != 0 && (_isAddTask || isEditable))
            {
                [self enableCell:tCell isEnable:true Color:@"Black"];
            }
            else
            {
                [self enableCell:tCell isEnable:false Color:@"Gray"];
            }

        }
    }
    else
    {
        if(self.taskData.prjId != 0 && ![self.taskData.cRID isEqualToString:@"0"]  && self.taskData.cRID != nil && ![self.taskData.billable isEqualToString:@""] && self.taskData.taskId != 0 && ![self.taskData.location isEqualToString:@"0"] && self.taskData.location != nil && self.taskData.subTaskId != 0 && (_isAddTask || isEditable))
        {
           [self enableCell:tCell isEnable:true Color:@"Black"];
        
        }
        else
        {
            [self enableCell:tCell isEnable:false Color:@"Gray"];
        }
    }
}


-(void)enableCell:(IGTaskHrsCell*)cell isEnable:(BOOL)enable Color:(NSString*)color
{
    cell.dateLabel.enabled = enable;
    cell.hrsTextField.enabled = enable;
    if([color isEqualToString:@"Black"])
        cell.hrsTextField.textColor = [UIColor blackColor];
    else
        cell.hrsTextField.textColor = [UIColor lightGrayColor];
}

- (void) getWeekDates :(NSDate*)weekendDate
{
    NSMutableArray* dates = [[NSMutableArray alloc]init];
    NSDate *startDate = [self getPreviousMonday:weekendDate];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:@"dd MMM, eee"];
    
    for(int i = 0; i < 7; i++)
    {
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = i;
        
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:startDate options:0];
        
        if([nextDate isEqualToDate:weekendDate])
        {
            weekendDateIndex = i;
        }
        
        NSString *dateString = [dateFormatter stringFromDate:nextDate];
        [dates addObject:dateString];
    }
    
    invalidDayCount = [self getInvalidDayCount:weekendDate];
    
    self.weekDates = dates;
}

- (NSDate*) getPreviousMonday:(NSDate*)weekendDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:@"eee"];
    NSString* dayStr = [dateFormatter stringFromDate:weekendDate];
    
    int dayNum = 0;
    
    if([dayStr isEqualToString:@"Tue"])
    {
        dayNum = -1;
    }
    else if([dayStr isEqualToString:@"Wed"])
    {
        dayNum = -2;
    }
    else if([dayStr isEqualToString:@"Thu"])
    {
        dayNum = -3;
    }
    else if([dayStr isEqualToString:@"Fri"])
    {
        dayNum = -4;
    }
    else if([dayStr isEqualToString:@"Sat"])
    {
        dayNum = -5;
    }
    else if([dayStr isEqualToString:@"Sun"])
    {
        dayNum = -6;
    }
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = dayNum;
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *startDate = [theCalendar dateByAddingComponents:dayComponent toDate:weekendDate options:0];
    
    return startDate;
}

- (int) getInvalidDayCount: (NSDate*)weekendDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:@"dd"];
    NSString* dateStr = [dateFormatter stringFromDate:weekendDate];
    
    int dayCount = 0;
    
    if([dateStr intValue] < 7)
    {
        dayCount = 7 - [dateStr intValue];
    }
    
    return dayCount;
}

- (IGTaskModalClass*) createCopyOfTask: (IGTaskModalClass*)task
{
    IGTaskModalClass* newTask = [[IGTaskModalClass alloc]init];
    newTask.dateText = task.dateText;
    newTask.projectText = task.projectText;
    newTask.description = task.description;
    newTask.location = task.location;
    newTask.timeSheetStatus = task.timeSheetStatus;
    
    newTask.prjId = task.prjId;
    newTask.taskId = task.taskId;
    newTask.categoryID = task.categoryID;
    newTask.subTaskId = task.subTaskId;
    newTask.cRID = task.cRID;
    newTask.wfol = task.wfol;
    
    newTask.totalTime = task.totalTime;
    newTask.monTime = task.monTime;
    newTask.tueTime = task.tueTime;
    newTask.wedTime = task.wedTime;
    newTask.thuTime = task.thuTime;
    newTask.friTime = task.friTime;
    newTask.satTime = task.satTime;
    newTask.sunTime = task.sunTime;
    newTask.hrsType = task.hrsType;
    newTask.billable = task.billable;
    
    newTask.cardEntryDetailId = task.cardEntryDetailId;
    newTask.cRID = task.cRID;
    newTask.payrollTaskId = task.payrollTaskId;
    
    return newTask;
}

- (void) loadTableViewPickerCell:(IGPickerCell*)cell forRow:(NSInteger)row
{
    NSMutableArray *pickerContent = [[NSMutableArray alloc]init];
    NSInteger selectRow = -1;
    switch (row)
    {
        case ProjectsCell:
            
            for(int i = 0; i < self.projectsList.count; i++)
            {
                [pickerContent addObject: self.projectsList[i][PrjNameKey] ];
            }
            [cell setDelegate:self];
            [cell setTag:ProjectsCell];
            [cell setSelectedTextColor:kColorLoginVCFontColor];
            [cell setPickerTitle:ProjectLabel];
            [cell initWithText:ProjectLabel pickerContents:pickerContent];
            
            [cell.pickerTextField setText:self.taskData.projectText];
            
            break;
            
        case LocationCell:
            
            pickerContent = [NSMutableArray arrayWithArray:self.locationList];
            [cell setDelegate:self];
            [cell setTag:LocationCell];
            [cell setPickerTitle:LocationLabel];
            [cell initWithText:LocationLabel pickerContents:pickerContent];
            
            for(int i = 0; i < 10; i++)
            {
                if([self.taskData.location isEqualToString:[_locationValues objectAtIndex:i]])
                {
                    [cell setSelectedIndex:i];
                    [cell.pickerTextField setText:[self.locationList objectAtIndex:i]];
                }
            }
           
            break;
            
        case kTaskTypeCellForUS:
            
            pickerContent = [NSMutableArray arrayWithArray:self.taskTypeForUSEmployees];
            [cell setDelegate:self];
            [cell setTag:kTaskTypeCellForUS];
            [cell setPickerTitle:kTaskTypeLabelForUS];
            [cell initWithText:kTaskTypeLabelForUS pickerContents:pickerContent];
            
            for(int i = 0; i < 2; i++)
            {
                if([self.taskData.billable isEqualToString:[self.taskCategoryValueForUSEmployees objectAtIndex:i]])
                {
                    [cell setSelectedIndex:i];
                    [cell.pickerTextField setText:[self.taskTypeForUSEmployees objectAtIndex:i]];
                }
            }
            
            break;
            
        case kTaskCategoryCellForUS:
            
        //IGCategoryTaskDataModel* model =
            
            for(int i = 0; i < self.categoryList.count; i++)
            {
                [pickerContent addObject: ((IGCategoryTaskDataModel*)self.categoryList[i]).categoryName];
                if(((IGCategoryTaskDataModel*)self.categoryList[i]).categoryID.integerValue == self.taskData.categoryID)
                {
                    selectRow = i;
                }
            }
            [cell setDelegate:self];
            [cell setTag:kTaskCategoryCellForUS];
            [cell setPickerTitle:kTaskCategoryLabelForUS];
            [cell initWithText:kTaskCategoryLabelForUS pickerContents:pickerContent];
            
            if(selectRow != -1)
                [cell.pickerTextField setText:[pickerContent objectAtIndex:selectRow]];
            else if(_categoryList != nil && _categoryList.count > 1 && self.taskData.categoryID != 0)
                {
                    cell.pickerTextField.text = ((IGCategoryTaskDataModel*)self.categoryList[1]).categoryName;
                    self.taskData.categoryID = ((IGCategoryTaskDataModel*)self.categoryList[1]).categoryID.integerValue;
                }
            
            break;

   
        case HrsTypeCell:
            
            pickerContent = [NSMutableArray arrayWithArray:self.hrsTypeList];
            [cell setDelegate:self];
            [cell setTag:HrsTypeCell];
            [cell setPickerTitle:HrsTypeLabel];
            [cell initWithText:HrsTypeLabel pickerContents:pickerContent];
            
            if([self.taskData.hrsType isEqualToString:HrsTypeOnsite])
            {
                [cell.pickerTextField setText:[self.hrsTypeList objectAtIndex:1]];
                [cell setSelectedIndex:1];
            }
            else
            {
                [cell.pickerTextField setText:[self.hrsTypeList objectAtIndex:2]];
                [cell setSelectedIndex:2];
                [self.taskData setHrsType:HrsTypeOffshore];
            }
           
            break;
            
        case TaskCell:
            for(int i = 0; i < self.taskList.count; i++)
            {
                [pickerContent addObject: ((IGTaskDataModel*)self.taskList[i]).taskName];
                if(((IGTaskDataModel*)self.taskList[i]).taskId.intValue == self.taskData.taskId)
                {
                    selectRow = i;
                }
            }
            [cell setDelegate:self];
            [cell setTag:TaskCell];
            [cell setPickerTitle:TaskLabel];
            [cell initWithText:TaskLabel pickerContents:pickerContent];
            
            if(selectRow != -1)
                [cell.pickerTextField setText:[pickerContent objectAtIndex:selectRow]];
            else if(self.taskList != nil && self.taskList.count > 1 && self.taskData.taskId != 0)
            {
                cell.pickerTextField.text = ((IGTaskDataModel*)self.taskList[1]).taskName;
                self.taskData.taskId = ((IGTaskDataModel*)self.taskList[1]).taskId.integerValue;
            }

            
         
            break;
            
        case SubtaskCell:
            for(int i = 0; i < self.subtaskList.count; i++)
            {
                [pickerContent addObject: ((IGTaskDataModel*)self.subtaskList[i]).taskName];
                if(((IGTaskDataModel*)self.subtaskList[i]).taskId.integerValue == self.taskData.subTaskId)
                {
                    selectRow = i;
                }
            }
            [cell setDelegate:self];
            [cell setTag:SubtaskCell];
            [cell setPickerTitle:SubtaskLabel];
            [cell initWithText:SubtaskLabel pickerContents:pickerContent];
            
            if(selectRow != -1)
                 [cell.pickerTextField setText:[pickerContent objectAtIndex:selectRow]];
            else if(self.subtaskList != nil && self.subtaskList.count > 1 && self.taskData.subTaskId != 0)
            {
                cell.pickerTextField.text = ((IGTaskDataModel*)self.subtaskList[1]).taskName;
                self.taskData.subTaskId = ((IGTaskDataModel*)self.subtaskList[1]).taskId.integerValue;
            }

            
            break;
            
        case CRIDCell:
            for(int i = 0; i < self.cridList.count; i++)
            {
                [pickerContent addObject: ((IGCRIdModel*)self.cridList[i]).CRName];
                NSString* cridValue = [NSString stringWithFormat:@"%@",((IGCRIdModel*)self.cridList[i]).CRId];
                if([cridValue isEqualToString:self.taskData.cRID])
                {
                    selectRow = i;
                }
            }
            [cell setDelegate:self];
            [cell setTag:CRIDCell];
            [cell setPickerTitle:CRIdLabel];
            [cell initWithText:CRIdLabel pickerContents:pickerContent];
            
            if(selectRow != -1)
                [cell.pickerTextField setText:[pickerContent objectAtIndex:selectRow]];
            else if(self.cridList != nil && self.cridList.count > 1 && self.taskData.cRID != nil)
            {
                cell.pickerTextField.text = ((IGCRIdModel*)self.cridList[1]).CRName;
                self.taskData.cRID = ((IGCRIdModel*)self.cridList[1]).CRId;
            }

            
            break;
        default:
            break;
    }
}

- (void) initHrsWithZero
{
    self.taskData.monTime = 0.0;
    self.taskData.tueTime = 0.0;
    self.taskData.wedTime = 0.0;
    self.taskData.thuTime = 0.0;
    self.taskData.friTime = 0.0;
    self.taskData.satTime = 0.0;
    self.taskData.sunTime = 0.0;
    self.taskData.totalTime = 0.0;
}

- (void)setHrsTextField: (UITextField*)textF forWeekday: (NSInteger)day
{
    switch(day)
    {
        case Weekday1:
            [textF setText:[NSString stringWithFormat:@"%02.02f",self.taskData.monTime]];
            break;
            
        case Weekday2:
            [textF setText:[NSString stringWithFormat:@"%02.02f",self.taskData.tueTime]];
            break;
            
        case Weekday3:
           [textF setText:[NSString stringWithFormat:@"%02.02f",self.taskData.wedTime]];
            break;
            
        case Weekday4:
            [textF setText:[NSString stringWithFormat:@"%02.02f",self.taskData.thuTime]];
            break;
            
        case Weekday5:
            [textF setText:[NSString stringWithFormat:@"%02.02f",self.taskData.friTime]];
            break;
            
        case Weekday6:
           [textF setText:[NSString stringWithFormat:@"%02.02f",self.taskData.satTime]];
            break;
            
        case Weekday7:
            [textF setText:[NSString stringWithFormat:@"%02.02f",self.taskData.sunTime]];
            break;
            
        default:
            
            break;
    }
}

#pragma mark - IGPickerCellDelegate

- (void) didSelectPickerObject:(IGPickerCell *)cell atIndex:(NSInteger)index value:(NSString *)object
{
    NSDictionary* project;
    switch (cell.tag) {
            
        case ProjectsCell:
            
            if(index)
            {
                projectSelected = YES;
                isSubtaskChangeRequired = true;
                project = [self.projectsList objectAtIndex:index];
//TODO: replaced [kAppDelegate employeeId] with [[IGUserDefaults sharedInstance].employee.staffID integerValue]
                [self fetchTaskFromServerForEmployee:[[IGUserDefaults sharedInstance].employee.staffID integerValue] projectId: ((NSNumber*)project[PrjIdKey]).integerValue forWeek:self.weekId];
                [self.taskData setPrjId: ((NSNumber*)project[PrjIdKey]).integerValue];
                [self.taskData setProjectText: project[PrjNameKey] ];
                
                [self fetchValidDaysForSelectedProject:[[IGUserDefaults sharedInstance].employee.staffID integerValue] projectId:((NSNumber*)project[PrjIdKey]).integerValue weekendDate:weekendDate];
            }
    
            break;
            
        case TaskCell:
            
            if(index)
            {
//TODO: replaced [kAppDelegate employeeId] with [[IGUserDefaults sharedInstance].employee.staffID integerValue]
                isSubtaskChangeRequired = true;
                [self fetchSubtaskFromServerForEmployee:[[IGUserDefaults sharedInstance].employee.staffID integerValue] projectId: self.taskData.prjId taskId:((IGTaskDataModel*)[self.taskList objectAtIndex:index]).taskId.integerValue];
            
                [self.taskData setTaskId: ((IGTaskDataModel*)[self.taskList objectAtIndex:index]).taskId.integerValue];
            }
            
            break;
            
        case SubtaskCell:
            
            if(index)
            {
                [self.taskData setSubTaskId:((IGTaskDataModel*)[self.subtaskList objectAtIndex:index]).taskId.integerValue];
            }
            break;
            
        case LocationCell:
            
            if(![object isEqualToString:@"Select"])
            self.taskData.location = [_locationValues objectAtIndex:index];
            
            break;
        
        case kTaskTypeCellForUS:
            
            isSubtaskChangeRequired = true;

               self.taskData.billable = [self.taskCategoryValueForUSEmployees objectAtIndex:index];
            [self categoryListForUSEmployee:[[IGUserDefaults sharedInstance].employee.staffID integerValue] projectId: self.taskData.prjId forWeek:self.weekId];
        
            
            break;
         
        case kTaskCategoryCellForUS:
            
            if(index)
            {
                [self.taskData setCategoryID:((IGCategoryTaskDataModel*)[self.categoryList objectAtIndex:index]).categoryID.integerValue];

                self.taskData.categoryType = [self.categoryList objectAtIndex:index];
            }
            
            break;
            
        #define kTaskTypeCellForUS  4
            
        case HrsTypeCell:
            
            if(index == 1)
            {
                [self.taskData setHrsType:HrsTypeOnsite];
            }
            else if(index == 2)
            {
                [self.taskData setHrsType:HrsTypeOffshore];
            }
            break;
            
        case CRIDCell:
        {
            if(index)
            {
                isSubtaskChangeRequired = true;
                selectedCRIID = 1;
                [self.taskData setCRID:[NSString stringWithFormat:@"%@",((IGCRIdModel*)[self.cridList objectAtIndex:index]).CRId]];
            
                if(self.taskData.cRID != nil && ![self.taskData.cRID isEqualToString:@"0"])
                [self getTaskListBasedOnCRI:self.taskData.cRID];
            }
        }
        break;
            
        default:
            break;
    }
    
    [self.tableView reloadData];
}

- (void) didOpenPickerView:(UIPickerView *)pickerView
{
    halfTransparentView = [[UIView alloc] initWithFrame:self.view.bounds];
    halfTransparentView.backgroundColor = [UIColor blackColor]; //or whatever...
    halfTransparentView.alpha = 0.5;
    [self.view addSubview: halfTransparentView];
}

- (void) didClosePickerView:(UIPickerView *)pickerView
{
    if(halfTransparentView)
    {
        [halfTransparentView removeFromSuperview];
    }
}

#pragma mark - UITextFieldDelegate

// Task Hrs TextField

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if([[textField text]floatValue] > 24.0)
    {
        [IGUtils showOKAlertWithTitle:kStringAlert message:@"Hours cannot be more than 24"];
        [textField setText:@"0.00"];
    }
    else
    {
        switch(textField.tag)
        {
            case Weekday1:
                self.taskData.monTime = [[textField text]floatValue];
                [self setHrsTextField:textField forWeekday:Weekday1];
            
                break;
            
            case Weekday2:
                self.taskData.tueTime = [[textField text]floatValue];
                [self setHrsTextField:textField forWeekday:Weekday2];
            
                break;
            
            case Weekday3:
                self.taskData.wedTime = [[textField text]floatValue];
                [self setHrsTextField:textField forWeekday:Weekday3];
            
                break;
            
            case Weekday4:
                self.taskData.thuTime = [[textField text]floatValue];
                [self setHrsTextField:textField forWeekday:Weekday4];
            
                break;
            
            case Weekday5:
                self.taskData.friTime = [[textField text]floatValue];
                [self setHrsTextField:textField forWeekday:Weekday5];
            
                break;
            
            case Weekday6:
                self.taskData.satTime = [[textField text]floatValue];
                [self setHrsTextField:textField forWeekday:Weekday6];
            
                break;
            
            case Weekday7:
                self.taskData.sunTime = [[textField text]floatValue];
                [self setHrsTextField:textField forWeekday:Weekday7];
            
                break;
            
                default:
            
                break;
        }
        
    
        self.taskData.totalTime = self.taskData.monTime + self.taskData.tueTime + self.taskData.wedTime + self.taskData.thuTime + self.taskData.friTime + self.taskData.satTime + self.taskData.sunTime;
        
        totalHrsLabel.text = [NSString stringWithFormat:@"%02.02f",self.taskData.totalTime];
    
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    editingIndexPath = [NSIndexPath indexPathForRow:textField.tag inSection:TaskHrsSection];
    if(textField.tag == weekendDateIndex)
    {
        [[[keyboardToolbar items]objectAtIndex:2]setTitle:@"Done"];
    }
    else
    {
        [[[keyboardToolbar items]objectAtIndex:2]setTitle:@"Next"];
    }
    textField.inputAccessoryView = keyboardToolbar;

    if(textField.text.floatValue == 0.0)
    {
        [textField setText:@""];
    }
    [self.tableView scrollToRowAtIndexPath:editingIndexPath atScrollPosition:UITableViewScrollPositionTop animated:YES];
   
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField.text.length > 5 && range.length == 0)
        return NO;
    
    return YES;
}

#pragma mark - Actions

- (IBAction)addToTask:(id)sender
{
    [self.view endEditing:YES];
    
    BOOL isWeekendDate = false;
    if([self.taskData validDays].count  == 2)
    {
        NSLog(@"%@",[[self.taskData validDays] objectAtIndex:0]);
        if([[[self.taskData validDays] objectAtIndex:0] intValue] == 5 || [[[self.taskData validDays] objectAtIndex:1] intValue] == 6)
        {
            isWeekendDate = true;
        }
    }
    
    if(self.taskData.totalTime == 0 && ![IGUserDefaults sharedInstance].employee.isContractor && !isWeekendDate)
    {
        [IGUtils showOKAlertWithTitle:kStringAlert message:@"Total hours cannot be 0"];
    }
    else if([self.taskData.location isEqualToString:@"Select"]  || [self.taskData.location isEqualToString:@"0"])
    {
       [IGUtils showOKAlertWithTitle:kStringAlert message:@"Please select your location"];
    }
    else if(![self.taskData.projectText isEqualToString:@""] && self.taskData.prjId != 0 && ![self.taskData.cRID isEqualToString:@"0"]  && self.taskData.cRID != nil && [self.taskData.billable isEqualToString:@"N"] && (self.taskData.taskId == 0) && ![self.taskData.location isEqualToString:@"0"] && self.taskData.location != nil && self.taskData.subTaskId == 0 && [IGUserDefaults sharedInstance].ifUserUSEmployee)
    {
        [_taskVC addNewTask:self.taskData];
        [self.navigationController popToViewController:_taskVC animated:YES];
    
    }
    else if(![self.taskData.projectText isEqualToString:@""] && self.taskData.prjId != 0 && ![self.taskData.cRID isEqualToString:@"0"]  && self.taskData.cRID != nil && ![self.taskData.billable isEqualToString:@""] && (self.taskData.taskId != 0) && ![self.taskData.location isEqualToString:@"0"] && self.taskData.location != nil && self.taskData.subTaskId != 0)
    {
        [_taskVC addNewTask:self.taskData];
        [self.navigationController popToViewController:_taskVC animated:YES];
        
    }

    else
    {
        [IGUtils showOKAlertWithTitle:kStringAlert message:@"Please fill all Details"];
    }
    /*else if([IGUserDefaults sharedInstance].employee.isContractor)
    {
        if(self.taskData.prjId != 0 && ![self.taskData.cRID isEqualToString:@"0"]  && self.taskData.cRID != nil && ![self.taskData.billable isEqualToString:@""] && self.taskData.taskId != 0 && ![self.taskData.location isEqualToString:@"0"] && self.taskData.location != nil && self.taskData.subTaskId != 0)
           {
               [_taskVC addNewTask:self.taskData];
               [self.navigationController popToViewController:_taskVC animated:YES];
           }
           else
           {
               [IGUtils showOKAlertWithTitle:kStringAlert message:@"Please fill all Details"];

           }
    }
    else
    {
        [_taskVC addNewTask:self.taskData];
        [self.navigationController popToViewController:_taskVC animated:YES];
    }*/
}

- (IBAction)resetTask:(id)sender
{
    [self initHrsWithZero];
    self.taskData = [[IGTaskModalClass alloc]init];
    self.taskList = nil;
    self.subtaskList = nil;
    projectSelected = NO;
    self.taskData.billable = @"N";
    self.taskData.wfol = 0;
    self.taskData.location = @"0";
    self.taskData.isUSEmployeeBillable = 0;
    
    [self.taskData setDescription:@""];
    
    [self.tableView reloadData];
}

- (void) editTask: (id)sender
{
    if(self.timesheetStatus == kIGTimeSheetStatusPending || self.timesheetStatus == kIGTimeSheetStatusRejected)
    {
        [self fetchValidDaysForSelectedProject:[[IGUserDefaults sharedInstance].employee.staffID integerValue] projectId:self.taskData.prjId weekendDate:weekendDate];
    }
    UIBarButtonItem *editBtn = (UIBarButtonItem*)sender;
    [editBtn setEnabled: NO];
    isEditable = true;
    [self.tableView reloadData];
}

- (void) woflSwitchChanged: (id)sender
{
    if([sender isOn])
    {
        self.taskData.wfol = 1;
    }
    else
    {
        self.taskData.wfol = 0;
    }
}

#pragma mark - Keyboard Notification

- (void)keyboardWillShow:(NSNotification *)notification
{
    if(editingIndexPath.section == TaskDetailSection)
    {
        return;
    }
    
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;

    UIEdgeInsets contentInsets = self.tableView.contentInset;
    contentInsets.bottom = keyboardSize.height - self.buttonsView.frame.size.height;
    contentInsets.top = -20;
    self.tableView.contentInset = contentInsets;
    self.tableView.scrollIndicatorInsets = contentInsets;
    
    [self showNavigationBar:NO];
}

- (void)keyboardWillHide:(NSNotification *)notification
{
    if(editingIndexPath.section == TaskDetailSection)
    {
        return;
    }

    UIEdgeInsets contentInset = self.tableView.contentInset;
    contentInset.bottom = 0;
    contentInset.top = 20;
    self.tableView.contentInset = contentInset;
    self.tableView.scrollIndicatorInsets = contentInset;

    [self showNavigationBar:YES];
}

- (void)showNavigationBar:(BOOL)show
{
    [self.navigationController setNavigationBarHidden:!show animated:YES];
//
//    UIView *statusBarBackgroundView = [self.view viewWithTag:5000];
//    
//    if (statusBarBackgroundView == nil)
//    {
//        statusBarBackgroundView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(self.view.frame), 20)];
//        statusBarBackgroundView.backgroundColor = kDefaultBarTintColor;
//        statusBarBackgroundView.hidden = YES;
//        statusBarBackgroundView.tag = 5000;
//        [self.view addSubview:statusBarBackgroundView];
//    }
//    
//    [self.view bringSubviewToFront:statusBarBackgroundView];
//    statusBarBackgroundView.hidden = show;
}


- (void)cancelNumberPad
{
    UITextField* currentTf = [hrsTfDict objectForKey:[NSNumber numberWithInteger:editingIndexPath.row]];
    [currentTf resignFirstResponder];
}

- (void)nextWithNumberPad
{
    UITextField* currentTf = [hrsTfDict objectForKey:[NSNumber numberWithInteger:editingIndexPath.row]];
    NSInteger nextTag = editingIndexPath.row + 1;
    // Try to find next responder
    UIResponder* nextResponder = [hrsTfDict objectForKey:[NSNumber numberWithInteger:nextTag]];
    if (nextResponder && nextResponder.canBecomeFirstResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [currentTf resignFirstResponder];
    }
}


#pragma mark - IGTextInputViewDelegate

- (void)textInputView:(IGTextInputView *)view didSelectButtonAtIndex:(NSInteger)index
{
    if (index == 1)
    {
        // Get the input text
        self.taskData.description = view.textView.text;
        [self.tableView beginUpdates];
        [self.tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:DescriptionRow inSection:TaskDetailSection], nil] withRowAnimation:UITableViewRowAnimationNone];
        [self.tableView endUpdates];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.inOutDetails.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    IGInOutTimeCell *cell = (IGInOutTimeCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"IGInOutTimeCell" forIndexPath:indexPath];
    
    NSAssert([cell isKindOfClass:[IGInOutTimeCell class]], @"Expected a cell of kind IGInOutTimeCell");
    
    IGInOutModalClass *inoutModal = self.inOutDetails[indexPath.row];
    cell.labelDay.text = inoutModal.day;
    cell.labelTime.text = inoutModal.totalWorkHours;    
    return cell;
}

@end
