//
//  IGDocumentViewer.h
//  IGTimeSheet
//
//  Created by Manish on 10/03/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGViewController.h"

@interface IGDocumentViewer : IGViewController

@property (nonatomic, strong) NSURL *fileURL;

@end
