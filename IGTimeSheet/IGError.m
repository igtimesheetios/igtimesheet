//
//  IGError.m
//  IGTimeSheet
//
//  Created by Manish on 11/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGError.h"

@implementation IGError

+ (NSError *)errorForService:(NSString *)service
                 description:(NSString *)description
                   errorCode:(IGErrorCode)errorCode
{
    NSString *localizedDescription = description;
    if (localizedDescription == nil)
    {
        localizedDescription = [self descriptionForError:errorCode];
    }
    
    NSDictionary *userInfo = @{
                               NSLocalizedDescriptionKey : [NSString stringWithFormat:@"%@: %@", service, localizedDescription]
                               };
    NSError *error = [[NSError alloc] initWithDomain:kIGErrorDomain
                                                code:errorCode
                                            userInfo:userInfo];
    return error;
}


#pragma mark -

+ (NSString *)descriptionForError:(IGErrorCode)errorCode
{
    switch (errorCode)
    {
        case kIGErrorCodeOffline:
        {
            return @"Connectivity Issue";
        }
            
        case kIGErrorCodeNullParameter:
        {
            return @"Nil Parameter encountered";
        }
            
        case kIGErrorCodeIncorrectParameterType:
        {
            return @"Incorrect Parameter Type";
        }
            
        default:
        {
            return @"---";
        }
    }

    return @"---";
}

@end
