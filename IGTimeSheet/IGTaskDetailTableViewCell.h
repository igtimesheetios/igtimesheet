//
//  TaskDetailTableViewCell.h
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGTaskDetailTableViewCell : UITableViewCell

@property(strong) IBOutlet UILabel* statusLabel;
@property(strong) IBOutlet UILabel* projectLabel;
@property(strong) IBOutlet UILabel* detailLabel;
@property(strong) IBOutlet UILabel* timeLabel;

@end
