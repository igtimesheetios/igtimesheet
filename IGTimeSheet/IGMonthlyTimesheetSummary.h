//
//  IGMonthlyTimesheetSummary.h
//  IGTimeSheet
//
//  Created by Manish on 23/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGMonthlyEffort : NSObject

@property (nonatomic, readonly) NSString *date;
@property (nonatomic, readonly) NSString *weekOrder;
@property (nonatomic, readonly) NSNumber *effort;
@property (nonatomic, readonly) NSNumber *weekID;

- (instancetype)initWithEffort:(NSDictionary *)effortInfo;

@end


@interface IGMonthlyTimesheetSummary : NSObject

@property (nonatomic, strong) NSString *monthName;

// An array containing object of type IGMonthlyEffort
@property (nonatomic, readonly) NSArray *timesheetSummary;

- (instancetype)initWithName:(NSString *)monthName timeSheetSummary:(NSArray *)timesheetSummary;

@end
