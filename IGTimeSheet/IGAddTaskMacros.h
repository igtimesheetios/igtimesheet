//
//  IGAddTaskMacros.h
//  IGTimeSheet
//
//  Created by Neha on 23/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#ifndef IGTimeSheet_IGAddTaskMacros_h
#define IGTimeSheet_IGAddTaskMacros_h

#define DefaultRowHeight    48.0
#define CustomRowHeight     80.0

#define kIGAddTaskSectionHeight  50.0

#define TaskDetailSection   0
#define TaskHrsSection      1

#define ProjectsCell        1
#define LocationCell        2
#define HrsTypeCell         6
#define TaskCell            7
#define SubtaskCell         8
#define CRIDCell            3
#define kTaskTypeCellForUS  4
#define kTaskCategoryCellForUS  5



#define ProjectLabel        @"Project"
#define LocationLabel       @"Location"
#define HrsTypeLabel        @"Hrs Type"
#define TaskLabel           @"Task"
#define SubtaskLabel        @"Subtask"
#define CRIdLabel           @"CRId"
#define kTaskTypeLabelForUS  @"Task Type"
#define kTaskCategoryLabelForUS  @"Task Category"

#define PrjIdKey            @"TrackID"
#define PrjNameKey          @"TrackName"

#define Weekday1            0
#define Weekday2            1
#define Weekday3            2
#define Weekday4            3
#define Weekday5            4
#define Weekday6            5
#define Weekday7            6

#define LocationSwitchRow   0
#define DescriptionRow      9
#define TotalHrsRow         7

#define HrsTypeOffshore     @"F"
#define HrsTypeOnsite       @"O"

#define TextNoida           @"Noida"
#define TextPune            @"Pune"
#define TextOffshore        @"Offshore"
#define TextOnsite          @"Onsite"

#define LocationTextList    [NSArray arrayWithObjects: @"Select", @"Pune", @"Noida",@"Mumbai",@"Banglore", @"USA", @"UK", @"ME", @"NZ", @"SG", nil]
#define HrsTypeList         [NSArray arrayWithObjects: @"Select", @"Onsite", @"Offshore", nil]
#define LocationValueList   [NSArray arrayWithObjects: @"0", @"P", @"G",@"I",@"B", @"A", @"K", @"M", @"N", @"S", nil]
#define kTaskTypeListForUS  [NSArray arrayWithObjects: @"Billable", @"Non-Billable", nil]
#define kTaskTypeListValueForUS  [NSArray arrayWithObjects: @"Y", @"N", nil]

#define LocationValuePune   @"P"
#define LocationValueNoida  @"G"
#define LocationValueUS     @"U"
#define LocationValueUK     @"K"
#define LocationValueME     @"M"
#define LocationValueNZ     @"N"
#define LocationValueSG     @"S"

#define TimeAppAccountCell            0
#define TimeAppProjectTaskCell        1
#define TimeAppProjectTypeCell        2
#define TimeAppProjectYearCell        3
#define TimeAppProjectMonthCell       4
#define TimeAppProjectDateCell        5

#define AccountLabel        @"Account"
#define ProjectTask         @"Project Task"
#define ProjectType         @"Type"
#define ProjectYear         @"Year"
#define ProjectMonth        @"Month"
#define WeekendEndDate      @"Weekend End Date"


#endif
