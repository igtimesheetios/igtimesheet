//
//  TaskViewParser.m
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGTaskViewParser.h"
#import "IGInOutModalClass.h"
#import "IGTaskModalClass.h"
#import "IGTaskViewMacros.h"
#import "IGMacro.h"

@implementation IGTaskViewParser

- (IGInOutModalClass *)createObjectFromDic:(NSDictionary *)dic
                                  forInKey:(NSString *)inKey
                                    outKey:(NSString *)outKey
                                 isWeekend:(BOOL)weekend
                                 remarkKey:(NSString *)remarkKey
                                  totalKey:(NSString *)totalKey
                                       day:(NSString *)day
{
    IGInOutModalClass* modalObj = [[IGInOutModalClass alloc] init];
    modalObj.dateText = @"";
    modalObj.inTimeText = [dic objectForKey:inKey];
    modalObj.outTimeText = [dic objectForKey:outKey];
    modalObj.remarks = [dic objectForKey:remarkKey];
    modalObj.totalWorkHours = [dic objectForKey:totalKey];
    modalObj.day = day;
    
    if([modalObj.inTimeText isEqualToString:EmptyIn] && (!weekend) && (![modalObj.remarks isEqualToString:@"-"]))
    {
        modalObj.buttonvisible = YES;
    }
    
    return modalObj;
}



- (NSArray *)parseInOutTimeResponse:(NSDictionary *)dataDic forWeekendDate:(NSDate *)weekendDate
{
    NSMutableArray* inOutArr = [[NSMutableArray alloc] init];
    NSDictionary* inOutDic = nil;
    
    NSArray* tempArr = [dataDic objectForKey:InOutTime];
    if(tempArr.count)
    {
        inOutDic = [[dataDic objectForKey:InOutTime] objectAtIndex:0];
    }
    else
    {
        return inOutArr;
    }
    
    int index = 0;
    while (index < 7)
    {
        switch (index)
        {
            case 0:
            {
                [inOutArr addObject:[self createObjectFromDic:inOutDic forInKey:InMon outKey:OutMon isWeekend:NO remarkKey:RemarkMon totalKey:TotalMon day:Monday]];
                break;
            }
                
            case 1:
            {
                [inOutArr addObject:[self createObjectFromDic:inOutDic forInKey:InTue outKey:OutTue isWeekend:NO remarkKey:RemarkTue totalKey:TotalTue day:Tuesday]];
                break;
            }
                
            case 2:
            {
                [inOutArr addObject:[self createObjectFromDic:inOutDic forInKey:InWed outKey:OutWed isWeekend:NO remarkKey:RemarkWed totalKey:TotalWed day:Wednesday]];
                break;
            }
                
            case 3:
            {
                [inOutArr addObject:[self createObjectFromDic:inOutDic forInKey:InThu outKey:OutThu isWeekend:NO remarkKey:RemarkThu totalKey:TotalThu day:Thursday]];
                break;
            }
            
            case 4:
            {
                [inOutArr addObject:[self createObjectFromDic:inOutDic forInKey:InFri outKey:OutFri isWeekend:NO remarkKey:RemarkFri totalKey:TotalFri day:Friday]];
                break;
            }
                
            case 5:
            {
                [inOutArr addObject:[self createObjectFromDic:inOutDic forInKey:InSat outKey:OutSat isWeekend:YES remarkKey:RemarkSat totalKey:TotalSat day:Saturday]];
                break;
            }
                
            case 6:
            {
                [inOutArr addObject:[self createObjectFromDic:inOutDic forInKey:InSun outKey:OutSun isWeekend:YES remarkKey:RemarkSun totalKey:TotalSun day:Sunday]];
                break;
            }
                
            default:
            {
                break;
            }
        }
        
        index++;
    }
    
    //Set Dates
    for(int i = 0; i < inOutArr.count; ++i)
    {
        NSTimeInterval timeInterval = (inOutArr.count - i - 1) * 24 * 3600;
        NSDate* dateOnDay = [weekendDate dateByAddingTimeInterval:-(timeInterval)];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"dd/MM/yyyy"];
        [[inOutArr objectAtIndex:i] setDateText:[dateFormatter stringFromDate:dateOnDay]];
    }
    
    return inOutArr;
}
- (NSArray *)parseTasksResponse:(NSDictionary *)dataDic
{
    NSMutableArray* tasksArr = [[NSMutableArray alloc] init];
    NSArray* tasks = [dataDic objectForKey:EntryDetails]; //CardEntry
    NSArray* projects = [dataDic objectForKey:ProjectList];//ProjectList
    NSArray *effortsDict = [dataDic objectForKey:TasksEffort];//EffortSummary
    
    
    for (int i = 0; i < tasks.count; ++i)
    {
         NSDictionary* task  = [tasks objectAtIndex:i];
        NSInteger taskProjectID = [[task objectForKey:ProjectId] integerValue];
        for(int j=0 ; j < effortsDict.count; j++){
            NSDictionary *effort = [effortsDict objectAtIndex:j];
            NSInteger effortProjcetID = [[effort objectForKey:ProjectId] integerValue];
            if(effortProjcetID == taskProjectID){
                [tasksArr addObject:[self createTaskFromDic:task withApprovalStatus:effort forProjectList:projects]];
                break;
            }
        }
    }
    
    return tasksArr;
}



- (IGTaskModalClass *)createTaskFromDic:(NSDictionary *)dic withApprovalStatus:(NSDictionary *)approvalStatus forProjectList:(NSArray *)projectList
//- (IGTaskModalClass *)createTaskFromDic:(NSDictionary *)dic forProjectList:(NSArray *)projectList

{
    IGTaskModalClass* modalObj = [[IGTaskModalClass alloc] init];
    
    modalObj.prjId = [[dic objectForKey:ProjectId] integerValue];
    
    for(int i = 0; i < projectList.count; ++i)
    {
        if([[[projectList objectAtIndex:i] objectForKey:Track] integerValue] == modalObj.prjId)
        {
            modalObj.projectText = [[projectList objectAtIndex:i] objectForKey:TrackName];
            modalObj.prjId = [[[projectList objectAtIndex:i] objectForKey:Track] integerValue];
            break;
        }
    }
    
    modalObj.location = [dic objectForKey:Location];
    modalObj.description = [dic objectForKey:Description];
    modalObj.hrsType = [dic objectForKey:HrsType];
    modalObj.billable = [dic objectForKey:Billable];
    modalObj.taskId = [dic objectForKey:TaskId] == [NSNull null] ? 0:[[dic objectForKey:TaskId] integerValue];
    modalObj.subTaskId = [dic objectForKey:SubtaskId] ==[NSNull null] ? 0
    :[[dic objectForKey:SubtaskId] integerValue];
    modalObj.cRID =   [dic objectForKey:@"CRID"] == [NSNull null] ? 0 : [dic objectForKey:@"CRID"];
    modalObj.categoryID =  [dic objectForKey:PayrollTaskID] == [NSNull null] ?0 :[[dic objectForKey:PayrollTaskID] integerValue];
    
    if([dic objectForKey:WFOL] != [NSNull null])
        modalObj.wfol = [[dic objectForKey:WFOL] integerValue];
    
    modalObj.cardEntryDetailId = [[dic objectForKey:EmpCardEntryId] integerValue];
    
    modalObj.timeSheetStatus = kIGTimeSheetStatusPending;
    
    
    
        //if(approvalStatus != nil){
                NSString *pmStatus = [approvalStatus objectForKey:@"PMStatus"];
                if(![pmStatus isEqual:[NSNull null]]){
                    if ([pmStatus isEqual:TimeSheetApprovedStatus]) {
                        modalObj.timeSheetStatus = kIGTimeSheetStatusApproved;
                    }
                    else if ([pmStatus isEqual:TimeSheetRejectedStatus]){
                        modalObj.timeSheetStatus = kIGTimeSheetStatusRejected;
                    }
                }
                else{
                    NSString *statusId = [dic objectForKey:@"statusid"];
                    if ([statusId isEqual:TimeSheetCompleteStatus]) {
                        modalObj.timeSheetStatus = kIGTimeSheetStatusCompleted;
                    }
                    else if ([statusId isEqual:TimeSheetPendingStatus]){
                        modalObj.timeSheetStatus = kIGTimeSheetStatusPending;
                    }
                }
                
            
            
       // }
    
//        else{
//            NSString *statusId = [dic objectForKey:@"statusid"];
//                if ([statusId isEqual:TimeSheetCompleteStatus]) {
//                    modalObj.timeSheetStatus = kIGTimeSheetStatusCompleted;
//                }
//                else if ([statusId isEqual:TimeSheetPendingStatus]){
//                    modalObj.timeSheetStatus = kIGTimeSheetStatusPending;
//                }
//        }
//    
    
    
    
    
//    modalObj.timeSheetStatus = kIGTimeSheetStatusPending;
//    if([[dic objectForKey:TimeSheetStatus] isEqual:TimeSheetCompleteStatus])
//    {
//        modalObj.timeSheetStatus = kIGTimeSheetStatusCompleted;
//    }
//    else if ([[dic objectForKey:TimeSheetStatus] isEqual:TimeSheetApprovedStatus])
//    {
//        modalObj.timeSheetStatus = kIGTimeSheetStatusApproved;
//    }
//    else if ([[dic objectForKey:TimeSheetStatus] isEqual:TimeSheetRejectedStatus])
//    {
//        modalObj.timeSheetStatus = kIGTimeSheetStatusRejected;
//    }
    
    modalObj.totalTime = [[dic objectForKey:TotalHrs] floatValue];
    modalObj.monTime   = [[dic objectForKey:EMon] floatValue];
    modalObj.tueTime   = [[dic objectForKey:ETue] floatValue];
    modalObj.wedTime   = [[dic objectForKey:EWed] floatValue];
    modalObj.thuTime   = [[dic objectForKey:EThu] floatValue];
    modalObj.friTime   = [[dic objectForKey:EFri] floatValue];
    modalObj.satTime   = [[dic objectForKey:ESat] floatValue];
    modalObj.sunTime   = [[dic objectForKey:ESun] floatValue];
    
    return modalObj;
}


//- (NSArray *)parseTasksResponse:(NSDictionary *)dataDic
//{
//    NSMutableArray* tasksArr = [[NSMutableArray alloc] init];
//    NSArray* tasks = [dataDic objectForKey:EntryDetails];
//    NSDictionary* projects = [dataDic objectForKey:ProjectList];
//    NSArray *approvalStatuses = [dataDic objectForKey:TasksEffort];
//    
//    
//    for (int i = 0; i < tasks.count; ++i)
//    {
//        NSDictionary* task  = [tasks objectAtIndex:i];
//        NSDictionary *approval;
//        for(int j=0 ; j<approvalStatuses.count; j++){
//                approval = [approvalStatuses objectAtIndex:j];
//        }
//        [tasksArr addObject:[self createTaskFromDic:task withApprovalStatus:approval forProjectList:projects]];
//
//
//        
//    }
//    
//    return tasksArr;
//}

- (NSString *)parseTotalHours:(NSDictionary *)dataDic
{
    NSArray* tasks = [dataDic objectForKey:TasksEffort];
    NSString* totalHrs = @"0";
    if(tasks.count)
    {
        totalHrs = [NSString stringWithFormat:@"%@",[[tasks objectAtIndex:0] objectForKey:TotalEfforts]];
    }
    
    return totalHrs;
}

- (NSArray *)parseProjectLsit:(NSDictionary *)dataDic
{
    NSArray* projects = [dataDic objectForKey:ProjectList];
    return projects;
}

- (NSInteger)parseWeekId:(NSDictionary *)dictionary
{
    return [[[[dictionary objectForKey:@"Week"] objectAtIndex:0] objectForKey:@"WeekId"] integerValue];
}

@end
