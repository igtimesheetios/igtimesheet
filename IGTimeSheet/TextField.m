//
//  TextField.m
//  IGTimeSheet
//
//  Created by Kamal on 03/03/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "TextField.h"

@implementation TextField

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender
{
    if (action == @selector(copy:) || action == @selector(select:) || action == @selector(selectAll:) || action == @selector(paste:) || action == @selector(cut:))
        return NO;
    return [super canPerformAction:action withSender:sender];
}

@end
