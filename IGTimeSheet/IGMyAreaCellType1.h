//
//  IGMonthlyTimeSheetCellType1.h
//  IGTimeSheet
//
//  Created by Kamal on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGMyAreaCellType1: UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *labelTitle;

@property (strong, nonatomic) IBOutlet UILabel *yearData;

@end
