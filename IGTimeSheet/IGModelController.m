//
//  GMModelController.m
//  GMPageController
//
//  Created by Upakul on 18/09/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "IGModelController.h"
#import "IGCommonHeaders.h"

#import "IGDataViewController.h"

/*
 A controller object that manages a simple model -- a collection of month names.
 
 The controller serves as the data source for the page view controller; it therefore implements pageViewController:viewControllerBeforeViewController: and pageViewController:viewControllerAfterViewController:.
 It also implements a custom method, viewControllerAtIndex: which is useful in the implementation of the data source methods, and in the initial configuration of the application.
 
 There is no need to actually create view controllers for each page in advance -- indeed doing so incurs unnecessary overhead. Given the data model, these methods create, configure, and return a new view controller on demand.
 */
@interface IGModelController()
@end

@implementation IGModelController

- (id)init
{
    self = [super init];
    if (self) {
        // Create the data model.
//        NSDictionary *tutorial0 = [NSDictionary dictionaryWithObjectsAndKeys:
//                                   @"0", kKeyIndex,
//                                   @"Tutorial00.png", kKeyImage,
//                                   @"Infogain Global Timesheet", kKeyTitle,
//                                   @"Welcome On-Board! Lets take a short tour..", kKeyDescription,
//                                   nil];

        NSDictionary *tutorial1 = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"0", kKeyIndex,
                                   @"Tutorial01.png", kKeyImage,
                                   @"Dashboard", kKeyTitle,
                                   @"Submit the time card or view missing or incomplete ones..", kKeyDescription,
                                   nil];
        NSDictionary *tutorial2 = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"1", kKeyIndex,
                                   @"Tutorial02.png", kKeyImage,
                                   @"View Leaves", kKeyTitle,
                                   @"Go through the leaves taken..", kKeyDescription,
                                   nil];
        NSDictionary *tutorial3 = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"2", kKeyIndex,
                                   @"Tutorial03.png", kKeyImage,
                                   @"My Area", kKeyTitle,
                                   @"View weekly or monthly summaries for the time sheets..", kKeyDescription,
                                   nil];
//        NSDictionary *tutorial4 = [NSDictionary dictionaryWithObjectsAndKeys:
//                                   @"4", kKeyIndex,
//                                   @"Tutorial04.png", kKeyImage,
//                                   @"Monthly TimeSheet Average", kKeyTitle,
//                                   @"View the monthly average of timsheet hours and the In/Out hours..", kKeyDescription,
//                                   nil];
        
        NSDictionary *tutorial5 = [NSDictionary dictionaryWithObjectsAndKeys:
                                   @"3", kKeyIndex,
                                   @"Tutorial05.png", kKeyImage,
                                   @"Help", kKeyTitle,
                                   @"Detailed help on \"How to use the application ?\" ..", kKeyDescription,
                                   nil];
//        NSDictionary *tutorial6 = [NSDictionary dictionaryWithObjectsAndKeys:
//                                   @"6", kKeyIndex,
//                                   @"Tutorial06.png", kKeyImage,
//                                   @"Holidays", kKeyTitle,
//                                   @"Keep a track of the Holiday Calendar..", kKeyDescription,
//                                   nil];
//        NSDictionary *tutorial7 = [NSDictionary dictionaryWithObjectsAndKeys:
//                                   @"7", kKeyIndex,
//                                   @"Tutorial07.png", kKeyImage,
//                                   @"Infogain Office", kKeyTitle,
//                                   @"Explore Infogain Offices around the world..", kKeyDescription,
//                                   nil];
//        NSDictionary *tutorial8 = [NSDictionary dictionaryWithObjectsAndKeys:
//                                   @"8", kKeyIndex,
//                                   @"Tutorial08.png", kKeyImage,
//                                   @"Weather", kKeyTitle,
//                                   @"How is the weather today?", kKeyDescription,
//                                   nil];
//        NSDictionary *tutorial9 = [NSDictionary dictionaryWithObjectsAndKeys:
//                                   @"9", kKeyIndex,
//                                   @"Tutorial09.png", kKeyImage,
//                                   @"Double Tap", kKeyTitle,
//                                   @"Want to share Feedback or Report a Bug? Just Double Tap on any Screen..", kKeyDescription,
//                                   nil];
       

        _pageData = [NSArray arrayWithObjects:tutorial1,tutorial2,tutorial3,tutorial5, nil];
    }
    return self;
}

- (IGDataViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard
{   
    // Return the data view controller for the given index.
    if (([self.pageData count] == 0) || (index >= [self.pageData count])) {
        return nil;
    }
    
    // Create a new view controller and pass suitable data.
    IGDataViewController *dataViewController = [storyboard instantiateViewControllerWithIdentifier:kStoryboardIdentifierGMDataVC];
    dataViewController.dataObject = self.pageData[index];
    dataViewController.rootViewController = self.rootViewController;
    dataViewController.modelController = self;
    return dataViewController;
}

- (NSUInteger)indexOfViewController:(IGDataViewController *)viewController
{   
     // Return the index of the given data view controller.
     // For simplicity, this implementation uses a static array of model objects and the view controller stores the model object; you can therefore use the model object to identify the index.
    return [self.pageData indexOfObject:viewController.dataObject];
}

#pragma mark - Page View Controller Data Source

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(IGDataViewController *)viewController];
    if ((index == 0) || (index == NSNotFound)) {
        return nil;
    }
    
    index--;
    return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
}

- (UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    NSUInteger index = [self indexOfViewController:(IGDataViewController *)viewController];
    if (index == NSNotFound) {
        return nil;
    }
    
    index++;
    if (index == [self.pageData count]) {
        return nil;
    }
    return [self viewControllerAtIndex:index storyboard:viewController.storyboard];
}

@end
