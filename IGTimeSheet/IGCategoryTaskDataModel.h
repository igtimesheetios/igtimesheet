//
//  IGTaskDataModel.h
//  IGTimeSheet
//
//  Created by Neha on 19/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGCategoryTaskDataModel : NSObject

@property(atomic) NSNumber* categoryID;
@property(strong) NSString* categoryName;

@end
