//
//  IGTextInputView.h
//  IGTimeSheet
//
//  Created by Manish on 04/03/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IGTextInputView;
@protocol IGTextInputViewDelegate <NSObject>

/*! Tells the delegate that a specified button is selected
 \param view The view object whose button is selected
 \param index The index of selected button. 0 - Cancel, 1 - Done
 */
- (void)textInputView:(IGTextInputView *)view didSelectButtonAtIndex:(NSInteger)index;
@end


/*! An instance of IGTextInputView is a means for displaying a TextView with Cancel and Done options.*/

@interface IGTextInputView : UIView

@property (weak, nonatomic) IBOutlet UIView *viewBackground;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelPlaceholder;
@property (weak, nonatomic) IBOutlet UITextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;
@property (weak, nonatomic) IBOutlet UIButton *buttonDone;

@property (nonatomic, weak) id<IGTextInputViewDelegate> delegate;

+ (instancetype)textInputView;

/*! Adds IGTextInputView on the given view
 \param aView The Parent View on which the IGTextInputView object should be added
 */
- (void)presentOnView:(UIView *)aView;

@end
