//
//  IGViewController.h
//  IGTimeSheet
//
//  Created by Manish on 10/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGViewController : UIViewController

- (void)setupBackButton;
- (void)setupOpaqueNavigationBar;
- (void)showSpinner:(BOOL)show;
- (void)goBack:(UIBarButtonItem *)barButtonItem;
@end
