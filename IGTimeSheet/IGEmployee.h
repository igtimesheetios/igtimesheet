//
//  IGEmployee.h
//  IGTimeSheet
//
//  Created by Manish on 23/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface IGEmployee : NSObject

@property (nonatomic, strong)   NSString *name;
@property (nonatomic, strong)   NSString *mailID;
@property (nonatomic, readonly) NSString *status;
@property (nonatomic, readonly) NSString *onsiteManager;
@property (nonatomic, readonly) NSString *payrollAdmin;
@property (nonatomic, readonly) NSString *timesheetType;
@property (nonatomic, strong)   NSString *staffID; //employeeId
@property (nonatomic, readonly) NSString *staffID1;
@property (nonatomic, strong) NSString *employeeNewID;
@property (nonatomic, readonly) NSString *projectManagerID;
@property (nonatomic, strong)   UIImage *image;

@property (nonatomic, readonly) NSNumber *baseLocationID;
@property (nonatomic, readonly) NSNumber *locationID;

@property (nonatomic, assign) BOOL isContractor;
@property (nonatomic, assign) BOOL isUSEmployee;
@property (nonatomic, assign) BOOL isPMRole;
@property (nonatomic, readonly) BOOL isAboveProjectManager;

@property (nonatomic, assign) BOOL isAuthenticated;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

- (void)reset;

@end
