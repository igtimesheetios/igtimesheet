//
//  IGTaskHrsCell.h
//  IGTimeSheet
//
//  Created by Neha on 20/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGTaskHrsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField* hrsTextField;
@property (weak, nonatomic) IBOutlet UILabel* dateLabel;

@end
