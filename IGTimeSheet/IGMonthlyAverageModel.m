//
//  MonthlyAverageModel.m
//  IGTimeSheet
//
//  Created by Manish on 18/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGMonthlyAverageModel.h"
#import "IGHourSummary.h"
#import "IGServiceDefine.h"

@implementation IGMonthlyAverageModel

- (id)initWithDictionary:(NSDictionary *)monthlyAverageInfoDictionary
{
    if (monthlyAverageInfoDictionary == nil)
    {
        NSLog(@"%s: ERROR ceating object. Input Parameter is nil", __FUNCTION__);
        return nil;
    }
    
    if ([monthlyAverageInfoDictionary respondsToSelector:@selector(objectForKey:)] == NO)
    {
        NSLog(@"%s: ERROR ceating object. Expected a Dictionary type, got %@", __FUNCTION__, NSStringFromClass([monthlyAverageInfoDictionary class]));
        return nil;
    }

    self = [super init];
    if (self)
    {
        NSArray *inoutArray = monthlyAverageInfoDictionary[kIGServiceKeyInOutHoursSummary];
        NSArray *timeSheetArray = monthlyAverageInfoDictionary[kIGServiceKeyTimeSheetHoursSummary];
        
        if (inoutArray == nil && timeSheetArray == nil)
        {
            NSLog(@"%s: ERROR ceating object. Both InOut and TimeSheet Array is nil", __FUNCTION__);
            return nil;
        }
        
        if ([inoutArray respondsToSelector:@selector(objectAtIndex:)] && inoutArray.count > 0)
        {
            NSDictionary *infoDictionary = inoutArray[0];
            if (infoDictionary)
            {
                _inOutHours = [[IGHourSummary alloc] initWithDictionary:infoDictionary];
            }
        }

        if ([timeSheetArray respondsToSelector:@selector(objectAtIndex:)] && timeSheetArray.count > 0)
        {
            NSDictionary *infoDictionary = timeSheetArray[0];
            if (infoDictionary)
            {
                _timeSheetHours = [[IGHourSummary alloc] initWithDictionary:infoDictionary];
            }
        }
    }
    
    return self;
}


@end
