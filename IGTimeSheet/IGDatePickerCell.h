//
//  IGDatePickerCell.h
//  IGTimeSheet
//
//  Created by Neha on 12/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGDatePickerCell : UITableViewCell<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>
{
    NSMutableArray *pickerContents;
    int selectedPickerValue;    
}

@property(strong) IBOutlet UITextField *pickerTextField;

- (IBAction)disclosureTapped:(id)sender;

@end
