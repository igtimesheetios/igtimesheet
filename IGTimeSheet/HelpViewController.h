//
//  HelpViewController.h
//  IGTimeSheet
//
//  Created by Rajat on 26/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGViewController.h"

@interface HelpViewController : IGViewController
@property (weak, nonatomic) IBOutlet UITableView *tblView;

@end
