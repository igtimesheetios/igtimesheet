//
//  IGTaskDescriptionCell.h
//  IGTimeSheet
//
//  Created by Neha on 19/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGTaskDescriptionCell : UITableViewCell

@property (weak) IBOutlet UILabel *descriptionLabel;
@property (weak) IBOutlet UILabel *titleLabel;

@end
