//
//  IGViewController.m
//  IGTimeSheet
//
//  Created by Manish on 10/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGViewController.h"
#import "AppDelegate.h"
#import "IGSideMenuRootVC.h"
#import "IGMacro.h"
#import "IGSpinKitView.h"

@interface IGViewController()
{
    IGSpinKitView *spinner;
}
@end

@implementation IGViewController

- (void)setupBackButton
{
    if (self.navigationController != nil)
    {
        NSInteger viewControllerCount = self.navigationController.viewControllers.count;
        
        NSString *imageName = nil;
        SEL selector = NULL;
        
        if (viewControllerCount ==  1)
        {
            imageName = @"menu-icon";
            selector = @selector(showMenu:);
        }
        else if (viewControllerCount > 1)
        {
            imageName = @"back-icon";
            selector = @selector(goBack:);
        }
        
        UIBarButtonItem *leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:imageName]
                                                                              style:UIBarButtonItemStylePlain
                                                                             target:self
                                                                             action:selector];
        self.navigationItem.leftBarButtonItem = leftBarButtonItem;
    }
}

- (void)setupOpaqueNavigationBar
{
    self.navigationController.navigationBar.opaque = YES;
    self.navigationController.navigationBar.translucent = NO;
}

- (void)showMenu:(UIBarButtonItem *)barButtonItem
{
    AppDelegate *appDelegate = kAppDelegate;
    [appDelegate.sideMenuRootVC presentLeftMenuViewController];
}

- (void)goBack:(UIBarButtonItem *)barButtonItem
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (void)showSpinner:(BOOL)show
{
    if (show)
    {
        if (!spinner)
        {
            spinner = [[IGSpinKitView alloc] initWithColor:kDefaultBarTintColor];
            spinner.hidesWhenStopped = YES;
            spinner.center = self.view.center;
        }
        
        if (spinner.superview == nil)
        {
            [self.view addSubview:spinner];
        }
        
        [spinner startAnimating];
    }
    else
    {
        [spinner stopAnimating];
    }
    
    self.view.userInteractionEnabled = !show;
}

@end
