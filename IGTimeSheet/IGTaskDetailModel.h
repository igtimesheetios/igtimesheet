//
//  IGTaskDetailModel.h
//  IGTimeSheet
//
//  Created by Manish on 27/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGTaskDetailModel : NSObject

// contains object of type IGTaskDetail
@property (nonatomic, readonly) NSArray *taskDetails;

- (instancetype)initWithDictionary:(NSDictionary *)taskDetailInfo;

@end
