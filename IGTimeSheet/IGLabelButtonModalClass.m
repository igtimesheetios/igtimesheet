//
//  LabelButtonModalClass.m
//  TaskView
//
//  Created by Hem on 10/02/15.
//  Copyright (c) 2015 Hem. All rights reserved.
//

#import "IGLabelButtonModalClass.h"

@implementation IGLabelButtonModalClass

@synthesize labelText, labelTextColor, buttonText, buttonTextColor, buttonVisible, buttonEnabled, backImage , backgroundColor , rowSelectable;

- (id)init
{
    self = [super init];
    if(self)
    {
        labelText = @"";
        labelTextColor = [UIColor blackColor];
        backgroundColor = [UIColor whiteColor];
        buttonText = @"";
        buttonTextColor = [UIColor blackColor];
        backImage = nil;
        buttonVisible = YES;
        buttonEnabled = YES;
        rowSelectable = NO;
    }
    
    return self;
}

@end
