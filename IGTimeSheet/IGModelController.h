//
//  GMModelController.h
//  GMPageController
//
//  Created by Upakul on 18/09/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGTutorialRootViewController.h"

@class IGDataViewController;

@interface IGModelController : NSObject <UIPageViewControllerDataSource>

@property (nonatomic, strong) IGTutorialRootViewController *rootViewController;
@property (nonatomic, strong) NSArray *pageData;

- (IGDataViewController *)viewControllerAtIndex:(NSUInteger)index storyboard:(UIStoryboard *)storyboard;
- (NSUInteger)indexOfViewController:(IGDataViewController *)viewController;

@end
