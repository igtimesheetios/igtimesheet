//
//  GMDataViewController.m
//  GMPageController
//
//  Created by Upakul on 18/09/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import "IGDataViewController.h"
#import "IGModelController.h"
#import "IGCommonHeaders.h"

@interface IGDataViewController () <UINavigationBarDelegate>

@end

@implementation IGDataViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Set exclusive touch
    for(UIView *temp in self.navigationBar.subviews)
    {
        [temp setExclusiveTouch:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
   // [self.navigationBar setBackgroundImage:[UIImage imageNamed:kImageScreenBG] forBarMetrics:UIBarMetricsDefault];
 //   [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:kImageScreenBG]]];
    
    self.navigationBar.topItem.title = [self.dataObject objectForKey:kKeyTitle];
    if ([[self.dataObject objectForKey:kKeyIndex] intValue] == ([self.modelController.pageData count] - 1)) {
        [self.navigationBar.topItem.rightBarButtonItem setTitle:kStringDone];
    }
    
    self.dataImage.image = [UIImage imageNamed:[self.dataObject objectForKey:kKeyImage]];
    self.descriptionLabel.text =[self.dataObject objectForKey:kKeyDescription];
    [self.pageControl setNumberOfPages:[self.modelController.pageData count]];
    [self.pageControl setUserInteractionEnabled:NO];
    [self.pageControl setCurrentPage:[[self.dataObject objectForKey:kKeyIndex] intValue]];
    
    [self.dataImage.layer setShadowColor:[UIColor whiteColor].CGColor];
    [self.dataImage.layer setShadowRadius:2.5];
    [self.dataImage.layer setShadowOpacity:1.0];
    [self.dataImage.layer setShadowOffset:CGSizeMake(0.0, 0.0)];

}

- (UIBarPosition)positionForBar:(id <UIBarPositioning>)bar
{
    return UIBarPositionTopAttached;
}

- (IBAction)skip:(id)sender {
    [self.rootViewController dismissTutorial:self];
}

- (IBAction)next:(id)sender {
    
    if ([[self.dataObject objectForKey:kKeyIndex] intValue] == ([self.modelController.pageData count] - 1)) {
        [self.rootViewController dismissTutorial:self];
        return;
    }
    
    NSUInteger retreivedIndex = [self.modelController indexOfViewController:self];
    IGDataViewController *targetPageViewController = [self.modelController viewControllerAtIndex:(retreivedIndex + 1) storyboard:self.storyboard];
    [self.rootViewController.pageViewController setViewControllers:[NSArray arrayWithObjects:targetPageViewController, nil] direction:UIPageViewControllerNavigationDirectionForward animated:YES completion:^(BOOL finished) {
        
    }];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

@end
