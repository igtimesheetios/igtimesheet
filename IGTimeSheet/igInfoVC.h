//
//  igInfoVC.h
//  IGCafe
//
//  Created by Manish on 22/06/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGViewController.h"

@interface igInfoVC : IGViewController

@property (nonatomic, strong) UIImage *image;

@end
