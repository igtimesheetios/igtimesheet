//
//  igMandatoryUpdateVC.m
//  IGCafe
//
//  Created by Pulkit on 27/07/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "igMandatoryUpdateVC.h"

@interface igMandatoryUpdateVC ()

@property (weak, nonatomic) IBOutlet UITextView *textViewDescription;
@property (weak, nonatomic) IBOutlet UIButton *buttonUpdate;

- (IBAction)update:(id)sender;

@end

@implementation igMandatoryUpdateVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    if ([IGUtils isValidString:self.updateDescriptionString])
    {
        self.textViewDescription.text = self.updateDescriptionString;
        self.textViewDescription.textColor = [UIColor whiteColor];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)update:(UIButton *)sender
{
#if TARGET_IPHONE_SIMULATOR
    [IGUtils showOKAlertWithTitle:@"Alert" message:@"Can not open URL in Simulator. Please test on real device."];
#else
    BOOL openingURL = [IGUtils openURLWithString:self.updateResponse.url];
    
    if (!openingURL)
    {
        [IGUtils showOKAlertWithTitle:@"Alert" message:@"Can not open update URL."];
    }
    
    sender.enabled = !openingURL;
#endif
}

@end
