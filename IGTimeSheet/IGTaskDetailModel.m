//
//  IGTaskDetailModel.m
//  IGTimeSheet
//
//  Created by Manish on 27/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGTaskDetailModel.h"
#import "IGTaskDetail.h"
#import "IGServiceDefine.h"

@implementation IGTaskDetailModel

- (instancetype)initWithDictionary:(NSDictionary *)taskDetailInfo
{
    if ([taskDetailInfo respondsToSelector:@selector(objectForKey:)] == NO)
    {
        return nil;
    }
    
    NSArray *taskDetails = [taskDetailInfo objectForKey:kIGServiceKeyDayWiseTaskDetail];
    if ([taskDetails respondsToSelector:@selector(objectAtIndex:)] == NO || taskDetails.count < 1)
    {
        return nil;
    }
    
    self = [super init];
    if (self)
    {
        NSUInteger count = taskDetails.count;
        
        NSMutableArray *taskDetailArray = [[NSMutableArray alloc] initWithCapacity:1];
        for (int i = 0; i < count; i++)
        {
            NSDictionary *taskInfo = taskDetails[i];
            IGTaskDetail *taskDetail = [[IGTaskDetail alloc] initWithDictionary:taskInfo];
            if (taskDetail != nil)
            {
                [taskDetailArray addObject:taskDetail];
            }
        }
        
        _taskDetails = [NSArray arrayWithArray:taskDetailArray];
    }
    return self;
}

@end
