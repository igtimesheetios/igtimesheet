//
//  IGBadgeView.h
//  IGTimeSheet
//
//  Created by Manish on 20/05/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

#define IGBADGE_PADDING     5


@interface IGBadgeView : UIView

@property (nonatomic, readonly) UILabel *labelTitle;

- (instancetype)initWithFrame:(CGRect)frame;

@end
