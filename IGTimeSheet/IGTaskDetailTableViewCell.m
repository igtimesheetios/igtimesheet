//
//  TaskDetailTableViewCell.m
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGTaskDetailTableViewCell.h"

@implementation IGTaskDetailTableViewCell

@synthesize statusLabel , projectLabel , detailLabel , timeLabel;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
