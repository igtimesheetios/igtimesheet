//
//  IGMyAreaModel.m
//  IGTimeSheet
//
//  Created by Manish on 23/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGMyAreaModel.h"
#import "IGService.h"
#import "IGServiceDefine.h"
#import "IGUserDefaults.h"
#import "IGEmployee.h"
#import "IGWeeklyTimesheetSummary.h"
#import "IGMonthlyTimesheetSummary.h"
#import "IGUtils.h"

@interface IGMyAreaModel ()

@property (nonatomic, assign) BOOL shouldFetchWeeklyTimesheetSummary;
@property (nonatomic, assign) BOOL shouldFetchMonthlyTimesheetSummary;

@end


@implementation IGMyAreaModel

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        _queryDate = [NSDate date];
        _shouldFetchWeeklyTimesheetSummary = YES;
        _shouldFetchMonthlyTimesheetSummary = YES;
    }
    return self;
}

- (void)setQueryDate:(NSDate *)queryDate
{
    _queryDate = queryDate;
    
    _shouldFetchWeeklyTimesheetSummary = YES;
    _shouldFetchMonthlyTimesheetSummary = YES;
}

- (void)fetchWeeklyTimesheetSummary:(void(^)(IGMyAreaModel *myAreaModel))completionBlock
                              error:(void(^)(NSError *error))errorBlock
{
    if (_shouldFetchWeeklyTimesheetSummary == NO)
    {
        return;
    }
    
    [[IGService sharedInstance] weeklyTimesheetSummaryForEmployee:self.employeeID
                                                             date:self.queryDate
        completion:^(id response)
        {
            _weeklyTimesheetSummary = [[IGWeeklyTimesheetSummary alloc] initWithDictionary:response];
            if (_weeklyTimesheetSummary != nil)
            {
                _shouldFetchWeeklyTimesheetSummary = NO;
            }
            else
            {
                _weekSummaryStatus = @"Weekly Timesheet Summary not available";
            }
            
            completionBlock(self);
        }
        error:^(NSError *error)
        {
            //TODO: test
            
            _weekSummaryStatus = @"Error loading Weekly Timesheet Summary";            
            _weeklyTimesheetSummary = nil;
            errorBlock(error);
        }];
}

- (void)fetchMonthlyTimesheetSummary:(void(^)(IGMyAreaModel *myAreaModel))completionBlock
                               error:(void(^)(NSError *error))errorBlock;
{
    if (_shouldFetchMonthlyTimesheetSummary == NO)
    {
        return;
    }
    
    [[IGService sharedInstance] monthlyTimesheetSummaryForEmployee:self.employeeID
                                                              date:self.queryDate
        completion:^(id response)
        {
            NSArray *keys = nil;
            if ([response respondsToSelector:@selector(allKeys)])
            {
                keys = [response allKeys];
            }

            if (keys && keys.count > 0)
            {
                NSUInteger keyCount = keys.count;
                NSMutableArray *mutableArray = [[NSMutableArray alloc] initWithCapacity:1];
                for (int i = 0; i < keyCount; i++)
                {
                    NSString *monthName = keys[i];
                    NSArray *effortSummary = response[monthName];
                    
                    IGMonthlyTimesheetSummary *monthlyTimesheetSummary = [[IGMonthlyTimesheetSummary alloc] initWithName:monthName
                                                                                                        timeSheetSummary:effortSummary];
                    
                    [mutableArray addObject:monthlyTimesheetSummary];
                }
                
                if  (mutableArray.count > 1)
                {
                    _monthlyTimesheetSummary = [mutableArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
                        IGMonthlyTimesheetSummary *timeSheetSummary1 = (IGMonthlyTimesheetSummary *)obj1;
                        IGMonthlyTimesheetSummary *timeSheetSummary2 = (IGMonthlyTimesheetSummary *)obj2;
                        
                        NSString *month1 = timeSheetSummary1.monthName;
                        NSString *month2 = timeSheetSummary2.monthName;
                        
                        NSDate *date1 = [IGUtils dateFromString:month1 dateFormat:@"MMM"];
                        NSDate *date2 = [IGUtils dateFromString:month2 dateFormat:@"MMM"];
                        
                        return [date2 compare:date1];
                    }];
                }
                else
                {
                    _monthlyTimesheetSummary = [NSArray arrayWithArray:mutableArray];
                }

                _shouldFetchMonthlyTimesheetSummary = NO;
            }
            else
            {
                _monthSummaryStatus = @"Monthly Timesheet Summary not available";
            }
            
            completionBlock(self);
        }
        error:^(NSError *error)
        {
            _monthSummaryStatus = @"Error loading Month Summary Status";
            _monthlyTimesheetSummary = nil;
            
            errorBlock(error);
        }];
}

@end
