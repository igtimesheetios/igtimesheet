//
//  IGInOutTableViewCell.m
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGInOutTableViewCell.h"

@implementation IGInOutTableViewCell

@synthesize dateLabel , inTimeLabel , outTimeLabel , redButton;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
