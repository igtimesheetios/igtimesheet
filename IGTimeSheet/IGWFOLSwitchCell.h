//
//  IGWFOLSwitchCell.h
//  IGTimeSheet
//
//  Created by Neha on 23/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGWFOLSwitchCell : UITableViewCell

@property IBOutlet UISwitch *woflSwitch;

@end
