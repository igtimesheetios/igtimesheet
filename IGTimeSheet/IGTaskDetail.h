//
//  IGTaskDetail.h
//  IGTimeSheet
//
//  Created by Manish on 27/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGTaskDetail : NSObject

@property (nonatomic, readonly) NSString *track;
@property (nonatomic, readonly) NSString *CRName;
@property (nonatomic, readonly) NSString *taskDescription;
@property (nonatomic, readonly) NSString *location;
@property (nonatomic, readonly) NSString *type;
@property (nonatomic, readonly) NSString *task;
@property (nonatomic, readonly) NSString *subTask;
@property (nonatomic, readonly) NSNumber *effort;

- (instancetype)initWithDictionary:(NSDictionary *)taskInfo;

@end
