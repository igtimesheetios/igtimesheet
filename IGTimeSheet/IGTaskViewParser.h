//
//  TaskViewParser.h
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGTaskViewParser : NSObject{
    int weekendDateIndex;
    int invalidDayCount;


}

- (NSArray *)parseInOutTimeResponse:(NSDictionary *)dataDic forWeekendDate:(NSDate *)weekendDate;
- (NSArray *)parseTasksResponse:(NSDictionary *)dataDic;
- (NSString *)parseTotalHours:(NSDictionary *)dataDic;
- (NSArray *)parseProjectLsit:(NSDictionary *)dataDic;
- (NSInteger)parseWeekId:(NSDictionary *)dictionary;

@end
