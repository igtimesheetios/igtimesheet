//
//  TestController.m
//  IGTimeSheet
//
//  Created by Manish on 09/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "TestController.h"

@implementation TestController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupBackButton];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    TestController *testVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TestController"];
    [self.navigationController pushViewController:testVC animated:YES];
}

@end
