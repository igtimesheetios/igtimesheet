//
//  igTermConditionVC.m
//  IGCafe
//
//  Created by Manish on 23/06/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "igTermConditionVC.h"
#import "IGCommonHeaders.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>

#define kTermsAndConditionsFileName             @"terms_and_conditions"

#define TAG_ALERT_QUIT_APPLICATION              3333

@interface igTermConditionVC () <EMSmoothAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageViewBackground;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIButton *buttonClose;

@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelSubTitle;

@property (weak, nonatomic) IBOutlet UIView *viewButtons;
@property (weak, nonatomic) IBOutlet UIButton *buttonAgree;
@property (weak, nonatomic) IBOutlet UIButton *buttonDisagree;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraintWebView;

- (IBAction)close:(id)sender;
- (IBAction)agree:(id)sender;
- (IBAction)disagree:(id)sender;

@end


@implementation igTermConditionVC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.buttonAgree.exclusiveTouch = YES;
    self.buttonDisagree.exclusiveTouch = YES;
    self.buttonClose.exclusiveTouch = YES;
    
    if (_isLaunchScene)
    {
        self.buttonClose.hidden = YES;
    }
    else
    {
        [self.viewButtons removeFromSuperview];
        self.bottomConstraintWebView.constant = 12;
    }
    
    NSString *filePath = [[[NSBundle mainBundle] pathForResource:kTermsAndConditionsFileName ofType:@"html"] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSAssert1(filePath != nil, kStringMessageFileNotFoundInBundle_, kTermsAndConditionsFileName);
    if (filePath)
    {

        NSURL *url = [NSURL URLWithString:filePath];
        [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [IGAnalytics trackScreen:screenTermsAndConditions];
}


- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}

- (IBAction)close:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)agree:(id)sender
{
    if (_isLaunchScene)
    {
        [FIRAnalytics setUserPropertyString:[IGUserDefaults sharedInstance].userName forName:@"UserEmail"];
        [FIRAnalytics logEventWithName:@"Login" parameters:@{kFIRParameterItemName:[IGUserDefaults sharedInstance].userName}];
        
        
        [IGAnalytics trackEventWith:@"iOS" action:@"Login" actionName:[NSString stringWithFormat:@"%@_%@",[IGUserDefaults sharedInstance].employee.employeeNewID,[IGUserDefaults sharedInstance].userName]];
        
        [IGUserDefaults sharedInstance].hasAcceptedTermsAndConditions = YES;
        [IGUserDefaults sharedInstance].employee.isAuthenticated = YES;
        [self showLogin];
    }
}

- (void)showLogin
{
//    Setup will automatically setup VCs to show login
    AppDelegate *appDelegate = kAppDelegate;
    [appDelegate setupRootViewControllerWithInfo:nil];
}

- (IBAction)disagree:(id)sender
{
    [IGUserDefaults sharedInstance].hasAcceptedTermsAndConditions = NO;
    [IGUserDefaults sharedInstance].employee.isAuthenticated = NO;
    
    NSString *title = kStringLogOut;
    NSString *message = kStringMessageYouNeedToAgreeToTermsAndConditions;
    igTSmoothAlertView *alert = [[igTSmoothAlertView alloc] initDropAlertWithTitle:title
                                                                           andText:message
                                                                   andCancelButton:YES
                                                                          andColor:kDefaultBarTintColor
                                                                          andImage:[UIImage imageNamed:kImageInfogainLogoCircle]];
    alert.tag = TAG_ALERT_QUIT_APPLICATION;
    [alert.defaultButton setTitle:kStringLogOut forState:UIControlStateNormal];
    alert.delegate = self;
    [alert show];
}




#pragma mark - EMSmoothAlertViewDelegate

- (void)alertView:(igTSmoothAlertView *)alertView didDismissWithButton:(UIButton *)button
{
    if (alertView.tag == TAG_ALERT_QUIT_APPLICATION)
    {
        if ([button isEqual:alertView.defaultButton])
        {
            [[IGUserDefaults sharedInstance] logout];
        }
    }
}

@end
