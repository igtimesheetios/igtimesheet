//
//  IGApprovalParser.h
//  IGTimeSheet
//
//  Created by Saurabh on 15/12/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>
@class IGPMAccounts;
@class IGApprovalTracks;
@class IGApprovalWeekend;
@interface IGApprovalParser : NSObject

-(IGPMAccounts *)parseAccountsWith:(NSDictionary *)dictAccounts;

-(IGApprovalTracks *)parseTracksWith:(NSDictionary *)dictTracks;

-(NSMutableArray *)parseWeekendDates:(NSDictionary *)dictWeekends;

-(NSMutableArray *)parseTimesheets:(NSDictionary *)dictTimeSheets;

-(NSMutableArray *)parseNonExistingimeSheets:(NSDictionary *)dictTimeSheets;

@end
