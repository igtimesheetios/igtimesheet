//
//  IGApprovalParser.m
//  IGTimeSheet
//
//  Created by Saurabh on 15/12/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import "IGApprovalParser.h"
#import "IGPMAccount.h"
#import "IGApprovalTrack.h"
#import "IGPMTimeSheet.h"

@implementation IGApprovalParser

-(NSMutableArray *)parseAccountsWith:(NSDictionary *)dictAccounts{
    
    NSMutableArray* accounts = [[NSMutableArray alloc] init];
    
    NSArray *tempAccounts = (NSArray *)[dictAccounts valueForKey:kApprovalAccounts];
    
    for (NSDictionary *dictAccount in tempAccounts) {
        
        IGAccount *account = [[IGAccount alloc] initWith:dictAccount];
        
        [accounts addObject:account];
    }
    
    tempAccounts = nil;
    
    return accounts;
}

-(IGApprovalTracks *)parseTracksWith:(NSDictionary *)dictTracks{
    IGApprovalTracks *tracks = [[IGApprovalTracks alloc] initWith:dictTracks];
    return tracks;
}

-(NSMutableArray *)parseWeekendDates:(NSDictionary *)dictWeekends{
    
    NSMutableArray *weekends = [NSMutableArray new];
    
    NSArray *tempWeekends = [dictWeekends valueForKey:kApprovalWeekends];
    
    for (NSDictionary *dictWeekend in tempWeekends) {
        
        IGApprovalWeekend *weekend = [[IGApprovalWeekend alloc] initWith:dictWeekend];
        
        [weekends addObject:weekend];
        
        weekend = nil;
    }
    
    tempWeekends = nil;
    
    return weekends;
}


-(NSMutableArray *)parseTimesheets:(NSDictionary *)dictTimeSheets{
    
    NSMutableArray *timesheets = [NSMutableArray new];
    
    NSArray *tempTimesheets = (NSArray *)[dictTimeSheets valueForKey:kApprovalTimesheetsData];

    for (NSDictionary *dictTimeSheet in tempTimesheets) {
        
        IGPMTimeSheet *pmTimesheet = [[IGPMTimeSheet alloc] initWith:dictTimeSheet];
        
        if (pmTimesheet) {
            
            [timesheets addObject:pmTimesheet];
        }
        pmTimesheet = nil;
    }

    tempTimesheets = nil;
    
    return timesheets;
}

-(NSMutableArray *)parseNonExistingimeSheets:(NSDictionary *)dictTimeSheets{
    
    NSMutableArray *timesheets = [NSMutableArray new];
    
    NSArray *tempTimesheet = (NSArray *)[dictTimeSheets valueForKey:@"Timesheet"];
    
    for (NSDictionary *dictTimeSheet in tempTimesheet) {
        
        IGNonExistingTimeSheets *neTimeSheet = [[IGNonExistingTimeSheets alloc] initWith:dictTimeSheet];
        
        [timesheets addObject:neTimeSheet];
        
        neTimeSheet = nil;
    }
    
    tempTimesheet = nil;
    
    return timesheets;

}

@end
