//
//  IGLoginVC.m
//  IGTimeSheet
//
//  Created by Manish on 09/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGLoginVC.h"
#import "NIDropDown.h"
#import "IGEmployee.h"

#import "IGCommonHeaders.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>
#define USERNAME_MAX_CHAR       50
#define PASSWORD_MAX_CHAR       20

#define TAG_ALERT_TIMESHEE_TDATA_INCORRECT          111
#define TAG_ALERT_UPDATE_RESPONSE_INCORRECT         222

#define CLEAR_TEXTFIELD         0

@interface IGLoginVC ()<UITextFieldDelegate, NIDropDownDelegate,EMSmoothAlertViewDelegate>
{
    NIDropDown *dropDown;
    UITextField *activeField;
    IGSpinKitView *spinner;
}

@property (weak, nonatomic) IBOutlet UIView *userNameView;
@property (weak, nonatomic) IBOutlet UIImageView *userImageView;
@property (weak, nonatomic) IBOutlet UIView *passwordView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordImageView;
@property (weak, nonatomic) IBOutlet UIView *domainView;
@property (weak, nonatomic) IBOutlet UIImageView *domainImageView;
@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *appLogoHeightConstraint;

@property (strong, nonatomic) NSArray *pickerDataArray;

@end

@implementation IGLoginVC

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        [self addKeyboardNotificationObserver:YES];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self addKeyboardNotificationObserver:YES];
    }
    return self;
}

- (void)dealloc
{
    [self addKeyboardNotificationObserver: NO];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.pickerDataArray = @[@"domain/igglobal"];
    
    [IGAnalytics trackScreen:@"Login iOS"];
    //Domain View Layout
    [self.domainView.layer setCornerRadius:6.0f];
    UIImage *domainImage = [UIImage imageNamed:@"login_role"];
    UIEdgeInsets  insets = UIEdgeInsetsMake(domainImage.size.height/2, domainImage.size.width/2, domainImage.size.height/2, domainImage.size.width/2);
    domainImage = [domainImage resizableImageWithCapInsets:insets];
    [self.domainImageView setImage:domainImage];
    
    //User Name View Layout
    [self.userNameView.layer setCornerRadius:6.0f];
    UIImage *userImage = [UIImage imageNamed:@"login_user"];
    insets = UIEdgeInsetsMake(userImage.size.height/2, userImage.size.width/2, userImage.size.height/2, userImage.size.width/2);
    userImage = [userImage resizableImageWithCapInsets:insets];
    [self.userImageView setImage:userImage];
    
    //Password View Layout
    [self.passwordView.layer setCornerRadius:6.0f];
    UIImage *passwordImage = [UIImage imageNamed:@"login_password"];
    insets = UIEdgeInsetsMake(passwordImage.size.height/2, passwordImage.size.width/2, passwordImage.size.height/2, passwordImage.size.width/2);
    passwordImage = [passwordImage resizableImageWithCapInsets:insets];
    [self.passwordImageView setImage:passwordImage];
    
    //Log In BUtton Layout
    [self.loginButton.layer setCornerRadius:8.0f];
    [self.loginButton.layer setBorderWidth:2.0];
    [self.loginButton.layer setBorderColor:kColorButtonShadow.CGColor];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self.userNameField hasText] && [self.passwordField hasText])
    {
        [IGUtils setupButton:self.loginButton enable:TRUE backgroundColor:kColorSigninGreen];
    }
    else
    {
        [IGUtils setupButton:self.loginButton enable:FALSE backgroundColor:nil];
    }
    
    self.contentViewWidthConstraint.constant = CGRectGetWidth(self.view.frame);
    self.contentViewHeightConstraint.constant = CGRectGetHeight(self.view.frame);
    
    if (CGRectGetHeight(self.view.frame) <= 480)
    {
        self.appLogoHeightConstraint.constant = 60;
    }
    
    [IGAnalytics trackScreen:screenLogin];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
}

- (UIStatusBarStyle)preferredStatusBarStyle
{
    return UIStatusBarStyleLightContent;
}


#pragma marks - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    activeField = textField;
    activeField.clearButtonMode = UITextFieldViewModeAlways;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    [IGUtils setupButton:self.loginButton enable:false backgroundColor:nil];
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.userNameField)
    {
        [self.passwordField becomeFirstResponder];
    }
    else if (textField == self.passwordField)
    {
        [self.passwordField resignFirstResponder];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    NSInteger length = textField.text.length + string.length - range.length;
    
    UITextField *otherTextField = self.userNameField;
    if (textField == self.userNameField)
    {
        otherTextField = self.passwordField;
    }
    
    if (length == 0 || ([otherTextField hasText] == NO))
    {
        [IGUtils setupButton:self.loginButton enable:FALSE backgroundColor:nil];
    }
    else
    {
        [IGUtils setupButton:self.loginButton enable:TRUE backgroundColor:kColorSigninGreen];
    }
    
    if ([textField isEqual:self.userNameField])
    {
        return (length <= USERNAME_MAX_CHAR);
    }
    else
    {
        return (length <= PASSWORD_MAX_CHAR);
    }
}

#pragma mark - Actions

- (IBAction)test:(UIButton *)sender
{
    [self.view endEditing:YES];
    if(dropDown == nil) {
        
        CGFloat height = 40 * self.pickerDataArray.count;
        dropDown = [[NIDropDown alloc] showDropDown:self.domainView
                                       heightOfView: height
                                    dataArrayForRow:self.pickerDataArray];
        dropDown.delegate = self;
    }
    else
    {
        [dropDown hideDropDown:self.domainView];
        dropDown = nil;
    }
}

- (IBAction)tapDetected:(UITapGestureRecognizer *)tapGesture
{
    [self.view endEditing:YES];
    
    if (dropDown != nil)
    {
        [dropDown hideDropDown:self.domainView];
        dropDown = nil;
    }
}


//TODO: To be deleted
//- (void)getTimeSheets
//{
//    kAlertAndReturnIfNetworkNotReachable;
//    [self showSpinner:YES];
//    [[IGService sharedInstance] loginDetailsWithUserName:self.userNameField.text
//                                           domain:[self.domainTextField.text substringFromIndex:7]
//        completion:^(id response)
//        {
//            [self showSpinner:NO];
//         
//            NSDictionary *jsonDictionary = (NSDictionary *)response;
////            [kAppDelegate setEmployeeId:[[[jsonDictionary[@"LoginDetail"] objectAtIndex:0] objectForKey:@"EMP_STAFFID"] integerValue]];
////            [kAppDelegate setDomain:[self.domainTextField.text substringFromIndex:7]];
////            [kAppDelegate setUserName:[[jsonDictionary[@"LoginDetail"] objectAtIndex:0]objectForKey:@"EMPLOYEE_NAME"]];
////            [kAppDelegate setLoginName:self.userNameField.text];
//            // Save Details in User defaults
//            NSDictionary *logingDetails = jsonDictionary[kIGServiceKeyLoginDetail][0];
//            IGEmployee *employee = [[IGEmployee alloc] initWithDictionary:logingDetails];
//            [IGUserDefaults sharedInstance].employee = employee;
//            //
//            
//            [kAppDelegate showSignedInScreenWithInfo:[jsonDictionary[@"TimesheetStatus"] objectAtIndex:0]];
//        }
//        error:^(NSError *error)
//        {
//            [self showSpinner:NO];
//            [IGUtils showOKAlertWithTitle:kStringError message:[error localizedDescription]];
//        }];
//}

- (IBAction)login:(id)sender
{
    if (dropDown != nil)
    {
        [dropDown hideDropDown:self.domainView];
        dropDown = nil;
        return;
    }
    
    [self resignTextFieldIfActive];
    
    //For User Name and Password Validation If Required;
    NSString *strRegExp=@"[A-Za-z0-9._]+";
    NSPredicate *userNameTest=[NSPredicate predicateWithFormat:@"SELF MATCHES %@",strRegExp];
    
    NSString *errorMessage = nil;
    NSInteger errorCode = 0;    
    if (self.userNameField.text.length == 0)
    {
        errorMessage = @"Please Enter User Name";
        errorCode = 1;
    }
    else if ([userNameTest evaluateWithObject:self.userNameField.text] == NO)
    {
        errorMessage = @"Please Enter Valid User Name";
        errorCode = 1;
    }
    else if(self.passwordField.text.length == 0)
    {
        errorMessage = @"Please Enter Password";
        errorCode = 2;
    }
    
    if (errorMessage)
    {
        [self validationError:errorMessage errorType:errorCode];
    }
    else
    {
        kAlertAndReturnIfNetworkNotReachable;
        [IGAnalytics trackButtonAction:actionLogin];
        [self showSpinner:YES];
        
        NSString *domain = [self.domainTextField.text substringFromIndex:7];
        NSString *userName = self.userNameField.text;
        
       
        [[IGService sharedInstance] lloginWithUserName:self.userNameField.text
                                              password:self.passwordField.text
                                                domain:domain
                                            completion:^(NSDictionary *userInfo, NSDictionary *timeSheetInfo) {
                                                
                                                if (timeSheetInfo != nil)
                                                {
                                                    NSLog(@"%s",__FUNCTION__);
                                                    [self showSpinner:NO];
                                                    
                                                    
                                                    [IGUserDefaults savePassword:self.passwordField.text];
                                                    [IGUserDefaults sharedInstance].domainID = domain;
                                                    [IGUserDefaults sharedInstance].userName = userName;
                                                    
                                                    if (false)
                                                    {
                                                        [kAppDelegate showMandatoryUpdateSceneWithResponse:nil];
                                                    }
                                                    else
                                                    {
                                                        if (timeSheetInfo != nil && [IGUtils isDictionary:timeSheetInfo])
                                                        {
                                                            [kAppDelegate setupRootViewControllerWithInfo:timeSheetInfo];
                                                        }
                                                        else
                                                        {
                                                            //TODO: handle error
                                                            [IGUtils showOKAlertWithTitle:@"Error" message:@"Could not fetch time sheet data. Please login again." delegate:self tag:TAG_ALERT_TIMESHEE_TDATA_INCORRECT];
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    //TODO: handle error
                                                    [IGUtils showOKAlertWithTitle:@"Error"
                                                                          message:@"Could not fetch the application versioning information. Please login again." delegate:self tag:TAG_ALERT_UPDATE_RESPONSE_INCORRECT];
                                                }                                                
                                            } error:^(NSError *error) {
                                                NSLog(@"In Login VC - Error Block");
                                                [self showSpinner:NO];
                                                [IGUtils showOKAlertWithTitle:kStringError message:[error localizedDescription]];
                                            }];
        
//TODO: To be deleted commented code
        
//        [[IGService sharedInstance] loginWithUserName:self.userNameField.text
//                                             password:self.passwordField.text
//                                               domain:domain
//            completion:^(id response)
//            {
//                [self showSpinner:NO];
//             
//                NSDictionary *jsonDictionary = (NSDictionary *)response;
//                NSString *status = [jsonDictionary objectForKey:@"LoginStatus"];
//                if ([status isEqualToString:@"Success"])
//                {
//                    
//                    
//                    
//                    
//                    NSString *base64EncodedString = [jsonDictionary objectForKey:kIGServiceKeyEmployeeImage];
//                    
//                    if (base64EncodedString != nil)
//                    {
//                        NSData *imageData = [[NSData alloc] initWithBase64EncodedString:base64EncodedString options:NSDataBase64DecodingIgnoreUnknownCharacters];
//                        if (imageData != nil)
//                        {
//                            [IGUserDefaults sharedInstance].userImage = [UIImage imageWithData:imageData];
//                        }
//                    }
//                    
//                    // Save to user defaults
//                    [IGUserDefaults savePassword:self.passwordField.text];
//                    
//                    
//                    
//                    [IGUserDefaults sharedInstance].domainID = domain;
//                    [IGUserDefaults sharedInstance].userName = userName;
//                    //
//                    
//                    [self getTimeSheets];
//                }
//                else
//                {
//#if CLEAR_TEXTFIELD
//                    self.passwordField.text = @"";
//                    [IGUtils setupButton:self.loginButton enable:FALSE backgroundColor:nil];
//#endif
//                    [IGUtils showOKAlertWithTitle:kStringError message:@"Username or Passowrd is not correct"];
//
//                }
//            }
//            error:^(NSError *error)
//            {
//                [self showSpinner:NO];
//
//#if CLEAR_TEXTFIELD
//                self.userNameField.text = @"";
//                self.passwordField.text = @"";
//                [IGUtils setupButton:self.loginButton enable:FALSE backgroundColor:nil];
//#endif
//                [IGUtils showOKAlertWithTitle:kStringError message:[error localizedDescription]];
//            }];
    }
}


#pragma mark - Form Validation

- (void)validationError:(NSString *)error errorType:(NSInteger)errorCode
{
    [IGUtils showOKAlertWithTitle:@"Credentials Error" message:error];

#if CLEAR_TEXTFIELD
    switch (errorCode)
    {
        case 1:
        {
            self.userNameField.text = nil;
            self.passwordField.text = nil;
            break;
        }
            
        case 2:
        {
            self.passwordField.text = nil;
            break;
        }
            
        default:
            break;
    }

    [IGUtils setupButton:self.loginButton enable:FALSE backgroundColor:nil];
#endif
    
    return;
}

#pragma marks - NIDropDownDelegateMethods

- (void) niDropDown:(NIDropDown *)sender
{
    dropDown = nil;
}

- (void) niDropDown:(NIDropDown *)sender didSelectRowAtIndex:(NSInteger)index
{
    self.domainTextField.text = self.pickerDataArray[index];
}


#pragma mark - KeyBoard Notifications

- (void)addKeyboardNotificationObserver:(BOOL)addObserver
{
    if (addObserver)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillShow:)
                                                     name:UIKeyboardWillShowNotification
                                                   object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(keyboardWillBeHidden:)
                                                     name:UIKeyboardWillHideNotification
                                                   object:nil];
    }
    else
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
        [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
    }
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    NSDictionary* info = [notification userInfo];
    CGRect kbRect = [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    kbRect = [self.view convertRect:kbRect fromView:nil];
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbRect.size.height, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;
    
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbRect.size.height;
    if (!CGRectContainsPoint(aRect, CGPointMake(0, CGRectGetMaxY(activeField.frame))))
    {
        [self.scrollView scrollRectToVisible:activeField.frame animated:YES];
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification
{
    UIEdgeInsets contentInsets = UIEdgeInsetsZero;
    
    [UIView animateWithDuration:0.3
                     animations:^{
                         self.scrollView.contentInset = contentInsets;
                     }];
    
    self.scrollView.scrollIndicatorInsets = contentInsets;
}

#pragma mark - Spinner

- (void)showSpinner:(BOOL)show
{
    if (show)
    {
        if (!spinner)
        {
            spinner = [[IGSpinKitView alloc] initWithColor:kDefaultBarTintColor];
            spinner.center = self.view.center;
        }

        
        if (spinner.superview == nil)
        {
            [self.view addSubview:spinner];
        }
        
        [spinner startAnimating];
    }
    else
    {
        [spinner stopAnimating];
    }
    
    self.view.userInteractionEnabled = !show;

}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch
{
    if (dropDown != nil)
    {
        CGPoint touchPoint = [touch locationInView:self.view];
        if (CGRectContainsPoint(dropDown.frame, touchPoint))
        {
            return NO;
        }
    }
    
    return YES;
}

- (void)resignTextFieldIfActive
{
    if ([self.userNameField isFirstResponder])
    {
        [self.userNameField resignFirstResponder];
    }
    else if ([self.passwordField isFirstResponder])
    {
        [self.passwordField resignFirstResponder];
    }
}

#pragma mark - EMSmoothAlertViewDelegate

-(void) alertView:(igTSmoothAlertView *)alertView didDismissWithButton:(UIButton *)button
{
    if (alertView.tag == TAG_ALERT_TIMESHEE_TDATA_INCORRECT || alertView.tag == TAG_ALERT_UPDATE_RESPONSE_INCORRECT)
    {
        //TODO: Check with incorrect data
        [[IGUserDefaults sharedInstance] logout];
    }
}

@end
