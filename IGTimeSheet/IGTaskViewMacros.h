//
//  TaskViewMacros.h
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#ifndef IGTimeSheet_TaskViewMacros_h
#define IGTimeSheet_TaskViewMacros_h

//In Out Time
#define InOutTime   @"InOutTime"

#define InMon       @"IN_MON"
#define InTue       @"IN_TUE"
#define InWed       @"IN_WED"
#define InThu       @"IN_THU"
#define InFri       @"IN_FRI"
#define InSat       @"IN_SAT"
#define InSun       @"IN_SUN"

#define OutMon      @"OUT_MON"
#define OutTue      @"OUT_TUE"
#define OutWed      @"OUT_WED"
#define OutThu      @"OUT_THU"
#define OutFri      @"OUT_FRI"
#define OutSat      @"OUT_SAT"
#define OutSun      @"OUT_SUN"

//Remarks
#define RemarkMon   @"MON_REMARKS"
#define RemarkTue   @"TUE_REMARKS"
#define RemarkWed   @"WED_REMARKS"
#define RemarkThu   @"THU_REMARKS"
#define RemarkFri   @"FRI_REMARKS"
#define RemarkSat   @"SAT_REMARKS"
#define RemarkSun   @"SUN_REMARKS"

//Total Time
#define TotalMon    @"TOT_MON"
#define TotalTue    @"TOT_TUE"
#define TotalWed    @"TOT_WED"
#define TotalThu    @"TOT_THU"
#define TotalFri    @"TOT_FRI"
#define TotalSat    @"TOT_SAT"
#define TotalSun    @"TOT_SUN"

//Week Day
#define Monday      @"Monday"
#define Tuesday     @"Tuesday"
#define Wednesday   @"Wednesday"
#define Thursday    @"Thursday"
#define Friday      @"Friday"
#define Saturday    @"Saturday"
#define Sunday      @"Sunday"


#define EmptyIn     @"-"

//Tasks dic keys
#define TasksEffort @"EffortSummary"
#define PrjName     @"ProjectName"

#define TotalEfforts    @"TotalEfforts"
#define EntryDetails    @"CardEntryDetail"
#define ProjectList     @"ProjectList"
#define ProjectId       @"ProjectId"
#define Track           @"TrackID"
#define TrackName       @"TrackName"
#define Location        @"Location"
#define HrsType         @"Type"
#define Billable        @"BillableType"
#define TaskId          @"TaskId"
#define SubtaskId       @"SubTaskId"
#define CRId            @"CRId"
#define Description     @"Description"
#define TotalHrs        @"TotalHrs"
#define WFOL            @"WFOL"
#define TimeSheetStatus @"statusid"
#define EmpCardEntryId  @"EmployeeCardEntryDetailId"
#define PayrollTaskID   @"PayrollTaskId"

#define EMon    @"Mon"
#define ETue    @"Tue"
#define EWed    @"Wed"
#define EThu    @"Thu"
#define EFri    @"Fri"
#define ESat    @"Sat"
#define ESun    @"Sun"

#endif
