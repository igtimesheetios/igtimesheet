//
//  IGViewLeavesModel.m
//  IGTimeSheet
//
//  Created by Kamal on 19/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGViewLeavesModel.h"

@implementation IGViewLeavesModel

- (id)init
{
    self = [super init];
    if(self)
    {
        self.leaveType = @"";
        self.leaveFrom = @"";
        self.leaveTo = @"";
        self.leaveFromHalf = @"";
        self.leaveToHalf = @"";
        self.status = @"";
        self.noOfDays =[[NSNumber alloc] initWithFloat:0.0];
    }
    
    return self;
}


@end
