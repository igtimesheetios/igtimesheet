//
//  IGAuthenticationVC.m
//  IGCafe
//
//  Created by Pulkit on 29/06/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGAuthenticationVC.h"
#import "RTSpinKitView.h"
#import "UIView+IGCircularView.h"
#import "IGCommonHeaders.h"
#import "IGCommonHeaders.h"
#import "igUpdateResponse.h"

#import <FirebaseAnalytics/FirebaseAnalytics.h>

#define TAG_AUTHENTICATION_FAILURE_ALERT          5555

@interface IGAuthenticationVC () <EMSmoothAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageViewLogo;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewUserImage;
@property (weak, nonatomic) IBOutlet UIView *viewWaveAnimation;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;
@property (weak, nonatomic) IBOutlet UIButton *buttonRetry;

- (IBAction)retryAuthentication:(id)sender;

@end

@implementation IGAuthenticationVC
{
    RTSpinKitView *spinner;
    AFHTTPRequestOperation *authenticationOperation;
}

- (void)dealloc
{
    if ([authenticationOperation isExecuting])
    {
        [authenticationOperation cancel];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.buttonRetry.hidden = YES;
    
    if (!spinner)
    {
        spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleWave color:[UIColor whiteColor]];
        spinner.hidesWhenStopped = YES;
        spinner.center = CGPointMake(self.viewWaveAnimation.bounds.size.width / 2, self.viewWaveAnimation.bounds.size.height / 2);
        
        if (spinner.superview == nil)
        {
            [self.viewWaveAnimation addSubview:spinner];
        }
    }
    
    [self.imageViewUserImage roundViewWithCornerRaidus:(CGRectGetWidth(self.imageViewUserImage.frame) * 0.5)
                                   borderWidth:2.0
                                   borderColor:[UIColor whiteColor]];
    self.imageViewLogo.layer.shadowColor = [UIColor darkGrayColor].CGColor;
    self.imageViewLogo.layer.shadowOpacity = 0.5f;
    self.imageViewLogo.layer.shadowOffset = CGSizeMake(0.5, 0.5);
    
    self.labelTitle.layer.shadowColor = [UIColor blackColor].CGColor;
    self.labelTitle.layer.shadowOpacity = 0.5f;
    self.labelTitle.layer.shadowOffset = CGSizeMake(0.5, 0.5);
    
    self.imageViewUserImage.backgroundColor = [IGUtils averageColorFromImage:[IGUserDefaults sharedInstance].employee.image];
    self.imageViewUserImage.image = [IGUserDefaults sharedInstance].employee.image;
    self.labelTitle.text = [IGUserDefaults sharedInstance].employee.name;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
//    [IGAnalytics trackScreen:screenAuthentication];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    if (spinner.isAnimating)
    {
        [spinner stopAnimating];
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

    [self performSelector:@selector(authenticate) withObject:nil afterDelay:0.5];
}

- (void) authenticate
{
    if ([[IGService sharedInstance] networkReachabilityAlert] == YES)
    {
        [spinner stopAnimating];
        
        self.labelMessage.text = kStringNetworkIssue;
        self.buttonRetry.hidden = NO;
        
        return;
    }
    
    __weak RTSpinKitView *weakSpinner = spinner;
    
    NSString *userName = [IGUserDefaults sharedInstance].userName;
    NSString *password = [IGUserDefaults retrievePassword];
    NSString *domain = [IGUserDefaults sharedInstance].domainID;

    
    void (^completionBlock)(NSDictionary *,NSDictionary *) = ^(NSDictionary *userInfo, NSDictionary *timeSheetInfo) {

        [weakSpinner stopAnimating];
        
        
        if (timeSheetInfo != nil)
        {
            if (false)
            {
                [kAppDelegate showMandatoryUpdateSceneWithResponse:nil];
            }
            else
            {
                
                //AutoLogin Completion block
                [FIRAnalytics setUserPropertyString:userName forName:@"UserEmail"];
                [FIRAnalytics logEventWithName:@"Login" parameters:@{kFIRParameterItemName:[IGUserDefaults sharedInstance].userName}];
                [IGAnalytics trackEventWith:@"iOS" action:@"AutoLogin" actionName:[NSString stringWithFormat:@"%@_%@",[IGUserDefaults sharedInstance].employee.employeeNewID,[IGUserDefaults sharedInstance].userName]];
                [IGAnalytics trackScreen:@"AutoLogin iOS"];
                
                AppDelegate *appDelegate = kAppDelegate;
                [appDelegate setupRootViewControllerWithInfo:timeSheetInfo];
            }
        }
    };
    
    void (^errorBlock)(NSError *) = ^(NSError *error) {
        
        [weakSpinner stopAnimating];
        if (error.code == 0)
        {
            [IGUtils showOKAlertWithTitle:kStringLoginError
                                  message:[error localizedDescription]
                                 delegate:self
                                      tag:TAG_AUTHENTICATION_FAILURE_ALERT];
        }
        else
        {
            self.labelMessage.text = [error localizedDescription];
            self.buttonRetry.hidden = NO;
        }
    };

    [spinner startAnimating];
    self.labelMessage.text = kStringAuthenticating;
    
//TODO: Finish up changin the implementaion and un comment
    [[IGService sharedInstance] lloginWithUserName:userName
                                          password:password
                                            domain:domain
                                        completion:completionBlock
                                             error:errorBlock];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)retryAuthentication:(id)sender
{
    self.buttonRetry.hidden = YES;
    [self authenticate];
}

#pragma mark - EMSmoothAlertViewDelegate

- (void)alertView:(igTSmoothAlertView *)alertView didDismissWithButton:(UIButton *)button
{
    if (alertView.tag == TAG_AUTHENTICATION_FAILURE_ALERT)
    {
        if ([button isEqual:alertView.defaultButton])
        {
            [IGUserDefaults sharedInstance].isUserLoggedIn = NO;
            AppDelegate *appDelegate = kAppDelegate;
            [appDelegate setupRootViewControllerWithInfo:nil];
        }
    }
}


@end
