//
//  TaskModalClass.m
//  TaskView
//
//  Created by Hem on 10/02/15.
//  Copyright (c) 2015 Hem. All rights reserved.
//

#import "IGTaskModalClass.h"

@implementation IGTaskModalClass

@synthesize dateText, projectText, description, totalTime , prjId, taskId, subTaskId, monTime, tueTime, wedTime, thuTime, friTime, satTime, sunTime,hrsType, cardEntryDetailId, cRID, payrollTaskId , billable, wfol, timeSheetStatus,categoryID,isUSEmployeeBillable;

@end
