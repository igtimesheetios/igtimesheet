//
//  IGUserDefaults.m
//  IGTimeSheet
//
//  Created by Manish on 06/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGUserDefaults.h"
#import "IGCommonHeaders.h"
#import <Instabug/Instabug.h>

#define kKeyPassword                    @"password"
#define kInstabugToken                  @"9d6eb6576b73cef8eeee4b346e9e0cbc"

@interface IGUserDefaults()
{
    IGEmployee *_employee;
}

@property (nonatomic, strong) NSString *password;
@property (nonatomic, assign) BOOL hasUserProfilePic;

@end

@implementation IGUserDefaults

@synthesize employee = _employee;

+ (instancetype)sharedInstance;
{
    static dispatch_once_t once;
    static id sharedInstance = nil;
    
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

+ (void)savePassword:(NSString *)password
{
    //TODO: save to keychain
    if (password == nil)
    {
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:kKeyPassword];
    }
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:password forKey:kKeyPassword];
    }
    
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString *)retrievePassword
{
    //TODO: retrieve from keychain
    return [[NSUserDefaults standardUserDefaults] objectForKey:kKeyPassword];
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        
        [userDefaults setBool:NO forKey:@"WebKitDiskImageCacheEnabled"];

//        _userImage = [UIImage imageNamed:@"profile-avtaar-pic"];
        
        _isUserLoggedIn = [userDefaults boolForKey:kKeyUserDefaultsIsUserLoggedIn];
        _hasAcceptedTermsAndConditions = [userDefaults boolForKey:kKeyUserDefaultsAcceptTerms];
        _isFirstTimeLogin = [userDefaults boolForKey:kKeyUserDefaultsFirstTimeLogin];
        _domainID = [userDefaults objectForKey:kIGServiceKeyDomainID];
        _userName = [userDefaults objectForKey:kIGServiceKeyUserName];
        _isDeviceTokenRegistered = [userDefaults boolForKey:kKeyUserDefaultsIsDeviceTokenRegistered];
    }
    
    return self;
}

- (void)setEmployee:(IGEmployee *)employee
{
    _employee = employee;
    [self saveUserToUserDefaults];
}

- (IGEmployee *)employee
{
    if (_employee == nil)
    {
        _employee = [[IGEmployee alloc] init];
        [self retrieveUserFromUserDefaults];
        [self retrieveUserImage];
    }
    
    return _employee;
}

- (void)setIsUserLoggedIn:(BOOL)isUserLoggedIn
{
    if (_isUserLoggedIn != isUserLoggedIn)
    {
        _isUserLoggedIn = isUserLoggedIn;
        [[NSUserDefaults standardUserDefaults] setBool:isUserLoggedIn forKey:kKeyUserDefaultsIsUserLoggedIn];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)setUserName:(NSString *)userName
{
    if ([_userName isEqualToString:userName] == NO)
    {
        _userName = userName;
        [[NSUserDefaults standardUserDefaults] setObject:_userName forKey:kIGServiceKeyUserName];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

/*
  Function: (void)setIfUserUSEmpoyee:(NSString *)isUSEmployee
  parms: isUSEmployee if user is US employee
 Description: function is used to set the employee location to nsuser default to check latyer inside the app.
 */

- (void)setIfUserUSEmpoyee:(NSInteger)isUSEmployee
{
    if (self.ifUserUSEmployee != isUSEmployee)
    {
        self.ifUserUSEmployee  = isUSEmployee;
        [[NSUserDefaults standardUserDefaults] setInteger:self.ifUserUSEmployee forKey:kIGServiceKeyisUserUSEmp];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)setDomainID:(NSString *)domainID
{
    if ([_domainID isEqualToString:domainID] == NO)
    {
        _domainID = domainID;
        [[NSUserDefaults standardUserDefaults] setObject:_domainID forKey:kIGServiceKeyDomainID];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)setHasAcceptedTermsAndConditions:(BOOL)hasAcceptedTermsAndConditions
{
    if (_hasAcceptedTermsAndConditions != hasAcceptedTermsAndConditions)
    {
        _hasAcceptedTermsAndConditions = hasAcceptedTermsAndConditions;
        [[NSUserDefaults standardUserDefaults] setBool:hasAcceptedTermsAndConditions forKey:kKeyUserDefaultsAcceptTerms];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void)setIsFirstTimeLogin:(BOOL)isFirstTimeLogin
{
    if (_isFirstTimeLogin != isFirstTimeLogin)
    {
        _isFirstTimeLogin = isFirstTimeLogin;
        [[NSUserDefaults standardUserDefaults] setBool:isFirstTimeLogin forKey:kKeyUserDefaultsFirstTimeLogin];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

- (void) setIsDeviceTokenRegistered:(BOOL)isDeviceTokenRegistered
{
    if (_isDeviceTokenRegistered != isDeviceTokenRegistered)
    {
        _isDeviceTokenRegistered = isDeviceTokenRegistered;
        [[NSUserDefaults standardUserDefaults] setBool:isDeviceTokenRegistered forKey:kKeyUserDefaultsIsDeviceTokenRegistered];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

//TODO: Persist data if required

- (void)setUserImage:(UIImage *)userImage
{
    if (userImage == nil)
    {
        _userImage = [UIImage imageNamed:@"profile-avtaar-pic"];
    }
    else
    {
        _userImage = userImage;
        [self saveUserImage];
    }
}

#pragma mark -

- (BOOL)loginWithUser:(NSDictionary *)userInfo updateInfo:(BOOL)update
{
    if ([userInfo respondsToSelector:@selector(objectForKey:)] &&
        (userInfo.allKeys.count != 0))
    {
        self.isUserLoggedIn = YES;
        
        if (self.hasAcceptedTermsAndConditions)
        {
            self.hasAcceptedTermsAndConditions = YES;
        }
        else
        {
            self.hasAcceptedTermsAndConditions = NO;
        }
        
        if (self.employee.isAuthenticated)
        {
            self.employee.isAuthenticated = YES;
        }
        else
        {
            self.employee.isAuthenticated = NO;
        }
        
        // retrieve user object
        IGEmployee *employee = self.employee;
        if (update)
        {
            // set values
            employee.name = userInfo[kIGServiceKeyEmployeeName];
            employee.staffID = userInfo[kIGServiceKeyEmployeeStaffID];
            employee.mailID = userInfo[kIGServiceKeyEmployeeMailID];
            
            employee.isContractor = [userInfo[kIGServiceKeyIsContractor] isEqualToString:@"0"]? NO : YES;
            employee.isPMRole = [[userInfo valueForKey:@"ISPM"] boolValue];
            employee.isUSEmployee = [userInfo[kIGServiceKeyisUserUSEmp] isEqualToString:@"0"]? NO : YES;
        
            [self saveUserImage];
            [self retrieveUserImage];
            
            // save to userDefaults
            [self saveUserToUserDefaults];
        }
        employee.isAuthenticated = YES;
        
        [self configureInstabug];
        return YES;
    }
    
    return NO;
}

- (void)configureInstabug
{
    /* Instabug */
    static dispatch_once_t once;
    dispatch_once(&once, ^{
        [Instabug startWithToken:kInstabugToken captureSource:IBGCaptureSourceUIKit invocationEvent:IBGInvocationEventNone];
        [Instabug setFeedbackSentAlertText:kStringMessageThankYouForYourValuebleFeedback];
        [Instabug setWillShowEmailField:NO];
        [Instabug setColorTheme:IBGColorThemeBlue];
        [Instabug setPromptPrimaryColor:kDefaultBarTintColor];
        [Instabug setPromptSecondaryColor:[UIColor whiteColor]];
        [Instabug setHeaderColor:[UIColor colorWithPatternImage:[UIImage imageNamed:kImageScreenBG]]];
        [Instabug setiPhoneShakingThreshold:1.25f];
    });
    
    if (self.isUserLoggedIn)
    {
        [Instabug setInvocationEvent:IBGInvocationEventShake];
        [Instabug setDefaultEmail:self.employee.mailID];
    }
    else
    {
        [Instabug setInvocationEvent:IBGInvocationEventNone];
        [Instabug setDefaultEmail:@""];
    }
}

- (void)logout
{
    AppDelegate *appDelegate = kAppDelegate;
    
    @synchronized (self)
    {
        
        self.isUserLoggedIn = NO;
        self.hasAcceptedTermsAndConditions = NO;
        
        [self deleteUserInformation];
        
        
        //TODO: Password to be handled in KeyChain
        [IGUserDefaults savePassword:nil];
    }
    
    [appDelegate setupRootViewControllerWithInfo:nil];
}


- (void)deleteUserInformation
{
    // update image for user before resetting user object
    self.employee.image = nil;
    [self saveUserImage];
    self.userImage = nil;
    
    // reset after updating image
    [self.employee reset];
    [self saveUserToUserDefaults];
    
}

- (void)saveUserToUserDefaults
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setObject:_employee.name forKey:kKeyUserDefaultsEmployeeName];
    [userDefaults setObject:_employee.staffID forKey:kKeyUserDefaultsEmployeeID];
    [userDefaults setObject:_employee.mailID forKey:kKeyUserDefaultsEmployeeEmail];
    [userDefaults synchronize];
}

- (void)retrieveUserFromUserDefaults
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    _employee.name = [userDefaults objectForKey:kKeyUserDefaultsEmployeeName];
    _employee.staffID = [userDefaults objectForKey:kKeyUserDefaultsEmployeeID];
    _employee.mailID = [userDefaults objectForKey:kKeyUserDefaultsEmployeeEmail];
}

- (void)saveUserImage
{
    if ([IGUtils isValidString:_employee.staffID])
    {
        NSString *fileName = _employee.staffID;
        igDirectory directory = igDocumentDirectory;
        
        if (self.userImage != nil)
        {
            NSData *data = UIImagePNGRepresentation(self.userImage);
            NSError *error = nil;
            if(![IGFileManager writeData:data fileName:fileName directory:directory error:&error])
            {
                NSLog(@"%s : Error : %@", __FUNCTION__, [error localizedDescription]);
            }
            self.hasUserProfilePic = YES;
        }
        else
        {
            self.userImage = [IGUserDefaults defaultProfilePic];
            [IGFileManager deleteFileNamed:fileName directory:directory];
            self.hasUserProfilePic = NO;
        }
    }
}

- (void)retrieveUserImage
{
    UIImage *image = nil;
    
    if ([IGUtils isValidString:_employee.staffID])
    {
        NSString *imageName = _employee.staffID;
        image = [IGFileManager imageWithName:imageName directory:igDocumentDirectory];
    }
    
    if (image)
    {
        _employee.image = image;
        self.hasUserProfilePic = YES;
    }
    else
    {
        _employee.image = [IGUserDefaults defaultProfilePic];
        self.hasUserProfilePic = NO;
    }
}

+ (UIImage *)defaultProfilePic
{
    UIImage *image = [UIImage imageNamed:kUserDefaultImage];
    NSAssert(image != nil, kStringMessageDefaultProfilePicNotFound);
    return image;
}

//TODO: Uncomment when token updatation service is up
- (BOOL) updateDeviceTokenOnServer
{
    AppDelegate *appDelegate = kAppDelegate;
    
    if(self.isUserLoggedIn && [IGUtils isValidString:appDelegate.deviceTokenString] && !_isDeviceTokenRegistered)
    {
        [[IGService sharedInstance] registerDeviceWithToken:appDelegate.deviceTokenString
                                                 deviceType:@"iOS"
                                                 employeeID:[IGUserDefaults sharedInstance].employee.staffID
                                                 completion:^(id response) {
                                                     
                                                     //TODO: Handle Response once service is updated
                                                     self.isDeviceTokenRegistered = YES;
                                                 }
                                                      error:^(NSError *error) {
                                                          
                                                          self.isDeviceTokenRegistered = NO;
                                                      }];
        return YES;
    }
    else
    {
        self.isDeviceTokenRegistered = NO;
        return NO;
    }
}


@end
