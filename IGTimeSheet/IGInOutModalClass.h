//
//  InOutModalClass.h
//  TaskView
//
//  Created by Hem on 10/02/15.
//  Copyright (c) 2015 Hem. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGInOutModalClass : NSObject

@property(strong) NSString* dateText;
@property(strong) NSString* inTimeText;
@property(strong) NSString* outTimeText;
@property(strong) NSString* remarks;
@property(atomic) BOOL buttonvisible;

@property (nonatomic, strong) NSString *totalWorkHours;

@property (nonatomic, strong) NSString *day;

@end
