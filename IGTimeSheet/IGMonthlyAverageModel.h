//
//  MonthlyAverageModel.h
//  IGTimeSheet
//
//  Created by Manish on 18/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

//typedef NS_ENUM(NSInteger, IGMonthlyAverageType)
//{
//    IGMonthlyAverageTypeTimesheet,
//    IGMonthlyAverageTypeInOut
//};
//

@class IGHourSummary;

@interface IGMonthlyAverageModel : NSObject

@property (nonatomic, readonly) IGHourSummary *inOutHours;
@property (nonatomic, readonly) IGHourSummary *timeSheetHours;

- (id)initWithDictionary:(NSDictionary *)monthlyAverageInfoDictionary;

@end
