//
//  IGViewLeavesParser.m
//  IGTimeSheet
//
//  Created by Kamal on 19/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGViewLeavesParser.h"
#import "IGViewLeavesModel.h"

#define kLeaveType                           @"SHORT_NAME"
#define kLeaveFrom                           @"LEAVE_FROM"
#define kLeaveTo                             @"LEAVE_TO"
#define kLeaveFromHalf                       @"LEAVE_FROM_HALF"
#define kLeaveToHalf                         @"LEAVE_TO_HALF"
#define kLeaveStatus                         @"STATUS"
#define kLeaveDays                           @"NO_DAYS"

@implementation IGViewLeavesParser

-(NSArray *)parseLeaveViewData:(NSArray *)leavesArray
{
    NSMutableArray* pLeaveViewArray = [[NSMutableArray alloc] init];
    for (int index = 0; index < leavesArray.count; index++) {
        NSDictionary *dictionary = leavesArray[index];
        IGViewLeavesModel *modelObject = [[IGViewLeavesModel alloc] init];
        modelObject.leaveType = [dictionary objectForKey:kLeaveType];
        modelObject.leaveFrom = [dictionary objectForKey:kLeaveFrom];
        modelObject.leaveTo = [dictionary objectForKey:kLeaveTo];
        modelObject.leaveFromHalf = [dictionary objectForKey:kLeaveFromHalf];
        modelObject.leaveToHalf = [dictionary objectForKey:kLeaveToHalf];
        modelObject.status = [dictionary objectForKey:kLeaveStatus];
        modelObject.noOfDays = [dictionary objectForKey:kLeaveDays];
        [pLeaveViewArray addObject:modelObject];
    }
    return pLeaveViewArray;
}

@end
