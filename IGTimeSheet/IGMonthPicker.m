//
//  IGMonthPickerViewController.m
//  IGTimeSheet
//
//  Created by Kamal on 27/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGMonthPicker.h"
#import "IGUtils.h"

#define IG_MAX_YEAR_COUNT                       5

@interface IGMonthPicker() <UIPickerViewDataSource, UIPickerViewDelegate>
{
    NSInteger yearString;
    NSInteger selectedMonthIndex;
}

@property (nonatomic, strong) NSArray *monthArray;

- (IBAction)doneButtonAction:(id)sender;
- (IBAction)cancelButtonAction:(id)sender;


@end


@implementation IGMonthPicker

+ (IGMonthPicker *)picker
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"IGMonthPicker" owner:nil options:nil];
    IGMonthPicker *picker = (IGMonthPicker *)views[0];
    NSAssert([picker isKindOfClass:[IGMonthPicker class]], @"Expected a view of type IGPicker");
    return picker;
}

- (void)presentOnView:(UIView *)view animated:(BOOL)animated;
{
    [self addToView:view];
    [self show:YES animated:animated];
}


#pragma mark -

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self commonInitialization];
        
    }
    return self;
}


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
//     setup picker data for month and year
        self.monthArray = @[
                            @"January",
                            @"February",
                            @"March",
                            @"April",
                            @"May",
                            @"June",
                            @"July",
                            @"August",
                            @"September",
                            @"October",
                            @"November",
                            @"December"
                            ];
}


#pragma mark -

- (void)setSelectedYear:(NSString *)year
{
    yearString = [year integerValue];
    NSLog(@"%ld", (long)yearString);
    _selectedYear = [@(yearString) stringValue];
}

- (NSInteger)month
{
    return (selectedMonthIndex + 1);
}


#pragma mark -

- (void)awakeFromNib
{
    self.translatesAutoresizingMaskIntoConstraints = NO;
    self.containerViewBottomConstraint.constant = - self.containerView.frame.size.height;
}

- (void)addToView:(UIView *)view;
{
    [view addSubview:self];
    
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:self
                                                                     attribute:NSLayoutAttributeTop
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:view
                                                                     attribute:NSLayoutAttributeTop
                                                                    multiplier:1
                                                                      constant:0];
    
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:self
                                                                        attribute:NSLayoutAttributeBottom
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:view
                                                                        attribute:NSLayoutAttributeBottom
                                                                       multiplier:1
                                                                         constant:0];
    
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:self
                                                                      attribute:NSLayoutAttributeLeading
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:view
                                                                      attribute:NSLayoutAttributeLeading
                                                                     multiplier:1
                                                                       constant:0];
    
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:self
                                                                       attribute:NSLayoutAttributeTrailing
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:view
                                                                       attribute:NSLayoutAttributeTrailing
                                                                      multiplier:1
                                                                        constant:0];
    
    [view addConstraints:@[topConstraint, bottomConstraint, leftConstraint, rightConstraint]];
}

- (void)show:(BOOL)show animated:(BOOL)animated
{
    if (show)
    {
        [self.pickerView reloadAllComponents];
            [self.pickerView selectRow:selectedMonthIndex inComponent:0 animated:NO];
        
        if (animated)
        {
            self.containerViewBottomConstraint.constant = 0;
            [self setNeedsUpdateConstraints];
            self.alpha = 0.0f;
            [UIView animateWithDuration:0.3
                             animations:^{
                                 self.alpha = 1.0f;
                                 [self layoutIfNeeded];
                             }];
        }
        else
        {
            self.containerViewBottomConstraint.constant = 0;
        }
    }
    else
    {
        if (animated)
        {
            self.containerViewBottomConstraint.constant = -self.containerView.frame.size.height;
            [self setNeedsUpdateConstraints];
            [UIView animateWithDuration:0.3
                             animations:^{
                                 self.alpha = 0.0f;
                                 [self layoutIfNeeded];
                             } completion:^(BOOL finished) {
                                 //                                 [self removeFromSuperview];
                             }];
        }
        else
        {
            [self removeFromSuperview];
        }
    }
}

- (IBAction)cancelButtonAction:(id)sender
{
    [self show:NO animated:YES];
}

- (IBAction)doneButtonAction:(id)sender
{
    [self show:NO animated:YES];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(monthPicker:)])
    {
        [self.delegate monthPicker:self];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self show:NO animated:YES];
}


#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component)
    {
        case 0:
        {
            NSDate *dateNow = [NSDate date];
            NSString *year = [IGUtils stringFromDate:dateNow dateFormat:@"yyyy"];
            if (yearString == [year integerValue])
            {
                NSString *monthString = [IGUtils stringFromDate:dateNow dateFormat:@"MM"];
                return [monthString integerValue];
            }
            else
                return 12;
        }
        default:
        {
            NSAssert1(0, @"Unrecognized Component %ld", (long)component);
            break;
        }
    }
    
    return 0;
}


#pragma mark UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component)
    {
        case 0:
        {
            return [self monthForRow:row];
        }
        default:
        {
            break;
        }
    }
    
    NSAssert1(0, @"Unrecognized Component %ld", (long)component);
    return @"-";
}

- (NSString *)monthForRow:(NSInteger)row
{
    NSAssert(row < self.monthArray.count, @"Selected Row Index is greater than month array count");
    return self.monthArray[row];
}

@end
