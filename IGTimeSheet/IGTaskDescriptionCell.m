//
//  IGTaskDescriptionCell.m
//  IGTimeSheet
//
//  Created by Neha on 19/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGTaskDescriptionCell.h"

@implementation IGTaskDescriptionCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
