//
//  IGWeeklyTimesheetSummary.h
//  IGTimeSheet
//
//  Created by Manish on 23/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGWeeklyTimesheetStatus : NSObject

@property (nonatomic, readonly) NSString *status;
@property (nonatomic, readonly) NSString *projectMangerStatus;

- (instancetype)initWithDictionary:(NSDictionary *)timesheetStatusInfo;

@end


@interface IGWeeklyEffort : NSObject

@property (nonatomic, readonly) NSString *date;
@property (nonatomic, readonly) NSString *weekDay;
@property (nonatomic, readonly) NSString *order;
@property (nonatomic, readonly) NSNumber *effort;

- (instancetype)initWithEffort:(NSDictionary *)effortInfo;

@end


@interface IGWeeklyTimesheetSummary : NSObject

@property (nonatomic, readonly) IGWeeklyTimesheetStatus *timesheetStatus;
@property (nonatomic, readonly) NSInteger weekID;

// An array containing object of type IGWeeklyEffort
@property (nonatomic, readonly) NSArray *timesheetSummary;

- (instancetype)initWithDictionary:(NSDictionary *)weeklyTimesheetInfo;

@end
