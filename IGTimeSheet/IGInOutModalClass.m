//
//  InOutModalClass.m
//  TaskView
//
//  Created by Hem on 10/02/15.
//  Copyright (c) 2015 Hem. All rights reserved.
//

#import "IGInOutModalClass.h"

@implementation IGInOutModalClass

@synthesize dateText, inTimeText, outTimeText, buttonvisible, remarks;

- (NSString *)description
{
    return [NSString stringWithFormat:@"Date: %@, Day: %@, in: %@, out: %@, visible: %d, remarks: %@, total: %@", self.dateText, self.day, self.inTimeText, self.outTimeText, self.buttonvisible, self.remarks, self.totalWorkHours];
}

@end
