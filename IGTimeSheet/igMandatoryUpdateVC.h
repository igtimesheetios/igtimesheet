//
//  igMandatoryUpdateVC.h
//  IGCafe
//
//  Created by Pulkit on 27/07/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "igUpdateResponse.h"
#import "IGCommonHeaders.h"

@interface igMandatoryUpdateVC : UIViewController

@property (nonatomic, strong) igUpdateResponse *updateResponse;
@property (nonatomic, strong) NSString *updateDescriptionString;

@end
