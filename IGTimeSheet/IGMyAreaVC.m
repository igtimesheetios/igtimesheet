//
//  IGMyAreaVC.m
//  IGTimeSheet
//
//  Created by Neha on 12/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGMyAreaVC.h"
#import "IGDatePickerCell.h"
#import "IGWeekSummaryCell.h"
#import "IGMonthSummaryCell.h"
#import "IGMyAreaCellType1.h"
#import "IGMyAreaCellType2.h"
#import "IGYearPicker.h"
#import "IGMonthPicker.h"
#import "IGWeekEndPicker.h"

#import "IGMyAreaModel.h"
#import "IGEmployee.h"
#import "IGWeeklyTimesheetSummary.h"
#import "IGMonthlyTimesheetSummary.h"
#import "IGTaskDetailVC.h"

#import "IGCommonHeaders.h"


#define kReuseIdentifierCellType1     @"IGTableViewCellType1"
#define kReuseIdentifierCellType2     @"IGTableViewCellType2"
#define kReuseIdentifierCellType3     @"IGTableViewCellType3"
#define kReuseIdentifierCellType4     @"IGTableViewCellType4"
#define kReuseIdentifierCellType5     @"IGTableViewCellType5"

#define kIGMyAreaSectionHeight              50.0

#define kIGMyAreaTest                       0


@interface IGMyAreaVC () <UIToolbarDelegate, UITableViewDataSource,
UITableViewDelegate, IGWeekSumamryCellDelegate>
{
    BOOL flag;
    NSString *yearString;
    NSString *monthString;
    NSString *weekDate;
    
    IGYearPicker *yearPicker;
    IGMonthPicker *monthPicker;
    IGWeekEndPicker *weekPicker;
    IGSpinKitView *spinner;
}

@property IBOutlet UISegmentedControl* segmentControl;
@property IBOutlet UITableView* tableView;

@property (nonatomic, strong) IGMyAreaModel *myAreaModel;

@property (strong, nonatomic) NSMutableArray *weekEndArray;

@property (strong, nonatomic) NSArray *monthArray;

- (IBAction)segmentSwitch:(id)sender;
//- (void)addSeperatorLineToCell:(UITableViewCell*)cell indent:(bool)hasIndent;

@end

@implementation IGMyAreaVC

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    if (self.myAreaModel == nil)
    {
        self.myAreaModel = [[IGMyAreaModel alloc] init];
        
#if kIGMyAreaTest
        self.myAreaModel.employeeID = 2110716;
        self.myAreaModel.queryDate = [NSDate dateWithTimeIntervalSinceNow:-(24*60*60*30*4)];
#else
        self.myAreaModel.employeeID = [[IGUserDefaults sharedInstance].employee.staffID integerValue];
        self.myAreaModel.queryDate = [NSDate date];
#endif
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self fetchDataForSegment:self.segmentControl.selectedSegmentIndex];
    
    [self setupOpaqueNavigationBar];
    [self setupBackButton];
    flag = TRUE;
    self.weekEndArray = [[NSMutableArray alloc] initWithCapacity:1];
    yearString = [IGUtils stringFromDate:[NSDate date] dateFormat:@"yyyy"];
    monthString = [IGUtils stringFromDate:[NSDate date] dateFormat:@"MM"];
    [self fetchWeekEndDateFromServer];
    //TODO: remove month array
    self.monthArray = @[
                        @"Jan",
                        @"Feb",
                        @"Mar",
                        @"Apr",
                        @"May",
                        @"Jun",
                        @"Jul",
                        @"Aug",
                        @"Sep",
                        @"Oct",
                        @"Nov",
                        @"Dec"
                        ];
    
    
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    self.tableView.contentInset = UIEdgeInsetsMake(0, 0, 10, 0);
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [IGAnalytics trackScreen:screenMyArea];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - Actions

- (IBAction)segmentSwitch:(UISegmentedControl *)sender
{
    [self fetchDataForSegment:sender.selectedSegmentIndex];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.segmentControl != nil)
    {
        switch (self.segmentControl.selectedSegmentIndex)
        {
            case 0:
            {
                return 3;
            }
                
            case 1:
            {
                return 3;
            }
                
            default:
            {
                return 0;
            }
        }
    }
    
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.segmentControl != nil)
    {
        switch (self.segmentControl.selectedSegmentIndex)
        {
            case 0:
            {
                switch (section)
                {
                    case 0:
                    {
                        return 3;
                    }
                        
                    case 1:
                    {
                        if (self.myAreaModel.weeklyTimesheetSummary != nil)
                        {
                            return 2;
                        }
                        return 0;
                    }
                        
                    case 2:
                    {
                        return self.myAreaModel.weeklyTimesheetSummary.timesheetSummary.count;
                    }
                        
                    default:
                    {
                        return 0;
                    }
                }
                
                break;
            }
                
            case 1:
            {
                switch (section)
                {
                    case 0:
                    {
                        return 3;
                    }
                        
                    case 1:
                    {
                        return self.myAreaModel.monthlyTimesheetSummary.count;;
                    }
                        
                    default:
                    {
                        return 0;
                    }
                }
                
                break;
            }
                
            default:
            {
                return 0;
            }
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (self.segmentControl.selectedSegmentIndex)
    {
        case 0:
        {
            return [self weeklySummaryCellAtIndexPath:indexPath tableView:tableView];
        }
            
        case 1:
        {
            return [self monthlySummaryCellAtIndexPath:indexPath tableView:tableView];
        }
            
        default:
        {
            return nil;
        }
    }
    
    return nil;
}

- (UITableViewCell *)weeklySummaryCellAtIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    switch (indexPath.section)
    {
        case 0:
        {
            if (indexPath.row == 0)
            {
                return [self cellType1ForTable:tableView indexPath:indexPath key:@"Year" value:yearString];
            }
            else if (indexPath.row == 1)
            {
                if (monthString != nil)
                {
                    return [self cellType1ForTable:tableView indexPath:indexPath key:@"Month" value:self.monthArray[[monthString integerValue] - 1]];
                }
                else
                {
                    return [self cellType1ForTable:tableView indexPath:indexPath key:@"Month" value:@"Select"];
                }
            }
            else if (indexPath.row == 2)
            {
                if (weekDate != nil)
                {
                    return [self cellType2ForTable:tableView indexPath:indexPath key:@"WeekEnd" value:weekDate];
                }
                else
                {
                    return [self cellType2ForTable:tableView indexPath:indexPath key:@"WeekEnd" value:@"Select"];
                }
            }

            break;
        }
            
        case 1:
        {
            NSString *key = @"---";
            NSString *value = @"---";
            switch (indexPath.row)
            {
                case 0:
                {
                    key = @"Status";
                    value = self.myAreaModel.weeklyTimesheetSummary.timesheetStatus.status;
                    break;
                }
                    
                case 1:
                {
                    key = @"Status by RM";
                    value = self.myAreaModel.weeklyTimesheetSummary.timesheetStatus.projectMangerStatus;
                }
                    
                default:
                {
                    break;
                }
            }
            
            UITableViewCell *cell = [self cellType3ForTable:tableView indexPath:indexPath key:key value:value];
            
            if(indexPath.row == 1)
            {
                [self addSeperatorLineToCell:cell indent:NO];
            }
            else
            {
                [self addSeperatorLineToCell:cell indent:YES];
            }
            
            return cell;
        }
            
        case 2:
        {
            IGWeeklyEffort *weeklyEffort = nil;
            
            if (indexPath.row < self.myAreaModel.weeklyTimesheetSummary.timesheetSummary.count)
            {
                weeklyEffort = self.myAreaModel.weeklyTimesheetSummary.timesheetSummary[indexPath.row];
            }
            
            return [self cellType4ForTable:tableView indexPath:indexPath value:weeklyEffort];
        }
            
        default:
        {
            return nil;
        }
    }
    
    return nil;
    
}

- (UITableViewCell *)monthlySummaryCellAtIndexPath:(NSIndexPath *)indexPath tableView:(UITableView *)tableView
{
    switch (indexPath.section)
    {
        case 0:
        {
            if (indexPath.row == 0)
            {
                return [self cellType1ForTable:tableView indexPath:indexPath key:@"Year" value:yearString];
            }
            else if (indexPath.row == 1)
            {
                if (monthString != nil)
                {
                    return [self cellType1ForTable:tableView indexPath:indexPath key:@"Month" value:self.monthArray[[monthString integerValue] - 1]];
                }
                else
                {
                    return [self cellType1ForTable:tableView indexPath:indexPath key:@"Month" value:@"Select"];
                }
            }
            else if (indexPath.row == 2)
            {
                if (weekDate != nil)
                {
                    return [self cellType2ForTable:tableView indexPath:indexPath key:@"WeekEnd" value:weekDate];
                }
                else
                {
                    return [self cellType2ForTable:tableView indexPath:indexPath key:@"WeekEnd" value:@"Select"];
                }
            }
            break;
        }
            
        case 1:
        {
            NSArray *monthlyTimesheetSummary = self.myAreaModel.monthlyTimesheetSummary;
            
            IGMonthlyTimesheetSummary *monthySummary = monthlyTimesheetSummary[indexPath.row];
            
            return [self cellType5ForTable:tableView indexPath:indexPath value:monthySummary];
        }
            
        default:
        {
            return nil;
        }
    }
    
    return nil;
}

- (UITableViewCell *)cellType1ForTable:(UITableView *)tableView
                             indexPath:(NSIndexPath *)indexPath
                                   key:(NSString *)key
                                 value:(NSString *)value
{
    IGMyAreaCellType1 *cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifierCellType1
                                                              forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    cell.labelTitle.text = key;
    cell.yearData.text = value;
    return cell;
}

- (UITableViewCell *)cellType2ForTable:(UITableView *)tableView
                             indexPath:(NSIndexPath *)indexPath
                                   key:(NSString *)key
                                 value:(NSString *)value
{
    IGMyAreaCellType2 *cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifierCellType2
                                                              forIndexPath:indexPath];
    cell.weekEndDate.text = value;
    cell.selectionStyle = UITableViewCellSelectionStyleDefault;
    return cell;
}

- (UITableViewCell *)cellType3ForTable:(UITableView *)tableView
                             indexPath:(NSIndexPath *)indexPath
                                   key:(NSString *)key
                                 value:(NSString *)value
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifierCellType3
                                                            forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    UILabel *label = (UILabel *)[cell viewWithTag:1000];
    NSAssert([label isKindOfClass:[UILabel class]], @"Expected a UILabel for tag 1000");
    label.text = key;
    
    label = (UILabel *)[cell viewWithTag:1001];
    NSAssert([label isKindOfClass:[UILabel class]], @"Expected a UILabel for tag 1001");
    label.text = value;
    
    UIColor *valueColor = kColorGrayType1;
    
    if (([value caseInsensitiveCompare:@"Approved"] == NSOrderedSame) ||
        ([value caseInsensitiveCompare:@"Completed"] == NSOrderedSame))
    {
        valueColor = kColorGreen;
    }
    else if ([value caseInsensitiveCompare:@"Pending"] == NSOrderedSame)
    {
        valueColor = [UIColor orangeColor];
    }
    else if ([value caseInsensitiveCompare:@"Rejected"] == NSOrderedSame)
    {
        valueColor = [UIColor redColor];
    }
    label.textColor = valueColor;
    
    
    return cell;
}

- (UITableViewCell *)cellType4ForTable:(UITableView *)tableView
                             indexPath:(NSIndexPath *)indexPath
                                 value:(IGWeeklyEffort *)weeklyEffort
{
    IGWeekSummaryCell *cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifierCellType4
                                                              forIndexPath:indexPath];
    NSAssert1([cell isKindOfClass:[IGWeekSummaryCell class]], @"Expected a cell of type %@", NSStringFromClass([IGWeekSummaryCell class]));
    
    cell.date.text = weeklyEffort.date;
    cell.weekday.text = weeklyEffort.weekDay;
    cell.efforts.text = [NSString stringWithFormat:@"%ld", (long)[weeklyEffort.effort integerValue]];
    cell.delegate = self;
    
    return cell;
}

- (UITableViewCell *)cellType5ForTable:(UITableView *)tableView
                             indexPath:(NSIndexPath *)indexPath
                                 value:(IGMonthlyTimesheetSummary *)monthlyTimesheetSummary
{
    IGMonthSummaryCell *cell = [tableView dequeueReusableCellWithIdentifier:kReuseIdentifierCellType5
                                                               forIndexPath:indexPath];
    NSAssert1([cell isKindOfClass:[IGMonthSummaryCell class]], @"Expected a cell of type %@", NSStringFromClass([IGMonthSummaryCell class]));
    
    NSArray *dataArray = monthlyTimesheetSummary.timesheetSummary;
    NSInteger weekCount = dataArray.count;
    
    [cell configureForWeekCount:weekCount];
    
    if (weekCount > 0)
    {
        for (int i = 0; i < weekCount; i++)
        {
            UILabel *label = nil;
            switch (i)
            {
                case 0:
                {
                    label = cell.labelWeek1;
                    break;
                }
                    
                case 1:
                {
                    label = cell.labelWeek2;
                    break;
                }
                    
                case 2:
                {
                    label = cell.labelWeek3;
                    break;
                }
                    
                case 3:
                {
                    label = cell.labelWeek4;
                    break;
                }
                    
                case 4:
                {
                    label = cell.labelWeek5;
                    break;
                }
                    
                case 5:
                {
                    label = cell.labelWeek6;
                    break;
                }
                    
                default:
                {
                    NSLog(@"Warning: Unhandled Week In Monthly Timesheet summary");
                    break;
                }
            }
            
            CGFloat effortValue = [[dataArray[i] effort] floatValue];
            label.text = [NSString stringWithFormat:@"%0.2f", effortValue];
        }
        
        IGMonthlyEffort *monthlyEffort = monthlyTimesheetSummary.timesheetSummary[0];
        NSString *dateString = monthlyEffort.date;
        NSDate *date = [IGUtils dateFromString:dateString dateFormat:@"dd MMM yyyy"];
        NSString *month = [IGUtils stringFromDate:date dateFormat:@"MMMM"];
        
        cell.labelMonth.text = month;
        
        NSNumber *totalEffort = [monthlyTimesheetSummary.timesheetSummary valueForKeyPath:@"@sum.effort"];
        cell.labelTotalHours.text = [NSString stringWithFormat:@"%0.2f", [totalEffort floatValue]];
    }
    
    return cell;
}



- (UITableViewCell *)testCellForTable:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"test"];
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"test"];
    }
    
    cell.textLabel.text = [NSString stringWithFormat:@"Section: %ld Row: %ld", (long)indexPath.section, (long)indexPath.row];
    return cell;
}


//    if (indexPath.section == 0)
//    {
//        IGDatePickerCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifierC forIndexPath:indexPath];
//        [self addSeperatorLineToCell:cell indent:NO];
//
//        return cell;
//    }
//    else if (indexPath.section == 1)
//    {
//        //TODO: create cell
//    }
//    else if (indexPath.section == 2)
//    {
//        if(self.segmentControl.selectedSegmentIndex == 0)
//        {
//            IGWeekSummaryCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifierB];
//
//            if (cell == nil)
//            {
//                cell = [[IGWeekSummaryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellReuseIdentifierB];
//            }
//
//            return cell;
//        }
//
//        IGMonthSummaryCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifierD];
//
//        if (cell == nil)
//        {
//            cell = [[IGMonthSummaryCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellReuseIdentifierD];
//        }
//
//        return cell;
//    }
//
//    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifierA];
//
//    if (cell == nil) {
//
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kCellReuseIdentifierA];
//    }
//
//    if(indexPath.row == 1)
//    {
//        [self addSeperatorLineToCell:cell indent:NO];
//        [cell.textLabel setText:@"Status by RM"];
//        [cell.detailTextLabel setText:@"Approved"];
//    }
//    else
//    {
//        [self addSeperatorLineToCell:cell indent:YES];
//        [cell.textLabel setText:@"Status"];
//        [cell.detailTextLabel setText:@"Completed"];
//    }
//
//    return cell;
//
//}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    
    if ((indexPath.section == 0) && (indexPath.row == 0))
    {
        [self openYearSelectionController:self];
    }
    if ((indexPath.section == 0) && (indexPath.row == 1))
    {
        [self openMonthSelectionController:self];
    }
    if ((indexPath.section == 0) && (indexPath.row == 2))
    {
        [self openWeekEndDateSelectionController:self];
    }
    
    NSLog(@"%s: %@", __FUNCTION__, indexPath);

}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section)
    {
        case 0:
        {
            if (indexPath.row == 0)
            {
                return 35.0f;
            }
            else
            {
                return 40.0f;
            }
        }
            
        case 1:
        {
            switch (self.segmentControl.selectedSegmentIndex)
            {
                case 0:
                    return 44;
                    
                case 1:
                {
                    IGMonthlyTimesheetSummary *monthySummary = self.myAreaModel.monthlyTimesheetSummary[indexPath.row];
                    return [IGMonthSummaryCell heightForWeekCount:monthySummary.timesheetSummary.count];
                }
                    
                default:
                    return 0;
            }
            
            return 0;
        }
            
        case 2:
        {
            switch (self.segmentControl.selectedSegmentIndex)
            {
                case 0:
                    return 160;
                    
                default:
                    return 0;
            }
            
            return 0;
        }
            
        default:
        {
            return 0;
        }
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 1)
    {
        return kIGMyAreaSectionHeight;
    }
    
    return 0;
}

- (UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    if(section == 1)
    {
        NSString *sectionTitle = nil;
        
        switch (self.segmentControl.selectedSegmentIndex)
        {
            case 0:
            {
                sectionTitle = @"WEEKLY TIMESHEET SUMMARY";
                break;
            }
                
            case 1:
            {
                sectionTitle = @"MONTHLY TIMESHEET SUMMARY";
                break;
            }
                
            default:
            {
                break;
            }
        }
        
        // Create label with section title
        UILabel *label = [[UILabel alloc] init];
        
        CGFloat height = 15;
        CGFloat marginBottom = 10;
        CGFloat positionY = kIGMyAreaSectionHeight - height - marginBottom;
        label.frame = CGRectMake(16, positionY, 284, height);
        label.textColor = kColorGrayType1;
        label.font = [UIFont systemFontOfSize:14.0];
        label.text = sectionTitle;
        label.backgroundColor = [UIColor clearColor];
        
        // Create header view and add label as a subview
        UIView* view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, tableView.bounds.size.width, kIGMyAreaSectionHeight)];
        
        view.backgroundColor = kColorGrayType2;
        [view addSubview:label];
        
        UIView *lineViewTop = [[UIView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, 1)];
        lineViewTop.backgroundColor = [UIColor lightGrayColor];
        [view addSubview:lineViewTop];
        
        UIView *lineViewBottom = [[UIView alloc] initWithFrame:CGRectMake(0, view.frame.size.height - 2 , view.frame.size.width, 1)];
        lineViewBottom.backgroundColor = [UIColor lightGrayColor];
        [view addSubview:lineViewBottom];
        
        return view;
    }
    
    UIView* view = [[UIView alloc] initWithFrame:CGRectZero];
    return view;
}


#pragma mark - IGWeekSumamryCellDelegate


- (void)viewTaskForCell:(IGWeekSummaryCell *)cell
{
    NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
    if (indexPath.row < self.myAreaModel.weeklyTimesheetSummary.timesheetSummary.count)
    {
        IGTaskDetailVC *taskDetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"IGTaskDetailVC"];
        
        IGWeeklyEffort *effort = self.myAreaModel.weeklyTimesheetSummary.timesheetSummary[indexPath.row];
        NSInteger order = [effort.order integerValue];
        NSString *employeeIDString = [IGUserDefaults sharedInstance].employee.staffID;
        NSInteger weekID = self.myAreaModel.weeklyTimesheetSummary.weekID;
        
        taskDetailVC.order = order;
        taskDetailVC.weekID = weekID;
        taskDetailVC.employeeIDString = employeeIDString;
        
        [self.navigationController pushViewController:taskDetailVC animated:YES];
        
    }
}

//- (CGFloat)tableView:(UITableView*)tableView heightForFooterInSection:(NSInteger)section
//{
//    if(section == 0)
//    {
//        return 20.0;
//    }
//
//    return 2;
//}

//- (UIView*)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
//{
//    UIView* view;
//
//    if(section == 0)
//    {
//        view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, tableView.bounds.size.width, 10.0)];
//
//        view.backgroundColor = [UIColor colorWithRed:239.0/255 green:239.0/255 blue:244.0/255 alpha:1];
//
//        return view;
//    }
//
//    view = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, tableView.bounds.size.width, 2.0)];
//
//    view.backgroundColor = [UIColor colorWithRed:239.0/255 green:239.0/255 blue:244.0/255 alpha:1];
//
//    return view;
//}




#pragma mark - Utility Methods

- (void)addSeperatorLineToCell:(UITableViewCell*)cell indent:(bool)indent
{
    UIView *lineView = nil;
    
    CGFloat indentation = 0.0f;
    
    if(indent)
    {
        indentation = 15.0f;
    }
    
    lineView = [[UIView alloc] initWithFrame:CGRectMake(indentation, cell.contentView.frame.size.height - 1 , self.tableView.frame.size.width, 1)];
    lineView.backgroundColor = [UIColor lightGrayColor];
    
    [cell.contentView addSubview:lineView];
}


#pragma mark -

- (void)fetchDataForSegment:(NSInteger)segmentIndex
{
    switch (segmentIndex)
    {
        case 0:
        {
            if (self.myAreaModel.shouldFetchWeeklyTimesheetSummary == YES)
            {
                kAlertAndReturnIfNetworkNotReachable
                
                [self showSpinner:YES];
                [self.myAreaModel fetchWeeklyTimesheetSummary:^(IGMyAreaModel *myAreaModel)
                    {
                        [self.tableView reloadData];
                        [self showSpinner:NO];
                    }
                    error:^(NSError *error)
                    {
                        [IGUtils showOKAlertWithTitle:kStringError
                                              message:[error localizedDescription]];
                        [self.tableView reloadData];
                        [self showSpinner:NO];
                    }];
            }
            else
            {
                [self.tableView reloadData];
            }
            
            break;
        }
            
        case 1:
        {
            if (self.myAreaModel.shouldFetchMonthlyTimesheetSummary == YES)
            {
                kAlertAndReturnIfNetworkNotReachable

                [self showSpinner:YES];
                [self.myAreaModel fetchMonthlyTimesheetSummary:^(IGMyAreaModel *myAreaModel)
                    {
                        [self.tableView reloadData];
                        [self showSpinner:NO];
                    }
                    error:^(NSError *error)
                    {
                        [IGUtils showOKAlertWithTitle:kStringError
                                              message:[error localizedDescription]];
                        [self.tableView reloadData];
                        [self showSpinner:NO];
                    }];
            }
            else
            {
                [self.tableView reloadData];
            }
            
            break;
        }
            
        default:
        {
            break;
        }
    }
}


#pragma mark -

- (IBAction)openYearSelectionController:(id)sender
{
    if (!yearPicker)
    {
        yearPicker = [IGYearPicker picker];
        yearPicker.delegate = (id)self;
        yearPicker.titleLabel.text = @"Select Year";
        [yearPicker presentOnView:self.view animated:YES];
    }
    else
    {
        [yearPicker show:YES animated:YES];
    }
}

- (IBAction)openMonthSelectionController:(id)sender
{
    if (!monthPicker)
    {
        monthPicker = [IGMonthPicker picker];
        monthPicker.delegate = (id)self;
        monthPicker.titleLabel.text = @"Select Month";
        monthPicker.selectedYear = yearString;
        [monthPicker presentOnView:self.view animated:YES];
    }
    else
    {
        monthPicker.selectedYear = yearString;
        [monthPicker presentOnView:self.view animated:YES];
    }
}

- (IBAction)openWeekEndDateSelectionController:(id)sender
{
    if (!weekPicker)
    {
        weekPicker = [IGWeekEndPicker picker];
        weekPicker.delegate = (id)self;
        weekPicker.titleLabel.text = @"Select Week";
        weekPicker.weekArray = self.weekEndArray;
        [weekPicker presentOnView:self.view animated:YES];
    }
    else
    {
        weekPicker.weekArray = self.weekEndArray;
        [weekPicker presentOnView:self.view animated:YES];
    }
}


#pragma mark - IGPickerDelegate

- (void)monthPicker:(IGMonthPicker *)picker
{
    NSInteger index = [picker.pickerView selectedRowInComponent:0] + 1;
    monthString = [@(index) stringValue];
    weekDate = nil;
    [self fetchWeekEndDateFromServer];
    
}

- (void)yearPicker:(IGYearPicker *)picker didSelectDate:(NSString *)year;
{
    yearString = year;
    if ([yearString isEqualToString:[IGUtils stringFromDate:[NSDate date] dateFormat:@"yyyy"]] && flag == TRUE)
    {
        monthString = [IGUtils stringFromDate:[NSDate date] dateFormat:@"MM"];
        flag = TRUE;
    }
    else
    {
        monthString = nil;
        flag = FALSE;
    }
    weekDate = nil;
    [self.tableView reloadData];
}

- (void)weekEndPicker:(IGWeekEndPicker *)picker didSelectDate:(NSString *)weekEnd
{
    NSInteger index = [picker.pickerView selectedRowInComponent:0];
    
    if ((self.weekEndArray.count != 0) && (index < self.weekEndArray.count))
    {
        weekDate = self.weekEndArray[index];
        self.myAreaModel.queryDate = [IGUtils dateFromString:weekDate dateFormat:@"dd MMM yyyy"];;
        [self fetchDataForSegment:self.segmentControl.selectedSegmentIndex];
        [self.tableView reloadData];
    }
}

- (void)fetchWeekEndDateFromServer
{
    kAlertAndReturnIfNetworkNotReachable
    
    [self showSpinner:YES];
    [[IGService sharedInstance] weekEndDates:yearString
                                       month:monthString
        completion:^(id response)
        {
            [self.weekEndArray removeAllObjects];
            weekDate = nil;
            
            if ([response respondsToSelector:@selector(objectForKey:)])
            {
                NSArray *dataArray = [(NSDictionary *)response objectForKey:@"Weekends"];
                if ([dataArray respondsToSelector:@selector(objectAtIndex:)])
                {
                    NSInteger count = [dataArray count];
                    if (count > 0)
                    {
                        for (int index = 0; index < count; index++)
                        {
                            NSString *weekEndDate = nil;
                            id weekInfo = dataArray[index];
                            if ([weekInfo respondsToSelector:@selector(objectForKey:)])
                            {
                                weekEndDate = [weekInfo objectForKey:@"Weekenddate"];
                            }
                            
                            if (weekEndDate != nil)
                            {
                                [self.weekEndArray addObject:weekEndDate];
                            }
                        }
                        
                        if ([yearString isEqualToString:[IGUtils stringFromDate:[NSDate date] dateFormat:@"yyyy"]] &&
                            [monthString isEqualToString:[IGUtils stringFromDate:[NSDate date] dateFormat:@"MM"]])
                        {
                            weekDate = [self.weekEndArray lastObject];
                        }
                    }
                }
            }
            
            [self.tableView reloadData];
            [self showSpinner:NO];
        }
        error:^(NSError *error)
        {
            [IGUtils showOKAlertWithTitle:kStringError message:[error localizedDescription]];
            [self showSpinner:NO];
        }];
}

@end
