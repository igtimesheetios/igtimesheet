//
//  IGPickerCell.m
//  IGTimeSheet
//
//  Created by Neha on 18/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGPickerCell.h"
#import "IGMacro.h"

@implementation IGPickerCell

@synthesize pickerContents;
@synthesize tag;
@synthesize delegate;
@synthesize pickerTitle;

- (void)initWithText: (NSString*)text pickerContents:(NSArray*)contents
{
    [self.pickerTextField setTintColor:[UIColor clearColor]];
    self.pickerTextField.text = nil;
    self.pickerTextField.delegate = self;

    if(!self.selectedIndex)
        self.selectedIndex = 0;
    
    self.pickerTextField.selectedTextRange = nil;
    self.pickerContents = contents;
    self.leftLabel.text = text;
    
    self.pickerView = [[UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
    self.pickerView.showsSelectionIndicator = YES;
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    [self.pickerView setBackgroundColor:[UIColor whiteColor]];
    [self.pickerView selectRow:self.selectedIndex inComponent:0 animated:NO];
    
    if(self.selectedTextColor)
    {
        [self.pickerTextField setTextColor:self.selectedTextColor];
    }
    
    // set change the inputView (default is keyboard) to UIPickerView
    self.pickerTextField.inputView = self.pickerView;
    
    // add a toolbar with Cancel & Done button
    UIToolbar *toolBar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    toolBar.barStyle = UIBarStyleBlackOpaque;
    toolBar.translucent = NO;
    toolBar.barTintColor = [UIColor colorWithRed:237.0/255 green:237.0/255 blue:237.0/255 alpha:1]; //[UIColor lightGrayColor];
    
    
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneTouched:)];

    UIBarButtonItem *titleButton = [[UIBarButtonItem alloc]initWithTitle:self.pickerTitle style:UIBarButtonItemStylePlain target:nil action:nil];
    
    [titleButton setTitleTextAttributes: [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName] forState:UIControlStateDisabled ];
    [titleButton setTitleTextAttributes: [NSDictionary dictionaryWithObject:[UIColor blackColor] forKey:NSForegroundColorAttributeName] forState:UIControlStateNormal];
    titleButton.enabled=NO;
    
    UIBarButtonItem *cancelButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelTouched:)];
    
    [toolBar setItems:[NSArray arrayWithObjects:cancelButton, [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],titleButton,[[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil], doneButton, nil]];
    self.pickerTextField.inputAccessoryView = toolBar;
}

- (void)awakeFromNib {
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)disclosureTapped:(id)sender
{
    [self.pickerTextField becomeFirstResponder];
}

- (void)cancelTouched:(UIBarButtonItem *)sender
{
    // hide the picker view
    [self.pickerTextField resignFirstResponder];
}

- (void)doneTouched:(UIBarButtonItem *)sender
{
    // hide the picker view
    [self.pickerTextField resignFirstResponder];
    
    if(self.selectedIndex != -1)
    {
        NSString* selectedObj = @"";
        
        if(self.pickerContents.count)
        {
            selectedObj = [self.pickerContents objectAtIndex:self.selectedIndex];
        }
        
        [delegate didSelectPickerObject:self atIndex:self.selectedIndex value:selectedObj];
        if(self.selectedTextColor)
        {
            [self.pickerTextField setTextColor:self.selectedTextColor];
        }
       // [self.pickerTextField setText:[self.pickerContents objectAtIndex:selectedPickerValue]];
    }
    
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.pickerContents count];
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    NSString *item = [self.pickerContents objectAtIndex:row];
    
    return item;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    self.selectedIndex = (int)row;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    return NO;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(self.pickerContents.count)
    {
        return YES;
    }
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    [delegate didOpenPickerView:self.pickerView];
}

- (void)textFieldDidEndEditing:(UITextField *)textField;
{
    [delegate didClosePickerView:self.pickerView];
}




@end
