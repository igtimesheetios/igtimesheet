//
//  UIView+IGCircularView.m
//  IGCafe
//
//  Created by Pulkit on 26/05/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "UIView+IGCircularView.h"

@implementation UIView (IGCircularView)

- (void) IGMakeCircularView {
    UIBezierPath *path = [UIBezierPath bezierPathWithOvalInRect:self.bounds];
    CAShapeLayer *layer = [CAShapeLayer layer];
    layer.path = path.CGPath;
    self.layer.mask = layer;
}

- (void)roundViewWithCornerRaidus:(CGFloat)cornerRadius
                      borderWidth:(CGFloat)borderWidth
                      borderColor:(UIColor *)borderColor
{
    self.layer.cornerRadius = cornerRadius;
    self.layer.borderWidth = borderWidth;
    if (borderColor != nil)
    {
        self.layer.borderColor = borderColor.CGColor;
    }
}

- (void)roundViewByRoundingCorners:(UIRectCorner)rectCorners
                       cornerRadii:(CGSize)cornerRadii;
{
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:self.bounds
                                               byRoundingCorners:rectCorners
                                                     cornerRadii:cornerRadii];
    
    CAShapeLayer *layer = (CAShapeLayer *)self.layer.mask;
    if (layer == nil || [layer isKindOfClass:[CAShapeLayer class]] == NO)
    {
        layer = [CAShapeLayer layer];
    }
    layer.path = path.CGPath;
    self.layer.mask = layer;
}


@end

