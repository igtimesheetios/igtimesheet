//
//  IGMonthSummaryCell.h
//  IGTimeSheet
//
//  Created by Neha on 17/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IGClipView;
@interface IGMonthSummaryCell : UITableViewCell

@property(strong) IBOutlet UIView *viewBackground;

@property(strong) IBOutlet IGClipView *totalHrsView;
@property(strong) IBOutlet UILabel *labelMonth;

@property(strong) IBOutlet UILabel *labelWeek1;
@property(strong) IBOutlet UILabel *labelWeek2;
@property(strong) IBOutlet UILabel *labelWeek3;
@property(strong) IBOutlet UILabel *labelWeek4;
@property(strong) IBOutlet UILabel *labelWeek5;
@property(strong) IBOutlet UILabel *labelWeek6;

@property(strong) IBOutlet UILabel *labelTotalHours;

+ (CGFloat)heightForWeekCount:(NSInteger)weekCount;

// WeekCount Range: 0-6
- (void)configureForWeekCount:(NSInteger)weekCount;

@end
