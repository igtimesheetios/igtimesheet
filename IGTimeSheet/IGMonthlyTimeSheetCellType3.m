//
//  IGMonthlyTimeSheetCellType2.m
//  IGTimeSheet
//
//  Created by Kamal on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGMonthlyTimeSheetCellType3.h"
#import "IGMacro.h"

@implementation IGMonthlyTimeSheetCellType3

- (void)awakeFromNib
{
    self.backgroundColor = kColorGrayType2;
    
    CALayer *layer = self.cell3MainView.layer;
    layer.cornerRadius = 8.0;
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowOffset = CGSizeMake(0, 0);
    layer.shadowRadius = 2.0f;
    layer.shadowOpacity = 0.5f;
}

@end
