//
//  IGCRIdModel.h
//  IGTimeSheet
//
//  Created by Lab2 on 29/01/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGCRIdModel : NSObject
@property(strong) NSString* CRId;
@property(strong) NSString* CRName;
@end
