//
//  IGHoursSummary.m
//  IGTimeSheet
//
//  Created by Manish on 18/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGHourSummary.h"
#import "IGServiceDefine.h"
#import "IGHour.h"

@implementation IGHourSummary

- (instancetype)initWithDictionary:(NSDictionary *)hoursInfo
{
    if (hoursInfo == nil)
    {
        NSLog(@"%s: ERROR ceating object. Input Parameter is nil", __FUNCTION__);
        return nil;
    }
    
    if ([hoursInfo respondsToSelector:@selector(objectForKey:)] == NO)
    {
        NSLog(@"%s: ERROR ceating object. Expected a Dictionary type, got %@", __FUNCTION__, NSStringFromClass([hoursInfo class]));
        return nil;
    }
    
    self = [super init];
    if (self)
    {
        NSMutableArray *hoursArray = [[NSMutableArray alloc] initWithCapacity:1];
        NSString *key = nil;
        NSString *value = nil;
        
        for (int weekIndex = 1; weekIndex <= 6; weekIndex++)
        {
            // check if value exists for key
            key = kIGServiceKeyWeek(weekIndex);
            value = hoursInfo[key];
            
            if (value)
            {
                IGWeeklyHour *weeklyHour = [[IGWeeklyHour alloc] init];
                weeklyHour.order = weekIndex;
                weeklyHour.week = value;
                
                key = kIGServiceKeyTotalHour(weekIndex);
                weeklyHour.totalHours = hoursInfo[key];
                
                key = kIGServiceKeyNumberOfDays(weekIndex);
                weeklyHour.numberOfDays = hoursInfo[key];
                
                key = kIGServiceKeyWeeklyAverage(weekIndex);
                weeklyHour.weeklyAverage = hoursInfo[key];
                
                [hoursArray addObject:weeklyHour];
            }
        }
        
        if (hoursArray.count != 0)
        {
            _weeklyHours = hoursArray;
        }
        
        
        IGMonthlyHour *monthlyHour = [[IGMonthlyHour alloc] init];
        monthlyHour.totalHoursAverage = hoursInfo[kIGServiceKeyTotalHoursAverage];
        monthlyHour.totalHours = hoursInfo[kIGServiceKeyTotalHours];
        monthlyHour.numberOfDays = hoursInfo[kIGServiceKeyTotalDays];
        monthlyHour.weeklyAverage = hoursInfo[kIGServiceKeyWeekAverage];
        
        
        _monthlyHour = monthlyHour;
        
        NSLog(@"%@", _weeklyHours);
        NSLog(@"%@", _monthlyHour);
    }
    return self;
}

@end

