//
//  NSDictionary+IGAddition.m
//  IGTimeSheet
//
//  Created by Manish on 23/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "NSDictionary+IGAddition.h"
#import "IGUtils.h"

@implementation NSDictionary (IGAddition)

- (NSString *)IGStringValueForKey:(NSString *)key
{
    NSString *stringValue = self[key];
    if ([IGUtils isValidString:stringValue] == NO)
    {
        stringValue = @"";
    }
    return stringValue;
}

- (NSNumber *)IGNumberValueForKey:(NSString *)key
{
    NSNumber *numberValue = self[key];
    if ([IGUtils isValidNumber:numberValue] == NO)
    {
        numberValue = @0;
    }
    return numberValue;
}

@end
