//
//  IGTaskViewVC.h
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGAddTaskVC.h"
#import "IGTaskModalClass.h"

@interface IGTaskViewVC : IGViewController<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray* _rowArray;
    NSMutableArray* _projectsArray;
    NSArray* _projectList;
    IBOutlet UITableView* _tableView;
    IGAddTaskVC* _addTaskViewCtrlr;
    NSDate* _weekendDate;
    NSInteger _empId;
    NSInteger _weekId;
    NSInteger _selectedProjID;
    
    IBOutlet UIButton* _submitButton;
    IBOutlet UIButton* _saveButton;
}

@property(nonatomic) BOOL isApproval;
- (void)initilizeDataForDate:(NSDate *)date employeeID:(NSInteger)employeeID andStatus:(NSString *)status;
- (void)addNewTask:(IGTaskModalClass *)task;

@end
