//
//  IGPickerCell.h
//  IGTimeSheet
//
//  Created by Neha on 18/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IGPickerCellDelegate;

@interface IGPickerCell : UITableViewCell<UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>

@property (strong) UIPickerView *pickerView;
@property (atomic) NSInteger tag;
@property (strong) NSArray *pickerContents;
@property (weak) IBOutlet UITextField *pickerTextField;
@property (weak) IBOutlet UILabel *leftLabel;
@property (weak) IBOutlet UIButton *disclosureBtn;
@property (nonatomic, weak) id <IGPickerCellDelegate> delegate;
@property (strong) UIColor* selectedTextColor;
@property (strong) NSString* pickerTitle;
@property (atomic) int selectedIndex;
@property (weak,nonatomic) IBOutlet NSLayoutConstraint *disclosureWidthConstraint;

- (IBAction)disclosureTapped:(id)sender;
- (void)initWithText: (NSString*)text pickerContents:(NSArray*)contents;

@end

@protocol IGPickerCellDelegate

- (void)didSelectPickerObject: (IGPickerCell*)cell atIndex:(NSInteger)index value:(NSString*)object;
- (void)didOpenPickerView: (UIPickerView*)pickerView;
- (void)didClosePickerView: (UIPickerView*)pickerView;

@end