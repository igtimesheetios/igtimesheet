//
//  IGPicker.m
//  IGTimeSheet
//
//  Created by Manish on 20/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGYearPicker.h"
#import "IGUtils.h"

#define IG_MAX_YEAR_COUNT                       5

@interface IGYearPicker() <UIPickerViewDataSource, UIPickerViewDelegate>
{
    NSInteger selectedYearIndex;
}

@property (nonatomic, strong) NSArray *yearArray;

- (IBAction)doneButtonAction:(id)sender;
- (IBAction)cancelButtonAction:(id)sender;


@end


@implementation IGYearPicker

+ (IGYearPicker *)picker
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"IGYearPicker" owner:nil options:nil];
    IGYearPicker *picker = (IGYearPicker *)views[0];
    NSAssert([picker isKindOfClass:[IGYearPicker class]], @"Expected a view of type IGPicker");
    return picker;
}

- (void)presentOnView:(UIView *)view animated:(BOOL)animated;
{
    [self addToView:view];
    [self show:YES animated:animated];
}


#pragma mark -

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        [self commonInitialization];
        
    }
    return self;
}


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self)
    {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    NSDate *dateNow = [NSDate date];
    
    NSString *yearString = [IGUtils stringFromDate:dateNow dateFormat:@"yyyy"];
    
    NSInteger year = [yearString integerValue];
    
    NSMutableArray *array = [[NSMutableArray alloc] initWithCapacity:1];
    for (int yearCount = 0; (year > 0) && (yearCount < IG_MAX_YEAR_COUNT); yearCount++, year--)
    {
        [array addObject:@(year)];
    }
    self.yearArray = [[NSArray alloc] initWithArray:array];
    
    
    _currentDate = dateNow;
    
    _yearString = yearString;
    
    selectedYearIndex = 0;
   
}


#pragma mark -

- (void)setCurrentDate:(NSDate *)date
{
    NSDate *dateNow = date;
    
    NSString *yearString = [IGUtils stringFromDate:dateNow dateFormat:@"yyyy"];
    
    NSInteger year = [yearString integerValue];
    
    NSNumber *minYearNumber = [self.yearArray lastObject];
    NSNumber *maxYearNumber = [self.yearArray firstObject];

    NSInteger minYear = [minYearNumber integerValue];
    NSInteger maxYear = [maxYearNumber integerValue];
    
    if (year >= minYear && year <= maxYear)
    {
        _currentDate = dateNow;
        
        _yearString = yearString;
        
        selectedYearIndex = [self.yearArray indexOfObject:@(year)];
    }
    else
    {
        dateNow = [NSDate date];
        
        yearString = [IGUtils stringFromDate:dateNow dateFormat:@"yyyy"];
        
        year = [yearString integerValue];

        _currentDate = dateNow;
        
        _yearString = yearString;
        
        selectedYearIndex = 0;
    }
}

- (NSInteger)year
{
    NSAssert(selectedYearIndex < self.yearArray.count, @"Selected Year Index is greater than year array count");

    NSNumber *year = self.yearArray[selectedYearIndex];
    return [year integerValue];
}


#pragma mark -

- (void)awakeFromNib
{
    self.translatesAutoresizingMaskIntoConstraints = NO;
    self.containerViewBottomConstraint.constant = - self.containerView.frame.size.height;
}

- (void)addToView:(UIView *)view;
{
    [view addSubview:self];
    
    NSLayoutConstraint *topConstraint = [NSLayoutConstraint constraintWithItem:self
                                                                     attribute:NSLayoutAttributeTop
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:view
                                                                     attribute:NSLayoutAttributeTop
                                                                    multiplier:1
                                                                      constant:0];
    
    NSLayoutConstraint *bottomConstraint = [NSLayoutConstraint constraintWithItem:self
                                                                        attribute:NSLayoutAttributeBottom
                                                                        relatedBy:NSLayoutRelationEqual
                                                                           toItem:view
                                                                        attribute:NSLayoutAttributeBottom
                                                                       multiplier:1
                                                                         constant:0];
    
    NSLayoutConstraint *leftConstraint = [NSLayoutConstraint constraintWithItem:self
                                                                      attribute:NSLayoutAttributeLeading
                                                                      relatedBy:NSLayoutRelationEqual
                                                                         toItem:view
                                                                      attribute:NSLayoutAttributeLeading
                                                                     multiplier:1
                                                                       constant:0];
    
    NSLayoutConstraint *rightConstraint = [NSLayoutConstraint constraintWithItem:self
                                                                       attribute:NSLayoutAttributeTrailing
                                                                       relatedBy:NSLayoutRelationEqual
                                                                          toItem:view
                                                                       attribute:NSLayoutAttributeTrailing
                                                                      multiplier:1
                                                                        constant:0];
    
    [view addConstraints:@[topConstraint, bottomConstraint, leftConstraint, rightConstraint]];
}

- (void)show:(BOOL)show animated:(BOOL)animated
{
    if (show)
    {
        [self.pickerView selectRow:selectedYearIndex inComponent:0 animated:NO];
        
        if (animated)
        {
            self.containerViewBottomConstraint.constant = 0;
            [self setNeedsUpdateConstraints];
            self.alpha = 0.0f;
            [UIView animateWithDuration:0.3
                             animations:^{
                                 self.alpha = 1.0f;
                                 [self layoutIfNeeded];
                             }];
        }
        else
        {
            self.containerViewBottomConstraint.constant = 0;
        }
    }
    else
    {
        if (animated)
        {
            self.containerViewBottomConstraint.constant = -self.containerView.frame.size.height;
            [self setNeedsUpdateConstraints];
            [UIView animateWithDuration:0.3
                             animations:^{
                                 self.alpha = 0.0f;
                                 [self layoutIfNeeded];
                             } completion:^(BOOL finished) {
//                                 [self removeFromSuperview];
                             }];
        }
        else
        {
            [self removeFromSuperview];
        }
    }
}

- (IBAction)cancelButtonAction:(id)sender
{
    [self show:NO animated:YES];
}

- (IBAction)doneButtonAction:(id)sender
{
    selectedYearIndex = [self.pickerView selectedRowInComponent:0];
    _yearString = [self yearForRow:selectedYearIndex];
    
    [self show:NO animated:YES];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(yearPicker:didSelectDate:)])
    {
        [self.delegate yearPicker:self didSelectDate:_yearString];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self show:NO animated:YES];
}


#pragma mark UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component)
    {
        case 0:
        {
            return self.yearArray.count;
        }
            
        default:
        {
            NSAssert1(0, @"Unrecognized Component %ld", (long)component);
            break;
        }
    }
    
    return 0;
}


#pragma mark UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component)
    {
        case 0:
        {
            return [self yearForRow:row];
        }
            
        default:
        {
            break;
        }
    }
    
    NSAssert1(0, @"Unrecognized Component %ld", (long)component);
    return @"-";
}

- (NSString *)yearForRow:(NSInteger)row
{
    NSAssert(row < self.yearArray.count, @"Selected Row Index is greater than year array count");

    NSNumber *year = self.yearArray[row];
    return [NSString stringWithFormat:@"%ld", (long)[year integerValue]];
}

@end
