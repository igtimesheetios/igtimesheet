//
//  IGLabelButtonTableViewCell.h
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGLabelButtonTableViewCell : UITableViewCell

@property(strong) IBOutlet UILabel* placeholderLabel;
@property(strong) IBOutlet UIButton* placeholderButton;

@end
