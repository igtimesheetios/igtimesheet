//
//  IGRequest.h
//  IGTimeSheet
//
//  Created by Manish on 06/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGRequest : NSObject

+ (NSDictionary *)requestParameterWithEmployeeID:(NSInteger)employeeID
                                            date:(NSDate *)date
                                         service:(NSString *)service
                                           error:(NSError **)error;

@end
