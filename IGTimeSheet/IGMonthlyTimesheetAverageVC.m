//
//  IGMonthlyTimesheetAverageVC.m
//  IGTimeSheet
//
//  Created by Kamal on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGMonthlyTimesheetAverageVC.h"
#import "IGMonthlyTimeSheetCellType1.h"
#import "IGMonthlyTimeSheetCellType2.h"
#import "IGMonthlyTimeSheetCellType3.h"
#import "IGMonthlyTimeSheetCellType4.h"
#import "DateSelectionViewController.h"

#import "IGMonthlyAverageModel.h"
#import "IGHourSummary.h"
#import "IGHour.h"
#import "IGYearPicker.h"
#import "IGMonthPicker.h"

#import "IGCommonHeaders.h"

#define kTableViewCellType1ReuseIdentifier                      @"IgMonthlyTimesheetCell1"
#define kTableViewCellType2ReuseIdentifier                      @"IgMonthlyTimesheetCell2"
#define kTableViewCellType3ReuseIdentifier                      @"IgMonthlyTimesheetCell3"
#define kTableViewCellType4ReuseIdentifier                      @"IgMonthlyTimesheetCell4"
#define kTableViewCellType5ReuseIdentifier                      @"IgMonthlyTimesheetCell5"

#define IG_MAX_YEAR_COUNT                       5
#define kIGMonthlyTimesheetAverageTest          1

@interface IGMonthlyTimesheetAverageVC () <UITableViewDataSource,
UITableViewDelegate, IGMonthPickerDelegate>
{
    BOOL flag;
    NSString *yearString;
    NSString *monthString;
    
    IGMonthlyAverageModel *monthlyAverageModel;
    IGHourSummary *hourSummary;
    
    NSDate *queryDate;
    
    IGYearPicker *yearPicker;
    IGMonthPicker *monthPicker;
}

@property (nonatomic, strong) NSString *weekEndDate;

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentedControl;

@property(strong, nonatomic) NSArray *monthArray;

- (IBAction)segmentSelected:(UISegmentedControl *)sender;

@end

@implementation IGMonthlyTimesheetAverageVC

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        [self commonInitialization];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        [self commonInitialization];
    }
    return self;
}

- (void)commonInitialization
{
    queryDate = [NSDate date];
    
    yearString = [IGUtils stringFromDate:queryDate dateFormat:@"yyyy"];
    monthString = [IGUtils stringFromDate:queryDate dateFormat:@"MM"];
    
    //TODO: remove month array
    self.monthArray = @[
                        @"Jan",
                        @"Feb",
                        @"Mar",
                        @"Apr",
                        @"May",
                        @"Jun",
                        @"Jul",
                        @"Aug",
                        @"Sep",
                        @"Oct",
                        @"Nov",
                        @"Dec"
                        ];
}

#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setupBackButton];
    [self setupOpaqueNavigationBar];
    
    self.title = @"Monthly TimeSheet Average";
    flag = TRUE;
    
    NSDate *date = nil;
#if kIGMonthlyTimesheetAverageTest
    date = [NSDate date];
#else
    date = queryDate;
#endif
    
    [self getMonthlyAverageForDate:queryDate];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [IGAnalytics trackScreen:screenMonthlyAverageTimesheet];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}


#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (hourSummary != nil)
    {
        return 3;
    }
    
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    switch (section)
    {
        case 0:
        {
            return 2;
        }
            
        case 1:
        {
            if (hourSummary != nil)
            {
                return hourSummary.weeklyHours.count;
            }
            
            return 1;
        }
            
        case 2:
        {
            if (hourSummary != nil)
            {
                return 1;
            }
            
            return 0;
        }
            
        default:
        {
            return 0;
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    switch (indexPath.section)
    {
        case 0:
        {
            if (indexPath.row == 0)
            {
                IGMonthlyTimeSheetCellType1 *cellType1 = [tableView dequeueReusableCellWithIdentifier:kTableViewCellType1ReuseIdentifier forIndexPath:indexPath];
                cellType1.selectionStyle = UITableViewCellSelectionStyleNone;
                cellType1.yearData.text = yearString;
                return cellType1;
                
            }
            else if(indexPath.row == 1)
            {
                IGMonthlyTimeSheetCellType2 *cellType2 = [tableView dequeueReusableCellWithIdentifier:kTableViewCellType2ReuseIdentifier  forIndexPath:indexPath];
                cellType2.selectionStyle = UITableViewCellSelectionStyleNone;
                if (monthString != nil) {
                    cellType2.monthLabel.text = self.monthArray[[monthString integerValue] - 1];
                }
                else
                {
                    cellType2.monthLabel.text = @"Select";
                }
                return cellType2;
                
            }
        }
            
        case 1:
        {
            if (hourSummary != nil)
            {
                IGMonthlyTimeSheetCellType3 *cellType3 = [tableView dequeueReusableCellWithIdentifier:kTableViewCellType3ReuseIdentifier forIndexPath:indexPath];
                
                if (indexPath.row < hourSummary.weeklyHours.count)
                {
                    IGWeeklyHour *weeklyHour = hourSummary.weeklyHours[indexPath.row];
                    cellType3.weekData.text = weeklyHour.week;
                    cellType3.totalHoursData.text = [NSString stringWithFormat:@"%0.2f",
                                                     [weeklyHour.totalHours floatValue]];
                    cellType3.numberOfDaysData.text = weeklyHour.numberOfDays;
                    cellType3.weeklyAverageData.text = [NSString stringWithFormat:@"%0.2f",
                                                        [weeklyHour.weeklyAverage floatValue]];
                }
                return cellType3;
            }
            else
            {
                UITableViewCell *cellType5 = [tableView dequeueReusableCellWithIdentifier:kTableViewCellType5ReuseIdentifier forIndexPath:indexPath];
                cellType5.selectionStyle = UITableViewCellSelectionStyleNone;
                return cellType5;
            }

        }
            
        case 2:
        {
            IGMonthlyTimeSheetCellType4 *cellType4 = [tableView dequeueReusableCellWithIdentifier:kTableViewCellType4ReuseIdentifier forIndexPath:indexPath];
            
            cellType4.totalHoursData.text = [NSString stringWithFormat:@"%0.2f",
                                             [hourSummary.monthlyHour.totalHours floatValue]];
            cellType4.numberOfDaysData.text = hourSummary.monthlyHour.numberOfDays;
            cellType4.weeklyAverageData.text = [NSString stringWithFormat:@"%0.2f",
                                                [hourSummary.monthlyHour.weeklyAverage floatValue]];
            
            return cellType4;
        }
            
        default:
        {
            return nil;
        }
    }
    
    return nil;
    
}


#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ((indexPath.section == 0) && (indexPath.row == 0))
    {
        [self openYearSelectionController:self];
    }
    if ((indexPath.section == 0) && (indexPath.row == 1))
    {
        [self openMonthSelectionController:self];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        
        if (indexPath.row == 0)
        {
            return 35.0f;
        }
        else
        {
            return 40.0f;
        }
        
    }
    
    return 148.0f;
}

- (IBAction)openYearSelectionController:(id)sender
{
    if (!yearPicker)
    {
        yearPicker = [IGYearPicker picker];
        yearPicker.delegate = (id)self;
        yearPicker.titleLabel.text = @"Select Year";
        yearPicker.currentDate = queryDate;
        [yearPicker presentOnView:self.view animated:YES];
    }
    else
    {
        yearPicker.currentDate = queryDate;
        [yearPicker show:YES animated:YES];
    }
}

- (IBAction)openMonthSelectionController:(id)sender
{
    if (!monthPicker)
    {
        monthPicker = [IGMonthPicker picker];
        monthPicker.delegate = (id)self;
        monthPicker.titleLabel.text = @"Select Month";
        monthPicker.selectedYear = yearString;
        [monthPicker presentOnView:self.view animated:YES];
    }
    else
    {
        monthPicker.selectedYear = yearString;
        [monthPicker presentOnView:self.view animated:YES];
    }
}

#pragma mark - IGPickerDelegate

- (void)monthPicker:(IGMonthPicker *)picker
{
    NSInteger index = [picker.pickerView selectedRowInComponent:0];
    NSString *monthName = self.monthArray[index];
    NSString *dateString = [NSString stringWithFormat:@"%@ %@", monthName, yearString];
    NSDate *date = [IGUtils dateFromString:dateString dateFormat:@"MMM yyyy"];
    [self getMonthlyAverageForDate:date];
//    [self.tableView reloadData];
    
}

- (void)yearPicker:(IGYearPicker *)picker didSelectDate:(NSString *)year;
{
    yearString = year;
    if ([yearString isEqualToString:[IGUtils stringFromDate:[NSDate date] dateFormat:@"yyyy"]] && flag == TRUE)
    {
        monthString = [IGUtils stringFromDate:[NSDate date] dateFormat:@"MM"];
        flag = TRUE;
    }
    else
    {
        monthString = nil;
        flag = FALSE;
    }
    [self.tableView reloadData];
}

#pragma mark -

- (void)getMonthlyAverageForDate:(NSDate *)date
{
    if ([date isEqualToDate:queryDate] && monthlyAverageModel != nil)
    {
        return;
    }
    kAlertAndReturnIfNetworkNotReachable;
    [self showSpinner:YES];
    AppDelegate *appDelegate = kAppDelegate;
//TODO: Replaced appDelegate.employeeId with [[IGUserDefaults sharedInstance].employee.staffId integerValue]
    [[IGService sharedInstance] monthlyTimesheetAverageForEmployee: [[IGUserDefaults sharedInstance].employee.staffID integerValue]
                                                              date:date
                                                        completion:^(id response)
     {
         monthlyAverageModel = [[IGMonthlyAverageModel alloc] initWithDictionary:response];
         [self updateDataForDate:date];
         
         [self showSpinner:NO];
         [self.tableView reloadData];
     }
                                                             error:^(NSError *error)
     {
         [self showSpinner:NO];
         hourSummary = nil;
         [IGUtils showOKAlertWithTitle:kStringError message:[error localizedDescription]];
         monthString = [IGUtils stringFromDate:date dateFormat:@"MM"];
         [self.tableView reloadData];
     }];
}

- (void)updateDataForDate:(NSDate *)date
{
    if ( ![date isKindOfClass:[NSDate class]] || [date isEqualToDate:queryDate])
    {
        return;
    }
    
    queryDate = date;
    
    yearString = [IGUtils stringFromDate:date dateFormat:@"yyyy"];
    monthString = [IGUtils stringFromDate:date dateFormat:@"MM"];
    
    [self updateDataForSelectedSegment];
}

- (void)updateDataForSelectedSegment
{
    hourSummary = ((self.segmentedControl.selectedSegmentIndex == 0) ?
                   monthlyAverageModel.timeSheetHours :
                   monthlyAverageModel.inOutHours);
}

- (IBAction)segmentSelected:(UISegmentedControl *)sender
{
    [self updateDataForSelectedSegment];
    [self.tableView reloadData];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    if (section == 0) {
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 1)];
        view.backgroundColor = [UIColor lightGrayColor];
        return view;
    }
    return nil;
}

@end
