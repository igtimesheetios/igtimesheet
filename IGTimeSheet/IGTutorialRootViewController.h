//
//  GMTutorialRootViewController.h
//  GMPageController
//
//  Created by Upakul on 18/09/14.
//  Copyright (c) 2014 Infogain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGTutorialRootViewController : UIViewController <UIPageViewControllerDelegate>

@property (strong, nonatomic) UIPageViewController *pageViewController;
- (IBAction)dismissTutorial:(id)sender;

@end
