//
//  IGDashboardVC.h
//  IGTimeSheet
//
//  Created by Rajat on 11/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGViewController.h"

@interface IGDashboardVC : IGViewController
{
    IBOutlet UITableView* _tableView;
}

@property(strong,nonatomic)UIRefreshControl *refreshControl;

- (void)populateView:(NSDictionary *)dictionary;
- (IBAction)datePicked:(id)sender;
- (void)refreshView;

@end
