//
//  IGError.h
//  IGTimeSheet
//
//  Created by Manish on 11/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kIGErrorDomain                     @"com.infogain.timesheet.errordomain"

typedef NS_ENUM(NSInteger, IGErrorCode) {
    kIGErrorCodeOffline = 0,
    kIGErrorCodeNullParameter,
    kIGErrorCodeIncorrectParameterType
};


@interface IGError : NSObject

+ (NSError *)errorForService:(NSString *)service
                 description:(NSString *)description
                   errorCode:(IGErrorCode)errorCode;

@end