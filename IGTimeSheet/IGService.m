//
//  IGService.m
//  IGTimeSheet
//
//  Created by Manish on 06/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGService.h"
#import "IGServiceDefine.h"
#import "IGError.h"
#import "IGRequest.h"
#import "IGTaskViewMacros.h"
#import "IGUtils.h"
#import "IGApprovalTrack.h"
#import "IGPMTimeSheet.h"


#define kErrorDomain                        @"com.igTimeSheet"

@interface IGService()
{
    AFHTTPRequestOperationManager *requestOperationManager;
    AFHTTPRequestOperation *versioningRequestOperation;
    AppDelegate *appDelegate;
}

@property (nonatomic, assign) BOOL connected;

@end

@implementation IGService

+ (instancetype)sharedInstance
{
    static dispatch_once_t once;
    static id sharedInstance;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    
    return sharedInstance;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        [[AFNetworkReachabilityManager sharedManager] setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status)
         {
             if ((status == AFNetworkReachabilityStatusReachableViaWWAN) || (status == AFNetworkReachabilityStatusReachableViaWiFi))
             {
                 self.connected = YES;
             }
             else
             {
                 self.connected = NO;
             }
             
             NSLog(@"network changed: %@", (self.connected == YES) ? @"Connected" : @"Disconnected");
             
             
         }];
        
        [[AFNetworkReachabilityManager sharedManager] startMonitoring];
        
        NSURL *baseURL = [NSURL URLWithString:kIGServiceBaseURL];
        requestOperationManager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:baseURL];
        requestOperationManager.requestSerializer = [[AFJSONRequestSerializer alloc] init];
          appDelegate = (AppDelegate *)APP_DELEGATE;

    }
    return self;
}

- (BOOL)networkReachabilityAlert
{
    if ((self.connected == NO))
    {
        [IGUtils showOKAlertWithTitle:@"Connection Error" message:@"Please check your network connection and try again"];
    }
    
    return !self.connected;    
}


#pragma mark - Public Methods

- (AFHTTPRequestOperation *)lloginWithUserName:(NSString *)userName
                                     password:(NSString *)password
                                       domain:(NSString *)domain
                                   completion:(void (^)(NSDictionary *userInfo, NSDictionary *timeSheetInfo))completionBlock
                                        error:(void (^)(NSError *error))errorBlock
{
    if ([IGUtils isValidString:userName] == NO)
    {
        errorBlock([NSError errorWithDomain:kErrorDomain code:0 userInfo:@{NSLocalizedDescriptionKey : @"Username is not valid"}]);
        return nil;
    }
    
    if ([IGUtils isValidString:password] == NO)
    {
        errorBlock([NSError errorWithDomain:kErrorDomain code:0 userInfo:@{NSLocalizedDescriptionKey : @"Password is not valid"}]);
        return nil;
    }
    
    NSString *version = [IGUtils bundleVersion];
    if ([IGUtils isValidString:version] == NO)
    {
        errorBlock([NSError errorWithDomain:kErrorDomain code:0 userInfo:@{NSLocalizedDescriptionKey : @"App Version String is not valid"}]);
        return nil;
    }
    
    
    NSDictionary *params = @{
                             kIGServiceKeyUserName: userName,
                             kIGServiceKeyPassword: password,
                             kIGServiceKeyDomainID: domain
                             };
    
    void (^successBlock)(id) = ^(id response) {
        
        NSLog(@"%s", __FUNCTION__);
        
        if ([IGUtils isDictionary:response])
        {
            NSString *status = response[kKeyLoginStatus];
            if ([IGUtils isValidString:status])
            {
                if ([status isEqualToString:kIGServiceValueSuccess])
                {
                    NSString *empNewID = [response objectForKey:kIGServiceKeyEmployeeNewID];
                    if(empNewID !=nil){
                         [[IGUserDefaults sharedInstance]employee].employeeNewID = empNewID;
                    }
                   
                    NSString *base64EncodedString = [response objectForKey:kIGServiceKeyEmployeeImage];
                    if (base64EncodedString != nil)
                    {
                        NSData *imageData = [[NSData alloc] initWithBase64EncodedString:base64EncodedString options:NSDataBase64DecodingIgnoreUnknownCharacters];
                        if (imageData != nil)
                        {
                            UIImage *img = [UIImage imageWithData:imageData];
                            [IGUserDefaults sharedInstance].userImage = img;
                        }
                    }

                    [self lloginDetailsWithUserName:userName
                                             domain:domain
                                         completion:^(NSDictionary *userInfo, NSDictionary *timeSheetInfo) {
                                             
                                             NSLog(@"%s", __FUNCTION__);
                                             
                                             NSDictionary *lUserInfo = userInfo;
                                             NSDictionary *lTimeSheetInfo = timeSheetInfo;
                                             
                                             if ([[IGUserDefaults sharedInstance] loginWithUser:userInfo updateInfo:YES])
                                             {
                                                 //Track Name for Analytics
                                                 NSString *loginID = [IGUserDefaults sharedInstance].userName;
                                                 
                                                 [IGAnalytics trackEventForCategory:kIGAnalyticsCategoryServerAPIHit
                                                                             action:kIGAnalyticsActionServerAPILogin
                                                                         actionName:loginID];
                                                 completionBlock(lUserInfo,lTimeSheetInfo);
                                                 
//                                                 [self checkUpdateWithCompletion:^(igUpdateResponse *updateResponse) {
//                                                     
//                                                     NSLog(@"%s", __FUNCTION__);
//                                                     completionBlock(updateResponse, lUserInfo, lTimeSheetInfo);
//                                                     
//                                                 } AndError:^(NSError *error) {
//                                                     
//                                                     NSLog(@"%s", __FUNCTION__);
//                                                     errorBlock(error);
//                                                 }];
                                                 
                                             }
                                             else
                                             {
                                                 errorBlock([NSError errorWithDomain:@"com.infogain.timesheet" code:0 userInfo:@{NSLocalizedDescriptionKey: kStringMessageServerDidNotReturnAValidUserData}]);
                                             }
                                             
                                         }
                                              error:^(NSError *error) {
                                                  NSLog(@"%s", __FUNCTION__);
                                                  errorBlock(error);
                                         }];
                }
                else
                {
                    errorBlock([NSError errorWithDomain:@"com.infogain.timesheet" code:0 userInfo:@{NSLocalizedDescriptionKey: kStringMessageIncorrectCredentialsPleaseCheckUsernamePasswordAndTryAgain}]);
                }
            }
            else
            {
                errorBlock([NSError errorWithDomain:@"com.infogain.timesheet" code:0 userInfo:@{NSLocalizedDescriptionKey: kStringMessageInvalidResponseFromServer}]);
            }
        }
        else
        {
            errorBlock([NSError errorWithDomain:@"com.infogain.timesheet" code:0 userInfo:@{NSLocalizedDescriptionKey: kStringMessageInvalidResponseFromServer}]);
        }
        
    };
    
    return [self POSTServiceWithName:kIGServiceLogin
                          parameters:params
                             success:successBlock
                               error:errorBlock];
}

- (AFHTTPRequestOperation *)lloginDetailsWithUserName:(NSString *)userName
                                               domain:(NSString *)domain
                                           completion:(void (^)(NSDictionary *userInfo, NSDictionary *timeSheetInfo))completionBlock
                                                error:(void (^)(NSError *error))errorBlock
{
    if ((userName == nil) || (domain == nil))
    {
        NSError *error = [IGError errorForService:kIGServiceLoginDetails description:nil errorCode:kIGErrorCodeNullParameter];
        errorBlock(error);
        return nil;
    }
    
    NSDictionary *params = @{
                             kIGServiceKeyUserName: userName,
                             kIGServiceKeyDomainID: domain
                             };
    
    void (^success)(id) = ^(id response){
        NSDictionary *jsonDictionary = (NSDictionary *)response;
        NSDictionary *logingDetails = jsonDictionary[kIGServiceKeyLoginDetail][0];
        NSDictionary *timeSheetDetails = [jsonDictionary[@"TimesheetStatus"] objectAtIndex:0];
        completionBlock(logingDetails,timeSheetDetails);
    };
    
    return [self POSTServiceWithName:kIGServiceLoginDetails
                          parameters:params
                             success:success
                               error:errorBlock];
}

//old
- (AFHTTPRequestOperation *)loginDetailsWithUserName:(NSString *)userName
                                              domain:(NSString *)domain
                                          completion:(void (^)(id response))completionBlock
                                               error:(void (^)(NSError *error))errorBlock
{
    if ((userName == nil) || (domain == nil))
    {
        NSError *error = [IGError errorForService:kIGServiceLoginDetails description:nil errorCode:kIGErrorCodeNullParameter];
        errorBlock(error);
        return nil;
    }
    
    NSDictionary *params = @{
                             kIGServiceKeyUserName: userName,
                             kIGServiceKeyDomainID: domain
                             };
    
    
    return [self POSTServiceWithName:kIGServiceLoginDetails parameters:params success:completionBlock error:errorBlock];
}

//old
- (AFHTTPRequestOperation *)loginWithUserName:(NSString *)userName
                                     password:(NSString *)password
                                       domain:(NSString *)domain
                                   completion:(void (^)(id response))completionBlock
                                        error:(void (^)(NSError *error))errorBlock
{
    if ((userName == nil) || (password == nil) || (domain == nil))
    {
        NSError *error = [IGError errorForService:kIGServiceLogin description:nil errorCode:kIGErrorCodeNullParameter];
        errorBlock(error);
        return nil;
    }

    NSDictionary *params = @{
                             kIGServiceKeyUserName: userName,
                             kIGServiceKeyPassword: password,
                             kIGServiceKeyDomainID: domain
                             };
    
    return [self POSTServiceWithName:kIGServiceLogin parameters:params success:completionBlock error:errorBlock];
}

- (AFHTTPRequestOperation *)imageForEmployee:(NSInteger)employeeID
                                  completion:(void(^)(UIImage *image))completionBlock
                                       error:(void (^)(NSError *error))errorBlock;
{
    NSError *error = nil;
    NSString *serviceName = kIGServiceEmployeeImage;
    NSString *employeeIDValue = [IGUserDefaults sharedInstance].employee.staffID;//[NSString stringWithFormat:@"%ld", (long)employeeID];
    NSDictionary *params = @{
                             kIGServiceKeyEmployeeId: employeeIDValue
                             };
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
        return nil;
    }
    
    return [self POSTServiceWithName:serviceName parameters:params success:^(id response) {
        NSDictionary *dataDictionary = (NSDictionary *)response;
        NSData *imageData = [[NSData alloc]initWithBase64EncodedString:[dataDictionary objectForKey:kIGServiceKeyEmployeeImage] options:NSDataBase64DecodingIgnoreUnknownCharacters];
        UIImage *employeeImage = nil;
        if (imageData == nil)
        {
            employeeImage = [UIImage imageNamed:@"profile-avtaar-pic"];
        }
        else
        {
            employeeImage = [UIImage imageWithData:imageData];
        }
        completionBlock(employeeImage);
    } error:errorBlock];
}

- (AFHTTPRequestOperation *)weekEndDates:(NSString *)year
                                   month:(NSString *)month
                              completion:(void (^)(id response))completionBlock
                                   error:(void (^)(NSError *error))errorBlock
{
    NSError *error = nil;
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
        errorBlock(error);
        return nil;
    }
    NSDictionary *params = @{
                             kIGServiceKeyYear:year,
                             kIGServiceKeyMonth:month
                             };
    
    return [self POSTServiceWithName: kIGServiceWeekEndDates
                          parameters:params
                             success:completionBlock
                               error:errorBlock];
}

- (AFHTTPRequestOperation *)inOutTimeForEmployee:(NSInteger)employeeID
                                            date:(NSDate *)date
                                      completion:(void (^)(id response))completionBlock
                                           error:(void (^)(NSError *error))errorBlock
{
    NSError *error = nil;
    NSDictionary *parameters = [IGRequest requestParameterWithEmployeeID:employeeID
                                                                    date:date
                                                                 service:kIGServiceInOutTime
                                                                   error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
        errorBlock(error);
        return nil;
    }
    
    return [self POSTServiceWithName:kIGServiceInOutTime
                          parameters:parameters
                             success:completionBlock
                               error:errorBlock];
}

- (AFHTTPRequestOperation *)taskListForEmployee:(NSInteger)employeeID
                                      projectID:(NSInteger)projectID
                                   isUSEmployee:(BOOL)isUSEmployee
                                            CRID:(NSString*)cRID
                             isBillableTaskType:(BOOL)isBillable
                                        forWeek:(NSInteger)weekId
                                     completion:(void (^)(id response))completionBlock
                                          error:(void (^)(NSError *error))errorBlock
{
    NSString *employeeIDValue = [IGUserDefaults sharedInstance].employee.staffID;//[NSString stringWithFormat:@"%ld", (long)employeeID];
    NSInteger isUSEmployeeValue = [IGUserDefaults sharedInstance].ifUserUSEmployee;
    NSInteger isBillableTaskType = isBillable ? 1: 0;
    
    NSDictionary *params = @{
                             kIGServiceKeyEmployeeId: employeeIDValue,
                             kIGServiceKeyProjectID: @(projectID),
                             kIGServiceKeyIsUSEmployee: @(isUSEmployeeValue),
                             kIGServiceKeyIsBillableTaskType: @(isBillableTaskType),
                             kIGServiceKeyWeekId: @(weekId),
                              kIGServiceKeyCRId:cRID
                             
                            };
    
    return [self POSTServiceWithName:kIGServiceTaskList parameters:params success:completionBlock error:errorBlock];
}



- (AFHTTPRequestOperation *)categoryListForUSEmployee:(NSInteger)employeeID
                                      projectID:(NSInteger)projectID
                                   isUSEmployee:(BOOL)isUSEmployee
                                                 CRID:(NSString*)cRID
                             isBillableTaskType:(NSString*)isBillable
                                        forWeek:(NSInteger)weekId
                                     completion:(void (^)(id response))completionBlock
                                          error:(void (^)(NSError *error))errorBlock
{
    NSString *employeeIDValue = [IGUserDefaults sharedInstance].employee.staffID;//[NSString stringWithFormat:@"%ld", (long)employeeID];
    NSInteger isUSEmployeeValue = [IGUserDefaults sharedInstance].ifUserUSEmployee;
    
    NSDictionary *params = @{
                             kIGServiceKeyEmployeeId: employeeIDValue,
                             kIGServiceKeyProjectID: @(projectID),
                             kIGServiceKeyIsUSEmployee: @(isUSEmployeeValue),
                             kIGServiceKeyIsBillableTaskType: isBillable,
                             kIGServiceKeyWeekId: @(weekId),
                             kIGServiceKeyCRId:cRID
                             };
    
    return [self POSTServiceWithName:kIGUSEmployeeTaskList parameters:params success:completionBlock error:errorBlock];
}

- (AFHTTPRequestOperation *)CRIListForEmployee:(NSString*)CRID
                                                completion:(void (^)(id response))completionBlock
                                                error:(void (^)(NSError *error))errorBlock
{
    
    NSDictionary *params = @{
                             kIGCRID: CRID
                             };
    
    return [self POSTServiceWithName:kIGServiceCRIList parameters:params success:completionBlock error:errorBlock];
}

- (AFHTTPRequestOperation *)subTaskListForEmployee:(NSInteger)employeeID
                                         projectID:(NSInteger)projectID
                                            taskID:(NSInteger)taskID
                                   timeSheetStatus:(IGTimeSheetStatus)timeSheetStatus
                                                     CRID:(NSString*)cRID
                                      isUSEmployee:(BOOL)isUSEmployee
                                        completion:(void (^)(id response))completionBlock
                                             error:(void (^)(NSError *error))errorBlock
{
    NSString *employeeIDValue = [IGUserDefaults sharedInstance].employee.staffID;//[NSString stringWithFormat:@"%ld", (long)employeeID];
    NSInteger isUSEmployeeValue = [IGUserDefaults sharedInstance].ifUserUSEmployee;

    NSString *timeSheetStatusValue = nil;

    switch (timeSheetStatus)
    {
        case kIGTimeSheetStatusPending:
        {
            timeSheetStatusValue = TimeSheetPendingStatus;
            break;
        }
        
        case kIGTimeSheetStatusCompleted:
        {
            timeSheetStatusValue = TimeSheetCompleteStatus;
            break;
        }
            
        default:
        {
            timeSheetStatusValue = TimeSheetPendingStatus;
            break;
        }
    }
    
    NSDictionary *params = @{
                             kIGServiceKeyEmployeeId: employeeIDValue,
                             kIGServiceKeyProjectID: @(projectID),
                             kIGServiceKeyTaskID: @(taskID),
                             kIGServiceKeyIsUSEmployee: @(isUSEmployeeValue),
                             kIGServiceKeyTimeSheetStatus: timeSheetStatusValue,
                             kIGServiceKeyCRId:cRID
                             };
    
    return [self POSTServiceWithName:kIGServiceSubTaskList
                          parameters:params
                             success:completionBlock
                               error:errorBlock];
}


- (AFHTTPRequestOperation *)leavesForEmployee:(NSInteger)employeeID
                                         date:(NSDate *)date
                                   completion:(void (^)(id response))completionBlock
                                        error:(void (^)(NSError *error))errorBlock
{
    NSError *error = nil;
    NSString *serviceName = kIGServiceViewLeaves;
    NSDictionary *parameters = [IGRequest requestParameterWithEmployeeID:employeeID
                                                                    date:date
                                                                 service:serviceName
                                                                   error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
        errorBlock(error);
        return nil;
    }
    
    return [self POSTServiceWithName:serviceName
                          parameters:parameters
                             success:completionBlock
                               error:errorBlock];
}

- (AFHTTPRequestOperation *)weeklyTimesheetSummaryForEmployee:(NSInteger)employeeID
                                                         date:(NSDate *)date
                                                   completion:(void (^)(id response))completionBlock
                                                        error:(void (^)(NSError *error))errorBlock
{
    NSError *error = nil;
    NSString *serviceName = kIGServiceWeeklyTimeSheetSummary;
    NSDictionary *parameters = [IGRequest requestParameterWithEmployeeID:employeeID
                                                                    date:date
                                                                 service:serviceName
                                                                   error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
        errorBlock(error);
        return nil;
    }
    
    return [self POSTServiceWithName:serviceName
                          parameters:parameters
                             success:completionBlock
                               error:errorBlock];
}


- (AFHTTPRequestOperation *)monthlyTimesheetSummaryForEmployee:(NSInteger)employeeID
                                                          date:(NSDate *)date
                                                    completion:(void (^)(id response))completionBlock
                                                         error:(void (^)(NSError *error))errorBlock
{
    NSError *error = nil;
    NSString *serviceName = kIGServiceMonthlyTimeSheetSummary;
    NSDictionary *parameters = [IGRequest requestParameterWithEmployeeID:employeeID
                                                                    date:date
                                                                 service:serviceName
                                                                   error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
        errorBlock(error);
        return nil;
    }
    
    return [self POSTServiceWithName:serviceName
                          parameters:parameters
                             success:completionBlock
                               error:errorBlock];
}

- (AFHTTPRequestOperation *)viewTaskForEmployee:(NSString *)employeeID
                                         weekID:(NSInteger)weekID
                                          order:(NSInteger)order
                                     completion:(void (^)(id response))completionBlock
                                          error:(void (^)(NSError *error))errorBlock
{
    NSString *serviceName = kIGServiceDayWiseTaskDetail;
    
    
    if ([IGUtils isValidString:employeeID] == NO)
    {
        NSError *error = [IGError errorForService:serviceName
                                      description:@"Employee Code is not a valid String"
                                        errorCode:kIGErrorCodeNullParameter];
        
        errorBlock(error);
        return nil;
    }
    
    NSDictionary *parameters = @{
                                 kIGServiceKeyEmployeeId: employeeID,
                                 kIGServiceKeyWeekID:@(weekID),
                                 kIGServiceKeyOrder:@(order)
                                 };
    
    return [self POSTServiceWithName:kIGServiceDayWiseTaskDetail
                          parameters:parameters
                             success:completionBlock
                               error:errorBlock];
}

- (AFHTTPRequestOperation *)removeTask:(NSInteger)cardEntryDetailId
                                                    completion:(void (^)(id response))completionBlock
                                                         error:(void (^)(NSError *error))errorBlock
{
    NSError *error = nil;
    NSString *serviceName = kIGServiceRemoveTask;
    NSDictionary *parameters = @{
                                 kIGServiceKeyEmployeeCardEntryId:@(cardEntryDetailId)
                                 };
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
        errorBlock(error);
        return nil;
    }
    
    return [self POSTServiceWithName:serviceName
                          parameters:parameters
                             success:completionBlock
                               error:errorBlock];
}

- (NSArray *)getTasksToSubmitForArray:(NSArray *)tasks forEmployee:(NSInteger)employeeID forWeek:(NSInteger)weekId withStatus:(NSString *)status
{
    NSMutableArray* paramsArr = [[NSMutableArray alloc] init];
    NSString *employeeIDValue = [IGUserDefaults sharedInstance].employee.staffID;//[NSString stringWithFormat:@"%ld", (long)employeeID];
    NSString* isUSEmployeeValue = @"N";
    if([IGUserDefaults sharedInstance].ifUserUSEmployee)
        isUSEmployeeValue = @"Y";
    
    
    
    for(int i = 0; i < tasks.count; ++i)
    {
        IGTaskModalClass* task = [tasks objectAtIndex:i];
        NSString *employeeCardEntryIDValue = [NSString stringWithFormat:@"%ld", (long)task.cardEntryDetailId];
        
        NSDictionary *params = @{
                                 kIGServiceKeyEmployeeCardEntryId:employeeCardEntryIDValue,
                                 kIGServiceKeyEmployeeId: employeeIDValue,
                                 kIGServiceKeyProjectID: @(task.prjId),
                                 kIGServiceKeyWeekId: @(weekId),
                                 kIGServiceKeyStatusId: status,
                                 kIGServiceKeyComments:@"",
                                 kIGServiceKeyLocation:task.location,
                                 kIGServiceKeyLocationType:task.hrsType,
                                 kIGServiceKeyTaskID:@(task.taskId),
                                 kIGServiceKeySubTaskId:@(task.subTaskId),
                                 kIGServiceKeyCRId:task.cRID,
                                 kIGServiceKeyDescription:task.description,
                                 EMon:@(task.monTime),
                                 ETue:@(task.tueTime),
                                 EWed:@(task.wedTime),
                                 EThu:@(task.thuTime),
                                 EFri:@(task.friTime),
                                 ESat:@(task.satTime),
                                 ESun:@(task.sunTime),
                                 kIGServiceKeyModifiedBy:employeeIDValue,
                                 kIGServiceKeyIsBillable:task.billable,
                                 kIGServiceKeyintPayrollTaskId:[NSString stringWithFormat:@"%ld",(long)task.categoryID],
                                 kIGServiceKeyTransMidOfWeek:@(0),
                                 kIGServiceKeyWFOL:@(task.wfol),
                                 kIGServiceKeyIsUSRecord:isUSEmployeeValue
                                 };
        
        [paramsArr addObject:params];
    }
    
    return paramsArr;
}


- (AFHTTPRequestOperation *)submitTask:(NSArray *)tasks forEmployee:(NSInteger)employeeID
                            withStatus:(NSString *)status forWeek:(NSInteger)weekId
                                     completion:(void (^)(id response))completionBlock
                                          error:(void (^)(NSError *error))errorBlock
{
    NSArray* params = [self getTasksToSubmitForArray:tasks forEmployee:employeeID forWeek:weekId withStatus:status];
    
    return [self POSTServiceWithName:kIGServiceSubmit arrParameters:params success:completionBlock error:errorBlock];
}

- (AFHTTPRequestOperation *)monthlyTimesheetAverageForEmployee:(NSInteger )employeeID
                                                          date:(NSDate *)date
                                                    completion:(void (^)(id response))completionBlock
                                                         error:(void (^)(NSError *error))errorBlock
{
    NSError *error = nil;
    NSString *serviceName = kIGServiceMonthlyTimeSheetAverage;
    NSDictionary *parameters = [IGRequest requestParameterWithEmployeeID:employeeID
                                                                    date:date
                                                                 service:serviceName
                                                                   error:&error];
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
        errorBlock(error);
        return nil;
    }
    
    return [self POSTServiceWithName:serviceName
                          parameters:parameters
                             success:completionBlock
                               error:errorBlock];
}

- (AFHTTPRequestOperation *)DuplicateTimeSheetForEmployee:(NSInteger)employeeID
                                                   weekId:(NSInteger)weekId
                                               completion:(void (^)(id response))completionBlock
                                                    error:(void (^)(NSError *error))errorBlock
{
    NSError *error = nil;
    NSDictionary *params = @{
                             kIGServiceKeyEmployeeId:[IGUserDefaults sharedInstance].employee.staffID,//[NSString stringWithFormat:@"%ld",(long)employeeID],
                             kIGServiceKeyLoginBy:[NSString stringWithFormat:@"%ld",(long)employeeID],
                             kIGServiceKeyWeekId: @(weekId),
                             kIGServiceKeyIsUSEmployee:@([IGUserDefaults sharedInstance].ifUserUSEmployee)
                             };
    
    if (error)
    {
        NSLog(@"%@", [error localizedDescription]);
        errorBlock(error);
        return nil;
    }
    
    return [self POSTServiceWithName:kIGServiceDuplicateTimeSheet
                          parameters:params
                             success:completionBlock
                               error:errorBlock];
}


#pragma mark - Private Methods

- (AFHTTPRequestOperation *)POSTServiceWithName:(NSString *)serviceName
                                     parameters:(id)parameters
                                        success:(void (^)(id response))completionBlock
                                          error:(void (^)(NSError *error))errorBlock
{
    //TODO: test reachability
    NSString *jsonRequest = [IGUtils jsonStringFromObject:parameters];
    NSLog(@"Request: %@%@ \n %@", requestOperationManager.baseURL, serviceName, jsonRequest);


    
    
    return [requestOperationManager POST:serviceName
                              parameters:parameters
                                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                     NSLog(@"Response: %@%@ : \n %@", requestOperationManager.baseURL, serviceName, responseObject);
                                     completionBlock(responseObject);
                                 } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                     NSLog(@"ERROR %@%@ : \n %@", requestOperationManager.baseURL, serviceName, [error localizedDescription]);
                                     errorBlock(error);
                                 }];
}

- (AFHTTPRequestOperation *)POSTServiceWithName:(NSString *)serviceName
                                     arrParameters:(NSArray *)parameters
                                        success:(void (^)(id response))completionBlock
                                          error:(void (^)(NSError *error))errorBlock
{
    //TODO: test reachability
    NSString *jsonRequest = [IGUtils jsonStringFromObject:parameters];
    NSLog(@"Request: %@%@ \n %@", requestOperationManager.baseURL, serviceName, jsonRequest);
    
   
  
    return [requestOperationManager POST:serviceName
                              parameters:parameters
                                 success:^(AFHTTPRequestOperation *operation, id responseObject) {
                                     NSLog(@"Response: %@%@ : \n %@", requestOperationManager.baseURL, serviceName, responseObject);
                                     completionBlock(responseObject);
                                 } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                                     NSLog(@"ERROR %@%@ : \n %@", requestOperationManager.baseURL, serviceName, [error localizedDescription]);
                                     errorBlock(error);
                                 }];
}



//- (igUpdateResponse *)checkUpdateWithCompletion:(void (^)(BOOL isUpdateAvailable, NSString *title, NSString *message, NSString *path))completionBlock
//                            AndError:(void (^)(NSError *error))errorBlock{
//TODO: Replace with below
- (igUpdateResponse *)checkUpdateWithCompletion:(void (^)(igUpdateResponse *updateResponse))completionBlock AndError:(void (^)(NSError *error))errorBlock{

    NSString *versionCheckURLString = kVersionCheckURL;
    NSURL *versionCheckURL = [NSURL URLWithString:versionCheckURLString];
    NSURLRequest *versionCheckRequest = [[NSURLRequest alloc] initWithURL:versionCheckURL cachePolicy:NSURLRequestReloadIgnoringLocalAndRemoteCacheData timeoutInterval:10];
    versioningRequestOperation = [[AFHTTPRequestOperation alloc] initWithRequest:versionCheckRequest];
    
    
    __block NSString *isUpdateAvailable;
    __block NSString *isMandatory;
    __block NSString *title;
    __block NSString *message;
    __block NSString *url = @"Initializing with non nil";
    __block igUpdateResponse *updateResponse;
    
    void (^successBlock)(AFHTTPRequestOperation *, id) = ^(AFHTTPRequestOperation *operation, id responseObject){
        if (responseObject != nil)
        {
            NSError *pListError = nil;
            NSDictionary *dataFromPlist = [NSPropertyListSerialization propertyListWithData:responseObject
                                                                                    options:0
                                                                                     format:0
                                                                                      error:&pListError];
            if (!pListError)
            {
                
                if ([IGUtils isDictionary:dataFromPlist])
                {
                    NSDictionary *iOSUpdateDictionary = [dataFromPlist objectForKey:@"iOS"];
                    NSString *versionOnServer_String = [iOSUpdateDictionary objectForKey:kKeyLatestVersion_iOS];
                    NSNumber *versionOnServer = [NSNumber numberWithFloat:[versionOnServer_String floatValue]];
                    
                    NSNumber *versionOnDevice = [NSNumber numberWithFloat:[[IGUtils bundleVersion] floatValue]];
                    
                    if ([versionOnServer floatValue] > [versionOnDevice floatValue])
                    {
                        
                        isUpdateAvailable = kStringYES;
                        isMandatory = [iOSUpdateDictionary objectForKey:@"isMandatory"];
                        url = [iOSUpdateDictionary objectForKey:@"Path"];
                        title = kStringUpdateAvailable;
                        message = [NSString stringWithFormat:kStringANewVersion_IsAvailableForDownload,versionOnServer_String];
                        
                    }
                    else
                    {
                        isUpdateAvailable = kStringNO;
                        isMandatory = kStringNO;
                        title = kStringNoNewUpdates;
                        message = kStringThereAreNoNewUpdatesForigTimeSheet;
                    }

                }
                else
                {
                    //TODO: handle error
                }
                
            }
            else
            {
                isUpdateAvailable = kStringNO;
                isMandatory = kStringNO;
                title = kStringServerError;
                message = kStringUnexpectedResponseFromServer;
            }
        }
        else
        {
            isUpdateAvailable = kStringNO;
            isMandatory = kStringNO;
            title = kStringServerError;
            message = kStringSomethingWentWrongPleaseTryAgain;
        }
        NSDictionary *updateDictionary = @{
                                           kIGServiceKeyIsUpdate : isUpdateAvailable,
                                           kIGServiceKeyIsMandatory : isMandatory,
                                           kIGServiceKeyTitle : title,
                                           kIGServiceKeyMessage : message,
                                           kIGServiceKeyURL : url
                                           };
        updateResponse = [[igUpdateResponse alloc] initWithDictionary:updateDictionary];
//TODO: Replace this with 4 param completion
        completionBlock(updateResponse);
//        completionBlock(isUpdateAvailable,title,message,path);
    };
    
    
    void (^failureBlock)(AFHTTPRequestOperation *,id) = ^(AFHTTPRequestOperation *operation, NSError *error)
    {
//        isUpdateAvailable = kStringNO;
//        isMandatory = kStringNO;
//        title = kStringServerError;
//        message = kStringSomethingWentWrongPleaseTryAgain;
//        NSDictionary *updateDictionary = @{
//                                           kIGServiceKeyIsUpdate : isUpdateAvailable,
//                                           kIGServiceKeyIsMandatory : isMandatory,
//                                           kIGServiceKeyTitle : title,
//                                           kIGServiceKeyMessage : message,
//                                           kIGServiceKeyURL : path
//                                           };
//        updateResponse = [[igUpdateResponse alloc] initWithDictionary:updateDictionary];
        
        completionBlock(updateResponse);
//        TODO: Replace
//        errorBlock(updateResponse, error);
        
    };
    
    [versioningRequestOperation setCompletionBlockWithSuccess:successBlock
                                                      failure:failureBlock];

    [versioningRequestOperation start];
    
    return nil;
}


- (AFHTTPRequestOperation *)registerDeviceWithToken:(NSString *)token
                                         deviceType:(NSString *)deviceType
                                         employeeID:(NSString *)employeeID
                                         completion:(void (^)(id response))completionBlock
                                              error:(void (^)(NSError *error))errorBlock
{
    if ([IGUtils isValidString:token] && [IGUtils isValidString:employeeID])
    {
        //TODO: Make parameters as per service document
        NSDictionary *parameters = @{
                                     @"deviceToken": token,
                                     @"deviceType": @"iOS",
                                     @"id": employeeID
                                     };
        //TODO: Replace service name with service name as mentioned in service document
        [self POSTServiceWithName:@"Replace Name Of Service As given in service document"
                       parameters:parameters
                          success:completionBlock
                            error:errorBlock];
    }
    else
    {
        errorBlock([NSError errorWithDomain:kErrorDomain code:0 userInfo:@{NSLocalizedDescriptionKey: @"Parameters not valid"}]);
    }
    
    return nil;
}


-(AFHTTPRequestOperation *)validDaysOfPorject:(NSInteger)employeeID
                                      projectID:(NSInteger)projectID
                                        weekendDate:(NSDate *)weekendDate
                                     completion:(void (^)(id response))completionBlock
                                          error:(void (^)(NSError *error))errorBlock
{
    NSString *employeeIDValue = [IGUserDefaults sharedInstance].employee.staffID;//[NSString stringWithFormat:@"%ld", (long)employeeID];
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"yyyy-MM-dd";
    NSLog(@"Date : %@",weekendDate);
    
    NSString *stringDate = [format stringFromDate:weekendDate];

    NSLog(@"%@", stringDate);
    NSDictionary *params = @{
                             @"Empid": employeeIDValue,
                             @"projectId": @(projectID),
                             @"WeekendDate": stringDate
                             };
    
    appDelegate.requestType = kIGServiceValidDaysRequest;
    return [self POSTServiceWithName:kIGServiceValidDaysOfProject parameters:params success:completionBlock error:errorBlock];
}


#pragma mark - PMApproval Webservices

- (AFHTTPRequestOperation *)accounts:(NSString*)PMID
                          completion:(void (^)(id response))completionBlock
                               error:(void (^)(NSError *error))errorBlock{
    NSDictionary * params = @{
                              kIGServiceApprovalPMID : PMID
                              };
    return [self POSTServiceWithName:kIGServiceApprovalAccounts parameters:params success:completionBlock error:errorBlock];
    
}

- (AFHTTPRequestOperation *)tracksWith:(NSString *)accountID
                                  pmID:(NSString *)pmID
                                    year:(NSString *)year
                                    completion:(void (^)(id response))completionBlock
                                    error:(void (^)(NSError *error))errorBlock{
    NSDictionary * params = @{
                              kIGServiceApprovalPMID : pmID,
                              kIGServiceApprovalAccountID: accountID,
                              kIGServiceApprovalYear: year
                              };
    
    return [self POSTServiceWithName:kIGServiceApprovalTracks parameters:params success:completionBlock error:errorBlock];
    
}

- (AFHTTPRequestOperation *)timesheets:(IGApprovalTimeSheetRequest *)request
                            completion:(void (^)(id response))completionBlock
                                 error:(void (^)(NSError *error))errorBlock{

    NSDictionary * params = @{
                              kIGServiceApprovalAccountID: request.accountID,
                              kIGServiceApprovalProjectID:request.projectID,
                              kIGServiceApprovalWeekID: request.WeekID,
                              kIGServiceApprovalStatusID:request.statusID,
                              kIGServiceApprovalLoginBy: request.loginBy
                              
                              };

    
    return [self POSTServiceWithName:kIGServiceApprovalTimeSheets parameters:params success:completionBlock error:errorBlock];
    
}


- (AFHTTPRequestOperation *)approve:(NSSet *)timeSheetList approve:(BOOL)isApproval
                         completion:(void (^)(id response))completionBlock
                              error:(void (^)(NSError *error))errorBlock{
    
    NSString *endPoint = isApproval ? kIGServiceApprovalAppoveTimeSheet: kIGServiceApprovalRejectTimeSheet;
    
    NSMutableArray *timeSheetArray = [NSMutableArray new];
    
    for (IGEmployeeTimesheet *employeeTS in timeSheetList) {
        
        NSDictionary * params = @{
                                  kIGServiceApprovalProjectID:employeeTS.projectID,
                                  kIGServiceApprovalWeekID: [NSString stringWithFormat:@"%ld",employeeTS.weekendID],
                                  kIGServiceApprovalLoginBy: [[IGUserDefaults sharedInstance] employee].staffID,
                                  kIGServiceApprovalPMID:employeeTS.oldEmployeeID,
                                  kIGServiceApprovalNewEmployeeID:employeeTS.employeeID
                                  };
        [timeSheetArray addObject:params];
    }
    
    return [self POSTServiceWithName:endPoint parameters:timeSheetArray success:completionBlock error:errorBlock];

}

- (AFHTTPRequestOperation *)add:(NSString *)note
                             id:(NSString *)cardEntryID
                        addedBy:(NSString *)managerID
                     complestion:(void (^)(id response))completionBlock
                          error:(void (^)(NSError *error))errorBlock{
    
    NSDictionary * params = @{
                              kIGServiceApprovalNote:note,
                              kIGServiceApprovalCardEntryID:cardEntryID,
                              kIGServiceApprovalLoginBy: managerID
                              };
    return [self POSTServiceWithName:kIGServiceApprovalAddNote parameters:params success:completionBlock error:errorBlock];
    
}


- (AFHTTPRequestOperation *)months:(NSString *)year
                        completion:(void (^)(id response))completionBlock
                             error:(void (^)(NSError *error))errorBlock{
    NSDictionary * params = @{
                              kIGServiceApprovalYear:year
                              };
    
    return [self POSTServiceWithName:kIGServiceApprovalMonths parameters:params success:completionBlock error:errorBlock];

}

- (AFHTTPRequestOperation *)timeSheetDetail:(NSString *)timeSheetID
                                 completion:(void (^)(id response))completionBlock
                                      error:(void (^)(NSError *error))errorBlock{
    NSDictionary * params = @{
                              kIGServiceApprovalCardEntryID:timeSheetID
                              };
    
    return [self POSTServiceWithName:kIGServiceApprovalTotalHours parameters:params success:completionBlock error:errorBlock];

}


@end
