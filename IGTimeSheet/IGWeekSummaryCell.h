//
//  IGWeekSummaryCell.h
//  IGTimeSheet
//
//  Created by Neha on 12/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IGWeekSummaryCell;
@protocol IGWeekSumamryCellDelegate <NSObject>
- (void)viewTaskForCell:(IGWeekSummaryCell *)cell;
@end


@interface IGWeekSummaryCell : UITableViewCell

@property(strong) IBOutlet UIView *viewBackground;
@property(strong) IBOutlet UILabel *date;
@property(strong) IBOutlet UILabel *weekday;
@property(strong) IBOutlet UILabel *efforts;

@property (nonatomic, weak) id<IGWeekSumamryCellDelegate> delegate;

- (IBAction)viewTask:(id)sender;

@end
