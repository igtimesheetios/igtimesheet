//
//  IGDayWiseTaskDetailVC.h
//  IGTimeSheet
//
//  Created by Rajat on 27/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGViewController.h"

@interface IGTaskDetailVC : IGViewController

@property (nonatomic, assign) NSInteger order;
@property (nonatomic, strong) NSString *employeeIDString;
@property (nonatomic, assign) NSInteger weekID;
@property (weak, nonatomic) IBOutlet UILabel *noTaskViewLabel;

@end
