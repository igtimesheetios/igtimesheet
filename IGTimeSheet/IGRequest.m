//
//  IGRequest.m
//  IGTimeSheet
//
//  Created by Manish on 06/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGRequest.h"
#import "IGError.h"
#import "IGServiceDefine.h"
#import "IGUserDefaults.h"

@implementation IGRequest

+ (NSDictionary *)requestParameterWithEmployeeID:(NSInteger)employeeID
                                            date:(NSDate *)date
                                         service:(NSString *)service
                                           error:(NSError **)error
{
    NSString *employeeIDString = [IGUserDefaults sharedInstance].employee.staffID;///[NSString stringWithFormat:@"%ld", employeeID];
    
    if (date == nil)
    {
        *error = [IGError errorForService:service
                                      description:nil
                                        errorCode:kIGErrorCodeNullParameter];
        return nil;
    }
    
    if ([date isKindOfClass:[NSDate class]] == NO)
    {
        NSString *description = [NSString stringWithFormat:@"Expected a Date object. Got %@", NSStringFromClass([date class])];
        
        *error = [IGError errorForService:service
                                      description:description
                                        errorCode:kIGErrorCodeIncorrectParameterType];
        return nil;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterLongStyle];
    [dateFormatter setTimeStyle:NSDateFormatterNoStyle];
    NSString *dateString = [dateFormatter stringFromDate:date];
    
    
    NSDictionary *parameters = @{
        kIGServiceKeyDate: dateString,
        kIGServiceKeyEmployeeId: employeeIDString,
        kIGServiceKeyIsUSRecord:[NSString stringWithFormat:@"%ld", [IGUserDefaults sharedInstance].ifUserUSEmployee]
    };

//    NSDictionary *parameters = @{
//                                 kIGServiceKeyDate: @"September 18, 2016",
//                                 kIGServiceKeyEmployeeId: @"2000812",
//                                 kIGServiceKeyIsUSRecord:@"0"
//                                 };

    return parameters;
    
}

@end
