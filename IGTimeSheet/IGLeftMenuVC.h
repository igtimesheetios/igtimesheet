//
//  IGLeftMenuVC.h
//  IGTimeSheet
//
//  Created by Manish on 09/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@class IGLeftMenuVC;
@protocol IGLeftMenuVCDelegate <NSObject>

- (void)leftMenu:(IGLeftMenuVC *)leftMenuVC selectionIndex:(NSInteger)index;

@end


@class IGSideMenuRootVC;
@interface IGLeftMenuVC : UIViewController<UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UIImageView *userImageView;

@property (strong, nonatomic) IBOutlet UITableView *menuVCTableView;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *widthConstraint;

@property (strong, nonatomic) NSArray *optionsArray;
@property (strong, nonatomic) NSArray *imageArray;
@property (nonatomic, weak) id<IGLeftMenuVCDelegate> delegate;

@end
