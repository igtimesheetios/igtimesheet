//
//  IGEmployee.m
//  IGTimeSheet
//
//  Created by Manish on 23/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGEmployee.h"
#import "IGServiceDefine.h"
#import "IGUtils.h"
#import "NSDictionary+IGAddition.h"

@implementation IGEmployee

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    if ([dictionary respondsToSelector:@selector(objectForKey:)] == NO)
    {
        NSLog(@"Expected a dictionary. Got %@", NSStringFromClass([dictionary class]));
        return nil;
    }
    
    self = [super init];
    if (self)
    {
        _name = [dictionary IGStringValueForKey:kIGServiceKeyEmployeeName];
        _mailID = [dictionary IGStringValueForKey:kIGServiceKeyEmployeeMailID];
        _status = [dictionary IGStringValueForKey:kIGServiceKeyEmployeeStatus];
        _onsiteManager = [dictionary IGStringValueForKey:kIGServiceKeyOnsiteManager];
        _payrollAdmin = [dictionary IGStringValueForKey:kIGServiceKeyPayrollAdmin];
        _timesheetType = [dictionary IGStringValueForKey:kIGServiceKeyTimesheetType];
        _staffID = [dictionary IGStringValueForKey:kIGServiceKeyEmployeeStaffID];
        _employeeNewID = [dictionary IGStringValueForKey:kIGServiceKeyEmployeeNewID];
        _staffID1 = [dictionary IGStringValueForKey:kIGServiceKeyEmployeeStaffID1];
        _projectManagerID = [dictionary IGStringValueForKey:kIGServiceKeyProjectManagerID];
        
        _baseLocationID = [dictionary IGNumberValueForKey:kIGServiceKeyEmployeeBaseLocationID];
        _locationID = [dictionary IGNumberValueForKey:kIGServiceKeyEmployeeLocationID];
        

        //TODO: compare for possible values
        NSString *isContractor = [dictionary IGStringValueForKey:kIGServiceKeyIsContractor];
        _isContractor = [isContractor isEqualToString:@"0"] ? NO: YES;
        
        NSString *isUSEmployee = [dictionary IGStringValueForKey:kIGServiceKeyIsUSEmployee];
        _isUSEmployee = [isUSEmployee isEqualToString:@"0"] ? NO: YES;
        
        //check if employee is PM role
        NSString *isProjectManager = [dictionary IGStringValueForKey:kIGServiceKeyIsProjectManger];
        _isPMRole = [isProjectManager isEqualToString:@"0"] ? NO: YES;
        
        //check if employee is in US location or not
            
        CFBooleanRef isAbovePM = (__bridge CFBooleanRef)(dictionary[kIGServiceKeyIsAboveProjectManger]);
        _isAboveProjectManager = (isAbovePM == kCFBooleanTrue) ? YES: NO;
    }
    
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:
            @"Name: %@"
            "\rMailID: %@"
            "\rStatus: %@"
            "\rOnSiteManager: %@"
            "\rPayrollAdmin: %@"
            "\rTimesheetType : %@"
            "\rBaseLocation :%@"
            "\rLocationID : %@"
            "\rStaffID : %@"
            "\rStaffID1 :%@"
            "\rProjectManagerID :%@"
            "\rIsContractor: %d"
            "\risAboveProjectManager: %d"
            , _name, _mailID, _status, _onsiteManager,
            _payrollAdmin, _timesheetType, _baseLocationID,
            _locationID, _staffID, _staffID1, _projectManagerID,
            _isContractor, _isAboveProjectManager];
}

- (void)reset
{
    _name = @"";
    _mailID = @"";
    _status = @"";
    _onsiteManager = @"";
    _payrollAdmin = @"";
    _timesheetType = @"";
    _staffID = @"";
    _staffID1 = @"";
    _projectManagerID = @"";
    _image = nil;
    _baseLocationID = nil;
    _locationID = nil;
    _isContractor = NO;
    _isAboveProjectManager = NO;
    _isAuthenticated = NO;
    _isUSEmployee = NO;
    _employeeNewID = @"";
}

@end
