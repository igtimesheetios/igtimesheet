//
//  IGWeekEndCellTableViewCell.h
//  IGTimeSheet
//
//  Created by Kamal on 27/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGMyAreaCellType2: UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *weekEndDate;

@end
