//
//  IGAddTaskParser.h
//  IGTimeSheet
//
//  Created by Neha on 19/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#define WarningKey @"WarningMessage"

#import <Foundation/Foundation.h>

@interface IGAddTaskParser : NSObject

- (NSArray *)parseTasksList:(NSDictionary *)dictionary;
- (NSArray *)parseSubTasksList:(NSDictionary *)dictionary;
- (NSArray *)parseCRIdList:(NSDictionary *)dictionary;
- (NSArray *)parseCategoryList:(NSDictionary *)dictionary;


@end
