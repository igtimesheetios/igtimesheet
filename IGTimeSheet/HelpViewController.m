//
//  HelpViewController.m
//  IGTimeSheet
//
//  Created by Rajat on 26/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "HelpViewController.h"
#import "IGCommonHeaders.h"

#define kTableViewCellType1ReuseIdentifier                      @"HelpCellType1"

#define kfont   @"HelveticaNeue-light"

@interface HelpViewController ()

@end

@implementation HelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupBackButton];
    
    [self setupOpaqueNavigationBar];
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.navigationItem.title =@"Help";
    NSDictionary *titleDetail = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [UIFont fontWithName:kfont size:17.0f],NSFontAttributeName,
                                 [UIColor whiteColor] , NSForegroundColorAttributeName,
                                 nil];
    self.navigationController.navigationBar.titleTextAttributes = titleDetail;
    
    self.tblView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [IGAnalytics trackScreen:screenHelp];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    if (cell == nil) {
        cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellType1ReuseIdentifier
                                               forIndexPath:indexPath];
        if(indexPath.row == 0)
        {
            cell.textLabel.text = @"Timesheet US Help";
            cell.textLabel.font = [UIFont fontWithName:kfont size:16.0f];
            
        }
        else if(indexPath.row == 1)
        {
            cell.textLabel.text = @"Timesheet & Billing-GDC Help";
            cell.textLabel.font = [UIFont fontWithName:kfont size:16.0f];
            
        }
        
    }
    
    cell.backgroundColor = [UIColor clearColor];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [IGUtils underImplementationAlert];
}

@end

