//
//  IGDashboardModel.m
//  IGTimeSheet
//
//  Created by Manish on 12/05/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGDashboardModel.h"

@interface IGDashboardModel ()

@property (nonatomic, assign) NSInteger numberOfSections;
@property (nonatomic, strong) NSArray *sectionTypes;

@end


@implementation IGDashboardModel

- (void)refresh
{
    [self updateNumberOfSection];
    [self updateSectionTypes];
}

- (IGDashboardSection)sectionTypeForSection:(NSInteger)sectionIndex
{
    if (self.sectionTypes.count == 0)
    {
        return kIGDashboardSectionUndefined;
    }
    
    return [self.sectionTypes[sectionIndex] integerValue];
}

- (NSInteger)sectionIndexForSectionType:(IGDashboardSection)dashboardSection;
{
    if ([self.sectionTypes containsObject:@(dashboardSection)])
    {
        return [self.sectionTypes indexOfObject:@(dashboardSection)];
    }
    
    return -1;
}


- (NSInteger)numberOfRowsInSection:(NSInteger)section
{
    IGDashboardSection dashboardSection = [self sectionTypeForSection:section];

    switch (dashboardSection)
    {
        case kIGDashboardSectionUndefined:
        {
            return 0;
        }
            
        case kIGDashboardSectionMissingTimesheet:
        {
            if (self.expandMissingTimesheet)
            {
                return self.missingTimesheet.count;
            }
            else
            {
                return 0;
            }
        }
            
        case kIGDashboardSectionInProgressTimesheet:
        {
            if (self.expandInProgressTimesheet)
            {
                return self.inProgressTimesheet.count;
            }
            else
            {
                return 0;
            }
        }
            
        case kIGDashboardSectionEnterTimesheet:
        {
            return 3;
        }
            
        case kIGDashboardSectionSubmit:
        {
            return 1;
        }
        
        default:
            break;
    }

    return 0;
}


#pragma mark - 

- (void)updateNumberOfSection
{
    _numberOfSections = 2;
    
    if (self.missingTimesheet.count != 0)
    {
        _numberOfSections++;
    }
    
    if (self.inProgressTimesheet.count != 0)
    {
        _numberOfSections++;
    }
}

- (void)updateSectionTypes
{
    switch (_numberOfSections)
    {
        case 2:
        {
            self.sectionTypes = @[
                                  @(kIGDashboardSectionEnterTimesheet),
                                  @(kIGDashboardSectionSubmit)
                                  ];
            break;
        }
            
        case 3:
        {
            if (self.missingTimesheet.count != 0 )
            {
                self.sectionTypes = @[
                                      @(kIGDashboardSectionMissingTimesheet),
                                      @(kIGDashboardSectionEnterTimesheet),
                                      @(kIGDashboardSectionSubmit)
                                      ];
            }
            else if (self.inProgressTimesheet.count != 0)
            {
                self.sectionTypes = @[
                                      @(kIGDashboardSectionInProgressTimesheet),
                                      @(kIGDashboardSectionEnterTimesheet),
                                      @(kIGDashboardSectionSubmit)
                                      ];
            }
            else
            {
                self.sectionTypes = nil;
            }
            
            break;
        }
            
        case 4:
        {
            self.sectionTypes = @[
                                  @(kIGDashboardSectionMissingTimesheet),
                                  @(kIGDashboardSectionInProgressTimesheet),
                                  @(kIGDashboardSectionEnterTimesheet),
                                  @(kIGDashboardSectionSubmit)
                                  ];
            
            break;
        }
            
        default:
        {
            self.sectionTypes = nil;
            break;
        }
    }
}


@end
