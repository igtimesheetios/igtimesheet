//
//  IGViewLeavesVC.m
//  IGTimeSheet
//
//  Created by Rajat on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGViewLeavesVC.h"
#import "IGViewLeavesParser.h"
#import "IGViewLeavesModel.h"
#import "IGViewLeavesCellType1.h"
#import "IGViewLeavesCellType2.h"
#import "IGViewLeavesCellType3.h"
#import "IGViewLeavesCellType4.h"
#import "DateSelectionViewController.h"

#import "IGCommonHeaders.h"

//Picker View Headers File

#import "IGYearPicker.h"
#import "IGMonthPicker.h"
#import "IGWeekEndPicker.h"

#define kTableViewCellType1ReuseIdentifier                      @"IgViewLeavesCell1"
#define kTableViewCellType2ReuseIdentifier                      @"IgViewLeavesCell2"
#define kTableViewCellType3ReuseIdentifier                      @"IgViewLeavesCell3"
#define kTableViewCellType4ReuseIdentifier                      @"IgViewLeavesCell4"

@interface IGViewLeavesVC () <UITableViewDataSource, UITableViewDelegate>{
    
    BOOL flag;
    NSString *yearString;
    NSString *monthString;
    NSString *weekDate;
    
    IGYearPicker *yearPicker;
    IGMonthPicker *monthPicker;
    IGWeekEndPicker *weekPicker;
}

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property(strong, nonatomic) NSArray *monthArray;

@property (atomic) NSArray *leavesData;

@property (atomic) NSDate *leaveDate;

@property (strong, nonatomic) NSMutableArray *weekEndArray;

@end

@implementation IGViewLeavesVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupBackButton];
    [self setupOpaqueNavigationBar];
    self.automaticallyAdjustsScrollViewInsets = YES;
    self.navigationItem.title =@"View Leaves";
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName]];
    
    // Date Data
    flag = TRUE;
    self.weekEndArray = [[NSMutableArray alloc] initWithCapacity:1];
    yearString = [IGUtils stringFromDate:[NSDate date] dateFormat:@"yyyy"];
    monthString = [IGUtils stringFromDate:[NSDate date] dateFormat:@"MM"];
    //TODO: remove month array
    self.monthArray = @[
                        @"Jan",
                        @"Feb",
                        @"Mar",
                        @"Apr",
                        @"May",
                        @"Jun",
                        @"Jul",
                        @"Aug",
                        @"Sep",
                        @"Oct",
                        @"Nov",
                        @"Dec"
                        ];
    
    kAlertAndReturnIfNetworkNotReachable;
    [self fetchWeekEndDateFromServer];
//    [self fetchDataFromServerForLeavesDetail];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [IGAnalytics trackScreen:screenViewLeaves];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.leavesData count] == 0) {
        return 2;
    }
    else
    {
        return [self.leavesData count] + 1;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if(section == 0)
    {
        return 3;
    }
    else
    {
        if ([self.leavesData count] == 0)
        {
            return 1;
        }
        return 7;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        if (indexPath.row == 0 || indexPath.row == 1)
        {
            IGViewLeavesCellType1 *cellType1 = [tableView dequeueReusableCellWithIdentifier:kTableViewCellType1ReuseIdentifier forIndexPath:indexPath];
            cellType1.selectionStyle = UITableViewCellSelectionStyleNone;
            if (indexPath.row == 0) {
                cellType1.labelTitle.text = @"Year";
                cellType1.yearData.text = yearString;
            }
            else
            {
                cellType1.labelTitle.text = @"Month";
                if (monthString != nil) {
                    cellType1.yearData.text = self.monthArray[[monthString integerValue] - 1];
                }
                else
                {
                    cellType1.yearData.text = @"Select";
                }
                
            }
            return cellType1;
        }
        else if (indexPath.row == 2)
        {
            IGViewLeavesCellType2 *cellType2 = [tableView dequeueReusableCellWithIdentifier:kTableViewCellType2ReuseIdentifier forIndexPath:indexPath];
            cellType2.selectionStyle = UITableViewCellSelectionStyleNone;
            if (weekDate != nil) {
                cellType2.weekEndDate.text = weekDate;            }
            else
            {
                cellType2.weekEndDate.text = @"Select";
            }
            return cellType2;
        }
    }
    else{
        if ([self.leavesData count] == 0)
        {
            IGViewLeavesCellType4 *cellType4 = [tableView dequeueReusableCellWithIdentifier:kTableViewCellType4ReuseIdentifier forIndexPath:indexPath];
            return cellType4;
        }
        else
        {
            IGViewLeavesCellType3 *cellType3 = [tableView dequeueReusableCellWithIdentifier:kTableViewCellType3ReuseIdentifier forIndexPath:indexPath];
            IGViewLeavesModel *model = self.leavesData[indexPath.section - 1];
            switch (indexPath.row) {
                case 0:
                    cellType3.textName.text=@"Leave Type";
                    cellType3.textValue.text= model.leaveType;
                    break;
                case 1:
                    cellType3.textName.text= @"Leave From";
                    cellType3.textValue.text=model.leaveFrom;
                    break;
                case 2:
                    cellType3.textName.text= @"Leave To";
                    cellType3.textValue.text=model.leaveTo;
                    break;
                case 3:
                    cellType3.textName.text= @"Leave From Half";
                    cellType3.textValue.text=model.leaveFromHalf;
                    break;
                case 4:
                    cellType3.textName.text= @"Leave To Half";
                    cellType3.textValue.text=model.leaveToHalf;
                    break;
                case 5:
                    cellType3.textName.text= @"Status";
                    cellType3.textValue.text=model.status;
                    break;
                case 6:
                    cellType3.textName.text= @"Number of Days";
                    cellType3.textValue.text=[model.noOfDays stringValue];
                    break;
                default:
                    break;
            }
            return cellType3;
        }
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, -10, tableView.frame.size.width, 10)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, -1, tableView.frame.size.width, 1)];
    [label setBackgroundColor:[UIColor colorWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:1.0]];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0]];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if ((indexPath.section == 0) && (indexPath.row == 0))
    {
        [self openYearSelectionController:self];
    }
    if ((indexPath.section == 0) && (indexPath.row == 1))
    {
        [self openMonthSelectionController:self];
    }
    if ((indexPath.section == 0) && (indexPath.row == 2))
    {
        [self openWeekEndDateSelectionController:self];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0)
    {
        return 0;
    }
    
    return 10;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 10)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 9, tableView.frame.size.width, 1)];
    [label setBackgroundColor:[UIColor colorWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:1.0]];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0]];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            return 35.0f;
        }
        else
        {
            return 40.0f;
        }
    }
    return 43.0f;
}

-(void)fetchDataFromServerForLeavesDetail
{
    [self showSpinner:YES];
//TODO: Replaced appDelegate.employeeId with [[IGUserDefaults sharedInstance].employee.staffId integerValue]
    [[IGService sharedInstance] leavesForEmployee:[[IGUserDefaults sharedInstance].employee.staffID integerValue] date:self.leaveDate completion:^(id response)
     {
         [self showSpinner:NO];
         NSDictionary *dictionary = (NSDictionary *)response;
         NSArray *array = [dictionary objectForKey:@"Leaves"];
         IGViewLeavesParser *parser =[[IGViewLeavesParser alloc] init];
         self.leavesData = [[NSArray alloc] initWithArray:[parser parseLeaveViewData:array]];
         NSLog(@"%@", self.leavesData);
         [self.tableView reloadData];
     } error:^(NSError *error) {
         [self showSpinner:NO];
         NSLog(@"Error");
     }];
}

-(void)fetchWeekEndDateFromServer
{
    [self showSpinner:YES];
    [[IGService sharedInstance] weekEndDates:yearString month:monthString completion:^(id response) {
        NSDictionary *dictionary = (NSDictionary *)response;
        NSArray *dataArray = [dictionary objectForKey:@"Weekends"];
        [self.weekEndArray removeAllObjects];
        for (int index = 0; index < [dataArray count]; index++)
        {
            [self.weekEndArray addObject:[dataArray[index] objectForKey:@"Weekenddate"]];
        }
        if ([yearString isEqualToString:[IGUtils stringFromDate:[NSDate date] dateFormat:@"yyyy"]] &&
            [monthString isEqualToString:[IGUtils stringFromDate:[NSDate date] dateFormat:@"MM"]])
        {
            weekDate = self.weekEndArray[[self.weekEndArray count] - 1];
        }
        self.leaveDate = [IGUtils dateFromString:weekDate dateFormat:@"dd MMM yyyy"];
        [self fetchDataFromServerForLeavesDetail];
        [self showSpinner:NO];
        [self.tableView reloadData];
    } error:^(NSError *error) {
        [IGUtils showOKAlertWithTitle:kStringError message:[error localizedDescription]];
        [self showSpinner:NO];
    }];
    
}

#pragma marks - pickerView

- (IBAction)openYearSelectionController:(id)sender
{
    if (!yearPicker)
    {
        yearPicker = [IGYearPicker picker];
        yearPicker.delegate = (id)self;
        yearPicker.titleLabel.text = @"Select Year";
        [yearPicker presentOnView:self.view animated:YES];
    }
    else
    {
        [yearPicker show:YES animated:YES];
    }
}

- (IBAction)openMonthSelectionController:(id)sender
{
    if (!monthPicker)
    {
        monthPicker = [IGMonthPicker picker];
        monthPicker.delegate = (id)self;
        monthPicker.titleLabel.text = @"Select Month";
        monthPicker.selectedYear = yearString;
        [monthPicker presentOnView:self.view animated:YES];
    }
    else
    {
        monthPicker.selectedYear = yearString;
        [monthPicker presentOnView:self.view animated:YES];
    }
}

- (IBAction)openWeekEndDateSelectionController:(id)sender
{
    if (!weekPicker)
    {
        weekPicker = [IGWeekEndPicker picker];
        weekPicker.delegate = (id)self;
        weekPicker.titleLabel.text = @"Select Week";
        weekPicker.weekArray = self.weekEndArray;
        [weekPicker presentOnView:self.view animated:YES];
    }
    else
    {
        weekPicker.weekArray = self.weekEndArray;
        [weekPicker presentOnView:self.view animated:YES];
    }
}


#pragma mark - IGPickerDelegate

- (void)monthPicker:(IGMonthPicker *)picker
{
    NSInteger index = [picker.pickerView selectedRowInComponent:0] + 1;
    monthString = [@(index) stringValue];
    weekDate = nil;
    kAlertAndReturnIfNetworkNotReachable;
     self.leavesData = nil;
    [self fetchWeekEndDateFromServer];
    
}

- (void)yearPicker:(IGYearPicker *)picker didSelectDate:(NSString *)year;
{
    yearString = year;
    if ([yearString isEqualToString:[IGUtils stringFromDate:[NSDate date] dateFormat:@"yyyy"]] && flag == TRUE)
    {
        monthString = [IGUtils stringFromDate:[NSDate date] dateFormat:@"MM"];
        flag = TRUE;
    }
    else
    {
        monthString = nil;
        flag = FALSE;
    }
    weekDate = nil;
    self.leavesData = nil;
    [self.tableView reloadData];
}

- (void)weekEndPicker:(IGWeekEndPicker *)picker didSelectDate:(NSString *)weekEnd
{
    NSInteger index = [picker.pickerView selectedRowInComponent:0];
    weekDate = self.weekEndArray[index];
    self.leaveDate = [IGUtils dateFromString:weekDate dateFormat:@"dd MMM yyyy"];
    kAlertAndReturnIfNetworkNotReachable;
     self.leavesData = nil;
    [self fetchDataFromServerForLeavesDetail];
    [self.tableView reloadData];
}

@end
