//
//  IGMonthSummaryCell.m
//  IGTimeSheet
//
//  Created by Neha on 17/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGMonthSummaryCell.h"
#import "IGMacro.h"
#import "IGUtils.h"
#import "IGClipView.h"

#define kIGMonthlySummaryCellCornerRadius   9.0
#define WEEK_VIEW_HEIGHT                    35

@interface IGMonthSummaryCell ()
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewWeek1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewWeek2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewWeek3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewWeek4;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewWeek5;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintHeightViewWeek6;

@end


@implementation IGMonthSummaryCell

+ (CGFloat)heightForWeekCount:(NSInteger)weekCount
{
    return 58 + ((weekCount + 1) * WEEK_VIEW_HEIGHT);
}

- (void)configureForWeekCount:(NSInteger)weekCount
{
    NSArray *constraints = @[self.constraintHeightViewWeek1,
                             self.constraintHeightViewWeek2,
                             self.constraintHeightViewWeek3,
                             self.constraintHeightViewWeek4,
                             self.constraintHeightViewWeek5,
                             self.constraintHeightViewWeek6];
    
    NSInteger count = constraints.count;
    for (int i = 0; i < count; i++)
    {
        NSLayoutConstraint *constraint = constraints[i];
        
        if (i < weekCount)
        {
            constraint.constant = WEEK_VIEW_HEIGHT;
        }
        else
        {
            constraint.constant = 0;
        }
    }
}

- (void)awakeFromNib
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = kColorGrayType2;
    
    CALayer *layer = self.viewBackground.layer;
    layer.cornerRadius = kIGMonthlySummaryCellCornerRadius;
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowOffset = CGSizeMake(0, 0);
    layer.shadowRadius = 2.0f;
    layer.shadowOpacity = 0.5f;
    
    NSAssert([self.totalHrsView isKindOfClass:[IGClipView class]], @"Expected a view of type IGClipView");
    self.totalHrsView.corners = (UIRectCornerBottomLeft | UIRectCornerBottomRight);
    self.totalHrsView.cornerRadius = kIGMonthlySummaryCellCornerRadius;
}

@end
