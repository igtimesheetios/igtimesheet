//
//  IGMonthlyTimeSheetCellType2.h
//  IGTimeSheet
//
//  Created by Kamal on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGMonthlyTimeSheetCellType3 : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *cell3MainView;

@property (strong, nonatomic) IBOutlet UILabel *weekData;

@property (strong, nonatomic) IBOutlet UILabel *totalHoursData;

@property (strong, nonatomic) IBOutlet UILabel *numberOfDaysData;

@property (strong, nonatomic) IBOutlet UILabel *weeklyAverageData;

@end
