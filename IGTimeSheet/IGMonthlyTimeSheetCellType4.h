//
//  IGMonthlyTimeSheetCellType3.h
//  IGTimeSheet
//
//  Created by Kamal on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IGClipView;
@interface IGMonthlyTimeSheetCellType4 : UITableViewCell

@property (strong, nonatomic) IBOutlet UIView *cell4MainView;

@property (strong, nonatomic) IBOutlet UILabel *totalHoursData;

@property (strong, nonatomic) IBOutlet UILabel *numberOfDaysData;

@property (strong, nonatomic) IBOutlet UILabel *weeklyAverageData;

@property (strong, nonatomic) IBOutlet IGClipView *titleView;

@property (strong, nonatomic) IBOutlet UILabel *titleLabel;

@end
