//
//  IGTaskDetail.m
//  IGTimeSheet
//
//  Created by Manish on 27/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGTaskDetail.h"
#import "NSDictionary+IGAddition.h"
#import "IGServiceDefine.h"

@implementation IGTaskDetail

- (instancetype)initWithDictionary:(NSDictionary *)taskInfo
{
    if ([taskInfo respondsToSelector:@selector(objectForKey:)] == NO)
    {
        return nil;
    }
    
    self = [super init];
    if (self)
    {
        _track = [taskInfo IGStringValueForKey:kIGServiceKeyTrackName];
        _CRName = [taskInfo IGStringValueForKey:kIGServiceKeyCRName];
        _taskDescription = [taskInfo IGStringValueForKey:kIGServiceKeyDescription];
        _location = [taskInfo IGStringValueForKey:kIGServiceKeyLocation];
        _type = [taskInfo IGStringValueForKey:kIGServiceKeyLocationType];
        _task = [taskInfo IGStringValueForKey:kIGServiceKeyTaskName];
        _subTask = [taskInfo IGStringValueForKey:kIGServiceKeySubTaskName];
        _effort = [taskInfo IGNumberValueForKey:kIGServiceKeyEfforts];
    }
    return self;
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"Track:%@\r"
                               "CRName:%@\r"
                          "description:%@\r"
                             "location:%@\r"
                                 "type:%@\r"
                                 "task:%@\r"
                              "subtask:%@\r"
                               "effort:%@",
            _track, _CRName, _taskDescription, _location, _type, _task, _subTask, _effort];
}

@end
