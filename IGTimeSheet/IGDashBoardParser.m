//
//  IGDashBoardParser.m
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGDashBoardParser.h"
#import "IGDashboardObjectModel.h"
#import "IGDashboardMacros.h"

@implementation IGDashBoardParser

- (NSArray *)parseInProgressTimeSheet:(NSDictionary *)dictionary
{
    NSMutableArray* pTimeSheetArr = [[NSMutableArray alloc] init];
    
    NSString* responseString = [dictionary objectForKey:PTimeSheetKey];
    NSLog(@"%@",responseString);
    if(responseString.length)
    {
        NSArray* timesArr = [responseString componentsSeparatedByString:TimeSheetSeperator];
        for(int index = 0; index < timesArr.count; ++index)
        {
            IGDashboardObjectModel* modelObj = [[IGDashboardObjectModel alloc] init];
            modelObj.value = [timesArr objectAtIndex:index];
            modelObj.type  = PTimeSheet;
            [pTimeSheetArr addObject:modelObj];
        }
    }
    
    return pTimeSheetArr;
}

- (NSArray *)parseMissingTimeSheet:(NSDictionary *)dictionary
{
    NSMutableArray* mTimeSheetArr = [[NSMutableArray alloc] init];
    
    NSString* responseString = [dictionary objectForKey:MTimeSheetKey];
    if(responseString.length)
    {
        NSArray* timesArr = [responseString componentsSeparatedByString:TimeSheetSeperator];
        for(int index = 0; index < timesArr.count; ++index)
        {
            IGDashboardObjectModel* modelObj = [[IGDashboardObjectModel alloc] init];
            modelObj.value = [timesArr objectAtIndex:index];
            modelObj.type  = MTimeSheet;
            [mTimeSheetArr addObject:modelObj];
        }
    }
    
    return mTimeSheetArr;
}

@end
