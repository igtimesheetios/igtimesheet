//
//  IGPicker.h
//  IGTimeSheet
//
//  Created by Manish on 20/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IGYearPicker;
@protocol IGPickerDelegate <NSObject>

- (void)yearPicker:(IGYearPicker *)picker didSelectDate:(NSString *)yearString;

@end



@interface IGYearPicker : UIView

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *buttonDone;
@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewBottomConstraint;


@property (nonatomic, strong) NSDate *currentDate;

@property (nonatomic, readonly) NSString *yearString;
@property (nonatomic, readonly) NSString *monthString;

@property (nonatomic, readonly) NSInteger month;
@property (nonatomic, readonly) NSInteger year;

@property (nonatomic, weak) id<IGPickerDelegate> delegate;

+ (IGYearPicker *)picker;
- (void)presentOnView:(UIView *)view animated:(BOOL)animated;

- (void)show:(BOOL)show animated:(BOOL)animated;

@end
