//
//  IGMonthPickerViewController.h
//  IGTimeSheet
//
//  Created by Kamal on 27/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IGWeekEndPicker;
@protocol IGWeekEndPickerDelegate <NSObject>

- (void)weekEndPicker:(IGWeekEndPicker *)picker didSelectDate:(NSString *)weekEnd;

@end



@interface IGWeekEndPicker : UIView

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *buttonDone;
@property (weak, nonatomic) IBOutlet UIButton *buttonCancel;

@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *containerViewBottomConstraint;

@property (nonatomic, strong) NSArray *weekArray;

@property (nonatomic, readonly) NSString *weekIndex;

@property (nonatomic, weak) id<IGWeekEndPickerDelegate> delegate;

+ (IGWeekEndPicker *)picker;
- (void)presentOnView:(UIView *)view animated:(BOOL)animated;

- (void)show:(BOOL)show animated:(BOOL)animated;

@end
