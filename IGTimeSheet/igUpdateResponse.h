//
//  igUpdateResponse.h
//  IGCafe
//
//  Created by Pulkit on 27/07/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface igUpdateResponse : NSObject

@property (nonatomic, assign, readonly) BOOL hasNewUpdate;
@property (nonatomic, assign, readonly) BOOL isMandatory;

@property (nonatomic, strong, readonly) NSString *title;
@property (nonatomic, strong, readonly) NSString *message;
@property (nonatomic, strong, readonly) NSString *url;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
