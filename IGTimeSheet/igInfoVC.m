//
//  igInfoVC.m
//  IGCafe
//
//  Created by Manish on 22/06/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "igInfoVC.h"
#import "IGCommonHeaders.h"



#define TAG_ALERT_UPDATE                2222

@interface igInfoVC () <EMSmoothAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imageViewBackground;

@property (weak, nonatomic) IBOutlet UIView *viewInfo;
@property (weak, nonatomic) IBOutlet UILabel *labelTitle;
@property (weak, nonatomic) IBOutlet UILabel *labelVersion;

@property (weak, nonatomic) IBOutlet UIButton *buttonCheckForUpdates;
@property (weak, nonatomic) IBOutlet UIButton *buttonTermsAndConditions;
@property (weak, nonatomic) IBOutlet UIButton *buttonClose;

@property (weak, nonatomic) IBOutlet UIView *viewMessage;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *labelMessage;

@property (nonatomic, strong) UITapGestureRecognizer *tapGesutureRecognizer;

- (IBAction)checkForUpdates:(id)sender;
- (IBAction)showTermAndConditions:(id)sender;

@end


@implementation igInfoVC
{
    NSString *bundleVersion;
    
    BOOL checkingForUpdate;
    BOOL updateAvailable;
    
    BOOL shouldShowUpdateAlert;
    
    NSString *updateResponseTitle;
    NSString *updateResponseMessage;
    NSString *updateResponseURL;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
 
    self.imageViewBackground.image = _image;
    
    self.buttonClose.exclusiveTouch = YES;
    self.buttonCheckForUpdates.exclusiveTouch = YES;
    self.buttonTermsAndConditions.exclusiveTouch = YES;
    
    self.buttonCheckForUpdates.layer.cornerRadius = 2.0f;
    self.buttonTermsAndConditions.layer.cornerRadius = 2.0f;
    self.buttonCheckForUpdates.hidden = true;
    self.buttonTermsAndConditions.userInteractionEnabled = false;
    [self.viewInfo setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:kImageScreenBG]]];
    
    bundleVersion = [IGUtils bundleVersion];
    NSString *message = [NSString stringWithFormat:@"Version: %@", bundleVersion];
    self.labelVersion.text = message;
    
    self.viewInfo.layer.shadowOpacity = 0.5f;
    self.viewInfo.layer.shadowRadius = 3.0f;
    self.viewInfo.layer.shadowOffset = CGSizeMake(0, 0);
    
    if (_tapGesutureRecognizer == nil)
    {
        _tapGesutureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(viewTapped:)];
    }
    
    [self configureView];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.view addGestureRecognizer:_tapGesutureRecognizer];
    
//    [IGAnalytics trackScreen:screenInfo];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self.view removeGestureRecognizer:_tapGesutureRecognizer];
}

- (void)hideUpdateButton:(BOOL)hidden
{
    self.buttonCheckForUpdates.hidden = hidden;
    self.viewMessage.hidden = !hidden;
}

- (void)configureView
{
    [self hideUpdateButton:checkingForUpdate];
    
    if (updateAvailable)
    {
        if (shouldShowUpdateAlert)
        {
            igTSmoothAlertView *alert = [[igTSmoothAlertView alloc] initDropAlertWithTitle:updateResponseTitle
                                                                                   andText:updateResponseMessage
                                                                           andCancelButton:YES
                                                                                  andColor:kDefaultBarTintColor
                                                                                  andImage:[UIImage imageNamed:kImageInfogainLogoCircle]];
            alert.tag = TAG_ALERT_UPDATE;
            [alert.defaultButton setTitle:@"Update" forState:UIControlStateNormal];
            alert.delegate = self;
            [alert show];
            
            shouldShowUpdateAlert = NO;
        }
    }
    else
    {
        if (shouldShowUpdateAlert)
        {
            igTSmoothAlertView *alert = [[igTSmoothAlertView alloc] initDropAlertWithTitle:updateResponseTitle
                                                                               andText:updateResponseMessage
                                                                       andCancelButton:NO
                                                                                  andColor:kDefaultBarTintColor
                                                                                  andImage:[UIImage imageNamed:kImageInfogainLogoCircle]];
            [alert show];
            
            shouldShowUpdateAlert = NO;
        }
    }
}

#pragma mark - EMSmoothAlertViewDelegate

- (void)alertView:(igTSmoothAlertView *)alertView didDismissWithButton:(UIButton *)button
{
    if (alertView.tag == TAG_ALERT_UPDATE)
    {
        if ([button isEqual:alertView.defaultButton])
        {
            if ([IGUtils isValidString:updateResponseURL])
            {
                NSURL *updateURL = [NSURL URLWithString:updateResponseURL];
                if ([[UIApplication sharedApplication] canOpenURL:updateURL])
                {
                    [[UIApplication sharedApplication] openURL:updateURL];
                }
                else
                {
                    //TODO: modify
                    [IGUtils showOKAlertWithTitle:kStringError message:kStringCannotOpenURL];
                }
            }
        }
    }
}

#pragma mark - Button Actions

- (IBAction)checkForUpdates:(id)sender
{
    checkingForUpdate = YES;
    [self hideUpdateButton:checkingForUpdate];

    kAlertAndReturnIfNetworkNotReachable;
   
    void (^completionBlock)(igUpdateResponse *) = ^(igUpdateResponse *updateResponse) {

        updateAvailable = updateResponse.hasNewUpdate;
        updateResponseTitle = updateResponse.title;
        updateResponseMessage = updateResponse.message;
        updateResponseURL = updateResponse.url;
        
        checkingForUpdate = NO;
        
        shouldShowUpdateAlert = YES;
        [self configureView];
    };
    
    void (^errorBlock)(NSError *) = ^(NSError *error) {
        checkingForUpdate = NO;
        shouldShowUpdateAlert = YES;
        [self configureView];
        
    };
    
    [[IGService sharedInstance] checkUpdateWithCompletion:completionBlock
                                               AndError:errorBlock];
}

- (IBAction)showTermAndConditions:(id)sender
{
    // Do any additional task here
}

#pragma mark -

- (void)viewTapped:(UITapGestureRecognizer *)tapGestureRecoginzer
{
    CGPoint touchPoint = [tapGestureRecoginzer locationInView:self.view];
    if (CGRectContainsPoint(self.viewInfo.frame, touchPoint) == NO)
    {
        [self performSegueWithIdentifier:kStoryboardSegueIndentifierExitInfoVCSegue sender:self];
    }
}

@end
