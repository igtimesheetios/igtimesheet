//
//  LabelButtonModalClass.h
//  TaskView
//
//  Created by Hem on 10/02/15.
//  Copyright (c) 2015 Hem. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface IGLabelButtonModalClass : NSObject

@property(strong) NSString* labelText;
@property(strong) UIColor*  labelTextColor;
@property(strong) UIColor*  backgroundColor;
@property(strong) NSString* buttonText;
@property(strong) UIColor*  buttonTextColor;
@property(strong) UIImage* backImage;
@property(atomic) BOOL buttonVisible;
@property(atomic) BOOL buttonEnabled;
@property(atomic) BOOL rowSelectable;

@end
