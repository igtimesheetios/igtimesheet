//
//  IGSubmitTableViewCell.h
//  IGTimeSheet
//
//  Created by Hem on 02/03/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGSubmitTableViewCell : UITableViewCell

@property(weak) IBOutlet UIButton* submitButton;
@property(weak) IBOutlet UIButton* saveButton;

@end
