//
//  igUpdateResponse.m
//  IGCafe
//
//  Created by Pulkit on 27/07/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "igUpdateResponse.h"
#import "IGCommonHeaders.h"

@implementation igUpdateResponse

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    if ([IGUtils isDictionary:dictionary] == NO)
    {
        return nil;
    }
    
    self = [super init];
    if (self)
    {
        NSString *value = nil;
        value = dictionary[kIGServiceKeyIsUpdate];
        if ([value respondsToSelector:@selector(boolValue)])
        {
            _hasNewUpdate = [value boolValue];
        }
        
        value = dictionary[kIGServiceKeyMessage];
        if ([IGUtils isValidString:value])
        {
            _message = value;
        }
        else
        {
            _message = @"";
        }
        
        value = dictionary[kIGServiceKeyTitle];
        if ([IGUtils isValidString:value])
        {
            _title = value;
        }
        else
        {
            _title = @"";
        }
        
        value = dictionary[kIGServiceKeyIsMandatory];
        if ([value respondsToSelector:@selector(boolValue)])
        {
            _isMandatory = [value boolValue];
        }

        value = dictionary[kIGServiceKeyURL];
        if ([IGUtils isValidString:value])
        {
            _url = value;
        }
    }
    return self;
}

@end
