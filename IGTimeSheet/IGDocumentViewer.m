//
//  IGDocumentViewer.m
//  IGTimeSheet
//
//  Created by Manish on 10/03/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGDocumentViewer.h"
#import "IGCommonHeaders.h"

@interface IGDocumentViewer () <UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end


@implementation IGDocumentViewer

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupBackButton];
    
    if (self.fileURL != nil)
    {
        [self showSpinner:YES];
        [self.webView loadRequest:[NSURLRequest requestWithURL:self.fileURL]];
    }
    
    self.webView.backgroundColor = [UIColor whiteColor];
}


#pragma mark - UIWebViewDelegate

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self showSpinner:NO];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self showSpinner:NO];
    [IGUtils showOKAlertWithTitle:kStringError message:@"Error loading file"];
}

@end
