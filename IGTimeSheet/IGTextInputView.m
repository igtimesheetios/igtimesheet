//
//  IGTextInputView.m
//  IGTimeSheet
//
//  Created by Manish on 04/03/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGTextInputView.h"

@interface IGTextInputView () <UITextViewDelegate>
{
    CGFloat bottomConstraintConstant;
}

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintBottom;

- (IBAction)cancelAction:(id)sender;
- (IBAction)doneAction:(id)sender;

@end


@implementation IGTextInputView

+(instancetype)textInputView
{
    NSArray *views = [[NSBundle mainBundle] loadNibNamed:@"IGTextInputView" owner:nil options:nil];
    
    if (views.count > 0)
    {
        IGTextInputView *view = (IGTextInputView *)views[0];
        return view;
    }
    
    return nil;
}

- (void)presentOnView:(UIView *)aView
{
    self.translatesAutoresizingMaskIntoConstraints = NO;
    [aView addSubview:self];

    UIView *view = self;
    NSArray *constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[view]|"
                                                                   options:0
                                                                   metrics:nil
                                                                     views:NSDictionaryOfVariableBindings(view)];
    [aView addConstraints:constraints];
    
    constraints = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                                          options:0
                                                          metrics:nil
                                                            views:NSDictionaryOfVariableBindings(view)];
    [aView addConstraints:constraints];
    
    [self.textView becomeFirstResponder];
    
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.alpha = 1.0;
                     }];
}


#pragma mark -

- (void)awakeFromNib
{
    self.alpha = 0.0;
    self.viewBackground.layer.cornerRadius = 5.0f;
    self.buttonCancel.layer.cornerRadius = 5.0f;
    self.buttonDone.layer.cornerRadius = 5.0f;
    self.textView.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.textView.layer.borderWidth = 1.0f;
    self.textView.layer.cornerRadius = 5.0f;
    [self addKeyboardNotificationObserver];
    
    self.buttonCancel.layer.cornerRadius = CGRectGetWidth(self.buttonCancel.frame) / 2;
    self.buttonDone.layer.cornerRadius = CGRectGetWidth(self.buttonDone.frame) / 2;

    bottomConstraintConstant = self.constraintBottom.constant;
}

- (void)dealloc
{
    [self removeKeyboardNotification];
}


#pragma mark - Button Action

- (IBAction)cancelAction:(id)sender
{
    [self dismissWithButtonAtIndex:0];
}

- (IBAction)doneAction:(id)sender
{
    [self dismissWithButtonAtIndex:1];
}

- (void)dismissWithButtonAtIndex:(NSInteger)buttonIndex
{
    [self.textView resignFirstResponder];
    [self callDelegateForButtonAtIndex:buttonIndex];
    [self animateRemoveFromSuperview];
}


#pragma mark -

- (void)addKeyboardNotificationObserver
{
    [self removeKeyboardNotification];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)removeKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)keyboardShow:(NSNotification *)notification
{
    NSDictionary *userInfo = notification.userInfo;
    CGRect keyboardFrame = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat keyboardHeight = CGRectGetHeight(keyboardFrame);
    
    self.constraintBottom.constant = keyboardHeight + bottomConstraintConstant;
}

- (void)keyboardHide:(NSNotification *)notification
{
    self.constraintBottom.constant = bottomConstraintConstant;
}

- (void)animateRemoveFromSuperview
{
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.alpha = 0.0;
                     } completion:^(BOOL finished) {
                         [self removeFromSuperview];
                     }];
}


- (void)callDelegateForButtonAtIndex:(NSInteger)index
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(textInputView:didSelectButtonAtIndex:)])
    {
        [self.delegate textInputView:self didSelectButtonAtIndex:index];
    }
}


#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    [self hidePlaceholderIfRequired];
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    [self hidePlaceholderIfRequired];
}

- (void)hidePlaceholderIfRequired;
{
    self.labelPlaceholder.hidden = [self.textView hasText];
}

@end
