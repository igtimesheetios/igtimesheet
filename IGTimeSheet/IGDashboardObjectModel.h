//
//  DashboardObjectModel.h
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGDashboardObjectModel : NSObject

//Value is value of label or datepicker on view
@property(strong) NSString* value;
//Type is either , MissingTimeSheet, InProgressTimeSheet or CurrentTimeSheet
@property(atomic) NSInteger type;

@end
