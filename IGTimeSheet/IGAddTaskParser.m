//
//  IGAddTaskParser.m
//  IGTimeSheet
//
//  Created by Neha on 19/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#define TasksKey @"Tasks"
#define TaskIdKey @"TaskId"
#define TaskNameKey @"TaskName"

#define SubTasksKey @"SubTasks"
#define SubTaskIdKey @"SUBTASKID"
#define SubTaskNameKey @"SUBTASKNAME"

// for US Employees
#define kTaskCategoryKey @"TaskCategory"
#define kTaskCategoryIdKey @"Id"
#define kTaskCategoryNameKey @"TaskName"



#define CRKey @"CR"
#define CRIdKey @"CRID"
#define CRNameKey @"CRNAME"

#import "IGAddTaskParser.h"
#import "IGTaskDataModel.h"
#import "IGCRIdModel.h"
#import "IGCategoryTaskDataModel.h"

@implementation IGAddTaskParser

- (NSArray *)parseTasksList:(NSDictionary *)dictionary
{
    NSLog(@"AAAAAAAAA");
    NSMutableArray* tasksArr = [[NSMutableArray alloc] init];
    
    NSString *warningMessage = [dictionary objectForKey:WarningKey];
    NSLog(@"BBBBBBBBBB");
    if(warningMessage)
    {
        [tasksArr addObject:WarningKey];
        [tasksArr addObject:warningMessage];
        return tasksArr;
    }
    
    NSArray* responseList = [dictionary objectForKey:TasksKey];
    for (int i = 0; i < responseList.count; i++) {
        
        NSDictionary* taskData = responseList[i];
        IGTaskDataModel* taskModal = [[IGTaskDataModel alloc]init];
        [taskModal setTaskId:[taskData objectForKey:TaskIdKey]];
        [taskModal setTaskName:[taskData objectForKey:TaskNameKey]];
        [tasksArr addObject:taskModal];
        
    }
    
    return tasksArr;
}

//--------For US Employees------------------
- (NSArray *)parseCategoryList:(NSDictionary *)dictionary
{
    NSLog(@"AAAAAAAAA");
    NSMutableArray* categoryArr = [[NSMutableArray alloc] init];
    
    NSString *warningMessage = [dictionary objectForKey:WarningKey];
    NSLog(@"BBBBBBBBBB");
    if(warningMessage)
    {
        [categoryArr addObject:WarningKey];
        [categoryArr addObject:warningMessage];
        return categoryArr;
    }
    
    NSArray* responseList = [dictionary objectForKey:kTaskCategoryKey];
    for (int i = 0; i < responseList.count; i++) {
        
        NSDictionary* taskData = responseList[i];
        IGCategoryTaskDataModel* taskModal = [[IGCategoryTaskDataModel alloc]init];
        [taskModal setCategoryID:[taskData objectForKey:kTaskCategoryIdKey]];
        [taskModal setCategoryName:[taskData objectForKey:kTaskCategoryNameKey]];
        [categoryArr addObject:taskModal];
        
    }
    
    return categoryArr;
}


- (NSArray *)parseCRIdList:(NSDictionary *)dictionary
{
    NSMutableArray* cridArr = [[NSMutableArray alloc] init];
    
    NSArray* responseList = [dictionary objectForKey:CRKey];
    for (int i = 0; i < responseList.count; i++) {
        
        NSDictionary* taskData = responseList[i];
        IGCRIdModel * cridModel = [[IGCRIdModel alloc]init];
        [cridModel setCRId:[NSString stringWithFormat:@"%@",[taskData objectForKey:CRIdKey]]];
        [cridModel setCRName:[taskData objectForKey:CRNameKey]];
        [cridArr addObject:cridModel];
        
    }
    
    return cridArr;
}

- (NSArray *)parseSubTasksList:(NSDictionary *)dictionary
{
    NSMutableArray* tasksArr = [[NSMutableArray alloc] init];
    
    NSArray* responseList = [dictionary objectForKey:SubTasksKey];
    for (int i = 0; i < responseList.count; i++) {
        
        NSDictionary* taskData = responseList[i];
        IGTaskDataModel* taskModal = [[IGTaskDataModel alloc]init];
        [taskModal setTaskId:[taskData objectForKey:SubTaskIdKey]];
        [taskModal setTaskName:[taskData objectForKey:SubTaskNameKey]];
        [tasksArr addObject:taskModal];
        
    }
    
    return tasksArr;
}


@end
