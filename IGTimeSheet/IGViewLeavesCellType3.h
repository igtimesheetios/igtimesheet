//
//  IGViewLeavesCellType3.h
//  IGTimeSheet
//
//  Created by Kamal on 02/03/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGViewLeavesCellType3 : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *textName;

@property (strong, nonatomic) IBOutlet UILabel *textValue;

@end
