//
//  IGInOutTableViewCell.h
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGInOutTableViewCell : UITableViewCell

@property(strong) IBOutlet UILabel* dateLabel;
@property(strong) IBOutlet UILabel* inTimeLabel;
@property(strong) IBOutlet UILabel* outTimeLabel;
@property(strong) IBOutlet UIButton* redButton;

@end
