//
//  IGAddTaskVC.h
//  IGTimeSheet
//
//  Created by Neha on 18/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGService.h"
#import "IGTaskModalClass.h"
#import "IGMacro.h"
#import "IGViewController.h"


@interface IGAddTaskVC : IGViewController
{
    id _taskVC;
    BOOL _isAddTask;
    NSArray* _locationValues;
}

@property (strong) IBOutlet UITableView* tableView;

@property (strong) NSArray* taskList;
@property (strong) NSArray* subtaskList;
@property (strong) NSArray* cridList;
@property (strong) NSArray* categoryList;
@property (strong) NSArray* projectsList;
@property (strong) NSArray* hrsTypeList;
@property (strong) NSArray* locationList;
@property (strong) NSArray* taskTypeForUSEmployees;// task type for US employees- billable or non billable
@property (strong) NSArray* taskCategoryValueForUSEmployees;// Task type value for US employees- Y or N depend on billable or non billable
@property (atomic) NSInteger timesheetStatus;
@property (strong) NSArray* weekDates;
@property (atomic) NSInteger weekId;
@property (strong) IGTaskModalClass* taskData;
@property (nonatomic, weak) IBOutlet UIView* buttonsView;
@property (weak,nonatomic) IBOutlet NSLayoutConstraint *btnViewHeightConstraint;

@property (nonatomic, strong) NSArray *inOutDetails;

- (void) addTaskForProjects:(NSArray*)projects weekEndDate:(NSDate*)date withWeekId:(NSInteger)weekId timesheetStatus:(IGTimeSheetStatus)status parentView:(id)vc;
- (void) showTask:(IGTaskModalClass*)taskData weekEndDate:(NSDate*)date withWeekId:(NSInteger)weekId forProjects:(NSArray*)projectList;

- (IBAction)addToTask:(id)sender;
- (IBAction)resetTask:(id)sender;



@end
