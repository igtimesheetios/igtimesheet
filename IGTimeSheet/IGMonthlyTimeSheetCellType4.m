//
//  IGMonthlyTimeSheetCellType3.m
//  IGTimeSheet
//
//  Created by Kamal on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGMonthlyTimeSheetCellType4.h"
#import "IGMacro.h"
#import "IGUtils.h"
#import "IGClipView.h"

#define kIGMonthlyTimesheetCellCornerRadius         8.0

@implementation IGMonthlyTimeSheetCellType4

- (void)awakeFromNib
{
    self.backgroundColor = kColorGrayType2;
    
    CALayer *layer = self.cell4MainView.layer;
    layer.cornerRadius = kIGMonthlyTimesheetCellCornerRadius;
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowOffset = CGSizeMake(0, 0);
    layer.shadowRadius = 2.0f;
    layer.shadowOpacity = 0.5f;
    
    NSAssert([self.titleView isKindOfClass:[IGClipView class]], @"Expected a View of type IGClipView");
    self.titleView.corners = (UIRectCornerTopLeft | UIRectCornerTopRight);
    self.titleView.cornerRadius = kIGMonthlyTimesheetCellCornerRadius;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [IGUtils roundCorner:(UIRectCornerTopLeft | UIRectCornerTopRight)
                  radius:kIGMonthlyTimesheetCellCornerRadius
                 forView:self.titleView];
}

@end
