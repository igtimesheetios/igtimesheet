//
//  IGServiceKey.h
//  IGTimeSheet
//
//  Created by Manish on 11/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#ifndef IGTimeSheet_IGServiceKey_h
#define IGTimeSheet_IGServiceKey_h

#pragma mark - WebService Base URL

//#define kIGServiceBaseURL                               @"http://172.18.65.154:9810/api"

//#define kIGServiceBaseURL                               @"http://172.18.65.154:9810/api"
#define kIGServiceBaseURL                               @"https://timesheetapi.infogain.com:447/api"




#pragma mark - WebService Name

#define kIGServiceLogin                                 @"Aspire/login"
#define kIGServiceLoginDetails                          @"timeSheet/EmpDetail"
#define kIGServiceInOutTime                             @"TimeSheet/InOutTime"
#define kIGServiceTaskList                              @"TimeSheet/TaskList"
#define kIGServiceCRIList                               @"TimeSheet/CRClick"

//Employee US Category  Task list
#define kIGUSEmployeeTaskList                           @"Timesheet/TaskCategory"
#define kIGServiceSubmit                                @"TimeSheet/SubmitTimeSheet"
#define kIGServiceSubTaskList                           @"TimeSheet/SubTaskList"
#define kIGServiceSubmitTimeSheet                       @"TimeSheet/SubmitTimeSheet"
#define kIGServiceViewLeaves                            @"TimeSheet/ViewLeaves"
#define kIGServiceWeeklyTimeSheetSummary                @"TimeSheet/WeekTSSummary"
#define kIGServiceMonthlyTimeSheetSummary               @"TimeSheet/MonthlyTSSummary"
#define kIGServiceRemoveTask                            @"TimeSheet/RemoveTimeSheet"
#define kIGServiceMonthlyTimeSheetAverage               @"TimeSheet/MonthlyTSAverage"
#define kIGServiceEmployeeImage                         @"TimeSheet/EmpPhotograph"
#define kIGServiceDayWiseTaskDetail                     @"TimeSheet/DayWiseTaskDetail"
#define kIGServiceWeekEndDates                          @"TimeSheet/WeekendDates"
#define kIGServiceDuplicateTimeSheet                    @"TimeSheet/DuplicateTimesheet"
#define kIGServiceValidDaysOfProject                    @"TimeSheet/ValidateProjectDate"


#pragma mark - WebService Key

#define kIGServiceKeyDomainID                           @"DomainId"
#define kIGServiceKeyUserName                           @"UserName"
#define kIGServiceKeyPassword                           @"Password"
#define kIGServiceKeyEmployeeImage                      @"EmpImage"

//key to check if user is US employee or indian employee
#define kIGServiceKeyisUserUSEmp                        @"ISUS"

#define kIGServiceKeyLoginDetail                        @"LoginDetail"
#define kIGServiceKeyLoginStatus                        @"LoginStatus"
#define kIGServiceKeyTimesheetStatus                    @"TimesheetStatus"

#define kIGServiceKeyDate                               @"Date"
#define kIGServiceKeyDATE                               @"DATE"
#define kIGServiceKeydate                               @"date"

#define kIGServiceKeyEmployeeId                         @"EmployeeId"
#define kIGServiceKeyLoginBy                            @"LoginBy"

//CR ID
#define kIGCRID                                         @"CRID"

#define kIGServiceKeyProjectID                          @"ProjectId"
#define kIGServiceKeyTaskID                             @"TaskId"
#define kIGServiceKeyTimeSheetStatus                    @"TimeSheetStatus"
#define kIGServiceKeyIsUSEmployee                       @"ISUSEmp"      // 1 if Infogain US Employee, else 0
#define kIGServiceKeyIsBillableTaskType                 @"IsBillableTaskType" // 1 if billable, else 0

#define kIGServiceKeyEmployeeCardEntryId                @"EmployeeCardEntry_DetailId"
#define kIGServiceKeyWeekId                             @"WeekId"
#define kIGServiceKeyStatusId                           @"StatusId"
#define kIGServiceKeyComments                           @"Comments"
#define kIGServiceKeyLocation                           @"Location"
#define kIGServiceKeyLocationType                       @"Type"
#define kIGServiceKeySubTaskId                          @"SubTaskId"
#define kIGServiceKeyCRId                               @"CRId"
#define kIGServiceKeyDescription                        @"Description"
#define kIGServiceKeyModifiedBy                         @"ModifiedBy"
#define kIGServiceKeyIsUSRecord                         @"IsUSRecord"
#define kIGServiceKeyintPayrollTaskId                   @"intPayrollTaskId"
#define kIGServiceKeyTransMidOfWeek                     @"TransMidOfWeek"
#define kIGServiceKeyWFOL                               @"WFOL"
#define kIGServiceKeyIsBillable                         @"IsBillable"
#define kIGServiceKeyIsUSRecord                         @"IsUSRecord"

#define kIGServiceKeyInOutHoursSummary                  @"InOutHrsSummary"
#define kIGServiceKeyTimeSheetHoursSummary              @"TimeSheetHrsSummary"
#define kIGServiceKeyTotalHoursAverage                  @"TOTALHOURS_AVG"
#define kIGServiceKeyTotalDays                          @"TotalDays"
#define kIGServiceKeyTotalHours                         @"TotalHours"
#define kIGServiceKeyWeekAverage                        @"WEEKLYAVG_AVG"
#define kIGServiceKeyWeek(weekIndex)                    [NSString stringWithFormat:@"W%d", weekIndex]
#define kIGServiceKeyTotalHour(weekIndex)               [NSString stringWithFormat:@"W%dT", weekIndex]
#define kIGServiceKeyWeeklyAverage(weekIndex)           [NSString stringWithFormat:@"W%dW", weekIndex]
#define kIGServiceKeyNumberOfDays(weekIndex)            [NSString stringWithFormat:@"WDays%d", weekIndex]


#define kIGServiceKeyEmployeeName                       @"EMPLOYEE_NAME"
#define kIGServiceKeyEmployeeBaseLocationID             @"EMP_BaseLocationId"
#define kIGServiceKeyEmployeeLocationID                 @"EMP_LOCATION_ID"
#define kIGServiceKeyEmployeeMailID                     @"EMP_MAILID"
#define kIGServiceKeyEmployeeStaffID                    @"EMP_STAFFID"
#define kIGServiceKeyEmployeeNewID                      @"EmployeeNewId"
#define kIGServiceKeyEmployeeStaffID1                   @"EMP_STAFFID1"
#define kIGServiceKeyEmployeeStatus                     @"EMP_STATUS"
#define kIGServiceKeyIsContractor                       @"ISContractor"
#define kIGServiceKeyIsAboveProjectManger               @"IsAbovePM"
#define kIGServiceKeyIsProjectManger                    @"IsPM"

#define kIGServiceKeyOnsiteManager                      @"OnsiteManager"
#define kIGServiceKeyPayrollAdmin                       @"PayrollAdmin"
#define kIGServiceKeyProjectManagerID                   @"ProjectManagerId"
#define kIGServiceKeyTimesheetType                      @"TimesheetType"

#define kIGServiceKeyStatus                             @"Status"
#define kIGServiceKeyProjectManagerStatus               @"PMStatus"
#define kIGServiceKeyWeekTimeSheetSummary               @"WeekTimeSheetSummary"

#define kIGServiceKeyEffortsHrs                         @"EFFORT(HRS)"
#define kIGServiceKeyWeekday                            @"WEEKDAY"
#define kIGServiceKeyOrder                              @"order"

#define kIGServiceKeyMonthlyTimeSheetSummary            @"MonthlyTimeSheetSummary"
#define kIGServiceKeyEFFORTS                            @"EFFORTS"
#define kIGServiceKeyWeekID                             @"WeekId"
#define kIGServiceKeyWeekOrder                          @"WeekOrder"

#define kIGServiceKeyDayWiseTaskDetail                  @"DayWiseTaskDetail"

#define kIGServiceKeyTrackName                          @"TrackName"
#define kIGServiceKeyTaskName                           @"TaskName"
#define kIGServiceKeySubTaskName                        @"SubTaskName"
#define kIGServiceKeyCRName                             @"CRName"
#define kIGServiceKeyEfforts                            @"Efforts"

#define kIGServiceKeyYear                               @"Year"
#define kIGServiceKeyMonth                              @"Month"


#define kIGServiceValidDaysRequest                      10000

#pragma mark - WebService Value

#pragma mark - PMApproval

#define kIGServiceApprovalAccounts                  @"Timesheet/Account"
#define kIGServiceApprovalTracks                    @"TimeSheet/Tracks"
#define kIGServiceApprovalTimeSheets                @"TimeSheet/Show"
#define kIGServiceApprovalAppoveTimeSheet           @"TimeSheet/Approve"
#define kIGServiceApprovalRejectTimeSheet           @"TimeSheet/Reject"
#define kIGServiceApprovalAddNote                   @"TimeSheet/Note"
#define kIGServiceApprovalTotalHours                @"TimeSheet/TotalHrs"
#define kIGServiceApprovalMonths                    @"TimeSheet/Year"

#pragma mark - Approval WebserviceKeys 

#define kIGServiceApprovalNewEmployeeID @"NewEmpID"
#define kIGServiceApprovalPMID @"Empid"
#define kIGServiceApprovalAccountID @"Accountid"
#define kIGServiceApprovalYear @"Year"

#define kIGServiceApprovalAccountID @"Accountid"
#define kIGServiceApprovalProjectID @"projectid"
#define kIGServiceApprovalWeekID    @"Weekid"
#define kIGServiceApprovalStatusID  @"StatusId"
#define kIGServiceApprovalLoginBy   @"LoginBy"

#define kIGServiceApprovalCardEntryID   @"CardEntryID"
#define kIGServiceApprovalNote          @"Note"

/*[{
    "projectid":"18",
    "Weekid":"512",
    "Empid":"2141237",
    "LoginBy":"2130939",
    "note":"please fill task"
}]*/

#define SCREEN_WIDTH ([[UIScreen mainScreen] bounds].size.width)
#define SCREEN_HEIGHT ([[UIScreen mainScreen] bounds].size.height)
#define SCREEN_MAX_LENGTH (MAX(SCREEN_WIDTH, SCREEN_HEIGHT))
#define SCREEN_MIN_LENGTH (MIN(SCREEN_WIDTH, SCREEN_HEIGHT))

#define IS_IPAD (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
#define IS_LANDSCAPE  ([UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeLeft || [UIApplication sharedApplication].statusBarOrientation == UIDeviceOrientationLandscapeRight)

#define IS_IPAD_LANDSCAPE (IS_IPAD && IS_LANDSCAPE)
#define IS_IPHONE (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define IS_RETINA ([[UIScreen mainScreen] scale] >= 2.0)

#define IPHONE_5_LENGTH  568.0
#define IPHONE_6_LENGTH  667.0
#define IPHONE_6P_LENGTH 736.0
#define IPHONE_7_LENGTH  667.0
#define IPHONE_7P_LENGTH 736.0
#define IPAD_PRO_LENGTH 2732.0

#define IS_IPHONE_4_OR_LESS (IS_IPHONE && SCREEN_MAX_LENGTH < IPHONE_5_LENGTH)
#define IS_IPHONE_5 (IS_IPHONE && SCREEN_MAX_LENGTH == IPHONE_5_LENGTH)
#define IS_IPHONE_6 (IS_IPHONE && SCREEN_MAX_LENGTH == IPHONE_6_LENGTH)
#define IS_IPHONE_6P (IS_IPHONE && SCREEN_MAX_LENGTH == IPHONE_6P_LENGTH)
#define IS_IPHONE_7 (IS_IPHONE && SCREEN_MAX_LENGTH == IPHONE_7_LENGTH)
#define IS_IPHONE_7P (IS_IPHONE && SCREEN_MAX_LENGTH == IPHONE_7P_LENGTH)
#define IS_IPADPRO_12I (IS_IPAD && SCREEN_MAX_LENGTH == IPAD_PRO_LENGTH)


#define kIGServiceApproval
#endif
