//
//  IGViewLeavesModel.h
//  IGTimeSheet
//
//  Created by Kamal on 19/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGViewLeavesModel : NSObject

@property (atomic) NSString *leaveType;
@property(atomic) NSString *leaveFrom;
@property(atomic) NSString *leaveTo;
@property(atomic) NSString *leaveFromHalf;
@property(atomic) NSString *leaveToHalf;
@property(atomic) NSString *status;
@property(atomic) NSNumber *noOfDays;

@end
