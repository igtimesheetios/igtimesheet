//
//  IGMonthlyTimesheetSummary.m
//  IGTimeSheet
//
//  Created by Manish on 23/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGMonthlyTimesheetSummary.h"
#import "IGServiceDefine.h"
#import "NSDictionary+IGAddition.h"
#import "IGUtils.h"

@implementation IGMonthlyEffort

- (instancetype)initWithEffort:(NSDictionary *)effortInfo
{
    if ([effortInfo respondsToSelector:@selector(objectForKey:)] == NO)
    {
        return nil;
    }
    
    self = [super init];
    if (self)
    {
        _date = [effortInfo IGStringValueForKey:kIGServiceKeydate];
        _weekOrder = [effortInfo IGStringValueForKey:kIGServiceKeyWeekOrder];
        _effort = [effortInfo IGNumberValueForKey:kIGServiceKeyEFFORTS];
        _weekID = [effortInfo IGNumberValueForKey:kIGServiceKeyWeekID];
    }
    return self;
}

@end


@implementation IGMonthlyTimesheetSummary

- (instancetype)initWithName:(NSString *)monthName timeSheetSummary:(NSArray *)timesheetSummary
{
    if (([timesheetSummary respondsToSelector:@selector(objectAtIndex:)] == NO) ||
        (timesheetSummary.count == 0))
    {
        return nil;
    }
    
    self = [super init];
    if (self)
    {
        if ([IGUtils isValidString:monthName])
        {
            _monthName = monthName;
        }
        else
        {
            _monthName = @"";
        }
        
        NSInteger count = timesheetSummary.count;
        
        // count > 0, checked already
        
        NSMutableArray *efforts = [[NSMutableArray alloc] initWithCapacity:1];
        
        for (int i = 0; i < count; i++)
        {
            NSDictionary *effortInfo = timesheetSummary[i];
            IGMonthlyEffort *monthlyEffort = [[IGMonthlyEffort alloc] initWithEffort:effortInfo];
            if (monthlyEffort != nil)
            {
                [efforts addObject:monthlyEffort];
            }
        }
        
        if (efforts.count > 1)
        {
            NSSortDescriptor *orderSort = [[NSSortDescriptor alloc] initWithKey:@"weekOrder" ascending:YES];
            _timesheetSummary = [efforts sortedArrayUsingDescriptors:@[orderSort]];
        }
    }
    return self;
}


@end
