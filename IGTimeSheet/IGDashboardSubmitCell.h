//
//  IGDashboardSubmitCell.h
//  IGTimeSheet
//
//  Created by Rajat on 02/03/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGDashboardSubmitCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@end
