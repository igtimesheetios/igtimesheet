//
//  IGUserDefaults.h
//  IGTimeSheet
//
//  Created by Manish on 06/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "IGEmployee.h"


@interface IGUserDefaults : NSObject

+ (instancetype)sharedInstance;

@property (nonatomic, assign) BOOL isUserLoggedIn;
@property (nonatomic, assign) BOOL hasAcceptedTermsAndConditions;
@property (nonatomic, assign) BOOL isFirstTimeLogin;
@property (nonatomic, assign) BOOL isDeviceTokenRegistered;

@property (nonatomic, strong) NSString *domainID;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, assign) NSInteger ifUserUSEmployee;

@property   (nonatomic, strong) UIImage *userImage;

@property (nonatomic, strong) IGEmployee *employee;

+ (void)savePassword:(NSString *)password;
+ (NSString *)retrievePassword;

- (BOOL)loginWithUser:(NSDictionary *)userInfo updateInfo:(BOOL)update;
- (void)logout;

// if userId and image is valid, write to disk
// else delete from disk and setup default profile image.
- (void)saveUserImage;

// if userId is valid, retrieve image from disk
// else setup default profile image
- (void)retrieveUserImage;

- (BOOL)updateDeviceTokenOnServer;

@end
