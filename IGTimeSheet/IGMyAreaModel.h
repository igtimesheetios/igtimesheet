//
//  IGMyAreaModel.h
//  IGTimeSheet
//
//  Created by Manish on 23/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IGWeeklyTimesheetSummary;
@class IGMonthlyTimesheetSummary;

@interface IGMyAreaModel : NSObject

@property (nonatomic, readonly) BOOL shouldFetchWeeklyTimesheetSummary;
@property (nonatomic, readonly) BOOL shouldFetchMonthlyTimesheetSummary;

@property (nonatomic, assign) NSInteger employeeID;
@property (nonatomic, strong) NSDate *queryDate;

@property (nonatomic, strong) IGWeeklyTimesheetSummary *weeklyTimesheetSummary;

// contains object of kind IGMonthlyTimesheetSummary
@property (nonatomic, strong) NSArray *monthlyTimesheetSummary;

@property (nonatomic, strong) NSString *weekSummaryStatus;
@property (nonatomic, strong) NSString *monthSummaryStatus;


- (void)fetchWeeklyTimesheetSummary:(void(^)(IGMyAreaModel *myAreaModel))completionBlock
                              error:(void(^)(NSError *error))errorBlock;

- (void)fetchMonthlyTimesheetSummary:(void(^)(IGMyAreaModel *myAreaModel))completionBlock
                              error:(void(^)(NSError *error))errorBlock;

@end
