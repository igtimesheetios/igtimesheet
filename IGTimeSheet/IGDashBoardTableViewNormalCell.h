//
//  IGDashBoardTableViewNormalCell.h
//  IGTimeSheet
//
//  Created by Rajat on 12/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGDashBoardTableViewNormalCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *valueLabel;

@end
