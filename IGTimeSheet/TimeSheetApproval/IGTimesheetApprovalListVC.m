//
//  IGTimesheetApprovalListVC.m
//  IGTimeSheet
//
//  Created by Anjali on 05/11/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import "IGTimesheetApprovalListVC.h"
#import "IGApprovalListCell.h"
#import "IGTimesheetApproval.h"
#import "SectionView.h"
#import "IGTimesheetNoteVC.h"
#import "IGPMTimeSheet.h"
#import "IGApprovalTrack.h"
#import "IGUtils.h"
#import "AppDelegate.h"
#import "IGTaskViewVC.h"
#import "IGApprovalParser.h"
#import "IGApprovalTSDetailVC.h"
#import "IGTextInputView.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>


#define ALPHABETS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz"

@interface IGTimesheetApprovalListVC ()<UITableViewDelegate,UITableViewDataSource,ApprovalCellDelegate,IGApprovalAddNoteDelegate,IGTextInputViewDelegate>{
    
    NSMutableArray *approvalArray;
    NSMutableSet *timesheetsForSubmission;
    NSIndexPath *noteIndexPath;
    NSIndexPath *editingIndexPath;
    IGEmployeeTimesheet *__employeeTimeSheet__;
    BOOL isAllSelected;
    NSInteger totalTimeSheetCount;
    UIButton *selectAllBtn;
}

@property(weak,nonatomic)IBOutlet UITableView *approvalTable;
@property (weak,nonatomic) IBOutlet UILabel *lblNoTimeSheet;
@property (weak, nonatomic) IBOutlet UIImageView *imgNoTimeSheet;
@property (weak, nonatomic) IBOutlet UIButton *btnApprove;
@property (weak, nonatomic) IBOutlet UIButton *btnreject;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;
@end

@implementation IGTimesheetApprovalListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    isAllSelected = false;
    
    [self addSelectAllAction];
    [self setupBackButton];
    
    timesheetsForSubmission = [[NSMutableSet alloc] init];
    self.navigationItem.title = self.selectedWeekend.date;
    
    // Do any additional setup after loading the view.
    if ([self isApprovedOrCompletedTimeSheet]) {
        
        IGPMTimeSheet *pmTimeSheet = (IGPMTimeSheet *)[self.timeSheets firstObject];
        if (self.timeSheets.count == 0 ||(self.timeSheets.count == 1 && pmTimeSheet.projectID == nil)) {
            [self noTimeSheet];
        }
        [self calculateTotalCount];
        
    }else{
        if (self.timeSheets.count == 0) {
            [self noTimeSheet];
        }else{
            [self hideApproveReject];
        }
    }
}

-(void)noTimeSheet{
    self.lblNoTimeSheet.hidden = NO;
    self.imgNoTimeSheet.hidden = NO;
    self.imgNoTimeSheet.image = [UIImage imageNamed:@"NoTimesheet"];
    self.lblNoTimeSheet.text = @"There is no timesheet to show.";
    self.approvalTable.hidden = YES;
    [self hideApproveReject];

}

-(void)hideApproveReject{
    self.btnreject.hidden = YES;
    self.btnApprove.hidden = YES;
    self.bottomConstraint.constant = -44.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    if ([self isApprovedOrCompletedTimeSheet]) {
        SectionView *sectionHeader = [[[NSBundle mainBundle] loadNibNamed:@"SectionView" owner:self options:nil] objectAtIndex:0];
        
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(toggleSectionRowVisibility:)];
        
        if ([self isApprovedOrCompletedTimeSheet]){
            IGPMTimeSheet *pmTimeSheet = [self.timeSheets objectAtIndex:section];
            
            sectionHeader.projectName.text = pmTimeSheet.projectName;
            
        }
        return sectionHeader;
    }
    
    return nil;
    
//    if(section == 0){
    
//        UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0, CGRectGetHeight(sectionHeader.frame), CGRectGetWidth(sectionHeader.frame), 20.0)];
//        view2.backgroundColor = [UIColor whiteColor];
//        [sectionHeader addSubview:view2];
//        [sectionHeader addGestureRecognizer:tapGestureRecognizer];
//        sectionHeader.tag = section;
//        sectionHeader.weekDate.text = pmTimeSheet.projectName;
    
    
        
//    }else {
//        
//        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(sectionHeader.frame), 70.0)];
//        view.backgroundColor = [UIColor clearColor];
//        
//        UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(sectionHeader.frame), 20.0)];
//        view1.backgroundColor = [UIColor whiteColor];
//        [view addSubview:view1];
//        
//        sectionHeader.frame = CGRectMake(0, 20.0, CGRectGetWidth(sectionHeader.frame), CGRectGetHeight(sectionHeader.frame));
//        sectionHeader.weekDate.text = pmTimeSheet.projectName;
//        [view addSubview:sectionHeader];
//        
//        UIView *view2 = [[UIView alloc] initWithFrame:CGRectMake(0,50.0, CGRectGetWidth(sectionHeader.frame), 20.0)];
//        view2.backgroundColor = [UIColor whiteColor];
//        [view addSubview:view2];
//        view.tag = section;
//        [view addGestureRecognizer:tapGestureRecognizer];
//        return view;
//    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if ([self.timeSheetType.typeCode isEqualToString:@"A"] || [self.timeSheetType.typeCode isEqualToString:@"C"]) {
        
        if (self.timeSheets.count > 0) {
            return [self.timeSheets count];
        }
    }
    else if([self.timeSheetType.typeCode isEqualToString:@"N"] || [self.timeSheetType.typeCode isEqualToString:@"P"]){
        return 1;
    }
    return 0;
}


-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    if ([self isApprovedOrCompletedTimeSheet]){
        return 0.0001;
        
    }
    return 0.0;
}

-(CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section {
    
//    if(section == 0)
    if ([self isApprovedOrCompletedTimeSheet]){
        return 44.0;

    }

    return 0;
//    return 70;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if ([self.timeSheetType.typeCode isEqualToString:@"A"] || [self.timeSheetType.typeCode isEqualToString:@"C"]) {
        if (self.timeSheets) {
            
            IGPMTimeSheet *pmTimeSheet = [self.timeSheets objectAtIndex:section];
            
            return [pmTimeSheet.timesheet count];
        }
    }
    else if([self.timeSheetType.typeCode isEqualToString:@"N"] || [self.timeSheetType.typeCode isEqualToString:@"P"]){
        
        return [self.timeSheets count];
        
    }
    return 0;
}



- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    IGApprovalListCell *cell;
    
//    UIBezierPath *maskPath = [UIBezierPath
//                              bezierPathWithRoundedRect:cell.baseView.bounds
//                              byRoundingCorners:(UIRectCornerTopLeft | UIRectCornerTopRight)
//                              cornerRadii:CGSizeMake(4, 4)
//                              ];
    
//    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    
//    maskLayer.frame = cell.baseView.bounds;
//    maskLayer.path = maskPath.CGPath;
    
//    cell.baseView.layer.mask = maskLayer;
    
//    if(indexPath.row == 0){
//        CALayer *upperBorder = [CALayer layer];
//        upperBorder.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:223.0/255.0 blue:218.0/255.0 alpha:1.0].CGColor;
//        upperBorder.frame = CGRectMake(0, 0, cell.frame.size.width, 1.5f);
//        [cell.baseView.layer addSublayer:upperBorder];
//    }
    
//    CALayer *rightBorder = [CALayer layer];
//    rightBorder.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:223.0/255.0 blue:218.0/255.0 alpha:1.0].CGColor;
//    rightBorder.frame = CGRectMake(0, 0, 1.5f , CGRectGetHeight(cell.contentView.frame));
//    [cell.baseView.layer addSublayer:rightBorder];
//    
//    
//    CALayer *leftBorder = [CALayer layer];
//    leftBorder.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:223.0/255.0 blue:218.0/255.0 alpha:1.0].CGColor;
//    leftBorder.frame = CGRectMake(280,0, 1.5f , CGRectGetHeight(cell.contentView.frame));
//    [cell.baseView.layer addSublayer:leftBorder];
    

//    if(indexPath.row == pmTimeSheet.timesheet.count - 1){
//        CALayer *bottomBorder = [CALayer layer];
//        bottomBorder.backgroundColor = [UIColor colorWithRed:225.0/255.0 green:223.0/255.0 blue:218.0/255.0 alpha:1.0].CGColor;
//        bottomBorder.frame = CGRectMake(0,57, CGRectGetWidth(cell.frame), 1.5f);
//        [cell.baseView.layer addSublayer:bottomBorder];
//    }
    
    
    if (![self isApprovedOrCompletedTimeSheet]) {
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"NonExistingCell" forIndexPath:indexPath];
        
        IGNonExistingTimeSheets *netimeSheet = [self.timeSheets objectAtIndex:indexPath.row];
        cell.userName.text = netimeSheet.employeeName;
        cell.totalHours.text = netimeSheet.trackName;
        cell.employeeID.text = [NSString stringWithFormat:@"%ld",netimeSheet.NewEmployeeID];
                
    }else{
        
        cell = [tableView dequeueReusableCellWithIdentifier:@"approvalCell" forIndexPath:indexPath];

        
        IGPMTimeSheet *pmTimeSheet = [self.timeSheets objectAtIndex:indexPath.section];

        IGEmployeeTimesheet *employeeTimeSheet = [pmTimeSheet.timesheet objectAtIndex:indexPath.row];
        
        cell.userName.text =[NSString stringWithFormat:@"%@",employeeTimeSheet.employeeName] ;
        
        cell.totalHours.text = [NSString stringWithFormat:@"%.2f",employeeTimeSheet.totalHours];
        cell.employeeID.text = [NSString stringWithFormat:@"Employee ID:%@",employeeTimeSheet.employeeID];
        
        if ([employeeTimeSheet.locationID isEqualToString:@"US"]) {
            cell.denominatedHours.text = [NSString stringWithFormat:@"B:%.2f | NB:%.2f",employeeTimeSheet.billableHours,employeeTimeSheet.nonBillableHours];
        }else{
            cell.denominatedHours.hidden = YES;
        }
        cell.indexPath = indexPath;
        
        cell.cellDelegate = self;
        
        if ([timesheetsForSubmission containsObject:employeeTimeSheet]) {
            [cell.btnSelect setSelected:YES];
        }else{
            [cell.btnSelect setSelected:NO];
        }

    }
    return cell;
}


-(void)toggleSectionRowVisibility:(UITapGestureRecognizer *)gestureRecognizer{
    
    NSInteger tag = gestureRecognizer.view.tag;
    IGTimesheetApproval *model = [approvalArray objectAtIndex:tag];
    model.isVisible = !model.isVisible;
    NSIndexSet *indexSet = [[NSIndexSet alloc] initWithIndex:tag];
    [_approvalTable reloadSections:indexSet withRowAnimation:UITableViewRowAnimationAutomatic];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - UITableVIewDelegate

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath{
}

-(BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([self isApprovedOrCompletedTimeSheet]) {
        return YES;
    }
    return NO;
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Notes" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
//        noteIndexPath = indexPath;
//        [self presentAddNoteVC];
//    }];

    IGPMTimeSheet *pmTimeSheet = [self.timeSheets objectAtIndex:indexPath.section];
    
    IGEmployeeTimesheet *employeeTS = [pmTimeSheet.timesheet objectAtIndex:indexPath.row];

    UITableViewRowAction *editAction = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Notes" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath){
        
        editingIndexPath = [indexPath copy];
        
        IGTextInputView *view = [IGTextInputView textInputView];
        
        if (employeeTS.note != nil && employeeTS.note.length != 0) {
            view.textView.text = employeeTS.note;
        }
        view.labelTitle.text = @"Add Note";
        view.labelPlaceholder.text = @"Enter Note";
        view.delegate = self;
        [view presentOnView:self.view];

    }];
    

    return @[editAction];
}





-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    if ([self isApprovedOrCompletedTimeSheet]) {
        IGPMTimeSheet *pmTimeSheet = [self.timeSheets objectAtIndex:indexPath.section];
        
        __employeeTimeSheet__ = [pmTimeSheet.timesheet objectAtIndex:indexPath.row];
        
        [self performSegueWithIdentifier:@"IGTaskSegue" sender:self];

    }
    
}

#pragma mark - Add note Web-service

-(void)addNote:(NSString *)note id:(NSString *)cardEntryID addedBy:(NSString *)managerID{
    [self showSpinner:YES];
    
    [[IGService sharedInstance] add:note id:cardEntryID addedBy:managerID complestion:^(id response) {
         [self showSpinner:NO];
    } error:^(NSError *error) {
         [self showSpinner:NO];
    }];
    
}



#pragma mark - IGTextInputViewDelegate

-(void)textInputView:(IGTextInputView *)view didSelectButtonAtIndex:(NSInteger)index
{
    if (index == 1) {
        IGPMTimeSheet *pmTimeSheet = [self.timeSheets objectAtIndex:editingIndexPath.section];
        
        IGEmployeeTimesheet *employeeTS = [pmTimeSheet.timesheet objectAtIndex:editingIndexPath.row];
        
        employeeTS.note = view.textView.text;
        
        [pmTimeSheet.timesheet replaceObjectAtIndex:editingIndexPath.row withObject:employeeTS];
        
        [self addNote:view.textView.text id:employeeTS.ID addedBy:employeeTS.pmID];
   
    }
}


-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"IGTaskSegue"]) {
        
        IGApprovalTSDetailVC *tsDetail = (IGApprovalTSDetailVC *)[segue destinationViewController];
                
        [tsDetail initilizeDataFor:__employeeTimeSheet__.ID status:self.timeSheetType weekend:__employeeTimeSheet__.weekendDate employeeName:__employeeTimeSheet__.employeeName];

    }else{
        
        IGTimesheetNoteVC *addNoteVC = (IGTimesheetNoteVC *)segue.destinationViewController;
        
        addNoteVC.addNoteDelegate = self;
        
        addNoteVC.indexPath = noteIndexPath;
        
        IGPMTimeSheet *pmTimeSheet = [self.timeSheets objectAtIndex:noteIndexPath.section];
        
        IGEmployeeTimesheet *employeeTimeSheet = [pmTimeSheet.timesheet objectAtIndex:noteIndexPath.row];
        
        addNoteVC.note = employeeTimeSheet.note;
        
    }
    
}


#pragma mark - Add note delegate


-(void)didSelect:(NSIndexPath *)indexPath selected:(BOOL)isSelecetd{
    
    IGPMTimeSheet *pmTimeSheet = [self.timeSheets objectAtIndex:indexPath.section];
    
    IGEmployeeTimesheet *employeeTS = [pmTimeSheet.timesheet objectAtIndex:indexPath.row];
    
    if (isSelecetd) {
        if ([timesheetsForSubmission containsObject:employeeTS]) {
            [timesheetsForSubmission removeObject:employeeTS];
        }else{
            [timesheetsForSubmission addObject:employeeTS];
            if (timesheetsForSubmission.count == totalTimeSheetCount) {
                selectAllBtn.selected = YES;
            }
        }
    }else{
        if ([timesheetsForSubmission containsObject:employeeTS]) {
            [timesheetsForSubmission removeObject:employeeTS];
            selectAllBtn.selected = NO;
            
        }
    }
}

#pragma mark - Approve/Reject actions.

-(IBAction)sel_btnReject:(UIButton *)reject{
    kAlertAndReturnIfNetworkNotReachable;
    
    if (timesheetsForSubmission.count > 0) {
        //
        [[IGService sharedInstance] approve:timesheetsForSubmission approve:NO completion:^(id response){
            NSDictionary *responseDict = (NSDictionary *)response;
            
            [self showTestMessage:[responseDict valueForKey:[[responseDict allKeys] firstObject]]];
            
            if (![[[responseDict allKeys] firstObject] isEqualToString:@"Comment"] && ![[[responseDict allKeys] firstObject] isEqualToString:@"BillingLock"]) {
                
                [FIRAnalytics logEventWithName:@"Timesheet_Reject" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"Rejected_By:%@",[[IGUserDefaults sharedInstance] employee].employeeNewID]}];
                
                [IGAnalytics trackEventWith:@"iOS" action:@"Timesheet_Reject" actionName:[NSString stringWithFormat:@"Rejected By:%@_%@",[IGUserDefaults sharedInstance].employee.employeeNewID,[IGUserDefaults sharedInstance].userName]];

                
                [self refreshAftersubmission];
                [self.approvalTable reloadData];
            }
        } error:^(NSError *error) {
            // Code to handle Error
            [IGUtils showOKAlertWithTitle:kStringError message:ParseError];
        }];
    }else{
        [IGUtils showOKAlertWithTitle:@"Selction error" message:@"Please select a tmesheet to Reject."];
    }
}

-(IBAction)sel_btnApprove:(UIButton *)approve{
    
    kAlertAndReturnIfNetworkNotReachable;
    
    if (timesheetsForSubmission.count > 0) {
        //
        [[IGService sharedInstance] approve:timesheetsForSubmission approve:YES completion:^(id response){
            
            [self showTestMessage:[(NSDictionary *)response valueForKey:[[response allKeys] firstObject]]];
            
            [FIRAnalytics logEventWithName:@"TimeSheet_Approval" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"Approved_By:%@",[[IGUserDefaults sharedInstance] employee].employeeNewID]}];

            [IGAnalytics trackEventWith:@"iOS" action:@"Timesheet_Approval" actionName:[NSString stringWithFormat:@"Approved By:%@_%@",[IGUserDefaults sharedInstance].employee.employeeNewID,[IGUserDefaults sharedInstance].userName]];

            [self refreshAftersubmission];
            [self.approvalTable reloadData];
            
        } error:^(NSError *error) {
            // Code to handle Error
            [IGUtils showOKAlertWithTitle:kStringError message:ParseError];
        }];
        
        
    }else{
        [IGUtils showOKAlertWithTitle:@"Selction error" message:@"Please select a tmesheet for approval."];
    }
}


-(void)presentAddNoteVC{
    
    [self performSegueWithIdentifier:@"IGAddNoteSegue" sender:self];
}


-(void)showTestMessage:(NSString *)message{
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:nil
                                                                   message:message
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [self presentViewController:alert animated:YES completion:nil];
    
    int duration = 2; // duration in seconds
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        [alert dismissViewControllerAnimated:YES completion:^{
            if (selectAllBtn.selected) {
                selectAllBtn.selected = NO;
                [self goBack:nil];
            }

        }];
    });

}


-(void)refreshAftersubmission{
    
    for (IGEmployeeTimesheet *employeeTimeSheet in timesheetsForSubmission) {
    
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"projectID==%@",employeeTimeSheet.projectID];
        
        NSArray *filteredTimeSheet = [self.timeSheets filteredArrayUsingPredicate:predicate];
        IGPMTimeSheet *pmTimeSheet = [filteredTimeSheet firstObject];
        
        if( [pmTimeSheet.timesheet containsObject:employeeTimeSheet]){
            [pmTimeSheet.timesheet removeObject:employeeTimeSheet];
        }
        
    }
    
    [timesheetsForSubmission removeAllObjects];
}

-(void)addSelectAllAction{
 
    selectAllBtn = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [selectAllBtn setBackgroundImage:[UIImage imageNamed:@"SelectAllUnchecked"] forState:UIControlStateNormal];
    [selectAllBtn setBackgroundImage:[UIImage imageNamed:@"SelectAllChecked"] forState:UIControlStateSelected];
    [selectAllBtn setSelected:NO];
    SEL selectAll = @selector(selectAll:);
    [selectAllBtn addTarget:self action:selectAll forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:selectAllBtn];
}

-(void)selectAll:(UIButton *)selectAllbutton{
    
    
    if ([self isApprovedOrCompletedTimeSheet]) {
        if (selectAllBtn.isSelected) {
            
            if (timesheetsForSubmission.count < totalTimeSheetCount){
                for (IGPMTimeSheet *pmTimeSheet in self.timeSheets) {
                    [timesheetsForSubmission addObjectsFromArray:pmTimeSheet.timesheet];
                }
            }else{
                [timesheetsForSubmission removeAllObjects];
                selectAllBtn.selected = NO;
            }
            
        }else{
            [timesheetsForSubmission removeAllObjects];
            selectAllBtn.selected = YES;
            
            for (IGPMTimeSheet *pmTimeSheet in self.timeSheets) {
                [timesheetsForSubmission addObjectsFromArray:pmTimeSheet.timesheet];
            }
        }
        
        [self.approvalTable reloadData];

    }
}


-(void)calculateTotalCount{
    for (IGPMTimeSheet *pmTimeSheet in self.timeSheets) {
        totalTimeSheetCount += pmTimeSheet.timesheet.count;
    }
}



-(BOOL)isApprovedOrCompletedTimeSheet{
    if ([self.timeSheetType.typeCode isEqualToString:@"A"] || [self.timeSheetType.typeCode isEqualToString:@"C"]){
        return YES;
    }else{
        return NO;
    }
}


@end
