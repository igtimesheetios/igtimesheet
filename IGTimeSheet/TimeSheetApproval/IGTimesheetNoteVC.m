//
//  IGTimesheetNoteVC.m
//  IGTimeSheet
//
//  Created by Saurabh on 14/12/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import "IGTimesheetNoteVC.h"

@interface IGTimesheetNoteVC ()

@property(nonatomic,weak) IBOutlet UITextView *txtViewNote;
@property (nonatomic, weak) IBOutlet UIVisualEffectView *bgBlurrView;

@end

@implementation IGTimesheetNoteVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self addGesture];
    
    if (self.note) {
        [self displayNote];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)displayNote{
    self.txtViewNote.text = self.note;
}

-(void)addGesture{
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(blurrViewTapped)];
    
    [self.bgBlurrView addGestureRecognizer:tapGestureRecognizer];
}

-(void)blurrViewTapped{
    [self dismiss];
}

-(void)dismiss{
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - IBActions

-(IBAction)sel_btnAdd:(UIButton *)add{
    [self.addNoteDelegate addNoteFor:self.indexPath note:self.txtViewNote.text];
    [self dismiss];
}

-(IBAction)sel_btnCancel:(UIButton *)add{
    [self dismiss];
}


@end
