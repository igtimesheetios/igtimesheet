//
//  IGApprovalListCell.h
//  IGTimeSheet
//
//  Created by Anjali on 05/11/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ApprovalCellDelegate
-(void)didSelect:(NSIndexPath *)indexPath selected:(BOOL)isSelecetd;
@end

@interface IGApprovalListCell : UITableViewCell{
    
}

@property(weak,nonatomic)IBOutlet UIView *baseView;

@property (weak, nonatomic) IBOutlet UILabel    *userName;
@property (weak, nonatomic) IBOutlet UILabel    *totalHours;
@property (weak, nonatomic) IBOutlet UILabel    *denominatedHours;
@property (weak, nonatomic) IBOutlet UILabel    *nonBillableHours;
@property (weak, nonatomic) IBOutlet UILabel    *status;
@property (weak, nonatomic) IBOutlet UILabel    *employeeID;
@property (weak, nonatomic) IBOutlet UIButton   *btnSelect;
@property (nonatomic) NSIndexPath *indexPath;
@property (weak) id<ApprovalCellDelegate> cellDelegate;

@end
