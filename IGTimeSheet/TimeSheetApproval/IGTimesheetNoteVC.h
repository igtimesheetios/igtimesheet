//
//  IGTimesheetNoteVC.h
//  IGTimeSheet
//
//  Created by Saurabh on 14/12/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol IGApprovalAddNoteDelegate
-(void)addNoteFor:(NSIndexPath *)indexPath note:(NSString *)note;
@end

@interface IGTimesheetNoteVC : UIViewController
@property(weak)id<IGApprovalAddNoteDelegate> addNoteDelegate;
@property (strong,nonatomic) NSIndexPath *indexPath;
@property (strong,nonatomic) NSString *note;
@end
