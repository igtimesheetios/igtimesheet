//
//  IGApprovalTSDetailVC.h
//  IGTimeSheet
//
//  Created by Saurabh on 06/01/17.
//  Copyright © 2017 InfoGain. All rights reserved.
//

#import "IGViewController.h"
@class IGApprovalTimesheetType;
@class IGApprovalWeekend;

@interface IGApprovalTSDetailVC : IGViewController
-(void)initilizeDataFor:(NSString *)timeSheetID status:(IGApprovalTimesheetType *)timeSheetType weekend:(NSString *)date employeeName:(NSString *)employeeName ;
@end
