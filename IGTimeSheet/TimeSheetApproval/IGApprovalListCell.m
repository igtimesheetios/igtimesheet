//
//  IGApprovalListCell.m
//  IGTimeSheet
//
//  Created by Anjali on 05/11/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import "IGApprovalListCell.h"

@implementation IGApprovalListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(IBAction)sel_btnSelect:(UIButton *)selectButton{
    
    if (selectButton.isSelected) {
        [self.btnSelect setSelected:NO];
        if (self.cellDelegate != nil) {
            [self.cellDelegate  didSelect:self.indexPath selected:NO];
        }
 
    }else{
        [self.btnSelect setSelected:YES];
        if (self.cellDelegate != nil) {
            [self.cellDelegate  didSelect:self.indexPath selected:YES];
        }
    }
}
@end
