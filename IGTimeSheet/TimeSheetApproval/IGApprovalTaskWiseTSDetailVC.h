//
//  IGApprovalTaskWiseTSDetailVC.h
//  IGTimeSheet
//
//  Created by Saurabh on 06/01/17.
//  Copyright © 2017 InfoGain. All rights reserved.
//

#import "IGViewController.h"
#import "IGTimeSheetDetail.h"

@interface IGApprovalTaskWiseTSDetailVC : IGViewController

-(void)initializeDataWith:(IGTimeSheetDetail *)timeSheetDetail weekendDate:(NSString *)weekendDate;

@end
