//
//  IGApprovalTSDetailVC.m
//  IGTimeSheet
//
//  Created by Saurabh on 06/01/17.
//  Copyright © 2017 InfoGain. All rights reserved.
//

#import "IGApprovalTSDetailVC.h"
#import "IGService.h"
#import "IGApprovalParser.h"
#import "IGTimeSheetDetail.h"
#import "IGTaskDetailTableViewCell.h"
#import "IGApprovalTrack.h"
#import "IGApprovalTaskWiseTSDetailVC.h"

#define TaskRowHeight       73
#define TOTAL_HOURS_ID  @"TOTAL_HOURS"

@interface IGApprovalTSDetailVC ()<UITableViewDelegate,UITableViewDataSource>
{
    IGTimeSheetDetail *__tsDetail__;
}
@property (weak,nonatomic) IBOutlet UITableView *tblTSDetail;
@property (nonatomic) NSString *status;
@property (nonatomic) NSMutableArray *arrTimeSheetDetail;
@property (nonatomic) IGApprovalTimesheetType *timeSheetType;
@property (nonatomic) NSString *employeeName;
@property (nonatomic) NSString *date;
@end

@implementation IGApprovalTSDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.tblTSDetail.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    [self setupBackButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)initilizeDataFor:(NSString *)timeSheetID status:(IGApprovalTimesheetType *)timeSheetType weekend:(NSString *)date employeeName:(NSString *)employeeName {
    self.timeSheetType = timeSheetType;
    self.employeeName = employeeName;
    self.date = date;
    self.arrTimeSheetDetail = [NSMutableArray new];
    
    self.navigationItem.title = date;
    
    [[IGService sharedInstance] timeSheetDetail:timeSheetID completion:^(id response) {
        //Code to load the Table view
        
        double totalHours = 0.0;
        NSDictionary *dictTempTSdetail = [(NSDictionary *)response valueForKey:@"Timesheet"];
        for (NSDictionary *dictTimeSheetDetail in dictTempTSdetail) {
            
            IGTimeSheetDetail *tsDetail = [[IGTimeSheetDetail alloc] initWith:dictTimeSheetDetail];
            
            totalHours += tsDetail.efforts;
            
            [self.arrTimeSheetDetail addObject:tsDetail];
        }
        
        [self.arrTimeSheetDetail addObject:[self addTotalHoursToArray:totalHours]];
        
        [self.tblTSDetail reloadData];
        
    } error:^(NSError *error) {
        //Code to handle error
    }];
}

-(IGTimeSheetDetail *)addTotalHoursToArray:(double )totalHours{
    
    IGTimeSheetDetail *tsDetail = [[IGTimeSheetDetail alloc] init];
    tsDetail.efforts = totalHours;
    tsDetail.projectName = @"Total Hours";
    tsDetail.projectID = TOTAL_HOURS_ID;
    
    return tsDetail;
    
}
#pragma mark - UItableViewDataSource 

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [self.arrTimeSheetDetail count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    IGTaskDetailTableViewCell *tableCell = [tableView dequeueReusableCellWithIdentifier:@"TaskDetailCell"];
    
    IGTimeSheetDetail *tsDetail = [self.arrTimeSheetDetail objectAtIndex:indexPath.row];
    
    if ([tsDetail.projectID isEqualToString:TOTAL_HOURS_ID]) {
        tableCell.projectLabel.text = tsDetail.projectName;
        tableCell.timeLabel.text = [NSString stringWithFormat:@"%.2f",tsDetail.efforts];
        tableCell.timeLabel.textColor = [UIColor whiteColor];
        tableCell.projectLabel.textColor = [UIColor whiteColor];
        tableCell.contentView.backgroundColor = [UIColor colorWithRed:(23/255.0) green:(143/255.0) blue:(70/255.0) alpha:1];
        tableCell.statusLabel.hidden = YES;
        tableCell.detailLabel.hidden = YES;
        tableCell.selectionStyle = UITableViewCellSelectionStyleNone;

    }else{
        tableCell.statusLabel.text = self.timeSheetType.type;
        tableCell.detailLabel.text = tsDetail.tsDescription;
        tableCell.projectLabel.text = tsDetail.projectName;
        tableCell.timeLabel.text = [NSString stringWithFormat:@"%.2f",tsDetail.efforts];
        tableCell.statusLabel.hidden = NO;
        tableCell.detailLabel.hidden = NO;
    }
    
    if ([self.timeSheetType.typeCode isEqualToString:@"A"] || [self.timeSheetType.typeCode isEqualToString:@"C"]) {
        [tableCell.statusLabel setTextColor:[UIColor colorWithRed:(23/255.0) green:(143/255.0) blue:(70/255.0) alpha:1]];
    }else if([self.timeSheetType.typeCode isEqualToString:@"P"] ||[self.timeSheetType.typeCode isEqualToString:@"N"]){
        [tableCell.statusLabel setTextColor:[UIColor orangeColor]];
    }
    return tableCell;
}


#pragma mark - UITableView Delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return TaskRowHeight;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    __tsDetail__ = [self.arrTimeSheetDetail objectAtIndex:indexPath.row];
    
    if (![__tsDetail__.projectID isEqualToString:TOTAL_HOURS_ID]) {
        [self performSegueWithIdentifier:@"TaskWiseTSDetailSegue" sender:self];
    }
    
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    IGApprovalTaskWiseTSDetailVC *taskWiseDetailVC = (IGApprovalTaskWiseTSDetailVC *)[segue destinationViewController];
    [taskWiseDetailVC initializeDataWith:__tsDetail__ weekendDate:self.date];
}
@end
