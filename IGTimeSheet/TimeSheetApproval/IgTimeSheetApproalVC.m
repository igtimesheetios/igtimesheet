//
//  IgTimeSheetApproalVC.m
//  IGTimeSheet
//
//  Created by Anjali on 05/11/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import "IgTimeSheetApproalVC.h"
#import "IGTimesheetApprovalListVC.h"
#import "IGPickerCell.h"
#import "IGAddTaskMacros.h"
#import "IGMacro.h"
#import "IGService.h"
#import "IGApprovalParser.h"
#import "IGPMAccount.h"
#import "IGApprovalTrack.h"

#define kApprovalShowTimeSheetSegue @"IGApprovalShowTimeSheetSegue"

@interface IgTimeSheetApproalVC ()<IGPickerCellDelegate, UITextFieldDelegate>{
    NSArray * timeSheetApprovalTypeHeadingArray;
     UIView *halfTransparentView;
    NSString *selectedYear;
    IGApprovalMonth *selectedMonth;
    
    NSIndexPath *_accountIndexPath_;
    NSIndexPath *_trackIndexPath_;
    NSIndexPath *_typeIndexPath_;
    NSIndexPath *_yearIndexPath_;
    NSIndexPath *_monthIndexPath_;
    NSIndexPath *_weekendIndexPath_;
    
    IGApprovalWeekend *selecetdWeekend;
    IGApprovalTimesheetType *selecetdStatusType;
}
@property (strong) NSMutableArray* accounts;
@property (strong) NSArray* tracks;
@property (strong) NSArray* types;
@property (strong) NSArray* years;
@property (strong) NSArray* months;
@property (strong) NSArray* weekendEndDate;
@property (strong) NSArray* timesheets;

@property (strong,nonatomic) IGApprovalTimeSheetRequest *request;

- (void) loadTableViewPickerCell:(IGPickerCell*)cell forRow:(NSInteger)row;
- (IBAction)showTimeAppBtnClked:(UIButton *)sender;

-(void)getAccountsFor:(NSString *)employeeID;
@property (weak, nonatomic) IBOutlet UIButton *showBtn;
@property (strong, nonatomic) IBOutlet UITableView *timeSheetApprovalTable;

@end



@implementation IgTimeSheetApproalVC

-(void)preapareIndices{
    _accountIndexPath_ = [NSIndexPath indexPathForRow:0 inSection:0];
    _trackIndexPath_ = [NSIndexPath indexPathForRow:1 inSection:0];
    _typeIndexPath_ = [NSIndexPath indexPathForRow:2 inSection:0];
    _yearIndexPath_ = [NSIndexPath indexPathForRow:3 inSection:0];
    _monthIndexPath_ = [NSIndexPath indexPathForRow:4 inSection:0];
    _weekendIndexPath_ = [NSIndexPath indexPathForRow:5 inSection:0];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self preapareIndices];
    [self setupBackButton];
    [self setupOpaqueNavigationBar];
    self.navigationItem.title =@"Timesheet Approval";
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName]];
    [_showBtn setBackgroundColor:kDefaultBarTintColor];
    self.timeSheetApprovalTable.tableFooterView = [[UIView alloc] init];
    
    [self getAccountsFor:[IGUserDefaults sharedInstance].employee.staffID];
    
    timeSheetApprovalTypeHeadingArray =[[NSArray alloc]initWithObjects:@"Account",@"Project track",@"Type",@"Year",@"Month",@"Weekend End Date", nil];
    self.request = [[IGApprovalTimeSheetRequest alloc] initWith:[IGUserDefaults sharedInstance].employee.staffID ];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return timeSheetApprovalTypeHeadingArray.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    IGPickerCell* cell = [tableView dequeueReusableCellWithIdentifier:@"IGPickerCell" forIndexPath:indexPath];
    
    if (cell == nil)
    {
        cell = [[IGPickerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"IGPickerCell"];
    }
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    
        [self loadTableViewPickerCell:cell forRow:indexPath.row];

    return cell;
    
}

- (void) loadTableViewPickerCell:(IGPickerCell*)cell forRow:(NSInteger)row
{

    NSMutableArray *pickerContent = [[NSMutableArray alloc]init];
    //NSInteger selectRow = -1;
    switch (row)
    {
        case TimeAppAccountCell:
        {
            for(int i = 0; i < [self.accounts count]; i++)
            {
                IGAccount *account = self.accounts[i];
                [pickerContent addObject:account.name];
            }
            [cell setDelegate:self];
            [cell setTag:ProjectsCell];
            [cell setSelectedTextColor:[UIColor lightGrayColor]];
            [cell setPickerTitle:AccountLabel];
            [cell initWithText:[timeSheetApprovalTypeHeadingArray objectAtIndex:row] pickerContents:pickerContent];
            
             [cell.pickerTextField setText:@"Select"];
           // [cell.pickerTextField setText:@"ankur"];
        }
            break;
            
        case TimeAppProjectTaskCell:
            
            for(int i = 0; i < [self.tracks count]; i++)
            {
                IGApprovalTrack *track = self.tracks[i];
                [pickerContent addObject:track.trackName];
            }
            [cell setDelegate:self];
           // [cell setTag:LocationCell];
            [cell setSelectedTextColor:[UIColor lightGrayColor]];
            [cell setPickerTitle:ProjectTask];
            [cell initWithText:[timeSheetApprovalTypeHeadingArray objectAtIndex:row] pickerContents:pickerContent];
            
            [cell.pickerTextField setText:[(IGApprovalTrack *)[self.tracks firstObject] trackName]];
            
            
            break;
            
        case TimeAppProjectTypeCell:
            
            for(int i = 0; i < [self.types count]; i++)
            {
                IGApprovalTimesheetType *type = self.types[i];
                [pickerContent addObject:type.type];
            }
            [cell setDelegate:self];
           // [cell setTag:HrsTypeCell];
            [cell setSelectedTextColor:[UIColor lightGrayColor]];
            [cell setPickerTitle:ProjectType];
            [cell initWithText:[timeSheetApprovalTypeHeadingArray objectAtIndex:row] pickerContents:pickerContent];
            [cell.pickerTextField setText:[(IGApprovalTimesheetType *)[self.types firstObject] type]];
            
            break;
            
        case TimeAppProjectYearCell:
            
            pickerContent = [NSMutableArray arrayWithArray:self.years];
            [cell setDelegate:self];
           // [cell setTag:HrsTypeCell];
            [cell setPickerTitle:ProjectYear];
            [cell setSelectedTextColor:[UIColor lightGrayColor]];
            [cell initWithText:[timeSheetApprovalTypeHeadingArray objectAtIndex:row] pickerContents:pickerContent];
            [cell.pickerTextField setText:@"Select"];
            
            break;
            
        case TimeAppProjectMonthCell:
            
            for(int i = 0; i < [self.months count]; i++)
            {
                IGApprovalMonth *month = self.months[i];
                [pickerContent addObject:month.name];
            }
            [cell setDelegate:self];
            //[cell setTag:HrsTypeCell];
            [cell setSelectedTextColor:[UIColor lightGrayColor]];
            [cell setPickerTitle:ProjectMonth];
            [cell initWithText:[timeSheetApprovalTypeHeadingArray objectAtIndex:row] pickerContents:pickerContent];
            [cell.pickerTextField setText:@"Select"];
            
            break;
            
        case TimeAppProjectDateCell:
            
            for(int i = 0; i < [self.weekendEndDate count]; i++)
            {
                IGApprovalWeekend *weekend = self.weekendEndDate[i];
                [pickerContent addObject:weekend.date];
            }
            [cell setDelegate:self];
            //[cell setTag:HrsTypeCell];
            [cell setSelectedTextColor:[UIColor lightGrayColor]];
            [cell setPickerTitle:WeekendEndDate];
            [cell initWithText:[timeSheetApprovalTypeHeadingArray objectAtIndex:row] pickerContents:pickerContent];
            [cell.pickerTextField setText:[(IGApprovalWeekend *)[self.weekendEndDate firstObject] date]];
            self.request.WeekID = [(IGApprovalWeekend *)[self.weekendEndDate firstObject] ID];
            break;
            
        default:
            break;
    }
}


#pragma mark - IGPickerCellDelegate

- (void) didSelectPickerObject:(IGPickerCell *)cell atIndex:(NSInteger)index value:(NSString *)object
{
    cell.pickerTextField.text =object;
    
    if ([cell.pickerTitle isEqualToString:@"Account"]){
        
        IGAccount *account = [self.accounts objectAtIndex:index];
        
        self.request.accountID = account.aID;
        
        [self getTracksWith:account.aID pmID:[IGUserDefaults sharedInstance].employee.staffID];
        
    }else if ([cell.pickerTitle isEqualToString:@"Type"]){
        //Code to handle type selection.
        
        IGApprovalTimesheetType *timeSheetType = [self.types objectAtIndex:index];
        
        selecetdStatusType = timeSheetType;
        
        self.request.statusID = timeSheetType.typeCode;
        
    }
    else if ([cell.pickerTitle isEqualToString:@"Project Task"]){
        
        IGApprovalTrack *track = [self.tracks objectAtIndex:index];
        
        
        if ([track.trackID isEqualToString:@"0"]) {
            NSMutableArray *stringArray = [NSMutableArray new];
            for (IGApprovalTrack *track in self.tracks) {
                [stringArray addObject:track.trackID];
            }
            
            self.request.projectID = [stringArray componentsJoinedByString:@","];
            
            stringArray = nil;
        }else{
             self.request.projectID = track.trackID;
        }
    }else{
         if ([cell.pickerTitle isEqualToString:@"Month"]){
            
            IGApprovalMonth *month = [self.months objectAtIndex:index];
             
            selectedMonth = month;
             
            if (selectedYear != nil) {
                
                [self getWeekendDates];
            }
             
             self.request.WeekID = nil;
        }
         else if([cell.pickerTitle isEqualToString:@"Year"]){
             
             NSString *year = [self.years objectAtIndex:index];
             
             selectedYear = year;
             
             self.weekendEndDate = nil;
             
             self.weekendEndDate = [NSArray new];
             
             self.request.WeekID = nil;
             
             [self getMonthsFor:year];
             
             
         }else{
            //Code to handle for weekend date
            
            IGApprovalWeekend *weekend = [self.weekendEndDate objectAtIndex:index];
            
            selecetdWeekend = weekend;
            
            self.request.WeekID = [weekend ID];
        }
    }
    
}

- (void) didOpenPickerView:(UIPickerView *)pickerView
{
    halfTransparentView = [[UIView alloc] initWithFrame:self.view.bounds];
    halfTransparentView.backgroundColor = [UIColor blackColor]; //or whatever...
    halfTransparentView.alpha = 0.5;
    [self.view addSubview: halfTransparentView];
}

- (void) didClosePickerView:(UIPickerView *)pickerView
{
    if(halfTransparentView)
    {
        [halfTransparentView removeFromSuperview];
    }
}


- (IBAction)showTimeAppBtnClked:(UIButton *)sender {
    
    [self validateRequest];
}


-(void)getAccountsFor:(NSString *)employeeID{
    
    [self showSpinner:YES];
    
    [[IGService sharedInstance] accounts:employeeID completion:^(id response) {
        // Code to parse Accounts
        [self showSpinner:NO];
        NSLog(@"Response is As %@", (NSDictionary *)response);
        IGApprovalParser *parser = [[IGApprovalParser alloc] init];
        self.accounts = [parser parseAccountsWith:(NSDictionary *)response];
        
        if (self.accounts.count > 0) {
            [self.timeSheetApprovalTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:_accountIndexPath_, nil] withRowAnimation:UITableViewRowAnimationNone];
        }
        
    } error:^(NSError *error) {
        // code to handle errors
         [self showSpinner:NO];
    }];
}

-(void)getMonthsFor:(NSString *)year{
    
    [self showSpinner:YES];
    
    [[IGService sharedInstance] months:year completion:^(id response) {
        //
        [self showSpinner:NO];
        IGApprovalTracks *tracks = [[IGApprovalTracks alloc] init];
        self.months = [tracks parseMonths:[(NSDictionary *)response valueForKey:@"Month"]];
        
        [self.timeSheetApprovalTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:_monthIndexPath_,_weekendIndexPath_, nil] withRowAnimation:UITableViewRowAnimationNone];
        
    } error:^(NSError *error) {
        //
        [self showSpinner:NO];
        
    }];
    
}

-(void)getTracksWith:(NSString *)accountID pmID:(NSString *)pmID{
    [self showSpinner:YES];
    
    NSDate *date = [NSDate date];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY"];
    
    
    [[IGService sharedInstance] tracksWith:accountID pmID:pmID year:[dateFormatter stringFromDate:date] completion:^(id response) {
        //Code to handle Futher processing
        [self showSpinner:NO];
        
        IGApprovalParser *parser = [[IGApprovalParser alloc] init];
        
        IGApprovalTracks *tracks = [parser parseTracksWith:(NSDictionary *)response];
        
        if (tracks) {
            self.tracks = tracks.tracks;
            self.years = tracks.years;
            self.months = tracks.months;
            self.types = tracks.timesheetTypes;
            
            selecetdStatusType = [self.types firstObject];
            
            NSMutableArray *stringArray = [NSMutableArray new];

            for (IGApprovalTrack *track in self.tracks) {
                [stringArray addObject:track.trackID];
            }
            
            self.request.projectID = [stringArray componentsJoinedByString:@","];

            
            [self.timeSheetApprovalTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:_trackIndexPath_,_typeIndexPath_,_yearIndexPath_, nil] withRowAnimation:UITableViewRowAnimationNone];
        }
    } error:^(NSError *error) {
        //Code to handle error
        [self showSpinner:NO];
        
    }];
    
    dateFormatter = nil;
    date = nil;
}

-(void)getWeekendDates{
    
    [self showSpinner:YES];
    
    [[IGService sharedInstance] weekEndDates:selectedYear
                                       month:[NSString stringWithFormat:@"%ld",selectedMonth.value]
                                  completion:^(id response) {
                                      //Code to hande further processing
                                      [self showSpinner:NO];
                                      IGApprovalParser *parser = [[IGApprovalParser alloc] init];
                                      
                                      self.weekendEndDate = [parser parseWeekendDates:(NSDictionary *)response];
                                      
                                      selecetdWeekend = (IGApprovalWeekend *)[self.weekendEndDate firstObject] ;
                                      
                                      [self.timeSheetApprovalTable reloadRowsAtIndexPaths:[NSArray arrayWithObjects:_weekendIndexPath_, nil] withRowAnimation:UITableViewRowAnimationNone];
                                      
                                  } error:^(NSError *error) {
                                      //Code to handle error
                                      [self showSpinner:NO];
                                  }];
}


-(void)validateRequest{
    if (self.request) {
        
        if (self.request.projectID == nil) {
            [IGUtils showOKAlertWithTitle:@"Selection error" message:@"Select a project track."];
        }else if (self.request.accountID == nil){
            [IGUtils showOKAlertWithTitle:@"Selection error" message:@"Select an account."];
        }
        else if (self.request.WeekID == nil){
            [IGUtils showOKAlertWithTitle:@"Selection error" message:@"Select a weekend date."];
        }else if (self.request.statusID == nil){
            
            [IGUtils showOKAlertWithTitle:@"Selection error" message:@"Select type of timesheets to display."];
            
        }else{
            
            [self showSpinner:YES];
            
            [[IGService sharedInstance] timesheets:self.request completion:^(id response) {
                //Code to handle further processing
                [self showSpinner:NO];
                if (response) {
                    IGApprovalParser *parser = [[IGApprovalParser alloc] init];
                    
                    if ([selecetdStatusType.typeCode  isEqualToString: @"A"] || [selecetdStatusType.typeCode  isEqualToString: @"C"]) {
                        self.timesheets = [parser parseTimesheets:(NSDictionary *)response];
                    }
                    else{
                        self.timesheets = [parser parseNonExistingimeSheets:(NSDictionary *)response];
                    }
                    [self performSegueWithIdentifier:kApprovalShowTimeSheetSegue sender:self];
                    parser = nil;
                }
            } error:^(NSError *error) {
                //Code to handle error
                [self showSpinner:NO];
            }];
        }
    }
    else{
        [IGUtils showOKAlertWithTitle:@"Selection error" message:@"Select account and other details."];
    }
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:kApprovalShowTimeSheetSegue]) {

        IGTimesheetApprovalListVC *timeSheetsVC = (IGTimesheetApprovalListVC *)[segue destinationViewController];
        
        timeSheetsVC.selectedWeekend = selecetdWeekend;
        timeSheetsVC.timeSheetType = selecetdStatusType;
        
        timeSheetsVC.timeSheets = [[NSMutableArray alloc] initWithArray:self.timesheets];
    }
}
@end
