//
//  IGTimesheetApprovalListVC.h
//  IGTimeSheet
//
//  Created by Anjali on 05/11/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IGViewController.h"
@class IGApprovalTimesheetType;
@class IGApprovalWeekend;
@interface IGTimesheetApprovalListVC : IGViewController
@property (nonatomic) NSMutableArray *timeSheets;
@property (nonatomic) IGApprovalWeekend *selectedWeekend;
@property (nonatomic) IGApprovalTimesheetType *timeSheetType;
@end
