//
//  IGTimesheetApproval.h
//  IGTimeSheet
//
//  Created by Anjali on 05/11/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGTimesheetApproval : NSObject{
    
}

@property(nonatomic)NSInteger section ;
@property(nonatomic)Boolean isVisible;
@property(nonatomic)NSInteger rows;
@property(strong , nonatomic)NSString  *week;
@property(strong , nonatomic)NSString  *weekDate;
@property(strong , nonatomic)NSArray  *users;
@end
