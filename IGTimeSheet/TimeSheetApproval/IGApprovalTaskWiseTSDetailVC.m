//
//  IGApprovalTaskWiseTSDetailVC.m
//  IGTimeSheet
//
//  Created by Saurabh on 06/01/17.
//  Copyright © 2017 InfoGain. All rights reserved.
//

#import "IGApprovalTaskWiseTSDetailVC.h"
#import "IGAddTaskMacros.h"
#import "IGWFOLSwitchCell.h"
#import "IGTaskDescriptionCell.h"
#import "IGTotalHrsCell.h"
#import "IGTaskHrsCell.h"
#import "IGUtils.h"
#import "IGPickerCell.h"
#import "IGMacro.h"



#define kCellReuseIdentifierA @"AddTaskCellA"
#define kCellReuseIdentifierB @"AddTaskCellB"
#define kCellReuseIdentifierC @"AddTaskCellC"
#define kCellReuseIdentifierD @"AddTaskCellD"
#define kCellReuseIdentifierE @"AddTaskCellE"

#define kHeaderCellReuseIdentifierA @"AddTaskHeaderCellA"
#define kHeaderCellReuseIdentifierB @"AddTaskHeaderCellB"
#define kFooterCellReuseIdentifier @"AddTaskFooterCell"

#define DAY_MONDAY      0
#define DAY_TUESDAY     1
#define DAY_WEDNESSDAY  2
#define DAY_THURSDAY    3
#define DAY_FRIDAY      4
#define DAY_SATURDAY    5
#define DAY_SUBDAY      6

@interface IGApprovalTaskWiseTSDetailVC ()<UITableViewDelegate,UITableViewDataSource,IGPickerCellDelegate>{
    int weekendDateIndex;
    int invalidDayCount;
    
    
}

@property (nonatomic) IGTimeSheetDetail *tsDetail;
@property (weak, nonatomic) IBOutlet UITableView *tblTSDetail;
@property (strong) NSArray* weekDates;

@end

@implementation IGApprovalTaskWiseTSDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self setupBackButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)initializeDataWith:(IGTimeSheetDetail *)timeSheetDetail weekendDate:(NSString *)weekendDate{
    self.tsDetail = timeSheetDetail;
    [self getWeekDates:[IGUtils getDateFromString:weekendDate]];
    self.navigationItem.title = weekendDate;
    [self.tblTSDetail reloadData];
}

#pragma mark - UITableViewData Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    if(section == TaskDetailSection)
    {
        return 10;// add row- task type and task category for US employees
    }
    else if(section == TaskHrsSection)
    {
        return 8;
    }
    
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = nil;
    
    if(indexPath.section == TaskDetailSection)
    {
        if(indexPath.row == LocationSwitchRow)
        {
            IGWFOLSwitchCell* wfolCell = [tableView dequeueReusableCellWithIdentifier:kHeaderCellReuseIdentifierA];
            if (wfolCell == nil)
            {
                
                wfolCell = [[IGWFOLSwitchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kHeaderCellReuseIdentifierA];
            }
            [wfolCell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [wfolCell.woflSwitch setOn:self.tsDetail.isWFOL];
            wfolCell.woflSwitch.userInteractionEnabled = false;
            [self setSeperatorInsetZero:wfolCell];
            if(self.tsDetail.isUSRecord)
            {
                wfolCell.hidden = true;
            }
            cell = wfolCell;
        }
        else if(indexPath.row == DescriptionRow)
        {
            IGTaskDescriptionCell* taskDescriptionCell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifierB];
            if (taskDescriptionCell == nil)
            {
                
                taskDescriptionCell = [[IGTaskDescriptionCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kCellReuseIdentifierB];
            }
            
            [taskDescriptionCell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [taskDescriptionCell.descriptionLabel setText:self.tsDetail.tsDescription];
            cell = taskDescriptionCell;
//
            
        }else{
            IGPickerCell* cell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifierA];
            if (cell == nil)
            {
                cell = [[IGPickerCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellReuseIdentifierA];
                
            }
            [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [self loadTableViewPickerCell:cell forRow:indexPath.row];
            if(indexPath.row == kTaskTypeCellForUS || indexPath.row == kTaskCategoryCellForUS)
            {
                if(!self.tsDetail.isUSRecord)
                {
                    cell.hidden = true;
                }
            }
            return cell;

        }
    }else{
        if(indexPath.row == TotalHrsRow)
        {
            IGTotalHrsCell* totalHrsCell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifierD];
            if (totalHrsCell == nil) {
                totalHrsCell = [[IGTotalHrsCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kCellReuseIdentifierD];
                
            }
            [totalHrsCell setSelectionStyle:UITableViewCellSelectionStyleNone];
            [self setSeperatorInsetZero:cell];
            totalHrsCell.totalHrsLabel.text = [NSString stringWithFormat:@"%02.02f",self.tsDetail.efforts];
            cell = totalHrsCell;
        }
        else{
            IGTaskHrsCell* taskHoursCell = [tableView dequeueReusableCellWithIdentifier:kCellReuseIdentifierC];
            if (taskHoursCell == nil) {
                
                taskHoursCell = [[IGTaskHrsCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:kCellReuseIdentifierC];
            }
            [taskHoursCell setSelectionStyle:UITableViewCellSelectionStyleNone];
            taskHoursCell.dateLabel.text = [self.weekDates objectAtIndex:indexPath.row];
            [self setHrsTextField:taskHoursCell.hrsTextField forWeekday:indexPath.row];
            cell = taskHoursCell;
        }
    }
    
    return cell;
}


- (void) setSeperatorInsetZero: (UITableViewCell*)cell
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        cell.layoutMargins = UIEdgeInsetsZero;
    }
    
    if ([cell respondsToSelector:@selector(setPreservesSuperviewLayoutMargins:)]) {
        cell.preservesSuperviewLayoutMargins = false;
    }
}


#pragma mark - UITableVIew Delegate

- (UIView*)tableView:(UITableView*)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view;
    
    if(section == TaskDetailSection)
    {
        view = [[UIView alloc]initWithFrame: CGRectZero];
    }
    else
    {
        UITableViewCell* headerCell = [tableView dequeueReusableCellWithIdentifier:kHeaderCellReuseIdentifierB];
        [self setSeperatorInsetZero:headerCell];
        view = [headerCell contentView];
        [view setFrame:CGRectMake(0.0, 0.0, tableView.bounds.size.width, kIGAddTaskSectionHeight)];
    }
    
    return view;
    
}


- (CGFloat)tableView:(UITableView*)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == TaskHrsSection)
    {
        return kIGAddTaskSectionHeight;
    }
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.section == TaskDetailSection)
    {
        if(indexPath.row == LocationSwitchRow)
        {
            if(self.tsDetail.isUSRecord)
            {
                return 0;
            }
            
        }
        else if(indexPath.row == kTaskTypeCellForUS || indexPath.row == kTaskCategoryCellForUS)
        {
            if(!self.tsDetail.isUSRecord)
            {
                return 0;
            }
        }
        else if(indexPath.row == DescriptionRow)
        {
            
            NSString *cellText = self.tsDetail.tsDescription;
            CGSize constraintSize = CGSizeMake(280.0f, MAXFLOAT);
            CGSize labelSize = [cellText boundingRectWithSize:constraintSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:17.0]} context:nil].size;
            return labelSize.height + 50;
        }
        else if (indexPath.row == 0)
        {
            return CustomRowHeight;
        }
        
    }
    
    if(indexPath.row == TotalHrsRow)
    {
        return 55;
    }
    return DefaultRowHeight;
    
}


- (void) getWeekDates :(NSDate*)weekendDate
{
    NSMutableArray* dates = [[NSMutableArray alloc]init];
    NSDate *startDate = [self getPreviousMonday:weekendDate];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:@"dd MMM, eee"];
    
    for(int i = 0; i < 7; i++)
    {
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = i;
        
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:startDate options:0];
        
        if([nextDate isEqualToDate:weekendDate])
        {
            weekendDateIndex = i;
        }
        
        NSString *dateString = [dateFormatter stringFromDate:nextDate];
        [dates addObject:dateString];
    }
    
    invalidDayCount = [self getInvalidDayCount:weekendDate];
    
    self.weekDates = dates;
}


- (int) getInvalidDayCount: (NSDate*)weekendDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:@"dd"];
    NSString* dateStr = [dateFormatter stringFromDate:weekendDate];
    
    int dayCount = 0;
    
    if([dateStr intValue] < 7)
    {
        dayCount = 7 - [dateStr intValue];
    }
    
    return dayCount;
}

- (NSDate*) getPreviousMonday:(NSDate*)weekendDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:@"eee"];
    NSString* dayStr = [dateFormatter stringFromDate:weekendDate];
    
    int dayNum = 0;
    
    if([dayStr isEqualToString:@"Tue"])
    {
        dayNum = -1;
    }
    else if([dayStr isEqualToString:@"Wed"])
    {
        dayNum = -2;
    }
    else if([dayStr isEqualToString:@"Thu"])
    {
        dayNum = -3;
    }
    else if([dayStr isEqualToString:@"Fri"])
    {
        dayNum = -4;
    }
    else if([dayStr isEqualToString:@"Sat"])
    {
        dayNum = -5;
    }
    else if([dayStr isEqualToString:@"Sun"])
    {
        dayNum = -6;
    }
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = dayNum;
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *startDate = [theCalendar dateByAddingComponents:dayComponent toDate:weekendDate options:0];
    
    return startDate;
}


- (void)setHrsTextField: (UITextField*)textF forWeekday: (NSInteger)day
{
    switch(day)
    {
        case Weekday1:
            [textF setText:[NSString stringWithFormat:@"%02.02f",self.tsDetail.monday]];
            break;
            
        case Weekday2:
            [textF setText:[NSString stringWithFormat:@"%02.02f",self.tsDetail.tuesday]];
            break;
            
        case Weekday3:
            [textF setText:[NSString stringWithFormat:@"%02.02f",self.tsDetail.wednessday]];
            break;
            
        case Weekday4:
            [textF setText:[NSString stringWithFormat:@"%02.02f",self.tsDetail.thursday]];
            break;
            
        case Weekday5:
            [textF setText:[NSString stringWithFormat:@"%02.02f",self.tsDetail.friday]];
            break;
            
        case Weekday6:
            [textF setText:[NSString stringWithFormat:@"%02.02f",self.tsDetail.saturday]];
            break;
            
        case Weekday7:
            [textF setText:[NSString stringWithFormat:@"%02.02f",self.tsDetail.sunday]];
            break;
            
        default:
            
            break;
    }
}



- (void) loadTableViewPickerCell:(IGPickerCell*)cell forRow:(NSInteger)row
{
    NSMutableArray *pickerContent = [[NSMutableArray alloc]init];
    switch (row)
    {
        case ProjectsCell:
            
            [cell setDelegate:self];
            [cell setTag:ProjectsCell];
            [cell setSelectedTextColor:kColorLoginVCFontColor];
            [cell setPickerTitle:ProjectLabel];
            [cell initWithText:ProjectLabel pickerContents:pickerContent];
            
            [cell.pickerTextField setText:self.tsDetail.projectName];
            
            break;
            
        case LocationCell:
            
            [cell setDelegate:self];
            [cell setTag:LocationCell];
            [cell setPickerTitle:LocationLabel];
            [cell initWithText:LocationLabel pickerContents:pickerContent];
            cell.pickerTextField.text = self.tsDetail.location;
            
            break;
            
        case kTaskTypeCellForUS:
            
            [cell setDelegate:self];
            [cell setTag:kTaskTypeCellForUS];
            [cell setPickerTitle:kTaskTypeLabelForUS];
            
            [cell initWithText:kTaskTypeLabelForUS pickerContents:pickerContent];
            if(self.tsDetail.billable)
               cell.pickerTextField.text = @"Billable";
            else
                cell.pickerTextField.text = @"Non-Billable";
            break;
            
        case kTaskCategoryCellForUS:
            
            [cell setDelegate:self];
            [cell setTag:kTaskCategoryCellForUS];
            [cell setPickerTitle:kTaskCategoryLabelForUS];
            [cell initWithText:kTaskCategoryLabelForUS pickerContents:pickerContent];
            
                cell.pickerTextField.text = self.tsDetail.taskCategory;
            
            break;
            
            
        case HrsTypeCell:
            
            [cell setDelegate:self];
            [cell setTag:HrsTypeCell];
            [cell setPickerTitle:HrsTypeLabel];
            [cell initWithText:HrsTypeLabel pickerContents:pickerContent];
            [cell.pickerTextField setText:self.tsDetail.type];
            
            break;
            
        case TaskCell:
            [cell setDelegate:self];
            [cell setTag:TaskCell];
            [cell setPickerTitle:TaskLabel];
            [cell initWithText:TaskLabel pickerContents:pickerContent];
            
                [cell.pickerTextField setText:self.tsDetail.taskName];
            
            break;
            
        case SubtaskCell:
            [cell setDelegate:self];
            [cell setTag:SubtaskCell];
            [cell setPickerTitle:SubtaskLabel];
            [cell initWithText:SubtaskLabel pickerContents:pickerContent];
            
            [cell.pickerTextField setText:self.tsDetail.subTaskName];
            
            break;
            
        case CRIDCell:
            [cell setDelegate:self];
            [cell setTag:CRIDCell];
            [cell setPickerTitle:CRIdLabel];
            [cell initWithText:CRIdLabel pickerContents:pickerContent];

            [cell.pickerTextField setText:self.tsDetail.crName];
            
            break;
        default:
            break;
    }
}


@end
