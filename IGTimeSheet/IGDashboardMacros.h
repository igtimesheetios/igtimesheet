//
//  IGDashboardMacros.h
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#ifndef IGTimeSheet_IGDashboardMacros_h
#define IGTimeSheet_IGDashboardMacros_h


#define kTableViewCellType1ReuseIdentifier                      @"DashBoardCellType1"
#define kTableViewCellType2ReuseIdentifier                      @"DashBoardCellType2"
#define kTableViewCellType3ReuseIdentifier                      @"DashBoardCellType3"


//Types
//Missing TimeSheet
#define MTimeSheet  0
//In Progress TimeSheet
#define PTimeSheet  1
//Current Time sheet
#define CTimeSheet  2

//Headers
#define DashBoardTitle  @"Dashboard"
#define MTimeSheetTitle @"Missing Timesheet"
#define PTimeSheetTitle @"In-Progress Timesheet"
#define CTimeSheetTitle @"Enter Timesheet"

//Icons
#define MTimeSheetIcon  @"missing-timesheet-icon"
#define PTimeSheetIcon  @"inprogress-timesheet-icon"
#define CTimesheetIcon  @"enter-timesheet-icon"

//Keys
#define PTimeSheetKey   @"IN PROGRESS"
#define MTimeSheetKey   @"NO TIMECARD"

//Seprator
#define TimeSheetSeperator @", "

#endif
