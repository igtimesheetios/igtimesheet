//
//  IGUtils.m
//  IGTimeSheet
//
//  Created by Manish on 19/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGUtils.h"
#import "IGMacro.h"
#import "igTSmoothAlertView.h"

@implementation IGUtils

+ (BOOL)isValidString:(NSString *)string
{
    if (string == nil)
    {
        return NO;
    }
    
    if ([string respondsToSelector:@selector(length)] && string.length == 0)
    {
        return NO;
    }
    
    if ([string respondsToSelector:@selector(stringByTrimmingCharactersInSet:)])
    {
        if ([string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]].length == 0)
        {
            return NO;
        }
    }
    
    if ([string respondsToSelector:@selector(isEqualToString:)])
    {
        if ([string isEqualToString:@"(null)"] ||
            [string isEqualToString:@"<null>"])
        {
            return NO;
        }
    }
    else
    {
        return NO;
    }
    
    return YES;
}

+ (BOOL)isValidNumber:(NSNumber *)number
{
    if (number == nil)
    {
        return NO;
    }
    
    if ([number isKindOfClass:[NSNumber class]] == NO)
    {
        return NO;
    }
    
    return YES;
}

+ (BOOL)isArray:(NSObject *)object
{
    if (object != nil)
    {
        if ([object isKindOfClass:[NSArray class]] || [object respondsToSelector:@selector(objectAtIndex:)])
        {
            return YES;
        }
    }
    
    return NO;
}

+ (BOOL)isDictionary:(NSObject *)object
{
    if (object != nil)
    {
        if ([object isKindOfClass:[NSDictionary class]] || [object respondsToSelector:@selector(objectForKey:)])
        {
            return YES;
        }
    }
    
    return NO;
}

+ (NSString *)stringFromDate:(NSDate *)date dateFormat:(NSString *)dateFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = dateFormat;
    dateFormatter.timeZone = [NSTimeZone defaultTimeZone];
    return [dateFormatter stringFromDate:date];
}

+ (NSDate *)dateFromString:(NSString *)dateString dateFormat:(NSString *)dateFormat
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = dateFormat;
    dateFormatter.timeZone = [NSTimeZone systemTimeZone];
    return [dateFormatter dateFromString:dateString];
}

+ (void)showOKAlertWithTitle:(NSString *)title message:(NSString *)message
{
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:title
//                                                        message:message
//                                                       delegate:nil
//                                              cancelButtonTitle:kStringOK
//                                              otherButtonTitles:nil];
//    [alertView show];
    
    igTSmoothAlertView *alert = [[igTSmoothAlertView alloc] initDropAlertWithTitle:title
                                                                           andText:message
                                                                   andCancelButton:NO
                                                                          andColor:kDefaultBarTintColor
                                                                          andImage:[UIImage imageNamed:@"infogain-logo-circle"]];
    [alert show];
    
}

+ (void)showOKAlertWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate tag:(NSInteger)tag
{
    igTSmoothAlertView *alert = [[igTSmoothAlertView alloc] initDropAlertWithTitle:title
                                                                           andText:message
                                                                   andCancelButton:NO
                                                                          andColor:kDefaultBarTintColor
                                                                          andImage:[UIImage imageNamed:kImageInfogainLogoCircle]];
    alert.tag = tag;
    alert.delegate = delegate;
    [alert show];
    
}

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message cancelButton:(BOOL)hasCancelButton defaultButtonTitle:(NSString *)defaultButtonTitle delegate:(id)delegate tag:(NSInteger)tag
{
    igTSmoothAlertView *alert = [[igTSmoothAlertView alloc] initDropAlertWithTitle:title
                                                                           andText:message
                                                                   andCancelButton:hasCancelButton
                                                                          andColor:kDefaultBarTintColor
                                                                          andImage:[UIImage imageNamed:kImageInfogainLogoCircle]];
    [alert.defaultButton setTitle:defaultButtonTitle forState:UIControlStateNormal];
    alert.tag = tag;
    alert.delegate = delegate;
    [alert show];
}


+ (void)underImplementationAlert
{
    [self showOKAlertWithTitle:kStringAlert message:@"Under Implementation"];
}

+ (void)roundCorner:(UIRectCorner)corner radius:(CGFloat)radius forView:(UIView *)view
{
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:view.bounds
                                                   byRoundingCorners:corner
                                                         cornerRadii:CGSizeMake(radius, radius)];
    CAShapeLayer *maskLayer = [CAShapeLayer layer];
    maskLayer.frame = view.bounds;
    maskLayer.path = maskPath.CGPath;
    view.layer.mask = maskLayer;
}

+ (NSString *)jsonStringFromObject:(id)object
{
    if (object)
    {
        NSData *data = [NSJSONSerialization dataWithJSONObject:object options:NSJSONWritingPrettyPrinted error:nil];
        if (data)
        {
            NSString *jsonString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            if (jsonString)
            {
                return jsonString;
            }
        }
    }
    
    return @"";
}

+ (void)setupButton:(UIButton *)button enable:(BOOL)enable backgroundColor:(UIColor *)backgroundColor;
{
    button.enabled = enable;
    
    if (enable)
    {
        button.backgroundColor = backgroundColor;
        [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    }
    else
    {
        button.backgroundColor = kColorButtonGray;
        [button setTitleColor:kColorButtonFontColor forState:UIControlStateNormal];
    }
}

+ (NSString *)bundleVersion
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
}

+ (UIImage *)imageFromView:(UIView *)view
{
    UIImage *image = nil;
    UIGraphicsBeginImageContext(view.bounds.size);
    BOOL success = [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:NO];
    if (success) {
        image = UIGraphicsGetImageFromCurrentImageContext();
    }
    UIGraphicsEndImageContext();
    return image;
}

+ (BOOL)openURLWithString:(NSString *)urlString
{
    if ([IGUtils isValidString:urlString])
    {
        NSURL *url = [NSURL URLWithString:urlString];
        
        if ([[UIApplication sharedApplication] canOpenURL:url])
        {
            [[UIApplication sharedApplication] openURL:url];
            return YES;
        }
        else
        {
            NSLog(@"Cannot not open URL %@", url);
        }
    }
    else
    {
        NSLog(@"%s, URL String not valid %@", __FUNCTION__, urlString);
    }
    
    return NO;
}

+ (UIColor *)averageColorFromImage:(UIImage *)paramImage
{
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    unsigned char rgba[4];
    CGContextRef context = CGBitmapContextCreate(rgba, 1, 1, 8, 4, colorSpace, kCGImageAlphaPremultipliedLast | kCGBitmapByteOrder32Big);
    
    CGContextDrawImage(context, CGRectMake(0, 0, 1, 1), paramImage.CGImage);
    CGColorSpaceRelease(colorSpace);
    CGContextRelease(context);
    
    if(rgba[3] > 0) {
        CGFloat alpha = ((CGFloat)rgba[3])/255.0;
        CGFloat multiplier = alpha/255.0;
        return [UIColor colorWithRed:((CGFloat)rgba[0])*multiplier
                               green:((CGFloat)rgba[1])*multiplier
                                blue:((CGFloat)rgba[2])*multiplier
                               alpha:alpha];
    }
    else {
        return [UIColor colorWithRed:((CGFloat)rgba[0])/255.0
                               green:((CGFloat)rgba[1])/255.0
                                blue:((CGFloat)rgba[2])/255.0
                               alpha:((CGFloat)rgba[3])/255.0];
    }
}

+ (NSDate *)getDateFromString:(NSString *)dateString
{
    if([dateString rangeOfString:@"Jan"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Jan" withString:@"01"];
    }
    else if([dateString rangeOfString:@"Feb"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Feb" withString:@"02"];
    }
    else if([dateString rangeOfString:@"Mar"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Mar" withString:@"03"];
    }
    else if([dateString rangeOfString:@"Apr"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Apr" withString:@"04"];
    }
    else if([dateString rangeOfString:@"May"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"May" withString:@"05"];
    }
    else if([dateString rangeOfString:@"Jun"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Jun" withString:@"06"];
    }
    else if([dateString rangeOfString:@"Jul"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Jul" withString:@"07"];
    }
    else if([dateString rangeOfString:@"Aug"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Aug" withString:@"08"];
    }
    else if([dateString rangeOfString:@"Sep"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Sep" withString:@"09"];
    }
    else if([dateString rangeOfString:@"Oct"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Oct" withString:@"10"];
    }
    else if([dateString rangeOfString:@"Nov"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Nov" withString:@"11"];
    }
    else if([dateString rangeOfString:@"Dec"].length)
    {
        dateString = [dateString stringByReplacingOccurrencesOfString:@"Dec" withString:@"12"];
        
    }
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd MM yyyy";
    NSDate *date = [dateFormatter dateFromString:dateString];
    return date;
}



@end
