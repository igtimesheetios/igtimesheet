//
//  DashboardObjectModel.m
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGDashboardObjectModel.h"

@implementation IGDashboardObjectModel

@synthesize value, type;

- (id)init
{
    self = [super init];
    if(self)
    {
        value = @"";
        type = -1;
    }
    
    return self;
}

@end
