//
//  IGHoursSummary.h
//  IGTimeSheet
//
//  Created by Manish on 18/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IGMonthlyHour;

@interface IGHourSummary : NSObject

@property (nonatomic, strong) NSArray *weeklyHours;
@property (nonatomic, strong) IGMonthlyHour *monthlyHour;

- (instancetype)initWithDictionary:(NSDictionary *)hoursInfo;

@end