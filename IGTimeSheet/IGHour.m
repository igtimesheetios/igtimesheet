//
//  IGHour.m
//  IGTimeSheet
//
//  Created by Manish on 19/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGHour.h"
#import "IGUtils.h"

#define kNoStringValue              @"-"
#define kNoNumberValue              @0


@implementation IGHour

- (void)setTotalHours:(NSNumber *)totalHours
{
    if ([totalHours isKindOfClass:[NSNumber class]])
    {
        _totalHours = totalHours;
    }
    else
    {
        _totalHours = kNoNumberValue;
    }
}

- (void)setNumberOfDays:(NSString *)numberOfDays
{
    if ([IGUtils isValidString:numberOfDays])
    {
        _numberOfDays = numberOfDays;
    }
    else
    {
        _numberOfDays = kNoStringValue;
    }
}

- (void)setWeeklyAverage:(NSNumber *)weeklyAverage
{
    if ([weeklyAverage isKindOfClass:[NSNumber class]])
    {
        _weeklyAverage = weeklyAverage;
    }
    else
    {
        _weeklyAverage = kNoNumberValue;
    }
}


- (NSString *)description
{
    return [NSString stringWithFormat:@"\rTotalHours:%@"
            "\rNumberOfDays:%@"
            "\rWeeklyAverage:%@",
            _totalHours, _numberOfDays, _weeklyAverage];
}

@end


@implementation IGWeeklyHour

- (void)setWeek:(NSString *)week
{
    if ([IGUtils isValidString:week])
    {
        _week = week;
    }
    else
    {
        _week = kNoStringValue;
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ \rWeek:%@",
            [super description], _week];
}

@end


@implementation IGMonthlyHour

- (void)setTotalHoursAverage:(NSNumber *)totalHoursAverage
{
    if ([totalHoursAverage isKindOfClass:[NSNumber class]])
    {
        _totalHoursAverage = totalHoursAverage;
    }
    else
    {
        _totalHoursAverage = kNoNumberValue;
    }
}

- (NSString *)description
{
    return [NSString stringWithFormat:@"%@ \rTotalHoursAverage:%@",
            [super description], _totalHoursAverage];
}

@end
