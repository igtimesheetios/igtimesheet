//
//  TaskModalClass.h
//  TaskView
//
//  Created by Hem on 10/02/15.
//  Copyright (c) 2015 Hem. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGTaskModalClass : NSObject

@property(strong) NSString* dateText;
@property(strong) NSString* projectText;
@property(strong) NSString* description;
@property(strong) NSString* location;
@property(strong) NSString* categoryType  ;  // for US employees only
@property(atomic) NSInteger timeSheetStatus;

@property(atomic) NSInteger prjId;
@property(atomic) NSInteger taskId;
@property(atomic) NSInteger categoryID  ;  // for US employees only
@property(atomic) NSInteger subTaskId;
@property(atomic) NSInteger wfol;
@property(atomic)NSInteger isUSEmployeeBillable;// For non billable  US employees

@property(atomic) float totalTime;
@property(atomic) float monTime;
@property(atomic) float tueTime;
@property(atomic) float wedTime;
@property(atomic) float thuTime;
@property(atomic) float friTime;
@property(atomic) float satTime;
@property(atomic) float sunTime;
@property(strong) NSString* hrsType;
@property(strong) NSString* billable;

@property(atomic) NSInteger cardEntryDetailId;
@property(atomic) NSString* cRID;
@property(atomic) NSInteger payrollTaskId;

@property(atomic)NSMutableArray *validDays;


@end
