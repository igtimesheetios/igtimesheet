//
//  IGInOutTimeCell.h
//  IGTimeSheet
//
//  Created by Manish on 13/05/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGInOutTimeCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelDay;
@property (weak, nonatomic) IBOutlet UILabel *labelTime;

@end
