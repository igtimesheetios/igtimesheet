//
//  IGDayWiseTaskDetailVC.m
//  IGTimeSheet
//
//  Created by Rajat on 27/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGTaskDetailVC.h"
#import "IGService.h"
#import "DateSelectionViewController.h"
#import "AppDelegate.h"
#import "IGMacro.h"
#import "IGTaskDetailModel.h"
#import "IGTaskDetail.h"
#import "IGUtils.h"

@interface IGTaskDetailVC ()

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) IGTaskDetailModel *taskDetailModel;

//@property NSArray * dayWiseViewTask;
@end


@implementation IGTaskDetailVC
IGTaskDetail *taskDetail;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setupBackButton];
    [self setupOpaqueNavigationBar];
    
    self.automaticallyAdjustsScrollViewInsets = YES;
    self.navigationItem.title =@"View Task";
    [self.navigationController.navigationBar setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIColor whiteColor] forKey:NSForegroundColorAttributeName]];
    //    self.dayWiseViewTask = @[@"a",@"b",@"c"];
    
    [self fetchTaskDetails];
    
}
- (void)fetchTaskDetails
{
    if ([[IGService sharedInstance] networkReachabilityAlert] == YES)
    {
        self.tableView.hidden = YES;
        self.noTaskViewLabel.text = @"Error in getting Task Record";
        return;
    }

    [self showSpinner:YES];
    [[IGService sharedInstance] viewTaskForEmployee:_employeeIDString
                                             weekID:_weekID
                                              order:_order
                                         completion:^(id response)
     {
         _taskDetailModel = [[IGTaskDetailModel alloc] initWithDictionary:response];
         NSLog(@"%@", _taskDetailModel.taskDetails);
         if (_taskDetailModel)
         {
             [self.tableView reloadData];
         }
         else
         {
             self.tableView.hidden = YES;
             self.noTaskViewLabel.text = @"No Record Found";
         }
         [self showSpinner:NO];
     }
                                              error:^(NSError *error)
     {
         [IGUtils showOKAlertWithTitle:@"Error"
                               message:[error localizedDescription]];
         [self showSpinner:NO];
         self.tableView.hidden = YES;
         self.noTaskViewLabel.text = @"Error getting Task Record";

     }];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.taskDetailModel.taskDetails.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 7;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    taskDetail = _taskDetailModel.taskDetails[indexPath.section];
    
    if(indexPath.row == 5)
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"IGTaskCell1" forIndexPath:indexPath];
        
        UILabel *keyLabel1 = (UILabel *)[cell viewWithTag:121];
        UILabel *valueLabel1 = (UILabel *)[cell viewWithTag:122];
        
        
        NSAssert([keyLabel1 isKindOfClass:[UILabel class]], @"Expected a UILabel for tag 121");
        NSAssert([valueLabel1 isKindOfClass:[UILabel class]], @"Expected a UILabel for tag 122");
        
        
        
        keyLabel1.text= @"Description";
        valueLabel1.text=taskDetail.taskDescription;
        valueLabel1.lineBreakMode = NSLineBreakByWordWrapping;
        valueLabel1.numberOfLines = 0;
        //  valueLabel1.text = @"fdfgjhgfgiurhriuthtiueghjgheiughtegjheghejgejhgfhgffyruyirergwiuyrgwuigrwughfgwhfgdshjfgsdhjfgurwuirhyehdjfgsdhjfsjdhfgdsfuiryiurghwgrjhghjrgjkrrhkurhgeywrgywurgywrwghgfhdf";
        
    }
    
    else
    {
        cell = [tableView dequeueReusableCellWithIdentifier:@"IGTaskCell" forIndexPath:indexPath];
        
        
        UILabel *keyLabel = (UILabel *)[cell viewWithTag:1000];
        UILabel *valueLabel = (UILabel *)[cell viewWithTag:1001];
        
        
        
        NSAssert([keyLabel isKindOfClass:[UILabel class]], @"Expected a UILabel for tag 1000");
        NSAssert([valueLabel isKindOfClass:[UILabel class]], @"Expected a UILabel for tag 1001");
        
        
        
        
        switch (indexPath.row) {
            case 0:
                keyLabel.text=@"Location";
                valueLabel.text= taskDetail.location;
                break;
            case 1:
                keyLabel.text= @"Type";
                valueLabel.text=taskDetail.type;
                break;
            case 2:
                keyLabel.text= @"Task";
                valueLabel.text=taskDetail.task;
                break;
            case 3:
                keyLabel.text= @"Sub Task";
                valueLabel.text=taskDetail.subTask;
                break;
            case 4:
                keyLabel.text= @"CR Name";
                valueLabel.text=taskDetail.CRName;
                break;
            case 6:
                keyLabel.text= @"Efforts Hrs";
                valueLabel.text=[NSString stringWithFormat:@"%0.2f", [taskDetail.effort doubleValue]];
                break;
            default:
                break;
        }
    }
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 10;
}
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, -10, tableView.frame.size.width, 10)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, -1, tableView.frame.size.width, 1)];
    [label setBackgroundColor:[UIColor colorWithRed:200/255.0 green:200/255.0 blue:200/255.0 alpha:1.0]];
    [view addSubview:label];
    [view setBackgroundColor:[UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0]];
    return view;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0)
    {
        if (indexPath.row == 0)
        {
            //            [self openDateSelectionController:self];
            [tableView deselectRowAtIndexPath:indexPath animated:YES];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UILabel *label1 = [[UILabel alloc] initWithFrame:CGRectMake(0, 9, tableView.frame.size.width, 30)];
    [label1 setBackgroundColor:[UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0]];
    [view addSubview:label1];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20, 15, tableView.frame.size.width, 20)];
    label.text=taskDetail.track;
    label.font=[UIFont fontWithName:@"helveticaNeue-light" size:17];
    label.textColor =[UIColor colorWithRed:18/255.0 green:132/255.0 blue:255/255.0 alpha:1.0];
    [label setBackgroundColor:[UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0]];
    [view addSubview:label];
    UIView *view1 = [[UIView alloc] initWithFrame:CGRectMake(0, 40, tableView.frame.size.width, 1)];
    [view setBackgroundColor:[UIColor colorWithRed:246/255.0 green:246/255.0 blue:246/255.0 alpha:1.0]];
    [view1 setBackgroundColor:[UIColor lightGrayColor]];
    [view addSubview:view1];
    return view;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(indexPath.row == 5)
    {
        NSString *cellText = taskDetail.taskDescription;
        CGSize constraintSize = CGSizeMake(self.tableView.frame.size.width - 46, 9999);
        CGSize labelSize = [cellText boundingRectWithSize:constraintSize options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName : [UIFont systemFontOfSize:17.0]} context:nil].size;
        
        return ceil(labelSize.height) + 60;
    }
    
    return 43;
}


@end
