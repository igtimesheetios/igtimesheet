//
//  AppDelegate.h
//  IGTimeSheet
//
//  Created by Manish on 06/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "igUpdateResponse.h"
#define APP_DELEGATE  [[UIApplication sharedApplication] delegate]

@class RESideMenu;
@class IGSideMenuRootVC;

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) IGSideMenuRootVC *sideMenuRootVC;

//TODO: To be deleted
//TODO: Replace usage of app delegates emp id, userName, domain with that of IGUserDefault
//@property (atomic) NSInteger employeeId;
//@property (atomic) NSString *userName;
//@property (atomic) NSString *domain;
//@property (atomic) NSString *loginName;

@property (nonatomic, readonly) NSString *deviceTokenString;
@property (nonatomic, assign) BOOL hideLeftMenu;
@property (nonatomic) NSInteger requestType;

- (void)setupRootViewControllerWithInfo:(NSDictionary *)dictionary;
- (void)showSignInScreen;

//TODO: To be deleted
- (void)showSignedInScreenWithInfo:(NSDictionary *)dictionary;

- (void)showMandatoryUpdateSceneWithResponse:(igUpdateResponse *)response;

@end

