//
//  IGPMTimeSheet.h
//  IGTimeSheet
//
//  Created by Saurabh on 14/12/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Hour : NSObject
@property (nonatomic) double billable;
@property (nonatomic) double nonBillable;
@end

@interface Task : NSObject
@property (nonatomic)NSString *tID;
@property (nonatomic)NSString *name;
@end

@interface PMStatus:NSObject

@property (nonatomic) NSString *code;
@property (nonatomic) NSString *description;

@end

@interface IGPMTimeSheet : NSObject
@property (nonatomic) NSString *projectID;
@property (nonatomic) NSString *projectName;
@property (nonatomic) NSMutableArray *timesheet;
-(instancetype)initWith:(NSDictionary *)dictTimeSheet;
@end

@interface IGEmployeeTimesheet : NSObject

@property (nonatomic) NSString *ID;
@property (nonatomic) NSString *projectID;
@property (nonatomic) NSString *ProjectName;
@property (nonatomic) NSString *staffID;
@property (nonatomic) NSString *employeeName;
@property (nonatomic) double totalHours;
@property (nonatomic) double billableHours;
@property (nonatomic) double nonBillableHours;
@property (nonatomic) double wfolHours;
@property (nonatomic) NSString * emailID;
@property (nonatomic) NSString *approverStatus;
@property (nonatomic) NSString *pmID;
@property (nonatomic) NSString *oldEmployeeID;
@property (nonatomic) NSString *employeeID;
@property (nonatomic) NSString *approvedBy;
@property (nonatomic) NSString *financeStatus;
@property (nonatomic) NSString *PIFId;
@property (nonatomic) NSString *locationID;
@property (nonatomic) NSString *locationName;
@property (nonatomic) NSInteger   weekendID;
@property (nonatomic) NSString *weekendDate;
@property (nonatomic) NSString *delegatorID;
@property (nonatomic) NSString *orderID;
@property (nonatomic) NSString *note;

-(instancetype)initWith:(NSDictionary *)dictTimeSheet;
@end




@interface IGNonExistingTimeSheets : NSObject

@property (nonatomic)NSInteger trackID;
@property (nonatomic)NSString *trackName;
@property (nonatomic) NSString *oldEmployeID;
@property (nonatomic) NSInteger NewEmployeeID;
@property (nonatomic) NSString *employeeName;
@property (nonatomic) NSString *employeeMailID;
@property (nonatomic) NSString *week;
-(instancetype)initWith:(NSDictionary *)timeSheet;
@end
