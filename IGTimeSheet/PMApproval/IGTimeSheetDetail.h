//
//  IGTimeSheetDetail.h
//  IGTimeSheet
//
//  Created by Saurabh on 06/01/17.
//  Copyright © 2017 InfoGain. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface IGTimeSheetDetail : NSObject
@property (nonatomic)NSString *location;
@property (nonatomic)NSString *type;
@property (nonatomic)NSString *taskName;
@property (nonatomic)NSString *subTaskName;
@property (nonatomic)NSString *crName;
@property (nonatomic)NSString *tsDescription;
@property (nonatomic)double efforts;
@property (nonatomic)double monday;
@property (nonatomic)double tuesday;
@property (nonatomic)double wednessday;
@property (nonatomic)double thursday;
@property (nonatomic)double friday;
@property (nonatomic)double saturday;
@property (nonatomic)double sunday;
@property (nonatomic,getter=isWFOL) BOOL wfol;
@property (nonatomic,getter=isUSRecord) BOOL usRecord;
@property (nonatomic,getter=isBillable) BOOL billable;
@property (nonatomic) NSString *projectID;
@property (nonatomic) NSString *projectName;
@property (nonatomic) NSString *taskCategory;

-(instancetype)initWith:(NSDictionary *)timeSheetDetail;


@end
