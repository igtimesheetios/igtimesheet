//
//  IGApprovalTrack.h
//  IGTimeSheet
//
//  Created by Saurabh on 15/12/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kApprovalTracks @"Tracks"
#define kApprovalYears  @"Year"
#define kApprovalMonths @"Month"

#define kApprovalMonthValue @"mthvalue"
#define kApprovalMonthName  @"MTH"

#define kApprovalTrackID @"TrackID"
#define kApprovalTrackName @"TrackName"

#define kApprovalWeekends @"Weekends"
#define kApprovalWeekendID  @"WeekId"
#define kApprovalWeekendDate @"Weekenddate"
#define kApprovalTimesheetsData @"data"

@interface IGApprovalMonth : NSObject
@property (nonatomic) NSString *name;
@property (nonatomic) NSInteger      value;
-(instancetype)initWith:(NSDictionary *)months;
@end

@class IGApprovalTimesheetType;
@interface IGApprovalTrack : NSObject

@property (nonatomic) NSString *trackID;
@property (nonatomic) NSString *trackName;

-(instancetype) initWith:(NSDictionary *)dictTrack;
@end

@interface IGApprovalTracks : NSObject
@property (nonatomic) NSMutableArray *timesheetTypes;
@property (nonatomic) NSMutableArray *tracks;
@property (nonatomic) NSMutableArray *years;
@property (nonatomic) NSMutableArray *months;

-(instancetype) initWith:(NSDictionary *)dictTracks;
-(NSMutableArray *)parseMonths:(NSArray *)months;

@end


@interface IGApprovalWeekend : NSObject
@property (nonatomic) NSString *ID;
@property (nonatomic) NSString *date;
-(instancetype) initWith:(NSDictionary *)dictWeekends;
 @end

@interface IGApprovalTimesheetType : NSObject
@property (nonatomic) NSString *type;
@property (nonatomic) NSString *typeCode;
-(instancetype)initWith:(NSString *)type code:(NSString *)code;
@end

@interface IGApprovalTimeSheetRequest : NSObject
@property (nonatomic) NSString *accountID;
@property (nonatomic) NSString *projectID;
@property (nonatomic) NSString *WeekID;
@property (nonatomic) NSString *statusID;
@property (nonatomic) NSString *loginBy;

-(instancetype)initWith:(NSString *)employeeID;
@end

