//
//  IGPMAccount.h
//  IGTimeSheet
//
//  Created by Saurabh on 14/12/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>
#define kAccountID @"ACCOUNT_ID"
#define kAccountName @"ACCOUNT_NAME"
#define kAccountType @"TYPE"
#define kAccountAdmin @"Admin"
#define kApprovalAccounts @"Accounts"


@interface IGAccount : NSObject
@property (nonatomic) NSString *aID;
@property (nonatomic) NSString *name;
@property (nonatomic) NSString *type;
@property (nonatomic) NSString *admin;

-(instancetype)initWith:(NSDictionary *)account;

@end


@interface IGPMAccounts : NSMutableArray
-(instancetype)initWith:(NSDictionary *)accounts;
@end

