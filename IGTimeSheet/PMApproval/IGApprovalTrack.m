//
//  IGApprovalTrack.m
//  IGTimeSheet
//
//  Created by Saurabh on 15/12/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import "IGApprovalTrack.h"


@implementation IGApprovalMonth

-(instancetype)initWith:(NSDictionary *)month{
 
    self = [super init];
    
    if (self) {
        self.name = [month valueForKey:kApprovalMonthName];
        self.value = [[month valueForKey:kApprovalMonthValue] integerValue];
    }
    return self;
}

@end


@implementation IGApprovalTrack

-(instancetype) initWith:(NSDictionary *)dictTrack{
    self = [super init];
    
    if (self) {
        
        self.trackID = [NSString stringWithFormat:@"%ld",[[dictTrack valueForKey:kApprovalTrackID] integerValue]];
        self.trackName = [dictTrack valueForKey:kApprovalTrackName];
    }
    return self;
}

@end

@implementation IGApprovalTracks

-(instancetype) initWith:(NSDictionary *)dictTracks{
    
    self = [super init];
    if (self) {
        self.tracks = [NSMutableArray new];
        self.months = [NSMutableArray new];
        self.years = [NSMutableArray new];
        self.timesheetTypes = [NSMutableArray new];
        
        [self parseTracks:[dictTracks valueForKey:kApprovalTracks]];
        
        [self parseMonths:(NSArray *)[dictTracks valueForKey:kApprovalMonths]];
        
        [self parseYear:(NSArray *)[dictTracks valueForKey:kApprovalYears]];
        
        [self prepareTimesheetTypes];
    }
    return self;
}



-(NSMutableArray *)parseMonths:(NSArray *)months{
 
    NSMutableArray *tempMonths = [NSMutableArray new];
    for (NSDictionary *dictMonth in months) {
        
        IGApprovalMonth *month = [[IGApprovalMonth alloc] initWith:dictMonth];
        
        [tempMonths addObject:month];
        
        month = nil;
    }
    
    return tempMonths;
}

-(void)parseTracks:(NSArray *)tracks{
    
    IGApprovalTrack *allTrack = [[IGApprovalTrack alloc] init];
    allTrack.trackID = @"0";
    allTrack.trackName = @"All";
    [self.tracks addObject:allTrack];
    
    for (NSDictionary *dictTrack in tracks) {
        
        IGApprovalTrack *track = [[IGApprovalTrack alloc] initWith:dictTrack];
        
        [self.tracks addObject:track];
        
        track = nil;
    }
}

-(void)parseYear:(NSArray *)years{
    
    NSMutableArray *yearArray = [NSMutableArray new];
    for (NSDictionary *dictYear in years) {
        [yearArray addObject:[NSString stringWithFormat:@"%@",[dictYear valueForKey:kApprovalYears]]];
    }
    
    self.years = [[yearArray sortedArrayUsingComparator:^NSComparisonResult(id obj1, id obj2) {
        
        if ([obj1 integerValue] < [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if ([obj1 integerValue] > [obj2 integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        
        return NSOrderedSame;
    }] mutableCopy];
}


-(void)prepareTimesheetTypes{
    [self.timesheetTypes addObject:[[IGApprovalTimesheetType alloc] initWith:@"Approved" code:@"A"]];
    [self.timesheetTypes addObject:[[IGApprovalTimesheetType alloc] initWith:@"Completed" code:@"C"]];
    [self.timesheetTypes addObject:[[IGApprovalTimesheetType alloc] initWith:@"Non Existing" code:@"N"]];
    [self.timesheetTypes addObject:[[IGApprovalTimesheetType alloc] initWith:@"In progress" code:@"P"]];
}
@end

@implementation IGApprovalWeekend

-(instancetype) initWith:(NSDictionary *)dictWeekends{
 
    self = [super init];
    if (self) {
        self.ID = [NSString stringWithFormat:@"%@",[dictWeekends valueForKey:kApprovalWeekendID]];
        self.date = [dictWeekends valueForKey:kApprovalWeekendDate];
    }
    return self;
}

@end

@implementation IGApprovalTimesheetType

-(instancetype)initWith:(NSString *)type code:(NSString *)code{
    self = [super init];
    if (self) {
        self.typeCode = code;
        self.type = type;
    }
    
    return self;
}

@end



@implementation IGApprovalTimeSheetRequest

-(instancetype)initWith:(NSString *)employeeID{
    
    self = [super init];
    if (self) {
        // code to initialize variables
        self.loginBy = employeeID;
        self.projectID = @"0";
        self.statusID = @"A";
    }
    return self;
}

@end

