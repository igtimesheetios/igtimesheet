//
//  IGPMTimeSheet.m
//  IGTimeSheet
//
//  Created by Saurabh on 14/12/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import "IGPMTimeSheet.h"

#define kApprovalTimesheetID                @"Id"
#define kApprovalTimeSheetProjectID         @"ProjectId"

#define kApprovalTimeSheetProjectName       @"ProjectName"
#define kApprovalTimeSheetOldEmployeeID     @"EMP_STAFFID"
#define kApprovalTimeSheetNewEmployeeID     @"NewEmpID"
#define kApprovalTimeSheetEmployeeName      @"EMP_NAME"
#define kApprovalTimeSheetTotalHours        @"TOTAl_HOURS1"
#define kApprovalTimeSheetBillHours         @"TOTAL_BILLHOURS"
#define kApprovalTimeSheetNonBillHourse     @"TOTAL_NBILLHOURS"
#define kApprovalTimeSheetWFOLHours         @"TOTAL_WFOL"
#define kApprovalTimeSheetEmployeeEmailID   @"EMP_MAILID"
#define kApprovalTimeSheetApproverStatus    @"Approverstatus"
#define kApprovalTimeSheetPMID              @"PM"
#define kApprovalTimeSheetApprovedBy        @"ApprovedBy"
#define kApprovalTimeSheetLocationID        @"LOCATION_ID"
#define kApprovalTimeSheetLocatinName       @"LOCATION_NAME"
#define kApprobvalTimeSheetWeekID           @"WeekId"
#define kApprovalTimeSheetWeekendDate       @"WeekEndDate"
#define kApprovalTimeSheetDelegatorID       @"DeligatorId"
#define kApprovalTimeSheetComment           @"Comments"

#define PROJECTZERO @"0"
@implementation IGPMTimeSheet

-(instancetype)initWith:(NSDictionary *)dictTimeSheet{
    
    self = [super init];
    
    if (self) {
        self.projectID = [dictTimeSheet valueForKey:@"pid"];
        self.projectName = [dictTimeSheet valueForKey:@"PName"];
        self.timesheet = [[NSMutableArray alloc] init];

        if (![self.projectID isEqualToString:PROJECTZERO] && ![self.projectName isEqualToString:PROJECTZERO]) {
            
            for (NSDictionary *dictEmployeeTimesheet in (NSArray *)[dictTimeSheet valueForKey:@"timesheets"]) {
                
                IGEmployeeTimesheet *employeeTimeSheet = [[IGEmployeeTimesheet alloc] initWith:dictEmployeeTimesheet];
                
                [self.timesheet addObject:employeeTimeSheet];
                
                employeeTimeSheet = nil;
            }
        }else{
            return nil;
        }
    }
    return self;
}
@end

@implementation IGEmployeeTimesheet

-(instancetype)initWith:(NSDictionary *)dictTimeSheet{
    self = [super init];
    
    if (self) {
        self.ID = [dictTimeSheet valueForKey:kApprovalTimesheetID];
        self.projectID = [dictTimeSheet valueForKey:kApprovalTimeSheetProjectID];
        self.ProjectName = [dictTimeSheet valueForKey:kApprovalTimeSheetProjectName];
        self.oldEmployeeID = [dictTimeSheet valueForKey:kApprovalTimeSheetOldEmployeeID];
        self.employeeID = [dictTimeSheet valueForKey:kApprovalTimeSheetNewEmployeeID];
        self.employeeName = [dictTimeSheet valueForKey:kApprovalTimeSheetEmployeeName];
        self.totalHours = [[dictTimeSheet valueForKey:kApprovalTimeSheetTotalHours] doubleValue];
        self.billableHours = [[dictTimeSheet valueForKey:kApprovalTimeSheetBillHours] doubleValue];
        self.nonBillableHours = [[dictTimeSheet valueForKey:kApprovalTimeSheetNonBillHourse] doubleValue];
        self.wfolHours = [[dictTimeSheet valueForKey:kApprovalTimeSheetWFOLHours] doubleValue];
        self.emailID = [dictTimeSheet valueForKey:kApprovalTimeSheetEmployeeEmailID];
        self.approverStatus = [dictTimeSheet valueForKey:kApprovalTimeSheetApproverStatus];
        self.pmID = [dictTimeSheet valueForKey:kApprovalTimeSheetPMID];
        self.approvedBy = [dictTimeSheet valueForKey:kApprovalTimeSheetApprovedBy];
        self.locationID = [dictTimeSheet valueForKey:kApprovalTimeSheetLocationID];
        self.locationName = [dictTimeSheet valueForKey:kApprovalTimeSheetLocatinName];
        self.weekendID = [[dictTimeSheet valueForKey:kApprobvalTimeSheetWeekID] integerValue];
        self.weekendDate = [dictTimeSheet valueForKey:kApprovalTimeSheetWeekendDate];
        self.delegatorID = [dictTimeSheet valueForKey:kApprovalTimeSheetDelegatorID];
        self.note = [dictTimeSheet valueForKey:kApprovalTimeSheetComment];
    }
    
    return self;
}

@end



#define NETrackID @"TRACKID"
#define NETrackName @"TRACKNAME"
#define NEOldEmployeeID @"EMP_STAFFID"
#define NEEmployeeName @"EMP_NAME"
#define NEEmployeeMailID @"EMP_MAILID"
#define NENewEmployeeID   @"NewEmpID"

@implementation IGNonExistingTimeSheets

-(instancetype)initWith:(NSDictionary *)timeSheet{
    self = [super init];
    
    if (self) {
        self.trackID = [[timeSheet valueForKey:NETrackID] integerValue];
        
        self.trackName = [[timeSheet valueForKey:NETrackName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        self.oldEmployeID = [[timeSheet valueForKey:NEOldEmployeeID] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
        
        self.NewEmployeeID = [[timeSheet valueForKey:NENewEmployeeID] integerValue];
        
        self.employeeName = [[timeSheet valueForKey:NEEmployeeName] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    }
    
    return self;
}

@end
