//
//  IGTimeSheetDetail.m
//  IGTimeSheet
//
//  Created by Saurabh on 06/01/17.
//  Copyright © 2017 InfoGain. All rights reserved.
//

#import "IGTimeSheetDetail.h"
//@property (nonatomic)NSString *location;
//@property (nonatomic)NSString *type;
//@property (nonatomic)NSString *taskName;
//@property (nonatomic)NSString *subTaskName;
//@property (nonatomic)NSString *crName;
//@property (nonatomic)NSString *tsDescription;
//@property (nonatomic)double efforts;
//@property (nonatomic)double monday;
//@property (nonatomic)double tuesday;
//@property (nonatomic)double wednessday;
//@property (nonatomic)double thursday;
//@property (nonatomic)double friday;
//@property (nonatomic)double saturday;
//@property (nonatomic)double sunday;
//@property (nonatomic,getter=isWFOL) BOOL wfol;
//@property (nonatomic,getter=isUSRecord) BOOL usRecord;
//@property (nonatomic,getter=isBillable) BOOL billable;
//@property (nonatomic) NSString *projectID;
//@property (nonatomic) NSString *projectName;
//@property (nonatomic) NSString *taskCategory;


#define IGTSDetailLocation          @"Location"
#define IGTSDetailType              @"Type"
#define IGTSDetailTaskName          @"TASKNAME"
#define IGTSDetailSubTaskName       @"SUBTASKNAME"
#define IGTSDetailCRName            @"CRNAME"
#define IGTSDetailDescription       @"Description"
#define IGTSDetailEfforts           @"Efforts"
#define IGTSDetailMon               @"Mon"
#define IGTSDetailTues              @"Tue"
#define IGTSDetailWed               @"Wed"
#define IGTSDetailThu               @"Thu"
#define IGTSDetailFri               @"Fri"
#define IGTSDetailSat               @"Sat"
#define IGTSDetailSun               @"Sun"
#define IGTSDetailWFOL              @"WFOL"
#define IGTSDetailIsUSRecord        @"IsUSRecord"
#define IGTSDetailIsBillable        @"ISBillable"
#define IGTSDetailProjectID         @"ISBillable"
#define IGTSDetailProjectName       @"ProjectName"
#define IGTSDetailTaskCategory      @"TaskCategory"

@implementation IGTimeSheetDetail
-(instancetype)initWith:(NSDictionary *)timeSheetDetail{
    self = [super init];
    if (self) {
        self.location = [timeSheetDetail valueForKey:IGTSDetailLocation];
        self.type = [timeSheetDetail valueForKey:IGTSDetailType];
        self.taskName = [timeSheetDetail valueForKey:IGTSDetailTaskName] ;
        self.subTaskName = [timeSheetDetail valueForKey:IGTSDetailSubTaskName];
        self.crName = [timeSheetDetail valueForKey:IGTSDetailCRName];
        self.tsDescription = [timeSheetDetail valueForKey:IGTSDetailDescription];
        self.efforts = [[timeSheetDetail valueForKey:IGTSDetailEfforts] doubleValue];
        self.monday = [[timeSheetDetail valueForKey:IGTSDetailMon] doubleValue];
        self.tuesday = [[timeSheetDetail valueForKey:IGTSDetailTues] doubleValue];
        self.wednessday = [[timeSheetDetail valueForKey:IGTSDetailWed] doubleValue];
        self.thursday = [[timeSheetDetail valueForKey:IGTSDetailThu] doubleValue];
        self.friday = [[timeSheetDetail valueForKey:IGTSDetailFri] doubleValue];
        self.saturday = [[timeSheetDetail valueForKey:IGTSDetailSat] doubleValue];
        self.sunday = [[timeSheetDetail valueForKey:IGTSDetailSun] doubleValue];
        self.wfol = [[timeSheetDetail valueForKey:IGTSDetailWFOL] caseInsensitiveCompare:@"NO"] == NSOrderedSame ? NO:YES;
        self.usRecord = [[timeSheetDetail valueForKey:IGTSDetailIsUSRecord] isEqualToString:@"N"]? NO:YES;
        self.billable = [[timeSheetDetail valueForKey:IGTSDetailIsBillable] isEqualToString:@"N"]? NO:YES;
        self.projectID = [timeSheetDetail valueForKey:IGTSDetailProjectID];
        self.projectName = [timeSheetDetail valueForKey:IGTSDetailProjectName];
        self.taskCategory = [timeSheetDetail valueForKey:IGTSDetailTaskCategory];
    }
    return self;
}


@end
