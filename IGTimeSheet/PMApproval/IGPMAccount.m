//
//  IGPMAccount.m
//  IGTimeSheet
//
//  Created by Saurabh on 14/12/16.
//  Copyright © 2016 InfoGain. All rights reserved.
//

#import "IGPMAccount.h"

@implementation IGAccount

-(instancetype)initWith:(NSDictionary *)account{
    self = [super init];
    if (self) {
        self.aID = [account valueForKey:kAccountID];
        self.name = [account valueForKey:kAccountName];
        self.type = [account valueForKey:kAccountType];
        self.admin = [account valueForKey:kAccountAdmin];
    }
    return self;
}
@end


@implementation IGPMAccounts

-(instancetype)initWith:(NSDictionary *)accounts{
    
    self = [super init];
    
    if (self) {

        NSArray *tempAccounts = (NSArray *)[accounts valueForKey:kApprovalAccounts];

        for (NSDictionary *dictAccount in tempAccounts) {
            
            IGAccount *account = [[IGAccount alloc] initWith:dictAccount];
            
            [self addObject:account];

        }
        
        tempAccounts = nil;
        
    }
    return self;
}
@end
