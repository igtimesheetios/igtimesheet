//
//  IGLeftMenuVC.m
//  IGTimeSheet
//
//  Created by Manish on 09/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGLeftMenuVC.h"
#import "IGCommonHeaders.h"
#import "IGSideMenuRootVC.h"
#import "igInfoVC.h"
#import "UIImage+EMAdditions.h"
#import "igAnimationTransitionManager.h"


#define kTableViewCellReuseIdentifier @"leftMenuVCCell"

@interface IGLeftMenuVC()

@property (strong, nonatomic) IBOutlet UIView *contentView;

@property (strong, nonatomic) IBOutlet UILabel *employeeDetailsLabel;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *userImageHeightConstraint;

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *verticalSpacingConstraint;

@property (strong, atomic) UIImage *imageData;

- (IBAction)info:(id)sender;

@end

@implementation IGLeftMenuVC
{
    igAnimationTransitionManager *animationManager;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self.menuVCTableView registerClass:[UITableViewCell class] forCellReuseIdentifier: kTableViewCellReuseIdentifier];
    self.menuVCTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    self.userImageView.backgroundColor = [IGUtils averageColorFromImage:[IGUserDefaults sharedInstance].employee.image];
    self.userImageView.image = [IGUserDefaults sharedInstance].employee.image;
    
//TODO: Replaced appDelegate.employeeId with [[IGUserDefaults sharedInstance].employee.staffId integerValue]
//TODO: Replaced appDelegate.employeeId with [IGUserDefaults sharedInstance].employee.name
//    NSString *employeeDetails = [[[IGUserDefaults sharedInstance].employee.name stringByAppendingString:@" - "] stringByAppendingString:[@([[IGUserDefaults sharedInstance].employee.staffID integerValue]) stringValue]];
    
     NSString *employeeDetails = [[[IGUserDefaults sharedInstance].employee.name stringByAppendingString:@" - "] stringByAppendingString:[@([[IGUserDefaults sharedInstance].employee.employeeNewID integerValue]) stringValue]];
    self.employeeDetailsLabel.text = employeeDetails;
    
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
         _employeeDetailsLabel.font = [UIFont fontWithName:kFontName size:22];
    }
    else{
         _employeeDetailsLabel.font = [UIFont fontWithName:kFontName size:16];
    }


}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
  /*  if (CGRectGetHeight(self.view.frame) <= 480)
    {
        self.userImageHeightConstraint.constant = 70;
        self.verticalSpacingConstraint.constant = 20.0f;
    }
    if (CGRectGetWidth(self.view.frame) >= 375)
    {
        self.widthConstraint.constant = 60.0f;
    }*/
    
    [IGAnalytics trackScreen:screenMenu];
    
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    self.userImageView.layer.cornerRadius = self.userImageView.frame.size.height / 2;
    self.userImageView.clipsToBounds = YES;
    self.userImageView.layer.borderWidth = 3.0f;
    self.userImageView.layer.borderColor = [UIColor whiteColor].CGColor;
}

# pragma marks - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.optionsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = nil;
    if (cell == nil) {
        cell = [tableView dequeueReusableCellWithIdentifier:kTableViewCellReuseIdentifier
                                               forIndexPath:indexPath];
        cell.textLabel.text = self.optionsArray[indexPath.row];
        
        cell.imageView.image = [UIImage imageNamed:self.imageArray[indexPath.row]];
        cell.textLabel.textColor = [UIColor whiteColor];
        
       /* if ([[UIScreen mainScreen] bounds].size.width > 320)
        {
            cell.textLabel.font = [UIFont fontWithName:kFontName size:16.0f];
        }
        else
        {
            cell.textLabel.font = [UIFont fontWithName:kFontName size:14.0f];
        }*/
        
        if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
            cell.textLabel.font = [UIFont fontWithName:kFontName size:22];
            cell.imageView.frame = CGRectMake(cell.imageView.frame.origin.x, cell.imageView.frame.origin.y, 70, 70);
        }
        else{
            cell.textLabel.font = [UIFont fontWithName:kFontName size:16];
            cell.imageView.frame = CGRectMake(cell.imageView.frame.origin.x, cell.imageView.frame.origin.y, 44, 44);
        }
        
        cell.backgroundColor = [UIColor clearColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return cell;
}

# pragma marks - UITableViewDelegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad){
       return 70;    }
    else{
        return 36;
    }

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(leftMenu:selectionIndex:)])
    {
        [self.delegate leftMenu:self selectionIndex:indexPath.row];
    }
}

- (IBAction)logout:(id)sender
{
    [IGAnalytics trackButtonAction:actionLogout];
    
    [[IGUserDefaults sharedInstance] logout];
}

- (IBAction)info:(id)sender
{
//    NSString *bundleVersion = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"];
//    NSString *message = [NSString stringWithFormat:@"Version: %@", bundleVersion];
//    
//    [IGUtils showOKAlertWithTitle:@"igTimesheet"
//                          message:message];
}


- (IBAction)unwind:(UIStoryboardSegue *)segue
{
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"igInfoVCSegue"])
    {
        igInfoVC *infoVC = (igInfoVC *)segue.destinationViewController;
        NSAssert([infoVC isKindOfClass:[igInfoVC class]], @"Expected a class of kind igInfoVC");
        
        UIImage *snapshot = [IGUtils imageFromView:self.view.window];
        UIImage *blurImage = [snapshot emApplyBlurWithRadius:3 tintColor:[UIColor clearColor] saturationDeltaFactor:1 maskImage:nil];
        infoVC.image = blurImage;
        
        if (animationManager == nil)
        {
            animationManager = [[igAnimationTransitionManager alloc] init];
            animationManager.transitionDuration = 0.3f;
        }
        
        infoVC.transitioningDelegate = animationManager;
    }
}

@end
