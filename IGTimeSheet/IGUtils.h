//
//  IGUtils.h
//  IGTimeSheet
//
//  Created by Manish on 19/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@interface IGUtils : NSObject

+ (BOOL)isValidString:(NSString *)string;
+ (BOOL)isValidNumber:(NSNumber *)number;
+ (BOOL)isDictionary:(NSObject *)object;
+ (BOOL)isArray:(NSObject *)object;

+ (NSString *)stringFromDate:(NSDate *)date dateFormat:(NSString *)dateFormat;
+ (NSDate *)dateFromString:(NSString *)dateString dateFormat:(NSString *)dateFormat;

+ (void)showOKAlertWithTitle:(NSString *)title message:(NSString *)message;
+ (void)showOKAlertWithTitle:(NSString *)title message:(NSString *)message delegate:(id)delegate tag:(NSInteger)tag;
+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message cancelButton:(BOOL)hasCancelButton defaultButtonTitle:(NSString *)defaultButtonTitle delegate:(id)delegate tag:(NSInteger)tag;

+ (void)underImplementationAlert;

+ (void)roundCorner:(UIRectCorner)corner radius:(CGFloat)radius forView:(UIView *)view;

+ (NSString *)jsonStringFromObject:(id)object;

+ (void)setupButton:(UIButton *)button enable:(BOOL)enable backgroundColor:(UIColor *)backgroundColor;

+ (NSString *)bundleVersion;

+ (UIImage *)imageFromView:(UIView *)view;

+ (BOOL)openURLWithString:(NSString *)urlString;

+ (UIColor *)averageColorFromImage:(UIImage *)paramImage;

+ (NSDate *)getDateFromString:(NSString *)dateString;


@end
