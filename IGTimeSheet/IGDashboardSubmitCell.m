//
//  IGDashboardSubmitCell.m
//  IGTimeSheet
//
//  Created by Rajat on 02/03/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGDashboardSubmitCell.h"

@implementation IGDashboardSubmitCell

- (void)awakeFromNib {
    
    self.submitButton.layer.cornerRadius = 7.0f;

    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
