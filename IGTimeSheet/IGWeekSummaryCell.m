//
//  IGWeekSummaryCell.m
//  IGTimeSheet
//
//  Created by Neha on 12/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGWeekSummaryCell.h"
#import "IGMacro.h"
#import "IGUtils.h"

@implementation IGWeekSummaryCell

- (void)awakeFromNib
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.backgroundColor = kColorGrayType2;
    
    CALayer *layer = self.viewBackground.layer;
    
    layer.cornerRadius = 9.0f;
    layer.shadowColor = [UIColor blackColor].CGColor;
    layer.shadowOffset = CGSizeMake(0, 0);
    layer.shadowRadius = 2.0f;
    layer.shadowOpacity = 0.5f;
    
}

- (IBAction)viewTask: (id)sender
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(viewTaskForCell:)])
    {
        [self.delegate viewTaskForCell:self];
    }
}

@end
