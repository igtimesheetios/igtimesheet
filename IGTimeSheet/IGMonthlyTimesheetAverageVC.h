//
//  IGMonthlyTimesheetAverageVC.h
//  IGTimeSheet
//
//  Created by Kamal on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGViewController.h"

@interface IGMonthlyTimesheetAverageVC : IGViewController

@property (strong, nonatomic) NSString *selectedYear;

@end
