//
//  IGTaskViewVC.m
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGTaskViewVC.h"
#import "IGLabelButtonModalClass.h"
#import "IGLabelButtonTableViewCell.h"
#import "IGInOutModalClass.h"
#import "IGInOutTableViewCell.h"
#import "IGSubmitTableViewCell.h"
#import "IGTaskDetailTableViewCell.h"
#import "IGService.h"
#import "IGMacro.h"
#import "IGTaskViewParser.h"
#import "AppDelegate.h"
#import "IGDashboardVC.h"
#import "IGMyAreaVC.h"
#import "IGTaskViewMacros.h"
#import "IGViewLeavesVC.h"
#import "IGCommonHeaders.h"
#import <FirebaseAnalytics/FirebaseAnalytics.h>

//Cell Identifiers
#define LabelButtonCellId   @"LabelButtonCell"
#define InOutCellId         @"InOutCell"
#define HInOutCellId        @"HeaderInOutCell"
#define TaskCellId          @"TaskDetailCell"
#define SwitchCellId        @"SwitchCell"
#define NotifierCellId      @"NotifierCell"
#define SubmitCellId        @"SubmitCell"

//Heights
#define DefaultRowHeight    58
#define HInOutRowHeight     32
#define SwitchRowHeight     37
#define TaskRowHeight       85
#define NotifierRowHeight   92

#define HeaderInOut         @"HeaderInOut"
#define SwitchView          @"SwitchView"
#define NotifierView        @"NotifierView"
#define SubmitView          @"SubmitView"

//Submit Tags
#define SaveTag     1
#define SubmitTag   0

#define MinEffortPerDay 8

#define DuplicateSheetAction    @"Duplicate Previous Timesheet"
#define ViewLeavesAction        @"View Leaves"
#define ViewMyAreaAction        @"My Area"

@interface IGTaskViewVC ()
{
    IGSpinKitView *spinner;
    NSString* status;
    int weekendDateIndex;
     int invalidDayCount;
    int validDaysCount;

    


}

@property (nonatomic, strong) NSArray *inOutDetails;
@property (strong) NSArray* weekDates;


@end

@implementation IGTaskViewVC

- (void)viewWillAppear:(BOOL)animated
{
    validDaysCount = 0;

   // [self showSpinner:NO];
    @try {
        if(_rowArray !=0 && _rowArray.count >0)
        {
            IGLabelButtonModalClass* objButtonClass =  [_rowArray objectAtIndex:0];
            if(objButtonClass != nil && [objButtonClass isKindOfClass:[IGLabelButtonModalClass class]])
            {
                if([objButtonClass.buttonText isEqualToString:@"Completed"])
                {
                    return;
                }
            }
        }
        IGLabelButtonModalClass* totalHrs = nil;
        float totalTime = 0.0;

        for(int i = 0; i < _rowArray.count; ++i)
        {
            id modelItem = [_rowArray objectAtIndex:i];
            
            if([modelItem isKindOfClass:[IGTaskModalClass class]])
            {
              //  if(totalHrs != nil)
                {
                    totalHrs = [_rowArray objectAtIndex:(_rowArray.count - 2)];
                    totalHrs.buttonText = @"0.0";
                }
                totalTime = totalTime + [modelItem totalTime];
            }
        }


        if(totalHrs != nil)
        {
            totalHrs.buttonText = [NSString stringWithFormat:@"%.2f",totalTime];
            [_rowArray replaceObjectAtIndex:(_rowArray.count - 2) withObject:totalHrs];
        }
        [_tableView reloadData];

        [IGAnalytics trackScreen:screenTaskView];
    } @catch (NSException *exception) {
        
    }
}

- (void) didRotate:(NSNotification *)notification
{
    [_tableView reloadData];
}

- (void)fetchDataFromServerForEmployee:(NSInteger)empId atDate:(NSDate *)date
{
    [self showSpinner:YES];
    _empId = empId;
    kAlertAndReturnIfNetworkNotReachable
    [[IGService sharedInstance] inOutTimeForEmployee:empId date:date completion:^(id response)
     {
         [self showSpinner:NO];
         NSDictionary *dictionary = (NSDictionary *)response;
         IGTaskViewParser* parser = [[IGTaskViewParser alloc] init];
         
         _weekId = [parser parseWeekId:dictionary];
         _projectList = [NSArray arrayWithArray:[parser parseProjectLsit:dictionary]];
         
         if (!self.isApproval) {// IN out will be only in case of non aproal screen.
             self.inOutDetails = [parser parseInOutTimeResponse:dictionary forWeekendDate:date];
         }
         if(self.inOutDetails.count)
         {
             //fifth row
             [_rowArray addObject:HeaderInOut];
             //In Out Data
             [_rowArray addObjectsFromArray:self.inOutDetails];
         }
         
         //Add task header in case if it's not coming from Approval screen.
         IGLabelButtonModalClass* taskHeaderObj = [[IGLabelButtonModalClass alloc] init];
         taskHeaderObj.labelText = @"TASK";
         if (!self.isApproval) {
             taskHeaderObj.backImage = [UIImage imageNamed:@"add-task-icon.png"];
  
         }
         taskHeaderObj.backgroundColor = kColorGrayType2;
         taskHeaderObj.labelTextColor = [UIColor colorWithRed:(79/255.0) green:(79/255.0) blue:(79/255.0) alpha:1];
         [_rowArray addObject:taskHeaderObj];
         
         //Add tasks
         NSArray* tasks = [parser parseTasksResponse:dictionary];
         if(tasks.count)
         {
             [_rowArray addObjectsFromArray:tasks];
             
             IGLabelButtonModalClass* totalHrs = [[IGLabelButtonModalClass alloc] init];
             totalHrs.labelText = @"Total Hrs.";
             totalHrs.backgroundColor = [UIColor colorWithRed:(23/255.0) green:(143/255.0) blue:(70/255.0) alpha:1];
             totalHrs.labelTextColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
             totalHrs.buttonTextColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
             float totalTime  = 0.0;
             
             BOOL pendingRowExist = NO;
             NSMutableArray *array = [[NSMutableArray alloc] init];
             
             for(int i = 0; i < tasks.count; ++i)
             {
                 totalTime = totalTime + [[tasks objectAtIndex:i] totalTime];
                 IGTaskModalClass *model = [tasks objectAtIndex:i];
                 
                 NSInteger timeSheetStatus = model.timeSheetStatus;
                 NSNumber *number = [NSNumber numberWithInteger:timeSheetStatus];
                 [array addObject:number];
                 _selectedProjID = model.prjId;
               
             }
             
             NSCountedSet *filter = [NSCountedSet setWithArray:array];
             
             if ([filter countForObject:[NSNumber numberWithInteger:2]] == [array count] && !pendingRowExist) {
                 status = TimeSheetApprovedStatus;
                [[_rowArray objectAtIndex:0] setButtonText:@"Approved"];
             }
             else if ([filter countForObject:[NSNumber numberWithInteger:3]] == [array count] && !pendingRowExist){
                 status = TimeSheetRejectedStatus;
                 pendingRowExist = YES;
                 [[_rowArray objectAtIndex:0] setButtonText:@"InProgress"];
             }
             else if ([filter countForObject:[NSNumber numberWithInteger:1]] == [array count] && !pendingRowExist){
                 status = TimeSheetCompleteStatus;
                 [[_rowArray objectAtIndex:0] setButtonText:@"Completed"];
             }
             
             else if ([filter countForObject:[NSNumber numberWithInteger:0]] == [array count] && !pendingRowExist){
                 pendingRowExist = YES;
                 status = TimeSheetPendingStatus;
                 [[_rowArray objectAtIndex:0] setButtonText:@"InProgress"];
             }
             
             else if([filter containsObject:[NSNumber numberWithInteger:1]]){
                 status = TimeSheetCompleteStatus;
                 [[_rowArray objectAtIndex:0] setButtonText:@"Completed"];
             }
             else if([filter containsObject:[NSNumber numberWithInteger:0]]){
                 pendingRowExist = YES;
                 status = TimeSheetPendingStatus;
                 [[_rowArray objectAtIndex:0] setButtonText:@"InProgress"];
             }
             
             else{
                  pendingRowExist = YES;
                 [[_rowArray objectAtIndex:0] setButtonText:@"InProgress"];
             }
             
             totalHrs.buttonText = [NSString stringWithFormat:@"%.2f",totalTime];
             totalHrs.buttonEnabled = NO;
             [_rowArray addObject:totalHrs];
             
             if(tasks.count && pendingRowExist)
             {
                 [_rowArray addObject:SubmitView];
             }
         }
         else
         {
             [_rowArray addObject:NotifierView];
         }
         
         [self fetchValidDays];
         
         [_tableView reloadData];
     }
     error:^(NSError *error)
     {
         [IGUtils showOKAlertWithTitle:kStringError message:ParseError];
     }];
}


-(void)fetchValidDays
{
    [self showSpinner:YES];

    [[IGService sharedInstance] validDaysOfPorject:[[IGUserDefaults sharedInstance].employee.staffID integerValue]  projectID:_selectedProjID weekendDate:_weekendDate completion:^(id response)
     {
         NSLog(@"Response : %@",response);
         NSDictionary *dictionary = (NSDictionary *)response;
         int count = 0;
         if(dictionary != nil  && [dictionary objectForKey:@"ValidateProject"] != nil){
             
             NSString *responseStr = [dictionary objectForKey:@"ValidateProject"];
             if([responseStr containsString:@"mon"])
                 count++;
             if([responseStr containsString:@"tue"])
                 count++;
             if([responseStr containsString:@"wed"])
                 count++;
             if([responseStr containsString:@"thu"])
                 count++;
             if([responseStr containsString:@"fri"])
                count++;
             if([responseStr containsString:@"sat"])
                count++;
             if([responseStr containsString:@"sun"])
                count++;
             
         }
         [self showSpinner:NO];

         validDaysCount = count;
         
         
     } error:^(NSError *error)
     {
         [self showSpinner:NO];

         NSLog(@"Error  : %@",error);
     }];
}

#pragma mark - Spinner

- (void)showSpinner:(BOOL)show
{
    if (show)
    {
        if (!spinner)
        {
            spinner = [[IGSpinKitView alloc] initWithColor:kDefaultBarTintColor];
            spinner.center = self.view.center;
        }
        
        
        if (spinner.superview == nil)
        {
            [self.view addSubview:spinner];
        }
        
        [spinner startAnimating];
    }
    else
    {
        [spinner stopAnimating];
    }
    
    self.view.userInteractionEnabled = !show;
    
}

- (void)addNewTask:(IGTaskModalClass *)task
{
    id modalObj = [_rowArray objectAtIndex:(_rowArray.count - 1)];
    if([modalObj isEqualToString:NotifierView])
    {
        [_rowArray replaceObjectAtIndex:(_rowArray.count - 1) withObject:task];
        
        IGLabelButtonModalClass* totalHrs = [[IGLabelButtonModalClass alloc] init];
        totalHrs.labelText = @"Total Hrs.";
        totalHrs.backgroundColor = [UIColor colorWithRed:(23/255.0) green:(143/255.0) blue:(70/255.0) alpha:1];
        totalHrs.labelTextColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        totalHrs.buttonTextColor = [UIColor colorWithRed:1 green:1 blue:1 alpha:1];
        totalHrs.buttonText = [NSString stringWithFormat:@"%.2f",task.totalTime];
        totalHrs.buttonEnabled = NO;
        [_rowArray addObject:totalHrs];
        [_rowArray addObject:SubmitView];
    }
    else
    {
        [_rowArray insertObject:task atIndex:(_rowArray.count - 2)];
        IGLabelButtonModalClass* totalHrs = [_rowArray objectAtIndex:(_rowArray.count - 2)];
        totalHrs.buttonText = [NSString stringWithFormat:@"%.2f",(totalHrs.buttonText.floatValue + task.totalTime)];
    }
    
    [_tableView reloadData];
}

- (void)initilizeDataForDate:(NSDate *)date employeeID:(NSInteger)employeeID andStatus:(NSString *)status
{
    _rowArray = [[NSMutableArray alloc] init];
    _weekendDate = date;
    [self getWeekDates:_weekendDate];
    
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"dd/MM/yyyy"];
    self.title = [dateformate stringFromDate:date];
    
    //first row
    IGLabelButtonModalClass* modalObj1 = [[IGLabelButtonModalClass alloc] init];
    modalObj1.labelText = @"Status";
    modalObj1.buttonText = status;
    modalObj1.buttonTextColor = [UIColor colorWithRed:(247/255.0) green:(152/255.0) blue:(50/255.0) alpha:1];
    [_rowArray addObject:modalObj1];
    
    //second row
    /*IGLabelButtonModalClass* modalObj2 = [[IGLabelButtonModalClass alloc] init];
    modalObj2.labelText = @"Week";
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"dd/MM/yyyy"];
    modalObj2.buttonText = [dateformate stringFromDate:date];
    modalObj2.buttonTextColor = [UIColor colorWithRed:(79/255.0) green:(79/255.0) blue:(79/255.0) alpha:1];
    [_rowArray addObject:modalObj2];*/
    
    //third row
    //[_rowArray addObject:SwitchView];
    
    //fourth row only if screen is not loaded from approval screen.
    if (!self.isApproval) {
        IGLabelButtonModalClass* modalObj4 = [[IGLabelButtonModalClass alloc] init];
        modalObj4.labelText = @"EMPLOYEE IN-OUT";
        modalObj4.backgroundColor = kColorGrayType2;
        modalObj4.labelTextColor = [UIColor colorWithRed:(79/255.0) green:(79/255.0) blue:(79/255.0) alpha:1];
        modalObj4.buttonVisible = NO;
        [_rowArray addObject:modalObj4];
    }
    
    
    //Fetch data from server
//TODO: replaced [kAppDelegate employeeId] with [[IGUserDefaults sharedInstance].employee.staffID integerValue]
    [self fetchDataFromServerForEmployee:employeeID atDate:date];
    [_tableView reloadData];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    

    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didRotate:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"dd-MM-yyyy";
    self.navigationItem.title = [dateFormatter stringFromDate:_weekendDate];
    NSDictionary *titleDetail = [NSDictionary dictionaryWithObjectsAndKeys:
                                 [UIFont fontWithName:kFontName size:17.0f],NSFontAttributeName,
                                 [UIColor whiteColor] , NSForegroundColorAttributeName,
                                 nil];
    self.navigationController.navigationBar.titleTextAttributes = titleDetail;
    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:@"More"
                                                                           style:UIBarButtonItemStylePlain
                                                                          target:self
                                                                          action:@selector(showupActionSheet:)];
    self.navigationItem.rightBarButtonItem = rightBarButtonItem;
[self setupBackButton];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark --
#pragma mark DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    int count = _rowArray.count;
    return count;
}

- (IBAction)addTaskButtonClick:(id)sender
{
    _addTaskViewCtrlr = [self.storyboard instantiateViewControllerWithIdentifier:@"IGAddTaskVC"];
    [_addTaskViewCtrlr addTaskForProjects:_projectList weekEndDate:_weekendDate withWeekId:_weekId timesheetStatus:kIGTimeSheetStatusPending parentView:self];
    _addTaskViewCtrlr.inOutDetails = self.inOutDetails;
    [self.navigationController pushViewController:_addTaskViewCtrlr animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView  heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CGFloat rowHeight = DefaultRowHeight;
    
    if([[_rowArray objectAtIndex:indexPath.row] isKindOfClass:[IGTaskModalClass class]])
    {
        rowHeight = TaskRowHeight;
    }
    else if([[_rowArray objectAtIndex:indexPath.row]  isKindOfClass:[NSString class]])
    {
        rowHeight = HInOutRowHeight;
        
        if([[_rowArray objectAtIndex:indexPath.row] isEqualToString:SwitchView])
        {
            rowHeight = SwitchRowHeight;
        }
        else if([[_rowArray objectAtIndex:indexPath.row] isEqualToString:NotifierView])
        {
            rowHeight = NotifierRowHeight;
        }
        else if([[_rowArray objectAtIndex:indexPath.row] isEqualToString:SubmitView])
        {
            rowHeight = DefaultRowHeight;
        }
    }
    
    return rowHeight;
}

- (IGLabelButtonTableViewCell *)getLabelButtonCellForObj:(IGLabelButtonModalClass *)modalObj
{
    IGLabelButtonTableViewCell* cell = [_tableView dequeueReusableCellWithIdentifier:LabelButtonCellId];
    if (cell == nil)
    {
        cell = [[IGLabelButtonTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:LabelButtonCellId];
    }
    //Reset View
    [cell.placeholderButton setBackgroundImage:nil forState:UIControlStateNormal];
    cell.backgroundColor = modalObj.backgroundColor;
    [cell.placeholderLabel setTextColor:modalObj.labelTextColor];

    [cell.placeholderButton setEnabled:YES];
    [cell.placeholderButton setHidden:NO];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    
    //Populate Values
    cell.placeholderLabel.text = modalObj.labelText;
    cell.placeholderLabel.textColor = modalObj.labelTextColor;
    
    if(modalObj.rowSelectable == NO)
    {
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    if(modalObj.buttonVisible)
    {
        [cell.placeholderButton setTitle:modalObj.buttonText forState:UIControlStateNormal];
        [cell.placeholderButton setTitleColor:modalObj.buttonTextColor forState:UIControlStateNormal];
        [cell.placeholderButton setEnabled:modalObj.buttonEnabled];
        if(modalObj.backImage)
        {
            
            if([status isEqualToString:TimeSheetCompleteStatus] || [status isEqualToString:TimeSheetApprovedStatus]){
                cell.placeholderButton.hidden = YES;
                  [cell.placeholderButton setEnabled:NO];
            }else{
            [cell.placeholderButton setBackgroundImage:modalObj.backImage forState:UIControlStateNormal];
            [cell.placeholderButton addTarget:self action:@selector(addTaskButtonClick:) forControlEvents:UIControlEventTouchDown];
            }
        }
    }
    else
    {
        [cell.placeholderButton setEnabled:NO];
        [cell.placeholderButton setHidden:YES];
    }
    
    return cell;
}

- (IGInOutTableViewCell *)getInOutCellForObj:(IGInOutModalClass *)modalObj atIndexPath:(NSInteger)index
{
    IGInOutTableViewCell* cell = [_tableView dequeueReusableCellWithIdentifier:InOutCellId];
    if (cell == nil)
    {
        cell = [[IGInOutTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:InOutCellId];
    }
    
    
    //Reset View
    [cell.redButton setEnabled:YES];
    [cell.redButton setHidden:NO];
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    NSString *date = [_weekDates objectAtIndex:index-3];
    cell.dateLabel.text = date;

    
    
    //Populate data
//    cell.dateLabel.text = modalObj.dateText;
    cell.inTimeLabel.text = modalObj.inTimeText;
    cell.outTimeLabel.text = modalObj.outTimeText;
    
    if(!modalObj.buttonvisible)
    {
        [cell.redButton setEnabled:NO];
        [cell.redButton setHidden:YES];
    }
    
    return cell;
}


- (void) getWeekDates :(NSDate*)weekendDate
{
    NSMutableArray* dates = [[NSMutableArray alloc]init];
    NSDate *startDate = [self getPreviousMonday:weekendDate];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:@"dd MMM, eee"];
    
    for(int i = 0; i < 7; i++)
    {
        NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
        dayComponent.day = i;
        
        NSCalendar *theCalendar = [NSCalendar currentCalendar];
        NSDate *nextDate = [theCalendar dateByAddingComponents:dayComponent toDate:startDate options:0];
        
        if([nextDate isEqualToDate:weekendDate])
        {
            weekendDateIndex = i;
        }
        
        NSString *dateString = [dateFormatter stringFromDate:nextDate];
        [dates addObject:dateString];
    }
    
    invalidDayCount = [self getInvalidDayCount:weekendDate];
    
    self.weekDates = dates;
}

- (NSDate*) getPreviousMonday:(NSDate*)weekendDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:@"eee"];
    NSString* dayStr = [dateFormatter stringFromDate:weekendDate];
    
    int dayNum = 0;
    
    if([dayStr isEqualToString:@"Tue"])
    {
        dayNum = -1;
    }
    else if([dayStr isEqualToString:@"Wed"])
    {
        dayNum = -2;
    }
    else if([dayStr isEqualToString:@"Thu"])
    {
        dayNum = -3;
    }
    else if([dayStr isEqualToString:@"Fri"])
    {
        dayNum = -4;
    }
    else if([dayStr isEqualToString:@"Sat"])
    {
        dayNum = -5;
    }
    else if([dayStr isEqualToString:@"Sun"])
    {
        dayNum = -6;
    }
    
    NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
    dayComponent.day = dayNum;
    
    NSCalendar *theCalendar = [NSCalendar currentCalendar];
    NSDate *startDate = [theCalendar dateByAddingComponents:dayComponent toDate:weekendDate options:0];
    
    return startDate;
}

- (int) getInvalidDayCount: (NSDate*)weekendDate
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setFormatterBehavior:NSDateFormatterBehavior10_4];
    [dateFormatter setDateFormat:@"dd"];
    NSString* dateStr = [dateFormatter stringFromDate:weekendDate];
    
    int dayCount = 0;
    
    if([dateStr intValue] < 7)
    {
        dayCount = 7 - [dateStr intValue];
    }
    
    return dayCount;
}

- (IGTaskDetailTableViewCell *)getTaskCellForObj:(IGTaskModalClass *)modalObj
{
    IGTaskDetailTableViewCell* cell = [_tableView dequeueReusableCellWithIdentifier:TaskCellId];
    if (cell == nil)
    {
        cell = [[IGTaskDetailTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:TaskCellId];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    switch (modalObj.timeSheetStatus)
    {
        case kIGTimeSheetStatusApproved:
        {
            cell.statusLabel.text = @"Approved";
            [cell.statusLabel setTextColor:[UIColor colorWithRed:(23/255.0) green:(143/255.0) blue:(70/255.0) alpha:1]];
        }
            break;
        case kIGTimeSheetStatusCompleted:
        {
            cell.statusLabel.text = @"Completed";
            [cell.statusLabel setTextColor:[UIColor colorWithRed:(23/255.0) green:(143/255.0) blue:(70/255.0) alpha:1]];
        }
            break;
        case kIGTimeSheetStatusPending:
        {
            cell.statusLabel.text = @"Pending";
            [cell.statusLabel setTextColor:[UIColor orangeColor]];
        }
            break;
        case kIGTimeSheetStatusRejected:
        {
            cell.statusLabel.text = @"Rejected";
            [cell.statusLabel setTextColor:[UIColor redColor]];
        }
            break;
            
        default:
            break;
    }
    cell.projectLabel.text = modalObj.projectText;
    cell.timeLabel.text = [NSString stringWithFormat:@"%.2f",modalObj.totalTime];
    cell.detailLabel.text = modalObj.description;
    
    return cell;
}

- (IGSubmitTableViewCell *)getSubmitViewCell
{
    IGSubmitTableViewCell* cell = [_tableView dequeueReusableCellWithIdentifier:SubmitCellId];
    if (cell == nil)
    {
        cell = [[IGSubmitTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SubmitCellId];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell.submitButton setEnabled:YES];
    [cell.saveButton setEnabled:YES];
    
    if([[_rowArray objectAtIndex:_rowArray.count - 2] isKindOfClass:[NSString class]])
    {
        [cell.submitButton setEnabled:NO];
        [cell.saveButton setEnabled:NO];
    }
    
    return cell;
}

- (UITableViewCell *)getCellForObj:(NSString *)modalObj
{
    UITableViewCell* cell = nil;
    
    if([modalObj isEqualToString:HeaderInOut])
    {
        cell = [_tableView dequeueReusableCellWithIdentifier:HInOutCellId];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:HInOutCellId];
        }
        cell.separatorInset = UIEdgeInsetsMake(0.f, (_tableView.frame.size.width/2), 0.f, (_tableView.frame.size.width/2));
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else if([modalObj isEqualToString:SwitchView])
    {
        cell = [_tableView dequeueReusableCellWithIdentifier:SwitchCellId];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:SwitchCellId];
        }
        cell.separatorInset = UIEdgeInsetsMake(0.f, (_tableView.frame.size.width/2), 0.f, (_tableView.frame.size.width/2));
        [cell setBackgroundColor:[UIColor lightGrayColor]];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else if([modalObj isEqualToString:NotifierView])
    {
        cell = [_tableView dequeueReusableCellWithIdentifier:NotifierCellId];
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:NotifierCellId];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    else if([modalObj isEqualToString:SubmitView])
    {
        return [self getSubmitViewCell];
    }
    else
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:nil];
    }
    
    return cell;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    id modelClass = [_rowArray objectAtIndex:indexPath.row];
    if([modelClass isKindOfClass:[IGLabelButtonModalClass class]])
    {
        IGLabelButtonTableViewCell* cell = [self getLabelButtonCellForObj:[_rowArray objectAtIndex:indexPath.row]];
        
        return cell;
    }
    else if([modelClass isKindOfClass:[IGInOutModalClass class]])
    {
        if (!self.isApproval) {
            return [self getInOutCellForObj:[_rowArray objectAtIndex:indexPath.row] atIndexPath:indexPath.row];
        }else{
            return nil;
        }
    }
    else if([modelClass isKindOfClass:[IGTaskModalClass class]] )
    {
        return [self getTaskCellForObj:[_rowArray objectAtIndex:indexPath.row]];
       
    }
    else
    {
        return [self getCellForObj:[_rowArray objectAtIndex:indexPath.row]];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    // This will create a "invisible" footer
    return 0.01f;
}

/*-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)])
    {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)])
    {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)viewDidLayoutSubviews
{
    if ([_tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [_tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([_tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [_tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}*/

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath *)indexPath
{
    id rowObject = [_rowArray objectAtIndex:indexPath.row];
    if([rowObject isKindOfClass:[IGInOutModalClass class]])
    {
        if([(IGInOutModalClass *)rowObject buttonvisible])
        {
            [IGUtils showOKAlertWithTitle:[(IGInOutModalClass *)rowObject dateText] message:[(IGInOutModalClass *)rowObject remarks]];
        }
    }
    if([rowObject isKindOfClass:[IGLabelButtonModalClass class]])
    {
        if([[rowObject labelText] isEqualToString:@"TASK"] && ([rowObject backImage] != nil))
        {
             if(![status isEqualToString:TimeSheetCompleteStatus])
                    [self addTaskButtonClick:self];
        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL canEdit = NO;
    
    id modelObj = [_rowArray objectAtIndex:indexPath.row];
    if([modelObj isKindOfClass:[IGTaskModalClass class]])
    {
        if([modelObj timeSheetStatus] == kIGTimeSheetStatusPending || [modelObj timeSheetStatus] == kIGTimeSheetStatusRejected)
        {
            canEdit = YES;
        }
    }
    
    return canEdit;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        NSInteger cardEntryId = [[_rowArray objectAtIndex:indexPath.row] cardEntryDetailId];
        if(cardEntryId)
        {
            kAlertAndReturnIfNetworkNotReachable
            [[IGService sharedInstance] removeTask:cardEntryId completion:^(id response)
             {
                 [self showSpinner:NO];
                 NSDictionary *dictionary = (NSDictionary *)response;
                 if ([[dictionary objectForKey:@"TimeSheetDeletion"] isEqualToString:@"Failed"])
                 {
                     [IGUtils showOKAlertWithTitle:kStringError message:RemoveError];
                 }
             }
             error:^(NSError *error)
             {
                 [self showSpinner:NO];
                 [IGUtils showOKAlertWithTitle:kStringError message:ParseError];
             }];
        }
        
        IGLabelButtonModalClass* totalHrs = [_rowArray objectAtIndex:(_rowArray.count - 2)];
        IGTaskModalClass* task = [_rowArray objectAtIndex:indexPath.row];
        if([totalHrs isKindOfClass:[IGLabelButtonModalClass class]])
            totalHrs.buttonText = [NSString stringWithFormat:@"%.2f",(totalHrs.buttonText.floatValue - task.totalTime)];
        [_rowArray removeObjectAtIndex:indexPath.row];
        
        if(totalHrs.buttonText.floatValue == 0.0)
        {
            //[_rowArray removeObject:totalHrs];
    

        }
        __block NSInteger foundIndex = NSNotFound;
        
        [_rowArray enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
            if ([obj isKindOfClass:[IGTaskModalClass class]]) {
                foundIndex = idx;
                // stop the enumeration

                *stop = YES;

                [_tableView reloadData];

            }
        }];
        
        if (foundIndex == NSNotFound) {
            // You've found the first object of that class in the array
            [_rowArray removeObject:totalHrs];
            [_rowArray removeLastObject];
            [_rowArray addObject:NotifierView];


            [_tableView reloadData];

            
        }
    }
    else
    {
        NSLog(@"Unhandled editing style! %d", editingStyle);
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.destinationViewController isKindOfClass:[IGAddTaskVC class]])
    {
        IGAddTaskVC *destinationVC = (IGAddTaskVC *)segue.destinationViewController;
        destinationVC.inOutDetails = self.inOutDetails;
    }
    
    IGTaskModalClass* modelObj = [_rowArray objectAtIndex:[[_tableView indexPathForSelectedRow] row]];
    [segue.destinationViewController showTask:modelObj weekEndDate:_weekendDate withWeekId:_weekId forProjects:_projectList];
}

-(void)showupActionSheet:(UIBarButtonItem *)rightBarButton
{
    UIActionSheet *moreOptions = nil;
    
    if([[_rowArray lastObject] isKindOfClass:[NSString class]])
    {
        if([[_rowArray lastObject] isEqualToString:SubmitView])
        {
//            moreOptions = [[UIActionSheet alloc] initWithTitle:nil delegate:(id)self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:ViewLeavesAction, ViewMyAreaAction, nil];
            if(![IGUserDefaults sharedInstance].ifUserUSEmployee)
            {
                 moreOptions = [[UIActionSheet alloc] initWithTitle:nil delegate:(id)self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:ViewLeavesAction, nil];
            }
            
            
        }
        else
        {
//            moreOptions = [[UIActionSheet alloc] initWithTitle:nil delegate:(id)self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:DuplicateSheetAction, ViewLeavesAction, ViewMyAreaAction, nil];
            if([IGUserDefaults sharedInstance].ifUserUSEmployee){
                moreOptions = [[UIActionSheet alloc] initWithTitle:nil delegate:(id)self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:DuplicateSheetAction, nil];
            }
            else{
                moreOptions = [[UIActionSheet alloc] initWithTitle:nil delegate:(id)self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:DuplicateSheetAction, ViewLeavesAction, nil];
            }
            
        }
    }
    else
    {
//         moreOptions = [[UIActionSheet alloc] initWithTitle:nil delegate:(id)self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:ViewLeavesAction, ViewMyAreaAction, nil];
         if(![IGUserDefaults sharedInstance].ifUserUSEmployee){
             moreOptions = [[UIActionSheet alloc] initWithTitle:nil delegate:(id)self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:ViewLeavesAction, nil];
         }
        
        
    }
    
    if(moreOptions)
    {
        [moreOptions showInView:self.view];
    }
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:DuplicateSheetAction])
    {
        kAlertAndReturnIfNetworkNotReachable
        [[IGService sharedInstance] DuplicateTimeSheetForEmployee:_empId weekId:_weekId completion:^(id response)
         {
             [self showSpinner:NO];
             NSDictionary *dictionary = (NSDictionary *)response;
             if([[dictionary objectForKey:@"TimesheetDuplicacy"] isEqualToString:@"Successful"])
             {
                 [self.navigationController popViewControllerAnimated:TRUE];
             }
             else
             {
                 UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:kStringError message:[dictionary objectForKey:@"WarningMessage"] delegate:nil cancelButtonTitle:kStringOK otherButtonTitles:nil];
                 [alertView show];
             }
         }
         error:^(NSError *error)
         {
             [IGUtils showOKAlertWithTitle:kStringError message:ParseError];
         }];
    }
    else if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:ViewLeavesAction])
    {
        IGViewLeavesVC *myLeaveVC = [self.storyboard instantiateViewControllerWithIdentifier:@"IGViewLeave"];
        [self.navigationController pushViewController:myLeaveVC animated:YES];
    }
    else if([[actionSheet buttonTitleAtIndex:buttonIndex] isEqualToString:ViewMyAreaAction])
    {
        IGMyAreaVC *myAreaVC = [self.storyboard instantiateViewControllerWithIdentifier:@"IGMyAreaVC"];
        [self.navigationController pushViewController:myAreaVC animated:YES];
    }
}

/*
 get minimum effort for US employee in a week. today hours in a day should not be less then 8
 */
- (int)getMinTotalEffortForUSEmployees
{
    int minEffort = 0;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    NSString *dayName = [dateFormatter stringFromDate:_weekendDate];
    
    if([dayName isEqual:@"Monday"])
    {
        minEffort = 8;
    }
    else if([dayName isEqual:@"Tuesday"])
    {
        minEffort = 16;
    }
    else if([dayName isEqual:@"Wednesday"])
    {
        minEffort = 24;
    }
    else if([dayName isEqual:@"Thursday"])
    {
        minEffort = 32;
    }
    else if([dayName isEqual:@"Friday"])
    {
        minEffort = 40;
    }
    else
    {
        minEffort = 40;
        if(validDaysCount > 0 && validDaysCount < 7)
        {
            if([dayName isEqualToString:@"Saturday"] || [dayName isEqualToString:@"Sunday"])
                minEffort  = 40;
            else
            minEffort = validDaysCount*8;
            
        }
        else{
            int day = [[[self.title componentsSeparatedByString:@"-"] objectAtIndex:0] intValue];
            if(day < 7)
            {
                minEffort = minEffort - (7 - day) * 8;
            }
        }
    }
    
    return minEffort;
}


- (int)getMinTotalEffort
{
    int minEffort = 0;
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"EEEE"];
    NSString *dayName = [dateFormatter stringFromDate:_weekendDate];
    
    if([dayName isEqual:@"Monday"])
    {
        minEffort = 9;
    }
    else if([dayName isEqual:@"Tuesday"])
    {
        minEffort = 18;
    }
    else if([dayName isEqual:@"Wednesday"])
    {
        minEffort = 27;
    }
    else if([dayName isEqual:@"Thursday"])
    {
        minEffort = 36;
    }
    else if([dayName isEqual:@"Friday"])
    {
        minEffort = 45;
    }
    else
    {
        minEffort = 45;
        if(validDaysCount > 0 && validDaysCount < 7)
        {
            if([dayName isEqualToString:@"Saturday"] || [dayName isEqualToString:@"Sunday"])
                minEffort  = 45;
            else
                minEffort = validDaysCount*9;
            
        }
        else{
            int day = [[[self.title componentsSeparatedByString:@"-"] objectAtIndex:0] intValue];
            if(day < 7)
            {
                minEffort = minEffort - (7 - day) * 9;
            }
        }
    }
    
    
    return minEffort;
}

- (BOOL)isValidIndividualTimesForMinEffort:(int)minEffort forEffortDic:(NSDictionary *)dic
{
    BOOL validIndividualtime = YES;
    NSArray* dayArr = @[EMon,ETue,EWed,EThu,EFri];
    
    int start = 0;
    int end = dayArr.count;
    
    int day = [[[self.title componentsSeparatedByString:@"-"] objectAtIndex:0] intValue];
    if(day < 7)
    {
        start = 7 - day;
    }
    else
    {
        end = (minEffort/MinEffortPerDay);
    }
    
    for(int i = start; i < end; ++i)
    {
        if([[dic valueForKey:[dayArr objectAtIndex:i]] intValue] < MinEffortPerDay)
        {
            validIndividualtime = NO;
            break;
        }
    }
    
    return validIndividualtime;
}

- (BOOL)checkValidityOfSubmit
{
    BOOL validData = YES;
    NSMutableDictionary* timeDic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:0,TotalEfforts,0,EMon,0,ETue,0,EWed,0,EThu,0,EFri, nil];
    
    for(int i = 0; i < _rowArray.count; ++i)
    {
        id modelItem = [_rowArray objectAtIndex:i];
        
        if([modelItem isKindOfClass:[IGTaskModalClass class]])
        {
            [timeDic setObject:[NSNumber numberWithFloat:([[timeDic valueForKey:TotalEfforts] floatValue] + [modelItem totalTime])] forKey:TotalEfforts];
            
            [timeDic setObject:[NSNumber numberWithFloat:([[timeDic valueForKey:EMon] floatValue] + [modelItem monTime])] forKey:EMon];

            [timeDic setObject:[NSNumber numberWithFloat:([[timeDic valueForKey:ETue] floatValue] + [modelItem tueTime])] forKey:ETue];

            [timeDic setObject:[NSNumber numberWithFloat:([[timeDic valueForKey:EWed] floatValue] + [modelItem wedTime])] forKey:EWed];

            [timeDic setObject:[NSNumber numberWithFloat:([[timeDic valueForKey:EThu] floatValue] + [modelItem thuTime])] forKey:EThu];

            [timeDic setObject:[NSNumber numberWithFloat:([[timeDic valueForKey:EFri] floatValue] + [modelItem friTime])] forKey:EFri];
        }
        if([modelItem isKindOfClass:[IGTaskModalClass class]])
        {
            if(validDaysCount == 0)
                validDaysCount = (int)[[modelItem validDays] count];
        }
    }
    if([IGUserDefaults sharedInstance].employee.isContractor)
    {
        return validData;
    }
    
    int minTotalEffort = 0;
    if([IGUserDefaults sharedInstance].ifUserUSEmployee)
        minTotalEffort = [self getMinTotalEffortForUSEmployees];
    else
        minTotalEffort = [self getMinTotalEffort];
    if([[timeDic valueForKey:TotalEfforts] intValue] < minTotalEffort)
    {
        if([IGUserDefaults sharedInstance].ifUserUSEmployee)
            [IGUtils showOKAlertWithTitle:kStringError message:TotalWeekHrErrorForUS];
        else
            [IGUtils showOKAlertWithTitle:kStringError message:TotalWeekHrError];
        validData = NO;
    }
    else if(![self isValidIndividualTimesForMinEffort:minTotalEffort forEffortDic:timeDic])
    {
        [IGUtils showOKAlertWithTitle:kStringError message:TotalDayHrError];
        validData = NO;
    }
    
    return validData;
}


- (IBAction)submit:(UIButton *)sender
{
     status = TimeSheetPendingStatus;
    if ([sender tag] == SubmitTag)
    {
        //status = TimeSheetCompleteStatus;
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:kSuccesAlert
                                                        message:kSuccesAlertMessage
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                              otherButtonTitles:@"OK",nil];
        [alert show];
 
    }
    else
    {
        [IGAnalytics trackButtonAction:actionSaveTimesheet];
        
        [FIRAnalytics logEventWithName:@"Timesheet_Submission" parameters:@{kFIRParameterItemName:[NSString stringWithFormat:@"Submitted_By:%ld",(long)_empId]}];
        [IGAnalytics trackEventWith:@"iOS" action:@"Timesheet_Submission" actionName:[NSString stringWithFormat:@"Submitted By:%@_%@",[IGUserDefaults sharedInstance].employee.employeeNewID,[IGUserDefaults sharedInstance].userName]];

        
        NSMutableArray* pendingTasks = [[NSMutableArray alloc] init];
        
        BOOL pendingTask = NO;
        for(int i = 0; i < _rowArray.count; ++i)
        {
            id task = [_rowArray objectAtIndex:i];
            if([task isKindOfClass:[IGTaskModalClass class]])
            {
                if([task timeSheetStatus] == kIGTimeSheetStatusPending || [task timeSheetStatus] == kIGTimeSheetStatusRejected)
                {
                    pendingTask = YES;
                    [pendingTasks addObject:task];
                }
            }
        }
        
        if(!pendingTask)
        {
            [self.navigationController popViewControllerAnimated:TRUE];
        }
        else
        {
            kAlertAndReturnIfNetworkNotReachable
            [[IGService sharedInstance] submitTask:pendingTasks forEmployee:_empId withStatus:status forWeek:_weekId completion:^(id response)
             {
                 [self showSpinner:NO];
                 NSDictionary *dictionary = (NSDictionary *)response;
                 if([[dictionary objectForKey:@"TimeSheetEntry"] isEqual:@"Success"])
                 {
                     
                     [[_rowArray objectAtIndex:0] setButtonText:status];
                     [self.navigationController popViewControllerAnimated:TRUE];
                 }
                 else
                 {
                     NSString *msg = [dictionary valueForKey:@"WarningMessage"];
                     [IGUtils showOKAlertWithTitle:kStringError message:msg];
                     [[_rowArray objectAtIndex:0] setButtonText:@"InProgress"];
                 }
             }
                                             error:^(NSError *error)
             {
                 [IGUtils showOKAlertWithTitle:kStringError message:ParseError];
                 [[_rowArray objectAtIndex:0] setButtonText:@"InProgress"];
             }];
        }
    }
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        NSLog(@"Cancel Tapped.");
        return;
    }
    else if (buttonIndex == 1) {
        
        NSLog(@"OK Tapped. Hello World!");
        NSMutableArray* pendingTasks = [[NSMutableArray alloc] init];
        
        BOOL pendingTask = NO;
        BOOL isWeekendDate = false;
        [IGAnalytics trackButtonAction:actionSubmitTimesheet];
        if([self checkValidityOfSubmit] == NO)
        {
            return;
        }
        NSMutableArray  *timeArray = [[NSMutableArray alloc]init];
        
        for(int i = 0; i < _rowArray.count; ++i)
        {
            
            id task = [_rowArray objectAtIndex:i];
            if([task isKindOfClass:[IGTaskModalClass class]])
            {
                float projectTaskHour = [task totalTime];
                if([task validDays].count  == 2)
                {
                    NSLog(@"%@",[[task validDays] objectAtIndex:0]);
                    if([[[task validDays] objectAtIndex:0] intValue] == 5 || [[[task validDays] objectAtIndex:1] intValue] == 6)
                    {
                        isWeekendDate = true;
                    }
                }
                
                NSNumber *num = [NSNumber numberWithFloat:projectTaskHour];
                [timeArray addObject:num];
                if([task timeSheetStatus] == kIGTimeSheetStatusPending || [task timeSheetStatus] == kIGTimeSheetStatusRejected)
                {
                    
                    pendingTask = YES;
                    [pendingTasks addObject:task];
                }
            }
        }
        
        if(!pendingTask)
        {
            [self.navigationController popViewControllerAnimated:TRUE];
        }
        else
        {
            kAlertAndReturnIfNetworkNotReachable
            if([IGUserDefaults sharedInstance].employee.isContractor == false)
            {
                if((!isWeekendDate && [timeArray containsObject:[NSNumber numberWithInteger:0]]))
                {
                    [IGUtils showOKAlertWithTitle:kStringError message:@"You can not submit any task with 0 hours"];
                    return;
                }
            }

            status = TimeSheetCompleteStatus;

            [[IGService sharedInstance] submitTask:pendingTasks forEmployee:_empId withStatus:status forWeek:_weekId completion:^(id response)
             {
                 
                 [self showSpinner:NO];
                 NSDictionary *dictionary = (NSDictionary *)response;
                 if([[dictionary objectForKey:@"TimeSheetEntry"] isEqual:@"Success"])
                 {
                    [IGUtils showOKAlertWithTitle:kSuccesAlert message:SubmittedSuccessfully];

                     [[_rowArray objectAtIndex:0] setButtonText:status];
                     [self.navigationController popViewControllerAnimated:TRUE];
                 }
                 else
                 {
                     status = TimeSheetPendingStatus;

                     NSString *msg = [dictionary valueForKey:@"WarningMessage"];
                     [IGUtils showOKAlertWithTitle:kStringError message:msg];
                     [[_rowArray objectAtIndex:0] setButtonText:@"InProgress"];
                 }
             }
                                             error:^(NSError *error)
             {
                 status = TimeSheetPendingStatus;

                 [IGUtils showOKAlertWithTitle:kStringError message:ParseError];
                 [[_rowArray objectAtIndex:0] setButtonText:@"InProgress"];
             }];
        }
    }
}


@end
