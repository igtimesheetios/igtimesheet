//
//  IGLabelButtonTableViewCell.m
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import "IGLabelButtonTableViewCell.h"

@implementation IGLabelButtonTableViewCell

@synthesize placeholderButton , placeholderLabel;

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
