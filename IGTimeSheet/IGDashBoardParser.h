//
//  IGDashBoardParser.h
//  IGTimeSheet
//
//  Created by Hem on 16/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGDashBoardParser : NSObject

- (NSArray *)parseInProgressTimeSheet:(NSDictionary *)dictionary;
- (NSArray *)parseMissingTimeSheet:(NSDictionary *)dictionary;

@end
