//
//  IGDashboardTableViewDateCell.h
//  IGTimeSheet
//
//  Created by Rajat on 11/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IGDashboardTableViewDateCell : UITableViewCell

//@property (strong, nonatomic) IBOutlet UIButton *submitButton;
//@property (weak, nonatomic) IBOutlet UIDatePicker *datepicker;

@property (weak, nonatomic) IBOutlet UILabel *labelName;
@property (weak, nonatomic) IBOutlet UILabel *labelValue;

@end
