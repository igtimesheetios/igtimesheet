//
//  IGTaskDataModel.h
//  IGTimeSheet
//
//  Created by Neha on 19/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IGTaskDataModel : NSObject

@property(atomic) NSNumber* taskId;
@property(strong) NSString* taskName;

@end
