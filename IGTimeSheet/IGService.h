//
//  IGService.h
//  IGTimeSheet
//
//  Created by Manish on 06/02/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"
#import "IGTaskModalClass.h"
#import "igUpdateResponse.h"
#import "IGCommonHeaders.h"

#define kAlertAndReturnIfNetworkNotReachable    if ([[IGService sharedInstance] networkReachabilityAlert] == YES)\
                                                {\
                                                    return;\
                                                }
@class IGApprovalTimeSheetRequest;

@interface IGService : NSObject

// state of internet connection
@property (nonatomic, readonly, getter=isConnected) BOOL connected;

+ (instancetype)sharedInstance;

/*! If not connected to Network, presents an alert to inform user about network unavailability.
 Call this method before using any service related method to check for Network Reachability.
\returns YES - If alert is shown, NO otherwise
*/
- (BOOL)networkReachabilityAlert;

- (AFHTTPRequestOperation *)lloginWithUserName:(NSString *)userName
                                     password:(NSString *)password
                                       domain:(NSString *)domain
                                   completion:(void (^)(NSDictionary *userInfo, NSDictionary *timeSheetInfo))completionBlock
                                        error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)lloginDetailsWithUserName:(NSString *)userName
                                               domain:(NSString *)domain
                                           completion:(void (^)(NSDictionary *userInfo, NSDictionary *timeSheetInfo))completionBlock
                                                error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)loginWithUserName:(NSString *)userName
                                     password:(NSString *)password
                                       domain:(NSString *)domain
                                   completion:(void (^)(id response))completionBlock
                                        error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)loginDetailsWithUserName:(NSString *)userName
                                              domain:(NSString *)domain
                                          completion:(void (^)(id response))completionBlock
                                               error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)imageForEmployee:(NSInteger)employeeID
                                  completion:(void(^)(UIImage *image))completionBlock
                                       error:(void (^)(NSError *error))errorBlock;


- (AFHTTPRequestOperation *)inOutTimeForEmployee:(NSInteger)employeeID
                                            date:(NSDate *)date
                                      completion:(void (^)(id response))completionBlock
                                           error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)taskListForEmployee:(NSInteger)employeeID
                                      projectID:(NSInteger)projectID
                                   isUSEmployee:(BOOL)isUSEmployee
                                    CRID:(NSString*)cRID
                             isBillableTaskType:(BOOL)isBillable
                                    forWeek:(NSInteger)weekId
                                      completion:(void (^)(id response))completionBlock
                                           error:(void (^)(NSError *error))errorBlock;


// Category task for US employees
- (AFHTTPRequestOperation *)categoryListForUSEmployee:(NSInteger)employeeID
                                            projectID:(NSInteger)projectID
                                         isUSEmployee:(BOOL)isUSEmployee
                                           CRID:(NSString*)cRID 
                                   isBillableTaskType:(NSString*)isBillable
                                              forWeek:(NSInteger)weekId
                                           completion:(void (^)(id response))completionBlock
                                                error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)CRIListForEmployee:(NSString*)CRID
                                    completion:(void (^)(id response))completionBlock
                                         error:(void (^)(NSError *error))errorBlock;


- (AFHTTPRequestOperation *)subTaskListForEmployee:(NSInteger)employeeID
                                         projectID:(NSInteger)projectID
                                            taskID:(NSInteger)taskID
                                   timeSheetStatus:(IGTimeSheetStatus)timeSheetStatus
                                              CRID:(NSString*)cRID
                                      isUSEmployee:(BOOL)isUSEmployee
                                        completion:(void (^)(id response))completionBlock
                                             error:(void (^)(NSError *error))errorBlock;


- (AFHTTPRequestOperation *)weekEndDates:(NSString *)year
                                   month:(NSString *)month
                              completion:(void (^)(id response))completionBlock
                                   error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)leavesForEmployee:(NSInteger)employeeID
                                         date:(NSDate *)date
                                   completion:(void (^)(id response))completionBlock
                                        error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)weeklyTimesheetSummaryForEmployee:(NSInteger)employeeID
                                                         date:(NSDate *)date
                                                   completion:(void (^)(id response))completionBlock
                                                        error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)monthlyTimesheetSummaryForEmployee:(NSInteger)employeeID
                                                          date:(NSDate *)date
                                                    completion:(void (^)(id response))completionBlock
                                                         error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)viewTaskForEmployee:(NSString *)employeeID
                                         weekID:(NSInteger)weekID
                                          order:(NSInteger)order
                                     completion:(void (^)(id response))completionBlock
                                          error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)submitTask:(NSArray *)tasks forEmployee:(NSInteger)employeeID
                            withStatus:(NSString *)status forWeek:(NSInteger)weekId
                            completion:(void (^)(id response))completionBlock
                                 error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)monthlyTimesheetAverageForEmployee:(NSInteger)employeeID
                                                          date:(NSDate *)date
                                                    completion:(void (^)(id response))completionBlock
                                                         error:(void (^)(NSError *error))errorBlock;
- (AFHTTPRequestOperation *)removeTask:(NSInteger)cardEntryDetailId
                            completion:(void (^)(id response))completionBlock
                                 error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)DuplicateTimeSheetForEmployee:(NSInteger)employeeID
                                                   weekId:(NSInteger)weekId
                                               completion:(void (^)(id response))completionBlock
                                                    error:(void (^)(NSError *error))errorBlock;

- (igUpdateResponse *)checkUpdateWithCompletion:(void (^)(igUpdateResponse *updateResponse))completionBlock
                                       AndError:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)registerDeviceWithToken:(NSString *)token
                                         deviceType:(NSString *)deviceType
                                         employeeID:(NSString *)employeeID
                                         completion:(void (^)(id response))completionBlock
                                              error:(void (^)(NSError *error))errorBlock;

-(AFHTTPRequestOperation *)validDaysOfPorject:(NSInteger)employeeID
                                    projectID:(NSInteger)projectID
                                      weekendDate:(NSDate *)weekendDate
                                   completion:(void (^)(id response))completionBlock
                                        error:(void (^)(NSError *error))errorBlock;

#pragma mark - PMApproval methods

- (AFHTTPRequestOperation *)accounts:(NSString*)employeeID
                                     completion:(void (^)(id response))completionBlock
                                          error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)tracksWith:(NSString *)accountID
                                  pmID:(NSString *)pmID
                                  year:(NSString *)year
                            completion:(void (^)(id response))completionBlock
                                 error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)timesheets:(IGApprovalTimeSheetRequest *)request
                            completion:(void (^)(id response))completionBlock
                                 error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)approve:(NSSet *)timeSheetList approve:(BOOL)isApproval
                            completion:(void (^)(id response))completionBlock
                                 error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)add:(NSString *)note
                             id:(NSString *)cardEntryID
                        addedBy:(NSString *)managerID
                    complestion:(void (^)(id response))completionBlock
                          error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)months:(NSString *)year
                     completion:(void (^)(id response))completionBlock
                          error:(void (^)(NSError *error))errorBlock;

- (AFHTTPRequestOperation *)timeSheetDetail:(NSString *)timeSheetID
                        completion:(void (^)(id response))completionBlock
                             error:(void (^)(NSError *error))errorBlock;


@end
