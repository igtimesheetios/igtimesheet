//
//  IGAnalytics.h
//  igTimeSheet
//
//  Created by Manish on 05/03/15.
//  Copyright (c) 2015 InfoGain. All rights reserved.
//

/*
 Download the Google Analytics for iOS SDK and add these files from the SDK package to your app:
 
 GAI.h
 GAIDictionaryBuilder.h
 GAIEcommerceProduct.h
 GAIEcommerceProductAction.h
 GAIEcommercePromotion.h
 GAIFields.h
 GAILogger.h
 GAITrackedViewController.h
 GAITracker.h
 
 To use enhanced ecommerce features you'll also need to add the following file to your app:
 
 GAIEcommerceFields.h
 
 To use the IDFA (Identifier for Advertisers) requires that you link the following files to your app and then enable IDFA collection:
 
 libAdIdAccess.a
 AdSupport.framework
 
 
 The Google Analytics SDK uses the CoreData and SystemConfiguration frameworks, so you will need to add the following to your application target's linked libraries:
 
 CoreData.framework
 SystemConfiguration.framework
 libz.dylib
 libsqlite3.dylib
 libGoogleAnalyticsServices.a
 
 */


#import <Foundation/Foundation.h>

// Screen Names
extern NSString *screenLogin;
extern NSString *screenDashBoard;
extern NSString *screenTaskView;
extern NSString *screenAddTask;
extern NSString *screenViewLeaves;
extern NSString *screenMyArea;
extern NSString *screenMonthlyAverageTimesheet;
extern NSString *screenHelp;
extern NSString *screenMenu;

// Action Names
extern NSString *actionLogin;
extern NSString *actionLogout;
extern NSString *actionSubmitTimesheet;
extern NSString *actionSaveTimesheet;


typedef NS_ENUM(NSInteger, IGAnalyticsCategory)
{
    kIGAnalyticsCategoryUIAction,
    kIGAnalyticsCategoryServerAPIHit
};

typedef NS_ENUM(NSInteger, IGAnalyticsAction)
{
    kIGAnalyticsActionButtonPress,
    kIGAnalyticsActionServerAPILogin
};



@interface IGAnalytics : NSObject

+ (void)trackScreen:(NSString *)screenName;
+ (void)trackButtonAction:(NSString *)actionName;

+ (void)trackEventForCategory:(IGAnalyticsCategory)category
                       action:(IGAnalyticsAction)action
                   actionName:(NSString *)actionName;

+ (void)logInfo:(NSString *)message;

@end
