//
//  NIDropDown.h
//  NIDropDown
//
//  Created by Bijesh N on 12/28/12.
//  Copyright (c) 2012 Nitor Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NIDropDown;

// Protocol to define delegate Methods

@protocol NIDropDownDelegate

- (void) niDropDown:(NIDropDown *)sender;

- (void) niDropDown:(NIDropDown *)sender didSelectRowAtIndex:(NSInteger)index;

@end

// UIView interface to define drop down list View

@interface NIDropDown : UIView <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, retain) id <NIDropDownDelegate> delegate;
@property (nonatomic, retain) NSString *animationDirection;

-(void)hideDropDown:(UIView *)view;
-(id)showDropDown:(UIView *) view heightOfView:(CGFloat) height dataArrayForRow:(NSArray *)dataArray;

@end
