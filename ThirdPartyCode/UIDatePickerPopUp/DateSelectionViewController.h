
#import <UIKit/UIKit.h>

@class DateSelectionViewController;

//This block is called when the user selects a certain date if blocks are used.
typedef void (^DateSelectionBlock)(DateSelectionViewController *vc, NSDate *aDate);

//This block is called when the user cancels if blocks are used.
typedef void (^DateCancelBlock)(DateSelectionViewController *vc);

//These methods are used to inform the [delegate]([RMDateSelectionViewController delegate]) of an instance of RMDateSelectionViewController about the status of the date selection view controller.
@protocol DateSelectionViewControllerDelegate <NSObject>

//This method is called when the user selects a certain date.
- (void)dateSelectionViewController:(DateSelectionViewController *)vc didSelectDate:(NSDate *)aDate;

//This method is called when the user selects the cancel button or taps the darkened background (if the property [backgroundTapsDisabled]([RMDateSelectionViewController backgroundTapsDisabled]) of RMDateSelectionViewController returns NO).
- (void)dateSelectionViewControllerDidCancel:(DateSelectionViewController *)vc;

@end

@interface DateSelectionViewController : UIViewController

+ (instancetype)dateSelectionController;

+ (void)setLocalizedTitleForCancelButton:(NSString *)newLocalizedTitle;

+ (void)setLocalizedTitleForSelectButton:(NSString *)newLocalizedTitle;

@property (weak) id<DateSelectionViewControllerDelegate> delegate;

@property (nonatomic, readonly) UIDatePicker *datePicker;

@property (assign, nonatomic) BOOL hideNowButton;

/**
 *  When YES taps on the background view are ignored. Default value is NO.
 */
@property (assign, nonatomic) BOOL backgroundTapsDisabled;

- (void)show;

- (void)showWithSelectionHandler:(DateSelectionBlock)selectionBlock andCancelHandler:(DateCancelBlock)cancelBlock;

- (void)dismiss;

@end
